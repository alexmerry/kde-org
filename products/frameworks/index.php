<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Frameworks",
        'description' => 'The KDE Frameworks are a set of 80 add-on libraries for programming with Qt.'
    ]);

    require('../../aether/header.php');
    $site_root = "../../";

    $numberOfFrameworks = 80;
?>

<script src="/js/use-ekko-lightbox.js" defer></script>

<main class="container">

<h1>KDE Frameworks</h1>

<style>

    #main article { /* Old kde.org fix */
        margin: 0px -26px;
        text-shadow: none !important;
        text-align: center;
    }

    main article {
        padding: 20px;
        text-align: center;
    }

    main article + article,
    #main article + article {
        border-top: solid 2px rgba(0,0,0,.1);
    }

    main.container section {
        padding: 0px;
    }

    .splashLogo {
    /*
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;

        background-image: url(kirigami-splash.jpg);
        background-size: cover;
        background-position: left center;
        border-top-left-radius: 2px;
        border-top-right-radius: 2px;
      */
    }

    .splashLogo h1 {
    /*
        color: #FAFAFA !important;
        font-size: 32px !important;
        line-height: 120%;
        background-image: none !important;
    */
    }

    .splashLogo p {
    /*
        color: #FCFCFC !important;
        line-height: 120% !important;
    */
    }

    main.container section > article, #main article {
        min-height: 250px;
    }


    .columns {
        padding: 0px !important;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: row;
        min-height: 100%;
        align-items: stretch;
    }

    .columns > div {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        padding: 20px;
    }

    main .image,
    #main .image {
        background-origin: content-box;
        padding: 20px;
        background-repeat: no-repeat;
    }

    .section-responsive
    {
        background-color: #1e4b6b;
        background-image: linear-gradient(#4080b0 1px, transparent 1px),
            linear-gradient(90deg, #4080b0 1px, transparent 1px),
            linear-gradient(#34719e 1px, transparent 1px),
            linear-gradient(90deg, #34719e 1px, transparent 1px);
        background-size:100px 100px, 100px 100px, 20px 20px, 20px 20px;
        background-position: center center;

        align-items: stretch;
        min-height: 0px !important;
        color: #FFF  !important;
    }

    .section-responsive h1
    {
    padding-top: 20px;
        background: none !important;
        color: #FFF !important;
    }

    .section-responsive p
    {
        padding: 0px 20px 20px !important;
        color: #FFF !important;
        max-width: 1200px;
    }

    .section-responsive .image {
        background-image: url(kirigami-adapt.png);
        background-size: contain;
        background-position: center center;
        height: 60%;
        width: 100%;
        background-repeat: no-repeat;
        flex: 1 ;
        height: 400px;
    }

    .section-links {
        display: flex;
        flex-direction: column;
        text-align: center;
    }

    .section-links-content {
        display: flex;
        flex-wrap: wrap;
    }

    .section-links .section-links-content div {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        flex-basis: 33.33%;
    }

    .section-links .section-links-content div:nth-child(2),
    .section-links .section-links-content div:nth-child(5) {
        border-left: solid 1px rgba(0,0,0,.1);
        border-right: solid 1px rgba(0,0,0,.1);
    }

    .section-links h1 {
        border-bottom: solid 1px rgba(0,0,0,.1);
        padding-bottom: 20px;
    }

    .section-links-content div p {
        max-width: 90%;
        margin-bottom: 3em;
    }
    .section-links-content a {
        display: inline-block;
        color: #feefe2 !important;
        background-color: #e4720c;
        border-radius: 2px;
        padding: 10px 20px;
        width: 80%;
    }

    .section-links-content a:hover {
        background-color: #f37a0d;
    }

    .section-elegant {
        flex-direction: row-reverse;
    }

    .section-elegant .image {
        width: 60%;
        background-image: url(subsurface.png);
        background-size: contain;
        background-repeat: no-repeat;
        background-position: center center;
    }

    .section-open {
        background-image: url(community.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
    }
    .section-open * {
        color: #FFF !important;
        background: none !important;
        text-shadow: 0px 0px 5px rgba(0,0,0,.5) !important;
    }
    .section-examples {
        background-color: lightgrey;
    }
    .section-examples img {
        width: 90%;
        height: auto;
    }
    .splashLogo .image img {
        max-width: 90%;
        height: auto;
    }

    @media(max-width: 1200px) {
        main.container section > article, #main article {
            min-height: 250px;
        }
    }
    @media(max-width: 992px) {
        main.container section > article, #main article {
            min-height: 250px;
        }
        .section-links .section-links-content div {
            flex-basis: 50%;
        }
        .section-links .section-links-content div:nth-child(2),
        .section-links .section-links-content div:nth-child(5) {
            border:none;
        }
        .section-links .section-links-content div:nth-child(odd) {
            border-right: solid 1px rgba(0,0,0,.1);
        }
    }
    @media(max-width: 768px) {
        main.container section > article, #main article {
            min-height: 250px;
        }
        .section-responsive .image {
            height: 300px;
        }
        .columns {
            flex-direction: column;
        }
        .columns > div {
            border: none !important;
            width: 100% !important;
        }
    }

</style>


<article class="splashLogo columns">
    <div class="image">
        <img src="/announcements/qt-kde.png" width="320" height="180" style="margin: 1em;" />
    </div>
    <div>
        <h1 style="text-align: left;">KDE Frameworks</h1>
        <p style="text-align: left;"><?php print $numberOfFrameworks ?> add-on libraries for programming with Qt.</p>
    </div>
</article>


<article class="section-links">
    <h1>Get Started</h1>
    <div class="section-links-content columns">
        <div>
            <h2>Developer Documentation</h2>
            <p>
                Code an application in C++ with Qt and QML.
            </p>
            <a href="https://api.kde.org/frameworks/index.html" target="_blank">Learn more</a>
        </div>
        <div>
            <h2>Design Guidelines</h2>
            <p>
                Use our UI standards to their fullest for a flexible and consistent user experience if you are creating an app for the Linux desktop.
            </p>
            <a href="https://hig.kde.org/" target="_blank">Learn more</a>
        </div>
    </div>
</article>

<article class="section-convergent columns">
    <div class="image">
        <img src="platform-icons.png" style="margin: auto; max-width: 90%; height: auto;" />
    </div>
    <div>
        <h1>Features</h1>
        <p>
            The KDE Frameworks are <?php print $numberOfFrameworks ?> add-on libraries for coding applications with Qt.
        </p>
        <p>
            The individual Frameworks are well documented, tested and their API style will be familiar to Qt developers.
        </p>
        <p>
             Frameworks are developed under the proven KDE governance model with a predictable release schedule, a clear and vendor neutral contributor process, open governance and flexible LGPL or MIT licensing.
        </p>
        <p>
            The frameworks are cross-platform and function on Windows, Mac, Android and Linux.
        </p>
    </div>
</article>

<article class="section-responsive">
    <!-- <div class="image"></div> -->
    <div>
        <h1>Organization</h1>
        <p>
            The Frameworks have a clear dependency structure, divided into Categories and Tiers. The Categories refer to runtime dependencies:
        </p>
        <p style="text-align: left">
            <b>Functional</b> elements have no runtime dependencies.<br />
            <b>Integration</b> designates code that may require runtime dependencies for integration depending on what the OS or platform offers.<br />
            <b>Solutions</b> have mandatory runtime dependencies.
        </p>
        <p>
            The Tiers refer to compile-time dependencies on other Frameworks.
        </p>
        <p style="text-align: left">
            <b>Tier 1</b> Frameworks have no dependencies within Frameworks and only need Qt and other relevant libraries.<br />
            <b>Tier 2</b> Frameworks can depend only on Tier 1.<br />
            <b>Tier 3</b> Frameworks can depend on other Tier 3 Frameworks as well as Tier 2 and Tier 1.
        </p>
    </div>
</article>

<article class="columns">
    <!-- <div class="image"></div> -->
    <h1 style="margin: auto; font-weight: bold;">Highlights:</h1>
    <div>
            <p>
                <b>KArchive</b> offers support for many popular compression codecs in a self-contained, featureful and easy-to-use file archiving and extracting library. Just feed it files; there's no need to reinvent an archiving function in your Qt-based application!
            </p>
            <p>
                <b>ThreadWeaver</b> offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread execution by specifying dependencies between the threads and executing them satisfying these dependencies, greatly simplifying the use of multiple threads.
            </p>
            <p>
                <b>Breeze Icons</b>. KDE Frameworks includes two icon themes for your applications.  Breeze icons is a modern, recogniseable theme which fits in with all form factors.
            </p>
    </div>
    <div>
            <p>
                <b>KConfig</b> is a Framework to deal with storing and retrieving configuration settings. It features a group-oriented API. It works with INI files and XDG-compliant cascading directories. It generates code based on XML files.
            </p>
            <p>
                <b>KI18n</b> adds Gettext support to applications, making it easier to integrate the translation workflow of Qt applications in the general translation infrastructure of many projects.
            </p>
            <p>
                <a href="/products/kirigami"><b>Kirigami</b></a> is a responsive UI framework for QML.
            </p>
    </div>
</article>
<article class="section-examples">
    <h1>Examples</h1>
    <div class="columns">
        <div>
            <p>
                GammaRay is a cross platform (Linux, Windows, Mac) inspection tool for Qt apps from KDAB.  It uses <a href="https://api.kde.org/frameworks/syntax-highlighting/html/index.html">KSyntaxHighlighting</a> to colour text it shows.
            </p>
        </div>
        <div class="image"><a href="gammaray.png" data-toggle="lightbox"><img src="gammaray.png" /></a></div>
    </div>
    <div class="columns" style="clear: both">
        <div class="image"><a href="lxqt.png" data-toggle="lightbox"><img src="lxqt.png" /></a></div>
        <div>
            <p>
                LXQt is a simple desktop environment for Linux.  It uses <a href="https://api.kde.org/frameworks/solid/html/">Solid</a> for hardware integration, <a href="https://api.kde.org/frameworks/kwindowsystem/html/">KWindowSystem</a> for window management and <a href="https://api.kde.org/frameworks/kidletime/html/">KIdleTime</a> for power management.
            </p>
            <p>
                Good looks are added with <a href="https://api.kde.org/frameworks/oxygen-icons5/html/index.html">Oxygen Icons</a>, one of our icon theme frameworks.
            </p>
        </div>
    </div>
</article>
<article class="section-links">
    <h1>Get The Frameworks</h1>
    <div class="section-links-content columns">
        <div>
            <h2>Latest Version</h2>
            <p>
                Download the latest release
            </p>
            <a href="https://www.kde.org/announcements/" target="_blank">Release Announcements</a>
        </div>
        <div>
            <h2>Find more Qt Addons</h2>
            <p>
                The Qt library archive.
            </p>
            <a href="https://inqlude.org/" target="_blank">Inqlude</a>
        </div>
        <div>
            <h2>Build it Yourself</h2>
            <p>
                Our tool to compile KDE software
            </p>
            <a href="https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source" target="_blank">kdesrc-build</a>
        </div>
        <div>
            <h2>Linux Packages</h2>
            <p>
                Linux distros already package KDE Frameworks
            </p>
            <a href="https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro" target="_blank">Binary Packages</a>
        </div>
        <div>
            <h2>Windows</h2>
            <p>
                Our Craft tool builds KDE software on Windows and other platforms
            </p>
            <a href="https://community.kde.org/Craft" target="_blank">Craft</a>
        </div>
        <div>
            <h2>Android</h2>
            <p>
                Use KDE Frameworks on mobile
            </p>
            <a href="https://community.kde.org/Android" target="_blank">Android Tutorial</a>
        </div>
    </div>
</article>

</main>
<?php
  require('../../aether/footer.php');
