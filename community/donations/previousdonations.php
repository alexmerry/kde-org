<?php
    header("Cache-Control: public, max-age=10");
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "PayPal Donations",
        'cssFile' => '/css/announce.css'
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>PayPal Donations</h1>

The following contributions have been generously made through PayPal to KDE using the <a href="index.php">donation</a> form.  If you want to contribute in other ways, look at the overview page to see all the ways you can <a href="/community/donations">contribute</a>. We thank the donors listed below for their support!

<p>Note this list does not include campaign specific donations like:</p>
<ul>
    <li><a href="/fundraisers/randameetings2014/">Randa Meetings 2014 Fundraising</a></li>
    <li><a href="/fundraisers/yearend2014/">KDE End of Year 2014 Fundraising</a></li>
    <li><a href="/fundraisers/kdesprints2015/">KDE Sprints 2015 Fundraising</a></li>
    <li><a href="/fundraisers/yearend2016/">KDE End of Year 2016 Fundraising</a></li>
</ul>
<script src="https://cdn.kde.org/aether-devel/charjs.js"></script>

<?php
require("www_config.php");
function display_graph(int $year, $dbConnection)
{
    for ($i = 1; $i <= 12; $i++) {
        $i < 10 ? $i_s = "0".$i : $i_s = $i;
        $data[ $year."-".$i_s ] = 0;
    }
    $stmt = $dbConnection->prepare("SELECT SUM(amount) as don, DATE_FORMAT(date,\"%Y-%m\") as month from donations WHERE YEAR(date) = :year GROUP BY month ORDER BY month DESC");

    $stmt->execute([
        'year' => $year,
    ]);

    while ($row = $stmt->fetch()) {
        $data[ $row["month"] ] = $row["don"];
    } ?>
<div class="col-12 col-md-6 offset-md-3">
<canvas id="myChart<?= $year ?>" width="400" height="200"></canvas>
</div>
<script>
let ctx<?= $year ?> = document.getElementById('myChart<?= $year ?>');
new Chart(ctx<?= $year ?>, {
    type: 'bar',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            label: "Donation for the year <?= $year ?>",
            data: [<?php foreach ($data as $month) { echo($month . ","); } ?>],
            fill: false,
        }],
    },
    options: { scales: { yAxes: [{ ticks: { beginAtZero: true } }] } }
});
</script>
    <?php
}

$query = $dbConnection->prepare("SELECT *, UNIX_TIMESTAMP(date) AS date_t FROM donations WHERE date >= :beginn AND date <= :end ORDER BY date DESC;");
$count = $dbConnection->prepare("SELECT COUNT(*) WHERE date >= :beginn AND date <= :end ;");

for ($year = date("Y", time()); $year > 2001; $year--) {
	echo "<h3>$year</h3>";
	echo "<figure class='text-center'>";
    display_graph($year, $dbConnection);
	echo "</figure>";
	echo "<br><br>";
	for ($month = 12; $month >=1; $month--) {
        if (date("n", time()) + 1 <= $month) {
            continue;
        }
        $month < 10 ? $month_s = "0".$month : $month_s = $month;
        $count->execute([
            'beginn' => $year.'-'.$month_s.'-01',
            'end' => $year.'-'.$month_s.'-31 23:59:59',
        ]);
        if ($count->fetchColumn() === 0) {
            continue;
        }
        $query->execute([
            'beginn' => $year.'-'.$month_s.'-01',
            'end' => $year.'-'.$month_s.'-31 23:59:59',
        ]);
		$total = 0;
		echo "<table border=1 width=\"600\">";
		echo "<tr><th colspan=3>".date("Y - F", mktime(0,0,0,$month,1,$year))."</th></tr>";
		echo "<tr><th width=100>Date</th><th width=100>Amount</th><th width=400>Message</th></tr>";
		while ($row = $query->fetch()) {
			$msg = htmlentities($row["message"]);
			if ($msg == "") {
				$msg = "<i>Anonymous donation</i>";
			}
			$total += $row["amount"];
	
			echo "<tr>";
			echo "<td>".date("jS H:i", $row["date_t"])."</td>";
			echo "<td align=right>&euro;&nbsp;".number_format($row["amount"],2)."</td>";
			echo "<td>".$msg."</td>";
			echo "</tr>";
		}
		echo "<tr><th>Total</th><th>&euro;&nbsp;".number_format($total,2)."</th><th>&nbsp;</th></tr>";
		echo "</table><br>";
	}
}
?>


<p>Note: donations that have been made in the past in US$ are converted to Euro on Aug 8th 2011 with a current exchange rate of 1 US$ = 0.7002 Euro. The original amount is listed.</p>

</main>
<?php
  require('../../aether/footer.php');
