<?php
    // Don't show the global donation square, this is private anyway
    $page_disablekdeevdonatebutton = true;

    $page_title="Donations Statistics";
    require("www_config.php");
    require('../../../aether/config.php');
    $pageConfig = array_merge($pageConfig, [
        'title' => "Donations Statistics"
    ]);
    require('../../../aether/header.php');
    $site_root = "../../";
    echo '<main class="container">';

    function endsWith($haystack, $needle)
    {
        return substr($haystack, -strlen($needle)) === $needle;
    }

    echo "<h2>Per Month</h2>";

    for ($year = date("Y", time()); $year > 2010; $year--) {
        echo "<table border=1 width=\"600\">";
        echo "<tr><th colspan=7>".$year."</th></tr>";
        echo "<tr><th width=100>Month</th>
                  <th width=100>Number of Donations</th>
                  <th width=100>Total Amount</th>
                  <th width=100>Average Donation</th>
                  <th width=100>Donation StdDev</th>
                  <th width=100>Max Donation</th>
                  <th width=100>Min Donation</th>
                  </tr>";

        $stmtYear = $dbConnection->prepare('SELECT COUNT(*) as number, MIN(amount) as min_t, MAX(amount) as max_t, ROUND(AVG(amount), 2) as avg_t, ROUND(STDDEV(amount), 2) as stddev_t, SUM(amount) as sum_t FROM donations WHERE YEAR(date) = :year');
        $stmtYear->execute([
            'year' => $year
        ]);

        $stmtMonth = $dbConnection->prepare('SELECT COUNT(*) as number, MIN(amount) as min_t, MAX(amount) as max_t, ROUND(AVG(amount), 2) as avg_t, ROUND(STDDEV(amount), 2) as stddev_t, SUM(amount) as sum_t FROM donations WHERE date >= :begin AND date <= :end');
        $yearRow = $stmtYear->fetch();

        for ($month = 1; $month <= 12 ; $month++) {
            $month < 10 ? $month_s = "0".$month : $month_s = $month;
            
            $stmtMonth->execute([
                'begin' => $year.'-'.$month_s.'-01',
                'end' => $year.'-'.$month_s.'-31 23:59:59',
            ]);
            $row = $stmtMonth->fetch();
            echo "<tr><td>".date("F", mktime(0,0,0,$month,1,$year))."</td>
                      <td align=right>".$row['number']."</td>
                      <td align=right>".$row['sum_t']."</td>
                      <td align=right>".$row['avg_t']."</td>
                      <td align=right>".$row['stddev_t']."</td>
                      <td align=right>".$row['max_t']."</td>
                      <td align=right>".$row['min_t']."</td>
                  </tr>";
        }
        echo "<tr><td>Year Total</td>
                  <td align=right>".$yearRow['number']."</td>
                  <td align=right>".$yearRow['sum_t']."</td>
                  <td align=right>".$yearRow['avg_t']."</td>
                  <td align=right>".$yearRow['stddev_t']."</td>
                  <td align=right>".$yearRow['max_t']."</td>
                  <td align=right>".$yearRow['min_t']."</td>
              </tr>";
        echo "</table><br>";
    }

    echo "<h2>Per Week (promo data)</h2>";

    echo "<pre>\n";
    for ($year = date("Y", time()); $year > 2010; $year--) {
        $stmtYear = $dbConnection->prepare('SELECT COUNT(*) as number, MIN(amount) as min_t, MAX(amount) as max_t, ROUND(AVG(amount), 2) as avg_t, ROUND(STDDEV(amount), 2) as stddev_t, SUM(amount) as sum_t FROM donations WHERE YEAR(date) = :year');
        $stmtYear->execute([
            'year' => $year
        ]);

        $stmtWeek = $dbConnection->prepare('SELECT COUNT(*) as number, MIN(amount) as min_t, MAX(amount) as max_t, ROUND(AVG(amount), 2) as avg_t, ROUND(STDDEV(amount), 2) as stddev_t, SUM(amount) as sum_t FROM donations WHERE WEEK(date) = :week AND YEAR(date) = :year');
        $yearRow = $stmtYear->fetch();

        for ($week = 52; $week >= 0 ; $week--) {
            $stmtWeek->execute([
                'week' => $week,
                'year' => $year,
            ]);
            $row = $stmtWeek->fetch();
            echo $year.','.$week.','.$row['number'].','.($row['sum_t'] ? $row['sum_t'] : 0).'<br />';
        }
    }
    echo "</pre>\n";
?>

    <h2><a name="perurl" />Per Donation Url</h2>
    <form action="index.php#perurl" method="post">
        Period:
        <select id="period" name="period" onChange="check(this);">
            <option id="today" value="today">Today</option>
            <option id="yesterday" value="yesterday">Yesterday</option>
            <option id="month" value="month">This Month</option>
            <option id="all" value="all">All Time</option>
            <option id="custom" value="custom">Custom</option>
        </select>
        <div id="fromto" style="display: none">
            From: <input id="from" type='date' name="from" size="10"/>
            To: <input id="to" type='date' name="to" size="10"/>
        </div>
        <input type="submit" value="Change">
    </form>
<?php
    $period = "today";
    if (isset($_POST["period"])) {
        $period = $_POST["period"];
    }

    function cleanUrl(string $url): string
    {
        $newurl = $url;
        if (strpos($url, "//www.") === 0) {
            $newurl = "//".substr($url, 6);
        } else {
            $newurl = $url;
        }
        if (endsWith($newurl, "index.php/donation_button")) {
            $newurl = substr($newurl, 0, -strlen("index.php/donation_button"))."/donation_button";
        }
        if (endsWith($newurl, "index.php/donation_box")) {
            $newurl = substr($newurl, 0, -strlen("index.php/donation_box"))."/donation_box";
        }
        
        return $newurl;
    }
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = "SELECT COUNT(id) as number, SUM(amount) as sum_t, MIN(amount) as min_t, MAX(amount) as max_t, ROUND(AVG(amount), 2) as avg_t, ROUND(STDDEV(amount), 2) as stddev_t, donate_url FROM donations";
    $q = null;
    if ($period === "today") {
        $query .= ' WHERE DATE(date) = CURDATE() GROUP BY donate_url';
        $q = $dbConnection->query($query);
    } else if ($period === "yesterday") {
        $query .= ' WHERE DATE(date) = SUBDATE(CURDATE(), 1) GROUP BY donate_url';
        $q = $dbConnection->query($query);
    } else if ($period === "month") {
        $query .= ' WHERE YEAR(date) = YEAR(NOW()) AND MONTH(date) = MONTH(NOW()) GROUP BY donate_url';
        $q = $dbConnection->query($query);
    } else if ($period === "custom") {
        $query .= ' WHERE date >= :fromTime AND date <= :toTime GROUP BY donate_url';
        $q = $dbConnection->prepare($query);
        $q->execute([
            'fromTime' => $_POST['from'],
            'toTime' => $_POST['to'] . ' 23:59:59',  
        ]);
    } else if ($period === "all") {
        $q = $dbConnection->query($query . ' GROUP BY donate_url;');
    } else {
        echo "bug: $period";
    }

    echo "<table border=1 width=\"600\">";
    echo "<tr><th width=100>Url</th>
                  <th width=100>Number of Donations</th>
                  <th width=100>Total Amount</th>
                  <th width=100>Average Donation</th>
                  <th width=100>Donation StdDev</th>
                  <th width=100>Max Donation</th>
                  <th width=100>Min Donation</th>
                  </tr>";
    $total = 0;
    while ($row = $q->fetch()) { 
        echo "<tr><td>".cleanUrl($row['donate_url'])."</td>
                      <td align=right>".$row['number']."</td>
                      <td align=right>".$row['sum_t']."</td>
                      <td align=right>".$row['avg_t']."</td>
                      <td align=right>".$row['stddev_t']."</td>
                      <td align=right>".$row['max_t']."</td>
                      <td align=right>".$row['min_t']."</td>
              </tr>";
        $total += $row['number'];
    }
    echo "</table><br><p>Total number of donations for this period is: $total</p></main>";
?>

    <script type="text/javascript">
        function check(elem) {
            if (elem.selectedIndex == 4) {
                document.getElementById('fromto').style.display = "block";
                var from = <?php echo json_encode(isset($_POST["from"]) ? $_POST["from"] : ""); ?>;
                var to = <?php echo json_encode(isset($_POST["to"]) ? $_POST["to"] : ""); ?>;
                if (from && to) {
                    document.getElementById('from').value = from;
                    document.getElementById('to').value = to;
                } else {
                    var date = new Date();
                    document.getElementById('from').value = date.toJSON().slice(0, 10);
                    document.getElementById('to').value = date.toJSON().slice(0, 10);
                }
            } else {
                document.getElementById('fromto').style.display = "none";
            }
        }
        document.getElementById("<?php echo (isset($_POST["period"]) ? $_POST["period"] : "today") ?>").selected = true;
        check(document.getElementById("period"));
   </script>
<?php
    include "../../../aether/footer.php";
?>
