# kde.org

## Clone

```
svn co svn://anonsvn.kde.org/home/kde/trunk/www/sites/www kde-org
cd kde-org
git clone https://anongit.kde.org/websites/capacity.git media
```

## Build container

```
podman build -t kdeorg .
podman run --name kdeorg-apache -p 8082:80 kdeorg 
```

