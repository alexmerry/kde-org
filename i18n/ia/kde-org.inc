<?php
$text["Support KDE"] = "Supporta KDE";
$text["KDE Applications"] = "Applicationes de KDE";
$text["KGlobalAccel"] = "KGlobalAccel";
$text["KIO"] = "KIO";
$text["KWallet"] = "KWallet";
$text["KHTML"] = "KHTML";
$text["Solid"] = "Solido";
$text["General"] = "General";
$text["Highlights"] = "Evidentias ";
$text["Color Picker"] = "Selectionator de color";
$text["About KDE"] = "A proposio de KDE";
$text["Date"] = "Data";
$text["Amount"] = "Amonta";
$text["%1 raised"] = "%1 altiate";
$text["Notes:"] = "Notas:";
?>

