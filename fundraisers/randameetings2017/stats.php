<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:url" content="https://www.kde.org/fundraisers/randameetings2017/"/>
    <meta property="og:image" content="https://www.kde.org/fundraisers/randameetings2017/images/fundraising2017-ogimage.jpg"/>
    <meta property="og:type" content="website"/>
    <meta property="og:description" content="KDE is one of the biggest free software communities in the world and has been delivering high quality technology for nearly two decades. Randa Meetings is the largest sprint organized by KDE and, in 2016, it will be centered on bringing KDE technology on every device. This fundraising campaign aims at supporting the continuity of KDE efforts, over the year, towards this goal."/>
    <link rel="shortcut icon" href="images/kde-mini.png" />
    <title>KDE &#8210; Statistics for Randa Meetings 2017 Fundraising Campaign</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="top-navigation" data-spy="scroll" data-target="#navbar">

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-fixed-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="https://ev.kde.org/" class="navbar-brand" target="_blank">
                            <img class="img-responsive" src="images/logo.png" alt="logo"/>
                        </a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a aria-expanded="false" role="button" href="https://www.kde.org/fundraisers/randameetings2017/">Randa Meetings 2017 Fundraising Campaign</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <br/><br/>
            <div class="wrapper wrapper-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <img class="img-responsive" src="images/banner-fundraising2017.jpg" alt="banner"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Amount per Day</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content text-center">
                                    <div class="pull-left m-b-md">
                                        <div class="label label-info" style="background-color: #1ab394">2017</div>
                                        <div class="label label-info" style="background-color: #dcdcdc">2016</div>
                                    </div>
                                    <div>
                                        <canvas id="amountPerDay" height="140"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Donations per Day</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content text-center">
                                    <div>
                                        <div class="pull-left m-b-md">
                                            <div class="label label-info" style="background-color: #1ab394">2017</div>
                                            <div class="label label-info" style="background-color: #dcdcdc">2016</div>
                                        </div>
                                        <canvas id="donationsPerDay" height="140"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cumulative Amount per Day</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content text-center">
                                    <div>
                                        <div class="pull-left m-b-md">
                                          <div class="label label-info" style="background-color: #1ab394">2017</div>&nbsp;
                                          <div class="label label-info" style="background-color: #dcdcdc">2016</div>
                                        </div>
                                        <canvas id="cumulativeAmountPerDay" height="140"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cumulative Donations per Day</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content text-center">
                                    <div>
                                        <div class="pull-left m-b-md">
                                            <div class="label label-info" style="background-color: #1ab394">2017</div>
                                            <div class="label label-info" style="background-color: #dcdcdc">2016</div>
                                        </div>
                                        <canvas id="cumulativeDonationsPerDay" height="140"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="row text-center">
            <a href="https://ev.kde.org/" target="_blank">
                <img src="images/logo.png" alt=""/>
            </a>
        </div>
        <div class="social-icons">
            <ul>
                <li>
                    <a class="envelope" href="mailto:kde-ev-board@kde.org">
                        <i class="fa fa-envelope" title="Contact the KDE e.V. Board of Directors"></i>
                    </a>
                </li>
                <li>
                    <a class="twitter" href="https://twitter.com/kdecommunity" target="_blank">
                        <i class="fa fa-twitter" title="The KDE Community - Twitter"></i>
                    </a>
                </li>
                <li>
                    <a class="dribbble" href="https://plus.google.com/105126786256705328374/" target="_blank">
                        <i class="fa fa-google-plus" title="The KDE Community - Google+"></i>
                    </a>
                </li>
                <li>
                    <a class="facebook" href="https://www.facebook.com/kde/" target="_blank">
                        <i class="fa fa-facebook" title="The KDE Community - Facebook"></i>
                    </a>
                </li>
                <li>
                    <a class="linkedin" href="https://dot.kde.org" target="_blank">
                        <i class="fa fa-newspaper-o" title="dot.kde.org"></i>
                    </a>
                </li>
            </ul>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12 text-center">
The Randa Meetings 2017 fundraising campaign's banner on top of this page is a work of KDE VDG Member Andres Betts, used under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0</a>. This banner is licensed under <a href="http://creativecommons.org/licenses/by/4.0" target="_blank">CC BY 4.0</a> by Andres Betts.
            </div>
            <div class="col-md-12 text-center">
                KDE<sup>&#174;</sup> and <a href="/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>&#174;</sup> logo</a> are registered trademarks of <a href="http://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> | <a href="http://www.kde.org/community/whatiskde/impressum.php">Legal</a>
            </div>
        </div>
  </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>

    <!-- ChartJS-->
    <script src="js/plugins/chartJs/Chart.min.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <!-- Peity demo -->
    <script src="js/demo/peity-demo.js"></script>

    <?php
        $page_title="Randa Meetings 2017 fundraising";

        $goal=20000;
        $enddate="2017-09-17";
        $daystogo=floor((strtotime($enddate)-time())/(60*60*24));

        require("www_config.php");
        $stmt = $dbConnection->prepare("SELECT SUM(amount) AS amount_sum, count(*) donations, DATE_FORMAT(CREATED_AT, '%m%d') AS date_fmt FROM randameetings2017donations ORDER BY DATE_FORMAT(CREATED_AT, '%m%d');");
        $stmt->execute();

        $labels2017 = "";
        $values2017 = "";
        $cumulativeAmount2017 = "";
        $cumulativeAmount2017Value = 0;
        $cumulativeDonations2017 = "";
        $cumulativeDonations2017Value = 0;
        $donations2017 = "";
        $count = 1;
        while ($row = $stmt->fetch()) {
                //$labels2017.=", ".$row["date_fmt"];
                $labels2017.=", ".$count;
                $count++;
                $values2017.=", ".$row["amount_sum"];
                $cumulativeAmount2017Value += $row["amount_sum"];
                $cumulativeAmount2017.=", ".$cumulativeAmount2016Value;
                $cumulativeDonations2017Value += $row["donations"];
                $cumulativeDonations2017.=", ".$cumulativeDonations2016Value;
                $donations2016.=", ".$row["donations"];
        }
        $labels2017.="";
        $values2017.="";
        $cumulativeAmount2017.="";
        $cumulativeDonations2017.="";
        $donations2017.="";

        $stmt = $dbConnection->prepare("SELECT SUM(amount) AS amount_sum, count(*) donations, DATE_FORMAT(date, '%m%d') AS date_fmt FROM randameetings2016donations ORDER BY DATE_FORMAT(date, '%m%d');") or die ($dbConnection->error);
        $stmt->execute();

        $labels2016 = "";
        $values2016 = "";
        $cumulativeAmount2016 = "";
        $cumulativeAmount2016Value = 0;
        $cumulativeDonations2016 = "";
        $cumulativeDonations2016Value = 0;
        $donations2016 = "";
        $count = 1;
        while ($row = $stmt->fetch()) {
                //$labels2016.=", ".$row["date_fmt"];
                $labels2016.=", ".$count;
                $count++;
                $values2016.=", ".$row["amount_sum"];
                $cumulativeAmount2016Value += $row["amount_sum"];
                $cumulativeAmount2016.=", ".$cumulativeAmount2016Value;
                $cumulativeDonations2016Value += $row["donations"];
                $cumulativeDonations2016.=", ".$cumulativeDonations2016Value;
                $donations2016.=", ".$row["donations"];
        }
        $labels2016.="";
        $values2016.="";
        $cumulativeAmount2016.="";
        $cumulativeDonations2016.="";
        $donations2016.="";
    ?>

    <script>
        var lineData = {
            <?php
                echo "labels: [".substr((strlen($labels2016) > strlen($labels2017)) ? $labels2016:$labels2017, 2)."],";
            ?>
            datasets: [
              {
                 label: "2016",
                 fillColor: "rgba(220,220,220,0.5)",
                 strokeColor: "rgba(220,220,220,1)",
                 pointColor: "rgba(220,220,220,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(220,220,220,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
             ,
             {
                 label: "2017",
                 fillColor: "rgba(26,179,148,0.5)",
                 strokeColor: "rgba(26,179,148,0.7)",
                 pointColor: "rgba(26,179,148,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(26,179,148,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
            ]
        };

        var lineOptions = {
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            bezierCurve: true,
            bezierCurveTension: 0.4,
            pointDot: true,
            pointDotRadius: 4,
            pointDotStrokeWidth: 1,
            pointHitDetectionRadius: 20,
            datasetStroke: true,
            datasetStrokeWidth: 2,
            datasetFill: true,
            responsive: true,
        };

        var ctx = document.getElementById("amountPerDay").getContext("2d");
        var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        var lineData2 = {
            <?php
                echo "labels: [".substr((strlen($labels2016) > strlen($labels2017)) ? $labels2016:$labels2017, 2)."],";
            ?>
            datasets: [
              {
                 label: "2016",
                 fillColor: "rgba(220,220,220,0.5)",
                 strokeColor: "rgba(220,220,220,1)",
                 pointColor: "rgba(220,220,220,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(220,220,220,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
             ,
             {
                 label: "2017",
                 fillColor: "rgba(26,179,148,0.5)",
                 strokeColor: "rgba(26,179,148,0.7)",
                 pointColor: "rgba(26,179,148,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(26,179,148,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
            ]
        };

        var ctx2 = document.getElementById("donationsPerDay").getContext("2d");
        var myNewChart2 = new Chart(ctx2).Line(lineData2, lineOptions);

        var lineData3 = {
            <?php
                echo "labels: [".substr((strlen($labels2016) > strlen($labels2017)) ? $labels2016:$labels2017, 2)."],";
            ?>
            datasets: [
              {
                 label: "2016",
                 fillColor: "rgba(220,220,220,0.5)",
                 strokeColor: "rgba(220,220,220,1)",
                 pointColor: "rgba(220,220,220,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(220,220,220,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
             ,
             {
                 label: "2017",
                 fillColor: "rgba(26,179,148,0.5)",
                 strokeColor: "rgba(26,179,148,0.7)",
                 pointColor: "rgba(26,179,148,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(26,179,148,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
            ]
        };

        var ctx3 = document.getElementById("cumulativeAmountPerDay").getContext("2d");
        var myNewChart3 = new Chart(ctx3).Line(lineData3, lineOptions);

        var lineData4 = {
            <?php
                echo "labels: [".substr((strlen($labels2016) > strlen($labels2017)) ? $labels2016:$labels2017, 2)."],";
            ?>
            datasets: [
              {
                 label: "2016",
                 fillColor: "rgba(220,220,220,0.5)",
                 strokeColor: "rgba(220,220,220,1)",
                 pointColor: "rgba(220,220,220,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(220,220,220,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
             ,
             {
                 label: "2017",
                 fillColor: "rgba(26,179,148,0.5)",
                 strokeColor: "rgba(26,179,148,0.7)",
                 pointColor: "rgba(26,179,148,1)",
                 pointStrokeColor: "#fff",
                 pointHighlightFill: "#fff",
                 pointHighlightStroke: "rgba(26,179,148,1)",
                 <?php
                     echo "data: [".substr($values2016, 2)."]";
                 ?>
             }
            ]
        };

        var ctx4 = document.getElementById("cumulativeDonationsPerDay").getContext("2d");
        var myNewChart4 = new Chart(ctx4).Line(lineData4, lineOptions);
    </script>
    <script src="js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
</body>

</html>
