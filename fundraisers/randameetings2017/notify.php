<?php namespace Listener;

$do_debug = true;
die("Donation to the Randa Meeting 2017 are not supported anymore. See <a href="/donations">Donnations</a>");

require('paypalIPN.php');

use PaypalIPN;

$ipn = new PaypalIPN();

$verified = $ipn->verifyIPN();
$data_text = "";
foreach ($_POST as $key => $value) {
    $data_text .= $key . " = " . $value . "\r\n";
}
// Create debug file
if ($do_debug) {
    $debug = fopen('/tmp/randa2017.txt', 'a+');
    fwrite($debug, "verify: $data_text\n");
}

if ($verified) {
  require_once("www_config.php");
  $payment_amount = $_POST['mc_gross'];
  $payment_status = $_POST['payment_status'];
  $receiver_email = $_POST["receiver_email"];
  $txn_id = $_POST['txn_id'];
  if ($_POST['option_selection1'] === "Yes") {
      $name = $_POST['address_name'];
  } else {
      $name = "";
  }

  if ( $payment_status != "Completed") {
    if ($do_debug) {
        fwrite($debug, "Unexpected payment status: ".$payment_status."\n");
    }
    die("Payment status is ".$payment_status);
  }

  $receiver_email_found = false;
  if (strtolower($receiver_email) != strtolower($kdeEvMail)) {
    if ($do_debug) {
        fwrite($debug, "Unexpected receiver email: ".$receiver_email."\n");
    }
    die("Unexpected receiver email");
  }
  if ( $_POST['mc_currency'] != $currency ) {
    if ($do_debug) {
        fwrite($debug, "Unexpected payment currency: ".$payment_currency."\n");
    }
    die("Unknown currency used");
  }

  $stmt = $dbConnection->prepare("INSERT INTO randameetings2017donations(created_at, donor_name, amount, transaction_id) VALUES (now(),?,?,?)") or die ($dbConnection->error);
  $stmt->bind_param("sss", $name, $payment_amount, $txn_id);
  $return = $stmt->execute();

  if($return == false){
    if ($do_debug) {
        fwrite($debug, "Error running insert query\n");
        fwrite($debug, "Error:".mysqli_error($dbConnection)."\n" );
    }
  }
  $stmt->close();
} // End if verified
else {
    if ($do_debug) {
        fwrite($debug, "IPN Not Validated, an error ocurred \n");
    }
}
if ($do_debug) {
    fwrite($debug, "\n---------------------------------------\n" );
    fclose($debug);
}

// Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
header("HTTP/1.1 200 OK");
?>
