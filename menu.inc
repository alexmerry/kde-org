<?php

global $document_root;

$this->setName ("Home");


$section =& $this->appendSection("Community","/community");
$section->appendDir("About KDE", "community/whatiskde");
$section->appendDir("Announcements", "announcements/");
$section->appendLink("Events", "http://www.kde.org/events/", false);
$section->appendDir("Get Involved", "community/getinvolved");
$section->appendDir("Donate", "community/donations");
$section->appendDir("Code Of Conduct", "code-of-conduct/");
$section->appendLink("Manifesto", "http://manifesto.kde.org/", false);

$item = $section->appendLink("History", "community/history/");
$item->setHidden();
$item = $section->appendLink("Awards", "community/awards/");
$item->setHidden();

$section =& $this->appendSection("Workspaces","/workspaces");
$section->appendDir("Plasma Desktop", "workspaces/plasmadesktop");
$section->appendDir("Plasma Netbook", "workspaces/plasmanetbook");
$section->appendDir("Plasma Active", "http://plasma-active.org");

$section =& $this->appendSection("Applications","/applications");
$index = json_decode(file_get_contents("$document_root/applications/index.json"), true);
$categories = array_keys($index);
sort($categories);

foreach($categories as $category) {
    $item = $section->appendDir( $category, "applications/" . strtolower($category) );
	foreach($index[$category] as $application) {
		$app = $item->appendDir($application[1], $application[0]);
        	$app->setHidden();
	}
}

$section =& $this->appendSection("Developer Platform","/developerplatform");
$section->appendLink("Techbase Wiki", "http://techbase.kde.org", false);
$section->appendLink("API Docs", "http://api.kde.org/", false);
$section->appendLink("Tutorials", "http://techbase.kde.org/Development/Tutorials", false);
//$section->appendDir("IDE", "developerplatform/ide");
//$section->appendDir("SDK", "developerplatform/sdk");
//$section->appendDir("Case Studies", "developerplatform/casestudies");


$section =& $this->appendSection("Support",'/support');
$section->appendLink("International Sites", "support/international.php");
$section->appendLink("Documentation", "http://docs.kde.org", false);
$section->appendLink("Userbase Wiki", "http://userbase.kde.org", false);
$section->appendLink("Sys Admin Wiki", "http://techbase.kde.org/SysAdmin", false);
$section->appendLink("Forums", "http://forum.kde.org", false);
$section->appendLink("Report a Bug", "https://bugs.kde.org", false);
$section->appendDir("Mailing Lists", "support/mailinglists");
$section->appendDir("Security Advisories", "info/security");

// Sections not in menu, for Breadcrumb use only.
$section =& $this->appendSection("Get KDE Software", "/download", true);
$section->appendLink("Distributions Shipping KDE", "download/distributions.php");
$section->appendLink("KDE On CD-ROM", "download/cdrom.php");

?>
