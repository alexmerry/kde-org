<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships First Alpha of Next Generation Plasma Workspace");
  $site_root = "../";
  $release = '4.95.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<a href="plasma-2014.6/kickoff2.jpg"><img src="plasma-2014.6/kickoff2-wee.jpg" style="float: right; padding: 1ex; margin: 1ex; width: 500px;  border: 0; background-image: none; " alt="<?php i18n("Plasma Next");?>" /></a>

<?php i18n("April 2, 2014. 
KDE today releases the first Alpha version of the next-generation Plasma workspace. This kicks off the public testing phase for the next iteration of the popular Free software workspace, code-named 'Plasma Next' (referring to the 'next' Plasma release-more below). Plasma Next is built using QML and runs on top of a fully hardware-accelerated graphics stack using Qt 5, QtQuick 2 and an OpenGL(-ES) scenegraph. Plasma Next provides a core desktop experience that will be easy and familiar for current users of KDE workspaces or alternative Free Software or proprietary offerings. Plasma Next is planned to be released as 2014.06 on the 17th of June.
");?>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing Plasma Next Alpha 1 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("A variety of distributions offer frequently updated packages of Plasma. This includes Kubuntu, Fedora and openSUSE. See <a href='http://community.kde.org/Plasma/Next/UnstablePackages'>this wikipage</a> for an overview.");?>
</p>

<h4>
  <?php i18n("Compiling Plasma");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php print i18n_var("The complete source code for Plasma Next Alpha 1 may be <a href='http://download.kde.org/unstable/plasma/%1/'>freely downloaded</a>.  Planned future releases are listed on the <a href='http://techbase.kde.org/Schedules/Plasma/2014.6_Release_Schedule'>the Plasma release schedule.</a>", $release);?>

  <?php i18n("It comes with three tars which do not co-install with
the equivalents using KDE libs 4.  You will need to uninstall these
older versions or install into a separate prefix.");?> </p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='http://jointhegame.kde.org/'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
