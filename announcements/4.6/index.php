<?php

  $release = '4.6';
  $release_full = '4.6.0';
  $page_title = "KDE Puts You In Control with New Workspaces, Applications and Platform";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE is delighted to announce its latest set of releases, providing major updates to the KDE Plasma workspaces, KDE Applications and KDE Platform. These releases, versioned 4.6, provide many new features in each of KDE's three product lines. Some highlights include:
</p>
<?php
  centerThumbScreenshot("46-w09.png", "KDE Plasma Desktop, Gwenview and KRunner in 4.6");
?>

<?php
include("trailer-plasma.inc");
include("trailer-applications.inc");
include("trailer-platform.inc");

?>


<h4>
    Spread the Word and See What Happens: Tag as "KDE"
</h4>
<p align="justify">
KDE encourages everybody to <strong>spread the word</strong> on the Social Web.
Submit stories to news sites, use channels like delicious, digg, reddit, twitter,
identi.ca. Upload screenshots to services like Facebook, Flickr,
ipernity and Picasa and post them to appropriate groups. Create screencasts and
upload them to YouTube, Blip.tv, Vimeo and others. Do not forget to tag uploaded
material with the <em>tag <strong>kde</strong></em> so it is easier for everybody to find the
material, and for the KDE team to compile reports of coverage for the KDE SC <?php echo $release;?>
 announcement. <strong>Help us spreading the word, be part of it!</strong></p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/news/technology/kde_software_compilation_4_6_0_released"><img src="buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/f9d9t/kde_software_compilation_460_released/"><img src="buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde46"><img src="buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde46"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde46"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde46"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde46"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>

<h4>Support KDE</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Join the Game" align="left"/> </a>
<p align="justify"> KDE e.V.'s new <a
href="http://jointhegame.kde.org/">Supporting Member programme</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
