
<h3>
<a href="applications.php">
KDE’s Dolphin Adds Faceted Browsing
</a>
</h3>

<p>
<a href="applications.php">
<img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.6.0"/>
</a>
Many of the <b>KDE Application</b> teams have also released new versions. Particular highlights include improved routing capabilities in KDE’s virtual globe, Marble, and advanced filtering and searching using file metadata in the KDE file manager, Dolphin: Faceted Browsing. The KDE Games collection receives many enhancements and the image viewer Gwenview and screenshot program KSnapshot gain the ability to instantly share images to a number of popular social networking sites. For more details read the <a href="applications.php">KDE Applications 4.6 announcement</a>.<br /><br />
</p>
