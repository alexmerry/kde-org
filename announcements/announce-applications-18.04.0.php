<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 18.04.0");
  $site_root = "../";
  $version = "18.04.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("April 19, 2018. KDE Applications 18.04.0 are now released.");?>
</p>

<p align="justify">
<?php print i18n_var("We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("What's new in KDE Applications 18.04");?></h3>

<h4 style="clear:both;" ><?php print i18n_var("System");?></h3>
<figure style="float: right; margin: 0px"><a href="dolphin1804.png"><img src="dolphin1804.png" width="250" height="185" /></a></figure>
<p align="justify">
<?php print i18n_var("The first major release in 2018 of <a href='%1'>Dolphin</a>, KDE's powerful file manager, features many  improvements to its panels:", "https://www.kde.org/applications/system/dolphin/");?>
<ul>
<li><?php i18n("The 'Places' panel's sections can now be hidden if you prefer not to display them, and a new 'Network' section is now available to hold entries for remote locations.");?></li>
<li><?php i18n("The 'Terminal' panel can be docked to any side of the window, and if you try to open it without Konsole installed, Dolphin will show a warning and help you install it.");?></li>
<li><?php i18n("HiDPI support for the 'Information' panel has been improved.");?></li>
</ul>
<?php i18n("The folder view and the menus have been updated as well:");?>
<ul>
<li><?php i18n("The Trash folder now displays an 'Empty Trash' button.");?></li>
<li><?php i18n("A 'Show Target' menu item has been added to help finding symlink targets.");?></li>
<li><?php i18n("Git integration has been enhanced, as the context menu for git folders now displays two new actions for 'git log' and 'git merge'.");?></li>
</ul>
<?php i18n("Further improvements include:");?>
<ul>
<li><?php i18n("A new shortcut has been introduced giving you the option to open the Filter Bar simply by hitting the slash (/) key.");?></li>
<li><?php i18n("You can now sort and organize photos by the date they were taken.");?></li>
<li><?php i18n("The drag-and-drop of many small files within Dolphin became faster, and users can now undo batch rename jobs.");?></li>
</ul>
</p>
<figure style="float: right; margin: 0px"><a href="konsole1804.png"><img src="konsole1804_small.jpeg" width="250" height="151" /></a></figure>
<p align="justify">
<?php print i18n_var("To make working on the command line even more enjoyable, <a href='%1'>Konsole</a>, KDE's terminal emulator application, can now look prettier:", "https://www.kde.org/applications/system/konsole/");?>
<ul>
<li><?php i18n("You can download color schemes via KNewStuff.");?></li>
<li><?php i18n("The scrollbar blends in better with the active color scheme.");?></li>
<li><?php i18n("By default the tab bar is shown only when needed.");?></li>
</ul>
<?php i18n("Konsole's contributors did not stop there, and introduced many new features:");?>
<ul>
<li><?php i18n("A new read-only mode and a profile property to toggle copying text as HTML have been added.");?></li>
<li><?php i18n("Under Wayland, Konsole now supports the drag-and-drop menu.");?></li>
<li><?php i18n("Several improvements took place in regards to the ZMODEM protocol: Konsole can now handle the zmodem upload indicator B01, it will show the progress while transferring data, the Cancel button in the dialog now works as it should, and transferring of bigger files is better supported by reading them into memory in 1MB chunks.");?></li>
</ul>
<?php i18n("Further improvements include:");?>
<ul>
<li><?php i18n("Mouse wheel scrolling with libinput has been fixed, and cycling through shell history when scrolling with the mouse wheel is now prevented.");?></li>
<li><?php i18n("Searches are refreshed after changing the search match regular expression option and when pressing 'Ctrl' + 'Backspace' Konsole will match xterm behaviour.");?></li>
<li><?php i18n("The '--background-mode' shortcut has been fixed.");?></li>
</ul>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Multimedia");?></h3>
<p align="justify">
<?php print i18n_var("<a href='%1'>JuK</a>, KDE's music player, now has Wayland support. New UI features include the ability to hide the menu bar and having a visual indication of the currently playing track. While docking to the system tray is disabled, JuK will no longer crash when attempting to quit via the window 'close' icon and the user interface will remain visible.
Bugs regarding JuK autoplaying unexpectedly when resuming from sleep in Plasma 5 and handling the playlist column have also been fixed.", "https://juk.kde.org/");?>
</p>
<p>
<?php print i18n_var("For the 18.04 release, contributors of <a href='%1'>Kdenlive</a>, KDE's non-linear video editor, focused on maintenance:", "https://kdenlive.org/");?>
<ul>
<li><?php i18n("Clip resizing no longer corrupts fades and keyframes.");?></li>
<li><?php i18n("Users of display scaling on HiDPI monitors can enjoy crisper icons.");?></li>
<li><?php i18n("A possible crash on startup with some configurations has been fixed.");?></li>
<li><?php i18n("MLT is now required to have version 6.6.0 or later, and compatibility is improved.");?></li>
</ul>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Graphics");?></h3>
<figure style="float: right; margin: 0px"><a href="gwenview1804.png"><img src="gwenview1804.png" width="250" height="175" /></a></figure>
<p align="justify">
<?php print i18n_var("Over the last months, contributors of <a href='%1'>Gwenview</a>, KDE's image viewer and organizer, worked on a plethora of improvements. Highlights include:", "https://www.kde.org/applications/graphics/gwenview/");?>
<ul>
<li><?php i18n("Support for MPRIS controllers has been added so you can now control full screen slideshows via KDE Connect, your keyboard's media keys, and the Media Player plasmoid.");?></li>
<li><?php i18n("The thumbnail hover buttons can now be turned off.");?></li>
<li><?php i18n("The Crop tool received several enhancements, as its settings are now remembered when switching to another image, the shape of the selection box can now be locked by holding down the 'Shift' or 'Ctrl' keys and it can also be locked to the aspect ratio of the currently displayed image.");?></li>
<li><?php i18n("In full screen mode, you can now exit by using the 'Escape' key and the color palette will reflect your active color theme. If you quit Gwenview in this mode, this setting will be remembered and the new session will start in full screen as well.");?></li>
</ul>
<?php print i18n("Attention to detail is important, so Gwenview has been polished in the following areas:");?>
<ul>
<li><?php i18n("Gwenview will display more human-readable file paths in the 'Recent Folders' list, show the proper context menu for items in the 'Recent Files' list, and correctly forget everything when using the 'Disable History' feature.");?></li>
<li><?php i18n("Clicking on a folder in the sidebar allows toggling between Browse and View modes and remembers the last used mode when switching between folders, thus allowing faster navigation in huge image collections.");?></li>
<li><?php i18n("Keyboard navigation has been further streamlined by properly indicating keyboard focus in Browse mode.");?></li>
<li><?php i18n("The 'Fit Width' feature has been replaced with a more generalized 'Fill' function.");?></li>
</ul>
<figure style="float: right; margin: 0px"><a href="gwenviewsettings1804.png"><img src="gwenviewsettings1804.png" width="250" height="232" /></a></figure>
<?php print i18n("Even smaller enhancements often can make user's workflows more enjoyable:");?>
<ul>
<li><?php i18n("To improve consistency, SVG images are now enlarged like all other images when 'Image View → Enlarge Smaller Images' is turned on.");?></li>
<li><?php i18n("After editing an image or undoing changes, image view and thumbnail won't get out of sync anymore.");?></li>
<li><?php i18n("When renaming images, the filename extension will be unselected by default and the 'Move/Copy/Link' dialog now defaults to showing the current folder.");?></li>
<li><?php i18n("Lots of visual papercuts were fixed, e.g. in the URL bar, for the full screen toolbar, and for the thumbnail tooltip animations. Missing icons were added as well.");?></li>
<li><?php i18n("Last but not least, the full screen hover button will directly view the image instead of showing the folder's content, and the advanced settings now allow for more control over the ICC color rendering intent.");?></li>
</ul>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Office");?></h3>
<p align="justify">
<?php print i18n_var("In <a href='%1'>Okular</a>, KDE's universal document viewer, PDF rendering and text extraction can now be cancelled if you have poppler version 0.63 or higher, which means that if you have a complex PDF file and you change the zoom while it's rendering it will cancel immediately instead of waiting for the render to finish.", "https://www.kde.org/applications/graphics/okular/");?>
</p>
<p align="justify">
<?php print i18n("You will find improved PDF JavaScript support for AFSimple_Calculate, and if you have poppler version 0.64 or higher, Okular will support PDF JavaScript changes in read-only state of forms.");?>
</p>
<p>
<?php print i18n_var("Management of booking confirmation emails in <a href='%1'>KMail</a>, KDE's powerful email client, has been significantly enhanced to support train bookings and uses a Wikidata-based airport database for showing flights with correct timezone information. To make things easier for you, a new extractor has been implemented for emails not containing structured booking data.", "https://www.kde.org/applications/internet/kmail/");?>
</p>
<p align="justify">
<?php print i18n("Further improvements include:");?>
<ul>
<li><?php i18n("The message structure can again be shown with the new 'Expert' plugin.");?></li>
<li><?php i18n("A plugin has been added in the Sieve Editor for selecting emails from the Akonadi database.");?></li>
<li><?php i18n("The text search in the editor has been improved with regular expression support.");?></li>
</ul>
</p>

<h4 style="clear:both;" ><?php print i18n_var("Utilities");?></h3>
<figure style="float: right; margin: 0px"><a href="spectacle1804.png"><img src="spectacle1804.png" width="250" height="165" /></a></figure>
<p align="justify">
<?php print i18n_var("Improving the user interface of <a href='%1'>Spectacle</a>, KDE's versatile screenshot tool, was a major focus area:", "https://www.kde.org/applications/graphics/spectacle/");?>
<ul>
<li><?php i18n("The bottom row of buttons has received an overhaul, and now displays a button to open the Settings window and a new 'Tools' button that reveals methods to open the last-used screenshot folder and launch a screen recording program.");?></li>
<li><?php i18n("The last-used save mode is now remembered by default.");?></li>
<li><?php i18n("The window size now adapts to the aspect ratio of the screenshot, resulting in a more pleasant and space-efficient screenshot thumbnail.");?></li>
<li><?php i18n("The Settings window has been substantially simplified.");?></li>
</ul>
<?php print i18n("In addition, users will be able to simplify their workflows with these new features:");?>
<ul>
<li><?php i18n("When a specific window has been captured, its title can be automatically added to the name of the screenshot file.");?></li>
<li><?php i18n("The user can now choose whether or not Spectacle automatically quits after any save or copy operation.");?></li>
</ul>
<?php print i18n("Important bug fixes include:");?>
<ul>
<li><?php i18n("Drag-and-drop to Chromium windows now works as expected.");?></li>
<li><?php i18n("Repeatedly using the keyboard shortcuts to save a screenshot does not result in an ambiguous shortcut warning dialog anymore.");?></li>
<li><?php i18n("For rectangular region screenshots, the bottom edge of the selection can be adjusted more accurately.");?></li>
<li><?php i18n("Improved reliability of capturing windows touching screen edges when compositing is turned off.");?></li>
</ul>
</p>
<figure style="float: right; margin: 0px"><a href="kleopatra1804.gif"><img src="kleopatra1804.png" width="250" height="192" /></a></figure>
<p align="justify">
<?php print i18n_var("With <a href='%1'>Kleopatra</a>, KDE's certificate manager and universal crypto GUI, Curve 25519 EdDSA keys can be generated when used with a recent version of GnuPG. A 'Notepad' view has been added for text based crypto actions and you can now sign/encrypt and decrypt/verify directly in the application. Under the 'Certificate details' view you will now find an export action, which you can use to export as text to copy and paste. What's more, you can import the result via the new 'Notepad' view.", "https://www.kde.org/applications/utilities/kleopatra/");?>
</p>
<p align="justify">
<?php print i18n_var("In <a href='%1'>Ark</a>, KDE's graphical file compression/decompression tool with support for multiple formats, it is now possible to stop compressions or extractions while using the libzip backend for ZIP archives.", "https://www.kde.org/applications/utilities/ark/");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("Applications joining the KDE Applications release schedule");?></h3>
<p align="justify">
<?php print i18n_var("KDE's webcam recorder <a href='%1'>Kamoso</a> and backup program <a href='%2'>KBackup</a> will now follow the Applications releases. The instant messenger <a href='%3'>Kopete</a> is also being reintroduced after being ported to KDE Frameworks 5.", "https://userbase.kde.org/Kamoso", "https://www.kde.org/applications/utilities/kbackup/", "https://www.kde.org/applications/internet/kopete/");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("Applications moving to their own release schedule");?></h3>
<p align="justify">
<?php print i18n_var("The hex editor <a href='%1'>Okteta</a> will have its own release schedule after a request from its maintainer.", "https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("Bug Stomping");?></h3>
<p align="justify">
<?php print i18n_var("More than 170 bugs have been resolved in applications including the Kontact Suite, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular, Umbrello and more!");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors play an important part in KDE's success. While proprietary software companies have huge advertising budgets for new software releases, KDE often depends on word of mouth."); ?>
</p>

<p align="justify">
<?php print i18n_var("There are many ways to support the KDE Applications 18.04 release: you can report bugs, encourage others to join the KDE Community, or <a href='%1'>support the non-profit organization behind the KDE Community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Help us spread the word on the social web. You can submit stories to news sites like Reddit, Facebook and Twitter; upload screenshots of your new set-up to services like Snapchat, Instagram and Google+; or create screencasts and upload them to YouTube, Blip.tv, and Vimeo, or stream them live over Twitch!"); ?>
</p>

<p align="justify">
<?php i18n("Remember to tag your posts and uploaded materials with the <i>KDE</i> moniker, as this makes them easier to find and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 18.04 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 18.04 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 18.04 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 18.04");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 18.04 may be <a href='http://download.kde.org/stable/applications/18.04.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-18.04.0.php'>KDE Applications 18.04.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
