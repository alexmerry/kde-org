<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Plasma 5.2 complete changelog");
  $site_root = "../";
  $release = 'plasma-5.1.95';
  include "header.inc";
?>


<h3>Plasma NM</h3><ul>
<li>Replace foreach with Q_FOREACH to fix build with NM 1.0.0</li>
<li>Show only common devices in tooltip with active connections</li>
<li>Workaround for proper loading of VPN secrets</li>
<li>Do not show notifications for unknown connections</li>
<li>Make storing openconnect secrets optional</li>
<li>Make NM to store Openconnect secrets into KWallet</li>
<li>Do not display unknown connections</li>
<li>Remove dependency on libnm-util and libnm-glib</li>
<li>Make sure to search for a specific NM version</li>
<li>Make plasma-nm work with nm-qt using NetworkManager 1.0.0</li>
<li>Checking for security in wireless setting won't work for NM 1.0.0+</li>
<li>Avoid crash when trying to load secrets for empty setting</li>
<li>Fix mobile connection wizard manual apn setting.</li>
<li>fix display of non-ASCII SSIDs</li>
<li>Workaround: make sure we don't send completely empty map to NM back when asking for VPN secrets</li>
<li>Do not show Ok button for openconnect auth dialog</li>
<li>Fix CMake deprecation warning</li>
<li>fix build if WITH_MODEMMANAGER_SUPPORT is 0</li>
<li>port bluez connection support to bluez 5</li>
<li>always use vpn state to set connecting state if possible</li>
<li>Move NMQT to the list of required frameworks</li>
<li>correctly maintain connecting state and connection icons</li>
<li>Adapt to libnm-qt changes</li>
<li>Do not forget to set loaded auto-negotiate state</li>
<li>Make the refresh button visible only when there is enabled wireless device</li>
<li>Update OpenConnect storage of manually-accepted server certs</li>
<li>Update OpenConnect support for library version 5</li>
<li>Get name of the deactivated connection from id property to prevent missing name in notification</li>
<li>Add missing include (QDBusConnection).</li>
<li>Fix build with ModemManager disabled.</li>
<li>Display also VPN type in column with connection type</li>
<li>add column "connection type"</li>
<li>save/restore connections widget state (columns, sorting, ...)</li>
<li>MMModemLock is already declared as a metatype in libmm-qt</li>
<li>get rid of KComboBox</li>
<li>fix layout and tab stops</li>
<li>Fix strings broken by designer</li>
<li>OpenVPN: Add option for server certificate verification</li>
<li>Disable debug again This was not supposed to be pushed</li>
<li>Ask for a PIN code when activating a connection and modem is still locked</li>
<li>Move activating connections to the top</li>
<li>Use a proper ToolButton for the refresh button</li>
<li>Use Interactive resize mode instead of ResizeToContents</li>
<li>Use new connect api</li>
<li>update it</li>
<li>fix OPenVPN config tab and layout issues</li>
<li>Properly import routes</li>
<li>Using styled text shouldn't be necessary here</li>
<li>Remove redirect-gateway tag from exported/imported config file</li>
<li>don't crash on adding routes</li>
<li>turn the editor into a unique application</li>
<li>fix QUrl constructions</li>
<li>implement connect/disconnect actions</li>
<li>Display also channel in the BSSID combobox</li>
<li>Add frequency to the BSSID combobox</li>
<li>fix the form layout order</li>
<li>fix tab order</li>
<li>fix tab order</li>
<li>remove accel</li>
<li>fix tab order</li>
<li>optimize if branches and foreach loops</li>
<li>simplify notification handling</li>
<li>--deprecated</li>
<li>string fixes</li>
<li>check name of country is in map instead of filter by length</li>
<li>avoid overflowing text</li>
<li>Make sure we are trying to update existing modem connections</li>
<li>Fix crash when restarting NetworkManager</li>
<li>Watch for IpInterface change, otherwise it won't get updated This is necessary for ModemDevices as IpInterface is used for monitoring of current download/upload</li>
<li>unbreak version string</li>
<li>Fix crash when removing modem device</li>
<li>Fix crash when trying to set operator name for uninitialized GsmInterface GsmInterface is initialized only when modem is connected.</li>
<li>simplify code, use QComboBox::currentData()</li>
<li>fix password mode and getting secrets</li>
<li>don't leak the pending reply watcher</li>
<li>unbreak layout</li>
<li>don't use deprecated KAboutData::setProgramIconName</li>
<li>Port away from qDebug and qWarning</li>
<li>Add "My plan is not listed" to the first place in combobox when there is no available APN Otherwise the wizard will crash later.</li>
<li>Do not require 802-1x secrets when using LEAP security</li>
<li>Properly initialize new OpenVPN connections</li>
<li>set version number using same syntax as other parts of Plasma</li>
<li>Ask user if they want to copy their certificates under KDE configuration during OpenVPN import</li>
<li>Do not insert empty VPN banner detail</li>
<li>Update system tray icon even after a small signal drop/increase</li>
<li>Remove shebang from .desktop file</li>
<li>Add IPv6 tab for OpenVPN connections BUG:339652</li>
<li>use the new QMenu::addSection() instead of fiddling with the separator</li>
<li>Use regular IconItem for CompactRepresentation</li>
<li>cleanups</li>
<li>Round CompactRepresentation size to icon size</li>
</ul>
<h3>Oxygen </h3><ul>
<li>Only delete our own style objects when a plugin is unloaded</li>
<li>do not install window shadow if mdi's subwindow contains a KMainWindow. This is because KMainWindow affects event processing and make all input events captured by the MdiWindowShadow.</li>
<li>fixed margins</li>
<li>re-added viewDrawFocus option, with improved rendering CCMAIL: david@davidedmundson.co.uk</li>
<li>using QX11Info::isPlatformX11</li>
<li>uppercase style name</li>
<li>Fixed color role for accelerator</li>
<li>added inputWidgetEngine, for abstract item views, line editors, spinboxes and comboboxes it is needed to avoid animation conflicts between lists and buttons (when, e.g. there is a checkbox inside a list)</li>
<li>removed support for Q3ListView</li>
<li>Disable building of window decoration</li>
<li>Manually calculate viewport update rect on header hover change</li>
<li>Remove mnemonic underlines when alt-tabbing</li>
<li>Fix kde4 build/configure/cmake stage</li>
<li>Fixed member initialization removed unused headers backported from breeze</li>
<li>Replace manual _export.h files with CMake's generate_export_header. REVIEW: 121083</li>
<li>invert to #if !kde4 to achieve what was intended</li>
<li>fix build, Kdelibs4Migration header is only available and used on kf5</li>
<li>Sync advanced settings to KDE4</li>
<li>Added WeakPointer typedef to oxygendecorationdefines to allow compilation against both KDE4 and KF5 Use WeakPointer<Client> inside Oxygen::SizeGrip to keep track of client deletion and avoid crash. BUG: 340597</li>
<li>cleanup</li>
<li>fixed dialog titles</li>
<li>Do not takeout margins from toolbutton before rendering text, unless they have inline indicator, because some custom widgets do not respect sizeFromContents</li>
<li>- cleanup tab buttons positioning - center icon in tabs when there is no text and no buttons</li>
<li>Fixed Confirmation dialog when removing exceptions</li>
<li>properly handle RTL for corner rects and tabbar Do not clip away vertical margins for headers to accomodate custom widgets that set a two small vertical size for those. BUG: 340155 CCBUG: 340150</li>
<li>added text to right corner widget, to debug asymetric left and right corner buttons</li>
<li>re-fixed corner buttons positioning.</li>
<li>cleanup + comments</li>
<li>re-enabled KDE4 oxygen-settings compilation</li>
<li>un-ifdef old-style plugin declaration so that it can be used for both KF5 and KDE4</li>
<li>Allow to load style and decoration config plugins in both kde4 and kf5</li>
<li>moved actuall app execution inside Oxygen namespace to get proper typedefs</li>
<li>hide unused var</li>
<li>Align Style::tabWidgetCornerRect with Breeze code</li>
<li>moved code from header to imp</li>
<li>helper() -> _helper</li>
<li>fix qt4 build by checking for pkgconfig</li>
<li>add conditional install of translation files so it doesn't get added unconditionally by the release scripts</li>
<li>fixed tab order</li>
<li>uint32_t -> quint32 CCBUG: 337884</li>
<li>cosmetics</li>
<li>improved contrast for rendering checkbox marks, arrows, etc. BUG: 337433</li>
<li>Added hasParent method Do not add ItemDelegate to combobox views if inside QWebView, because ItemDelegate handling is broken in Qt for such widgets</li>
<li>Prefer const char[] over const char* const.</li>
<li>send dbus signal to reload deco configuration when changed (copied from kcmkwin) added Qt5::DBus as dependency</li>
<li>Make constants const, saves a few unnecessary symbols in the .data section.</li>
<li>fixed 'unmaximize' button centering</li>
<li>re-added reset method to deal with config changes in KDE4</li>
<li>Fixed scaling and positioning of buttons BUG: 316716</li>
<li>Also set TextColor in backgroundPalette This fixes tab close buttons color for inactive tabs in titleOutline mode</li>
<li>Move buttonDetailColor to cpp</li>
<li>QPalette.window().color() -> QPalette::color( QPalette::Window )</li>
<li>sanitize metrics name and moved to oxygendecorationdefines</li>
<li>simplified rendering of title outline for single item</li>
<li>Fixed background color for tabbed items in active outlined window Also fixed dragging</li>
<li>moved both titleBarTextColor methods to cpp</li>
</ul>
<h3> KHelpCenter </h3><ul>
<li>use ecm_optional_add_subdirectory for docs so translations can optionally be added optionally</li>
<li>Allow KHelpCenter from Plasma 5 to work with KDE4 applications.</li>
<li>Fix shortcut warning</li>
<li>Remove kdelibs4support. Fix includes</li>
<li>remove include moc</li>
<li>convert to reverse dns desktop</li>
<li>Port to qicon</li>
<li>Port to QFontComboBox</li>
<li>Don't use deprecated variable</li>
<li>port to new connect api</li>
<li>Remove some kde4support</li>
<li>Fix setShortcut warning.</li>
</ul>
<h3> KMenuEdit </h3><ul>
<li>Migrate settings</li>
<li>port to qDebug*</li>
<li>Fix includes</li>
<li>Remove not necessary includes</li>
</ul>
<h3>Plasma Desktop </h3><ul>
<li>reposition the widget explorer on panel delete</li>
<li>use plasma version number in knetattach</li>
<li>make version numbers use plasma version</li>
<li>give the default icon a Lyout with hints</li>
<li>consider screen position, not only size</li>
<li>Recuce weight of application style KCM</li>
<li>fix typo</li>
<li>Only add printable characters to search query</li>
<li>Use the proper way of testing for paste shortcut</li>
<li>Use QCollator for sorting flattened menu hierarchies.</li>
<li>Add missing QObject includes</li>
<li>Speed up subdialog hiding when the main dialog is hidden.</li>
<li>items height is the max between icon and text layout</li>
<li>reload the model when the save session changes</li>
<li>in the leave view, use PlasmaCore.IconItem</li>
<li>Make size combo box fit the text size</li>
<li>add init phase</li>
<li>Make sure the panel containment resizes when the theme margins change.</li>
<li>optionally add doc subdirectories so translations have the option of missing bits</li>
<li>Fix warnings</li>
<li>Show drag handle in applet sidebar</li>
<li>fix porting error, make sure to actually create the gtkRc file</li>
<li>Don't flicker.</li>
<li>Ease in a little calmer.</li>
<li>Delay fading in the panel spacer resize handles until the drag overlay arrives on target.</li>
<li>Improved panel applet handle behavior.</li>
<li>Switch away from KRun::run to properly detach launched apps.</li>
<li>Remove now useless include AKA fix build</li>
<li>Remove now-unused KonqOperations copy.</li>
<li>Port foldermodel from KonqOperations::doDrop to the new KIO::drop() job</li>
<li>Set highlightResizeDuration to 0</li>
<li>Account for config/delete button in panel config overlay not being present all the time</li>
<li>Force focus on the search field initially</li>
<li>Add Copyright header</li>
<li>Fix tabstop order for adding keyboard layouts</li>
<li>Remove KDE3 porting artifacts</li>
<li>Replace "" with QString()</li>
<li>react on workspace changes (area size, count of screens)</li>
<li>Hide the dialog first and then unload its content</li>
<li>Improve Escape handling in widget explorer</li>
<li>Don't needlessly force active focus on every key press</li>
<li>Whenever add widgets prompt closes focus the search again</li>
<li>Handle keyboard events in activity creation dialog</li>
<li>Only animate tabbar when plasmoid is expanded</li>
<li>Don't set an initial size in the qml file, the shell takes care of that</li>
<li>Port kcm_kded away from kdelibs4support</li>
<li>Bring back computer-laptop icon in Kickoff</li>
<li>Port to QIcon</li>
<li>Port to QIcon</li>
<li>Remove not necessary include moc</li>
<li>Scale down oversized face pixmaps.</li>
<li>Replace '&' with 'and' (see: HIG → Wording).</li>
<li>Convert to UTF-8 to make QtCreator happy</li>
<li>Notifications KCM: Drop page that does nothing</li>
<li>When switching to favorites page make sure the tab button is updated too</li>
<li>Fix connect.</li>
<li>Simplify && cleanup.</li>
<li>Fix conditional.</li>
<li>Make -Wparentheses happy.</li>
<li>Don't delete the prop unnecessarily.</li>
<li>Fix tooltip bring faded out when shown during window highlight.</li>
<li>Fix crash in kickoff</li>
<li>Improve cmake scripts regarding PackageKit</li>
<li>check on parent</li>
<li>disable empty action when trashcan is empty</li>
<li>give some margins</li>
<li>expand the dialog to make room for the text</li>
<li>remove unnecessary workaround</li>
<li>Add tooltip window to the list of highlighted windows.</li>
<li>no KUrl needed here</li>
<li>Add some spacing.</li>
<li>Fix panel applet handle sizing and layout.</li>
<li>Fix reference errors breaking offset calc for panel size handle.</li>
<li>kservice_desktop_to_json -> kcoreaddons_desktop_to_json</li>
<li>Don't rely on Dialog::focusOutEvent being able to tell focus moved to a child window.</li>
<li>Fix initial keyboard focus when opened by mouse.</li>
<li>Use same icon as Kickoff.</li>
<li>Cleanup.</li>
<li>calculate delta for wheel event</li>
<li>Implement HIG comments on Pager settings</li>
<li>unbreak GTK apps theming</li>
<li>Colors config module ported away from KDELibs4Suppot.</li>
<li>Containment: Redirect wheel events to ContainmentInterface when view is not overflowing.</li>
<li>Pass along timestamp when launching things.</li>
<li>Use latest X11 user time for creating StartupInfoId</li>
<li>Drop dependency on KDecoration</li>
<li>Remove KGlobal check and KComponentData object that overrode applications KAboutData when showing dialogs.</li>
<li>Minimize overdraw in Desktop view</li>
<li>Port ktouchpadenabler to KF5</li>
<li>implement defaults(), fix Revert and GHNS</li>
<li>remove dead/obsolete code</li>
<li>[desktop/views] Right margins should be counted using fixedMargins.right not .bottom</li>
<li>fix the Defaults in Splash screen kcm</li>
<li>don't duplicate entries when the Rever button is clicked</li>
<li>bouncing cursor is the default</li>
<li>Adjust to new dialog margin behavior.</li>
<li>Make the Keyboard KStatusNotifierItem icon use KFontUtils</li>
<li>Small code quality issues found when trying to understand the code</li>
<li>Remove dead code</li>
<li>Set KIO::Integration::AccessManager to null so we don't crash on close.</li>
<li>Add context to i18n call</li>
<li>set thr right margin too</li>
<li>Breeze is the default cursor theme</li>
<li>Use QLatin1String</li>
<li>String not used and there is a %1 which is never used too => remove it</li>
<li>Fix qml warnings in splash kcm</li>
<li>fix installing cursor themes from GHNS</li>
<li>fix target install dir</li>
<li>fix crash on Defaults</li>
<li>fix install target dir</li>
<li>remove obsolete and non-functional kthememanager themes</li>
<li>set X-KDE-Init-Phase</li>
<li>use Solid::PowerManagement::requestSleep(Solid::PowerManagement::StandbyState, 0, 0)</li>
<li>Add Q_PLUGIN_METADATA so the attica_kde.so plugin can be loaded by QPluginLoader.</li>
<li>Fix regression in event handler causing favorites to launch on right-click release.</li>
<li>kdelibs4support--. Port to new connect api</li>
<li>Port away from deprecated API.</li>
<li>Fix folder preview popups for .desktop directory links in containment mode.</li>
<li>Fix translate</li>
<li>USe ki18n_wrap_ui</li>
<li>Remove executable attribute</li>
<li>Fix includes</li>
<li>Port to QListWidget</li>
<li>Port to new connect api</li>
<li>Revert "Force PlainText in taskmanager tooltips"</li>
<li>Force PlainText in taskmanager tooltips</li>
<li>Don't use HTML formatting for plain text</li>
<li>Port to new connect api</li>
<li>Port to new connect api</li>
<li>Don't put scripts from ~/.config/autostart in autostart kcm list.</li>
<li>Fix terminology in configuration labels (KDE → Plasma).</li>
<li>Consistency++.</li>
<li>Sync default favorites to Kickoff again.</li>
<li>Hide activity files location option when activity linking is disabled.</li>
<li>Show activity-linked files in folder view option</li>
<li>Remove keyboard click volume</li>
<li>Remove combo "Graphical Effects"</li>
<li>Re-enable the ksplash KCM that was temporarily in plasma-workspace</li>
<li>Use Loader for "More Settings" menu in panelcontroller</li>
<li>Invoke the trash action on StandardKey.Delete.</li>
<li>Use the KDE default browser setting instead of looping over a hardcoded list of known browsers.</li>
<li>QString& browser → QString &browser</li>
<li>Launch new instances on Shift+Click.</li>
<li>Update default applications in kickoff</li>
<li>drop libkonq_export for internal libkonq copy</li>
<li>[kcms/lnf] Make a list variable const</li>
<li>Avoid binding loop</li>
<li>Update from the real libkonq, to make sure nothing that was removed creates a problem here.</li>
<li>Port from KonqOperations::doPaste to KIO::paste</li>
<li>correctly manage applets removal in the panel</li>
<li>[kcm/lookandfeel] Add support for changing DesktopSwitcher</li>
<li>parse error --</li>
<li>recalculate borders on prefix or theme change</li>
<li>[kcm/lookandfeel] Add support for changing WindowSwitcher</li>
<li>adjust margins</li>
<li>fix layout</li>
<li>add simple tooltips in the applet handle actions</li>
<li>Fix build of keyboards kcm on debian-based systems</li>
<li>make kde4 apps react to icon theme change</li>
<li>Convert to new connect api</li>
<li>convert to new connect api. Fix includes, fix forward declaration. kdelibs4support--</li>
<li>Fix typo</li>
<li>typo--</li>
<li>add margins into panel</li>
<li>convert to new api. kdelibs4support--</li>
<li>Port to reverse dns desktop, kdelibs4support--</li>
<li>code formatting for preview files</li>
<li>Switch to favorites after a new favorite has been added</li>
<li>Fix reconfiguring launch feedback</li>
<li>Baloo KCM: Remove unnecessary translation domain removal</li>
<li>Baloo KCM: "File Search" instead of "Desktop Search"</li>
<li>Add a basic doc file for the Baloo KCM</li>
<li>Add the Baloo "File Search" KCM</li>
<li>Hide the sidebar separator if only one type of sidebar item is shown.</li>
<li>Extract messages from qml files in look and feel kcm</li>
<li>fix broken tooltip icon</li>
<li>use KIO::emptyTrash(), comment out unused and expensive code</li>
<li>fix test</li>
<li>when resize or move buttons pressed, hide dialog</li>
<li>Fix warnings</li>
<li>make apply button work correctly</li>
<li>code formatting and renaming</li>
<li>revert back the change to pass geometry around</li>
<li>fixed preview for Sun models by adding include in grammar and improved preview implementation</li>
<li>show the expanded marker only if the theme has it</li>
<li>drop invalid config keys</li>
<li>drop invalid config keys</li>
<li>fix emptying the trash from the context menu</li>
<li>don't animate move in some situations</li>
<li>fix the offset slider in vertical centered panels</li>
<li>kdelibs4support--, convert to new connect api</li>
<li>kdelibs4support--, kdelibs4support--</li>
<li>Re-install test package if it already exists + wait for install to finish</li>
<li>Read/Write ColorScheme to configGroup General instead of KDE</li>
<li>Convert to QPushButton</li>
<li>kdelibs4support--. Remove not necessary include moc</li>
<li>fix broken dialog layout</li>
<li>solve layer bug on qml side</li>
<li>port UI code to kf5 (button groups)</li>
<li>port UI code to kf5</li>
<li>port layout restore to kf5</li>
<li>stop ibus daemon so it does not mess with our keyboard configuration</li>
<li>resurrect restoring keyboard layouts when new keyboard device is plugged in</li>
<li>fix export dialog</li>
<li>port to kde frameworks (non-UI changes)</li>
<li>port kDebug to qCDebug</li>
<li>Take keyboard geometry into account when showing layout preview GSoC 2013 project by Shivam Makkar (amourphious1992@gmail.com) REVIEW: 113413</li>
<li>fullRepresentation is null on quit</li>
<li>remove binding loop</li>
<li>close button at top-right on vertical panels</li>
<li>Add system action to manually save session.</li>
<li>use the runner match querydata if available</li>
<li>Take keyboard geometry into account when showing layout preview GSoC 2013 project by Shivam Makkar (amourphious1992@gmail.com) REVIEW: 113413</li>
<li>Avoid text rendering outside toolbox frame when dragging from corner -> edge</li>
<li>Fix BUG#105797: inappropriate fontconfig settings are saved when kcontrol/fonts is shown and no way to revert them</li>
<li>Make constants const, saves a few symbols in the .data section.</li>
<li>Run first match on Enter/Return in search field.</li>
<li>avoid crossing the offset and size handles</li>
</ul>
<h3> KWrited </h3><ul>
<li>Kill KDELibs4Support</li>
<li>Convert to qdebug</li>
<li>Don't use deprecated variable</li>
</ul>
<h3> Breeze </h3><ul>
<li>Only delete style objects created by us</li>
<li>int -> qreal Sorry ! BUG: 343041</li>
<li>use Flag enumeration to describe button position add horizontal offset for rendering properly extend horizontal hit area for buttons in maximize mode, padding is preserved, but Fitts law is fullfilled.</li>
<li>added default geometry in constructor</li>
<li>do not install window shadow if mdi's subwindow contains a KMainWindow. This is because KMainWindow affects event processing and make all input events captured by the MdiWindowShadow. BUG: 342570</li>
<li>also translate 'menu' icon properly to keep proper padding in maximized mode</li>
<li>Moved buttons geometry handling to Breeze::decoration Do not change size of titlebar when maximizing. Expend buttons hit area to satisfy Fitts law in maximize mode BUG: 342824 BUG: 342864</li>
<li>Do not draw separator in active windows if titlebar color match window background color</li>
<li>remove redundant icon directory that was added in error.</li>
<li>add missing file</li>
<li>new cursors</li>
<li>MenuItem spacing -> 3px ListItem spacing -> 3px CCMAIL: jamboarder@gmail.com</li>
<li>decrease spacing between checkbox/radiobutton mark and text 8 -> 6 px CCMAIL: jamboarder@gmail.com</li>
<li>removed commented line</li>
<li>translated down radio button and checkbox mark when pressed translate down pushbutton frame and content when pressed translate down content and arrow icon of non-flat, non-editable comboboxes when pressed translate down content and arrow icon for non-flat toolbuttons when pressed CCMAIL: jamboarder@gmail.com</li>
<li>Reduce margins for list headers and menubars from 8px down to 6px CCMAIL: jamboarder@gmail.com</li>
<li>more typedef -> using</li>
<li>typedef -> using</li>
<li>replace jpg with png version of Next wallpaper</li>
<li>add new icon dirs to cmake</li>
<li>restore i18n after upstream update.</li>
<li>upstream updates (emblems, emoticons, misc)</li>
<li>Keep track of palette change events, to update widgets for which the palettes was altered accordingly. BUG: 341940</li>
<li>Fix make install</li>
<li>Change icon directory structure to match upstream repo.</li>
<li>add new files</li>
<li>update the wallpaper</li>
<li>fixuifiles</li>
<li>animate size grip colouring</li>
<li>add Messages.sh for the breeze decoration</li>
<li>hide 'hide titlebar' widget since the corresponding option is not implemented</li>
<li>recalculate borders on shade change, in order to have 0 bottom border.</li>
<li>fixed titlebar area</li>
<li>never return border size smaller than the minimum value needed for comfortable resize</li>
<li>Correct wrong/moved subdir (windec → kdecoration).</li>
<li>cleanup directory structure removed old breeze decoration svg</li>
<li>fixed margins</li>
<li>add a sync more in the cfg by desperation</li>
<li>one sync less, more debug</li>
<li>implemented application menu button</li>
<li>implemented context help button</li>
<li>use QX11Info::isPlatformX11</li>
<li>use member ui rather than inheritance</li>
<li>fixed includes order</li>
<li>hide 'detect window' button on non X11 platforms</li>
<li>removed breezehelper use QX11Info::isPlatformX11 instead of Helper::isX11</li>
<li>removed 'VerySmall' button size</li>
<li>[windec] Add guards around X11 on Breeze::DetectWidget</li>
<li>Made settings provider a QObject properly re-read when settings are reconfigured</li>
<li>removed unused mask bit</li>
<li>added some auto</li>
<li>Properly implement exception border size</li>
<li>Added an internalSettings provider class to handle exceptions</li>
<li>Added all the code relevant to handle exceptions</li>
<li>removed unused CMakelists</li>
<li>Moved shared pointer typedef to breeze for larger use Use shared pointer for handling InternalSettings inside decoration</li>
<li>removed unused enumeration</li>
<li>separate config sources and plugin sources for readibility and because it might move to own library in the future</li>
<li>implemented drawSizeGrip and drawBorderOnMaximizedWindows options</li>
<li>use in-class initialization</li>
<li>fancy in-class initialization :)</li>
<li>added drawSizeGrip and drawBorderOnMaximizedWindows options</li>
<li>cleanup and fixed library loading</li>
<li>added breeze-settings5 standalone configuration app</li>
<li>added optional dependency upon kservice, needed for building breeze-settings</li>
<li>also initialize animation and opacity for standalone buttons CCMAIL: mgraesslin@kde.org</li>
<li>added copyright</li>
<li>added units</li>
<li>cleanup shadow rendering</li>
<li>Implement size grip for borderless windows - implemented resize using xcb - use xcb to move sizeGrip on resize because Qt move seems broken after widget is reparented - use xcb to get sizegrip' root coordinate, because Qt MapTo methods are broken after widget is reparented - protect all XCB calls behind typedef and runtime check - disable sizeGrip if not in X11</li>
<li>moved all button rendering to breezebutton, without caching removed breezeimageprovider adjust painter window to have more spacing around button</li>
<li>use NegativeText color from kdeglobals for close button</li>
<li>fixed outline color fading</li>
<li>fixed padding between buttons and text</li>
<li>Moved configuration to the same library as the decoration plugin added kcmodule keyword removed unnecessary code</li>
<li>Fixed update rect on caption changed Fixed too tight bounding rectangles Fixed text alignment into bounding rect to avoid jitter</li>
<li>Improved rendering of buttons, in sync with breeze style</li>
<li>reordered methods improved rendering of shaded windows</li>
<li>fixed shadow rendering in the corner</li>
<li>implemented animations for buttons</li>
<li>moved colors to public to allow access from button</li>
<li>added active state change animation</li>
<li>Added configuration widget Not (yet) loaded inside kwin decoration kcm</li>
<li>proper text alignment</li>
<li>added internal settings adjusted button sizes</li>
<li>simplified shadow rendering</li>
<li>adjusted button sizes</li>
<li>increased decoration margins increased button spacing introduced button size</li>
<li>formatting</li>
<li>added button sizes</li>
<li>renamed breezedeco to breezedecoration to be in par with class name fixed vertical positionning of buttons</li>
<li>renamed file consistently to class</li>
<li>moved image provider to own class</li>
<li>removed comment</li>
<li>removed 'grey' spacing between active window outline and window body reduced top size accordingly.</li>
<li>moved color settings class to own file some code formatting</li>
<li>added metrics dedicated file</li>
<li>fix readConfigValue</li>
<li>default to Breeze</li>
<li>Fixed color role for accelerator</li>
<li>added inputWidgetEngine, for abstract item views, line editors, spinboxes and comboboxes it is needed to avoid animation conflicts between lists and buttons (when, e.g. there is a checkbox inside a list)</li>
<li>removed Q3ListView support</li>
<li>[kdecoration] Share the DecorationShadow with all Decorations</li>
<li>add LGPL 3 for breeze icons with clairification as in Oxygen</li>
<li>Drop Aurorae based window decoration</li>
<li>Manually calculate viewport update rect on header hover change BUG: 340343</li>
<li>Fixed KDE4 compilation CCBUG: 341006</li>
<li>Remove mnemonic underlines when alt-tabbing</li>
<li>[kdecoration] DecorationShadow is a QSharedPointer</li>
<li>Decoration::client returns a QWeakPointer</li>
<li>[kdecoration] DecorationButton changed to use real for position and size</li>
<li>[kdecoration] paint takes QRect instead of QRegion</li>
<li>konversation possible icon</li>
<li>Removed unused members CCMAIL: staniek@kde.org</li>
<li>Fixed uninitialized member _value CCMAIL: staniek@kde.org</li>
<li>fixuifiles</li>
<li>[kdecoration] DecorationShadow uses QRect to describe element sizes</li>
<li>[kdecoration] DecorationShadow uses QMargins for padding</li>
<li>[kdecoration] Rename DecoratedClient::borderingScreenEdges to adjacentScreenEdges</li>
<li>[kdecoration] Decoration::titleRect renamed to ::titleBar</li>
<li>[kdecoration] ExtendedBorders renamed to ResizeOnlyBorders</li>
<li>[kdecoration] Decoration uses QMargins for borders</li>
<li>[kdecoration] Paint methods take repaint region</li>
<li>add plasma version number</li>
<li>Add a Preview mode to the Decoration Button</li>
<li>ImageProvider uses Breeze::Button* instead of KDecoration2::DecorationButton*</li>
<li>[kdecoration2] Pass QVariantList argument to parent Decoration</li>
<li>[kdecoration2] DecorationSettings is no longer a singleton</li>
<li>Do not takeout margins from toolbutton before rendering text, unless they have inline indicator, because some custom widgets do not respect sizeFromContents BUG: 340341</li>
<li>add rating-unrated icon</li>
<li>Add gtkbreeze kconf_update tool to set GTK themes</li>
<li>add message about orion runtime dep to cmake output</li>
<li>nicer spacing</li>
<li>add gtk3 css file to remove window shadows to tidy up client side decorations</li>
<li>don't use a QCoreApplication</li>
<li>use QStringLiteral, set font to Oxygen, return 1 on error</li>
<li>no need for close</li>
<li>use qstringliteral</li>
<li>use QStringLiteral</li>
<li>error handling on failure to open file</li>
<li>tidy spacing</li>
<li>use QStringLiteral</li>
<li>fast string operators</li>
<li>use QDir::separator and tidy spacing</li>
<li>further simplify cmake</li>
<li>simplify cmake file</li>
<li>tidy up file header</li>
<li>refactor to reduce duplicated code</li>
<li>complete gtk3</li>
<li>um yeah, something</li>
<li>prepare gtk 3</li>
<li>check for gtk 2 theme and set it</li>
<li>start gtkbreeze to set breeze settings fot qt</li>
<li>- cleanup tab buttons positioning - center icon in tabs when there is no text and no buttons BUG: 340257</li>
<li>Remove all links to XCB and QX11Extras on Mac CCMAIL: mk-lists@email.de</li>
<li>Added option to draw either thin or strong focus for menu and menubar items</li>
<li>Disable frame for side panels by default</li>
<li>Fixed compilation with gcc 4.8.2 by adding relevant .data() calls</li>
<li>also fix corner rects in center-tabs mode</li>
<li>properly handle RTL for corner rects and tabbar BUG: 340155</li>
<li>Do not clip away vertical margins for headers to accomodate custom widgets that set a two small vertical size for those. CCBUG: 340150</li>
<li>Fix finding of KF5</li>
<li>use unaltered Window background color for sidepanels</li>
<li>take out one pixel when rendering menubar item rect for consistency with menu item rect</li>
<li>cleanup optional package properties</li>
<li>made dependency on KF5::FrameworkIntegration optional</li>
<li>removed dependency upon KF5::Completion</li>
<li>KComboBox -> QComboBox</li>
<li>Fix positioning of cornerwidgets</li>
<li>Properly account for widget size when determining left/right corner rects CCMAIL: staniek@kde.org</li>
<li>Fixed arrow color in sunken, non-autoraised toolbuttons</li>
<li>rename Breeze-Dark.colors to BreezeDark.colors BUG:339849</li>
<li>install translations if they exist</li>
<li>lighter hover color</li>
<li>Fixed foreground color in non autoraised toolbuttons (specifically the 'sunken' without 'hover' case)</li>
<li>add more folder icons from upstream</li>
<li>uint32_t -> quint32</li>
<li>cleanup comments</li>
<li>compress breeze-dark svgs</li>
<li>updated mimetypes and a couple app icons from upstream</li>
<li>A touch better contrast for inactive window title text.</li>
<li>(re) Fixed foreground color in toolbuttons and named separators</li>
<li>Fix dark color scheme titlebar colors.</li>
<li>Add version of icon theme that works better with dark color schemes.</li>
<li>Install Breeze Dark color scheme.</li>
<li>Updated to Breeze color scheme based on full palette. New Breeze-Dark color scheme.</li>
<li>Remove KDE4 home directory fallback</li>
<li>renderButton -> renderDecorationButton</li>
<li>do not remove pushbutton margins from contentsRect BUG: 339508</li>
<li>improved scrollbar extended hit area properly deal with KTextEditor scrollbars CCBUG: 339413</li>
<li>added 'strong focus' option for menu and menubar items</li>
<li>Fixed tabbar base line extend in document mode</li>
<li>Reduced margins of line editors so that the total height matches buttons CCMAIL: jamboarder@gmail.com</li>
<li>Fixed non autoraised toolbutton text color when sunken or focused</li>
<li>Added sizeHint and minimumSizeHint to the information printed when widgetExplorerEnabled is set to true. CCBUG: 339508</li>
<li>properly handle RTL when expanding scrollbar hit area</li>
<li>propagate mouse events from scrollarea to scrollbar if matching to respect Fitt's law BUG: 339413</li>
<li>Changed flat toggled button background button to grey, on request from Andrew</li>
<li>Fixed background color for scrollarea corner widget</li>
<li>reduced checkbox and radio button size use 1pixel for outline and 2pixel for radius CCMAIL: jamboarder@gmail.com</li>
<li>update radiobutton and checkbox to use 1px outline instead of heavy 2 px outline.</li>
<li>Update confusing comment</li>
<li>Revert "Remove check for hasWidgetStyle"</li>
<li>Remove check for hasWidgetStyle</li>
<li>Create kde4 config if it does not exist</li>
<li>fixed disabled palette</li>
<li>Also disable pressed animations on option change BUG: 339506</li>
<li>added SidePanelForeground color cleanup palette alteration code</li>
<li>Added hasParent method Do not add ItemDelegate to combobox views if inside QWebView, because ItemDelegate handling is broken in Qt for such widgets BUG: 339466</li>
<li>Make constants const, saves a few symbols in the .data section.</li>
<li>restored topLevel layout margins removed call to genericLayoutItemRect This effectively revert commits 739b18f4b370f392c72478837bd3b6cc632c8246 and 3618ba5ed5c77cee8b25074c24ae73e2451bd23f</li>
<li>force kpagewidget margins to zero</li>
<li>set TopLevel layout margin to zero</li>
<li>implemented PushButton LayoutItemRect</li>
<li>removed unused parameter names from function declaration</li>
<li>Hide borders which border a screen edge</li>
<li>Add extended borders for no or no-side borders</li>
<li>Use proper color for caption</li>
<li>Improve rendering of the rounded borders</li>
<li>Use gridUnit based layouting of all elements</li>
<li>Add a DecorationShadow</li>
<li>Add support for border sizes</li>
<li>Introduce an "org.kde.kdecoration2" metadata section in JSON file</li>
<li>Set window decoration to opaque for maximized windows</li>
<li>Improve defining the border settings</li>
<li>Pass creation of DecorationButtons to DecorationButtonGroup</li>
<li>Initial import of new KDecoration2 based window decoration</li>
</ul>
<h3> KHotKeys </h3><ul>
<li>kDebug -> qDebug</li>
<li>Remove unused deprecated signal</li>
<li>Explicitly link KService</li>
<li>Tidy includes</li>
<li>Don't generate interface twice</li>
<li>Add test standalone app for easier debugging</li>
<li>kdelibs4support--</li>
<li>Prefer const char[] over const char* const where possible.</li>
</ul>
<h3> libksysguard </h3><ul>
<li>Fix build with Qt 5.5/dev branch in release mode</li>
<li>Add set_package_properties for webkitwidgets, and don't set QT5 to NOTFOUND</li>
<li>Fix linking, Qt5::Network is used.</li>
<li>Don't mess with RTL locales by elliding manually.</li>
<li>'<numid>...</numid>' isn't supported anymore.</li>
<li>relicence from GPL 2 or 3 to GPL 2 or 3 or later as approved by KDE e.V.  Approved by e-mail with John and Chris</li>
<li>add missing GPL-2</li>
<li>Remove kdelibs4support</li>
<li>Fix includes</li>
<li>Remove not necessary include moc</li>
<li>Convert to Categorized Logging</li>
<li>Search for all public deps in CMake config file</li>
<li>Get rid of KDELibs4Support</li>
<li>Don't use deprecated variable</li>
<li>Port KLineEdit to QLineEdit in ProcessWidgetUI</li>
</ul>
<h3>KInfoCenter </h3><ul>
<li>properly set the icon name</li>
<li>aboutData: fix broken icon</li>
<li>Unbreak starting KInfoCenter via KRun</li>
<li>add LGPL 2.1</li>
<li>Remove kdelibs4support</li>
<li>Fix includes</li>
<li>kdebug--</li>
<li>Port to QStandardPaths.</li>
<li>Use qapplication.h</li>
<li>Clean up</li>
<li>Fix show aboutdata. We need to create app after kaboutdata</li>
<li>Port to new connect api</li>
<li>Fix includes</li>
<li>Remove deprecated function</li>
<li>Port to QIcon</li>
<li>Fix forward declaration</li>
<li>reverse dns desktop</li>
<li>Port to new connect api</li>
</ul>
<h3> libkscreen </h3><ul>
<li>Make sure to XFlush after XUngrabServer</li>
<li>ConfigMonitor: don't request config when initial backend is received</li>
<li>set version number to 5.1.95 for 5.2 beta</li>
<li>ConfigSerializer: No need for explicit conversion to/from double</li>
<li>Config::canBeApplied: more descriptive debug messages</li>
<li>correct the order of params on QDBusServiceWatcher::serviceOwnerChanged</li>
<li>Avoid risk of two backends loading at the same time breaking things</li>
<li>Add missing include of signal.h</li>
<li>Remove CMake modules that are now part of ECM.</li>
<li>Specify required XCB components.</li>
<li>Delete d pointer of ConfigMonitor</li>
<li>Initialise ConfigMonitor singleton instance</li>
<li>Bump soname</li>
<li>Emit the new current config instead from setConfig</li>
<li>XRandR: update internal config immediatelly after applaying changes</li>
<li>XRandR: Don't leak XRRScreenResources when applying output changes</li>
<li>Output: fix apply() not resetting EDID</li>
<li>Clean up redundant qDebugs, convert to qCDebug</li>
<li>Fix typo in variable name</li>
<li>XRandR: reset output 'Enabled' property when disconnected</li>
<li>Output: don't overwrite EDID when not necessary</li>
<li>Fix logic when disabling blocked signals in Output</li>
<li>Config monitor: request EDID for connected outputs</li>
<li>Ungrab all the times, if not we break all the things :/</li>
<li>Grab/Ungrab the server when applying new settings</li>
<li>Things will not be flushed by Qt anymore (it uses xcb)</li>
<li>Better stirng for output statement</li>
<li>GetConfigOperation: don't get stuck on EDID retrieval when there are no outputs</li>
<li>Fake backend: always set screen, even when outputs are empty</li>
<li>Config serializer: fix crash when (de)serializing an incomplete Config</li>
<li>Ensure we don't crash when debugging.</li>
<li>Install backendmanager_p.h</li>
<li>XRandR: don't call XRandRScreen::currentSize() repeatadly</li>
<li>XRandR: don't cache invalid/empty EDID</li>
<li>XRandR: some more debug messageS</li>
<li>XRandR: only call setPrimaryOutput after disabling/changing/enabling all outputs</li>
<li>Output::apply(): delay change signals emission until all changes are done</li>
<li>BackendLauncher: emit configChanged after calling setConfig()</li>
<li>BackendLauncher: reduce the signal emission delay to 200ms</li>
<li>Implement TestConfigMonitor</li>
<li>Autotests: shutdown the backend in cleanupTestCase()</li>
<li>Fake: properly implement the FakeBackend DBus interface</li>
<li>BackendManager: further improve the shutdown and startup mechanism</li>
<li>Make TestScreenConfig pass again</li>
<li>Add autotest for ConfigSerializer</li>
<li>BackendManager: add shutdownBackend() method to terminate the backend launcher</li>
<li>Fix return value of ConfigOperation::exec()</li>
<li>Fake backend: add simple DBus API to allow tests simulating changes in config</li>
<li>Add missing config file</li>
<li>BackendManager: reset mLauncher after calling deleteLater()</li>
<li>BackendManager: reset crash counter after a minute</li>
<li>Fix settings primary output in Config::apply()</li>
<li>BackendManager: wait for service to appear on the bus</li>
<li>Install headers for the new public classes</li>
<li>Make Config::canBeApplied() work again</li>
<li>Fetch EDID information as part of GetConfigOperation</li>
<li>Output: fix retrieving EDID</li>
<li>Fix demarshalling of the incomming QVariantMap</li>
<li>Port autotests to the new async API</li>
<li>Port the printconfig test utility to the new async API</li>
<li>BackendLauncher: make it a QGuiApplication</li>
<li>BackendLauncher: fix exporting of the BackendDBusWrapper to DBus</li>
<li>Introduce new asynchronous API to get and set KScreen Config</li>
<li>Backends: fix build</li>
<li>Introduce out-of-process KScreen backends</li>
<li>Fix includes now that AbstractBackend is part of LibKScreen</li>
<li>Adapt KScreen::Output and KScreen::Edid to the changes in EDID delivery</li>
<li>Port all backends to the new AbstractBackend API</li>
<li>AbstractBackend: change API and move to libKF5KScreen</li>
<li>.gitignore</li>
<li>Const'ify</li>
<li>Fix ownership issues when cloning Config and Outputs</li>
<li>Make AbstractBackend::setConfig() a non-const method</li>
<li>Remove unused MOC includes</li>
<li>Port tests to QSharedPointer API</li>
<li>Port autotests to QSharedPointer API</li>
<li>Port XRandR1.1 backend to QSharedPointer API</li>
<li>Port XRandR backend to QSharedPointer API</li>
<li>Port QScreen backend to QSharedPointer API</li>
<li>Port Fake backend to QSharedPointer API</li>
<li>Wrap Config, Screen, Output and Mode classes to QSharedPointer</li>
</ul>
<h3> KFileMetadata </h3><ul>
<li>Add missing QIODevice include</li>
<li>baloo wants version numbers to match frameworks</li>
<li>Fix compilation, taglib headers were not found.</li>
<li>fix build/test by making translation properties testable</li>
<li>Fix OSX compile failure</li>
<li>Exiv2Extractor: Convert the latitude/longitude into doubles correctly</li>
<li>fix counting</li>
<li>port po strigi analyzer into KF5::KFileMetaData</li>
<li>ExtractionResult: Auto detect the mimetype if not present</li>
<li>Add PhotoGps Latitude/Longitude/Altitude properties</li>
</ul>
<h3> Powerdevil </h3><ul>
<li>Do not listen for Login1 Lock signal anymore</li>
<li>Do not start the fade effect if we are inhibit</li>
<li>Fix KScreen helper effect availability check</li>
<li>Fade screen before turning it off</li>
<li>No need to pass a parent, it's in a QScopedPointer anyway</li>
<li>Remove lock screen on suspend option and fade screen before suspending</li>
<li>Add KScreen fade effect connector for PowerDevil</li>
<li>Cleanup BrightnessOSDWidget</li>
<li>Fix warning about initialization order</li>
<li>Get rid of profile migrator</li>
<li>Wait for notification service before emitting them</li>
<li>Expose over DBus whether a lid is present</li>
<li>Add setBrightnessSilent calls that don't show the OSD</li>
<li>Turn off keyboard backlight when DPMS turns off the screen</li>
<li>Turn off keyboard brightness when screen gets dimmed</li>
<li>Add aboutToSuspend signal</li>
<li>Remove systemd version checks</li>
<li>Add a .reviewboardrc</li>
<li>Revert "Don't show OSD when setting brightness absolutely"</li>
<li>Revert "Emit signal when maximum brightness changes"</li>
<li>Emit signal when maximum brightness changes</li>
<li>Don't show OSD when brightness changes in the backend</li>
<li>Fix battery remaining time update with upower >= 0.99</li>
<li>Mark LibKWorkspace and ScreenSaverDBusInterface as REQUIRED</li>
<li>Port to QIcon</li>
<li>Strip PowerDevilCore, PowerDevilUI and PowerDevil's kded from kdelibs4support</li>
<li>Brightness fixes, part 2.</li>
</ul>
<h3> kde-cli-tools </h3><ul>
<li>Fix defines</li>
<li>kcmshell: return -1 if the wanted module was not found. REVIEW: 121181</li>
<li>Use QFile::decodeName for command in KDEsuDialog to fix encoding</li>
<li>port away from Q_WS_X11</li>
<li>add GPL and LGPL files</li>
<li>Port to QMimeType</li>
<li>set proper display name</li>
<li>don't use deprecated variabl</li>
</ul>
<h3> System Settings </h3><ul>
<li>Port to new connect api</li>
<li>Rename "Human Input Devices" to just "Input Devices"</li>
<li>restore KStandardGuiItems</li>
<li>Fix typos</li>
<li>Update systemsettings docbook to Plasma 5.2 REVIEW:120814</li>
<li>Use QStandardPath</li>
<li>Port away KHBox</li>
<li>Clean includes</li>
<li>Clean forward declaration</li>
<li>Fix warning about shortcut</li>
<li>USe ki18n_wrap_ui and remove not necessary include moc</li>
<li>Fix includes/Deprecated function</li>
</ul>
<h3> KWin </h3><ul>
<li>Overhaul of deco kcm</li>
<li>Handle case that XCURSOR_SIZE environment variable is not set</li>
<li>[kcmkwin/tabbox] Improve locating the services used in preview mode</li>
<li>[scene-xrender] Trigger a full decoration repaint when sizes are dirty</li>
<li>[scene-xrender] Implement support for DecorationShadow</li>
<li>Add <array> include where needed, to make it build on FreeBSD.</li>
<li>Improve reading the color scheme in Client</li>
<li>Update cursor theme when it changes at runtime</li>
<li>First try getting cursor theme from environment variable</li>
<li>[effects/startupfeedback] Use dedicated shader for bouncing cursor</li>
<li>Fix QTranslator installation for Qt translations</li>
<li>[kdecoration] Try falling back to default plugin and Aurorae</li>
<li>[glxbackend] Introduce env variable KWIN_USE_INTEL_SWAP_EVENT</li>
<li>[decoration] Use client's depth in X11Renderer for put_image</li>
<li>[glxbackend] Add default values for member variables</li>
<li>Increase so-version for kwineffect libraries</li>
<li>glx: Fix the swap event filter for DRI3 drivers</li>
<li>Notify when the client palette changes and proxy this to kdecoration</li>
<li>Recursion check while decoration updates the borders</li>
<li>Use layerRepaint with visibleRect from Client::map</li>
<li>[effects/blur] Disable cached blur for deleted windows</li>
<li>shortcut rendering textures for empty an rect</li>
<li>determine screenArea by geom_restore in checkWSP</li>
<li>Sync GetAddrInfo thread cancel in destructor</li>
<li>updateXTime for kde_net_wm_user_creation_time</li>
<li>Use swap events to drive repaints</li>
<li>Also send a QHoverEvent to the decoration when MotionNotify event occurs in input window, in order to get the right cursor shape and resize mode from extended borders REVIEW: 121556</li>
<li>Add OnScreenDisplayLayer which is placed even ontop of the active fullscreen window</li>
<li>Store shadows in GL_R8 textures when possible</li>
<li>Avoid converting images in GLTexture when possible</li>
<li>Add GLTexture::setSwizzle()</li>
<li>Add an internalFormat parameter to the GLTexture ctor</li>
<li>extract the strings also from QML</li>
<li>[aurorae] Send MouseButtonDblClicked to the QQuickWindow</li>
<li>[aurorae] Use mousePressAndHoldInterval from StyleHints</li>
<li>Add support for GL_ARB_texture_storage</li>
<li>Add a levels parameter to the GLTexture ctor</li>
<li>Make mipmap filters other than GL_LINEAR_MIPMAP_LINEAR work</li>
<li>Don't generate mipmaps in GLTexture::bind()</li>
<li>Fix cross-fading</li>
<li>[aurorae] Do not update shadow from ::paint</li>
<li>Fixed data type for _KDE_NET_WM_BLUR_BEHIND_REGION REVIEW: 121459</li>
<li>fixuifiles</li>
<li>glx: Port GlxBackend::initBuffer() to xcb</li>
<li>glx: Improve the fbconfig debug output</li>
<li>glx: Do additional sorting of fbconfigs</li>
<li>glx: Don't allocate storage for mipmaps in TFP textures</li>
<li>[kcmkwin/deco] Drop outdated source and ui files</li>
<li>[kcmkwin/deco] Bring back a decoration buttons configuration interface</li>
<li>Add ContextHelp button to default right buttons</li>
<li>Improving printing of supportInformation</li>
<li>Declare metatype for KWin::OpenGLPlatformInterface</li>
<li>Add supportInformation for new Decorations</li>
<li>Register metatype Options::WindowOperation</li>
<li>Fix build due to undefined reference</li>
<li>Fix warnings in kwin/effects/glide/glide_config.ui</li>
<li>[aurorae] Fix BorderSize for SVG based themes</li>
<li>[aurorae] Fix BorderSize in Plastik</li>
<li>[aurorae] Fix setting closeOnDoubleClickOnMenuButton</li>
<li>[kcmkwin/deco] Reconfigure deco after applying changes</li>
<li>[aurorae] Fix loading of theme's config values</li>
<li>Introduce categorized logging for kwin core</li>
<li>[kcmkwin/deco] Add configuration for decoration plugin/themes</li>
<li>[kcmkwin/deco] Bring back KNewStuff support</li>
<li>Test the generated shaders on startup</li>
<li>Change the default shader in SceneOpenGL2</li>
<li>cube: Always set the shader in WindowPaintData</li>
<li>cube: Remove some checks that are always true</li>
<li>screenshot: Use the new matrix API</li>
<li>Add a model-view and a projection matrix in WindowPaintData</li>
<li>Use the new ShaderManager API in SceneOpenGL2</li>
<li>Add a ModelViewProjectionMatrix uniform</li>
<li>Add support for generating shaders at runtime</li>
<li>[kcmkwin/deco] Fix defaults for border size and close window double click</li>
<li>[kcmkwin/deco] Only specify QStringLiteral("Normal") once</li>
<li>[kcmkwin/deco] Add a filter and a QSortFilterProxyModel</li>
<li>Bring back configuration options for Decorations</li>
<li>Introduce a texture cache for DecorationShadows in SceneOpenGLShadow</li>
<li>Grow the persistently mapped VBO as needed</li>
<li>Add support for GL_ARB_buffer_storage</li>
<li>[aurorae] Use QQuickRenderControl if compiled against Qt 5.4</li>
<li>Do not emit DecorationSettings::alphaChannelSupportedChanged on tear down</li>
<li>[aurorae] Better specify y value of titleBar</li>
<li>Delay decoration repaint to end of event cycle on create</li>
<li>Delete libkdecorations</li>
<li>Remove unused imports</li>
<li>[kcmkwin/kwindecoration] Exclude buttons.cpp and configdialog.cpp from build</li>
<li>Ensure that the X11Renderer doesn't render after reparente to Deleted</li>
<li>Drop dependency on kdecoration.h</li>
<li>Move MaximizeMode from KDecorationDefines to utils.h</li>
<li>Drop kdecoration.h include from placement.cpp</li>
<li>Drop kdecoration.h include from tabgroup.h</li>
<li>Toplevel does not inherit from KDecorationDefines any more</li>
<li>Drop kdecoration.h include from options.h</li>
<li>Workspace does not need to inherit from KDecorationDefines</li>
<li>Move Position from KDecorationDefines to Client</li>
<li>Move QuickTileMode from KDecorationDefines to Client</li>
<li>Move WindowOperation enum to Options</li>
<li>Do not use KDecorationDefines in Aurorae</li>
<li>Do not include kdecoration.h in plastik decoration plugin</li>
<li>Stop building libkdecorations</li>
<li>Options no longer inherits from KDecorationOptions</li>
<li>Move maximize button click config to Options</li>
<li>DecorationSettings provides QVector for decorationButtons</li>
<li>[autotests] Give Xephyr more time to startup</li>
<li>[kdecoration2] Decoration::shadow returns QSharedPointer</li>
<li>[kdecoration] Decoration::client returns a QWeakPointer</li>
<li>[kdecoration2] Detect double clicks on the decoration title bar</li>
<li>[kdecoration2] Decoration does not provide titleBarWheelEvent any more</li>
<li>DecorationButtonType::QuickHelp -> DecorationButtonType::ContextHelp</li>
<li>[kdecoration] ::paint uses QRect instead of QRegion</li>
<li>don't access array to check whether ptr is nullptr</li>
<li>step into the focus chain at the beginning</li>
<li>don't restore unrestorable window sizes on Q'unmax</li>
<li>ensure disjunct screenedges on overlapping screens</li>
<li>Cleanup: reduce usage of QDesktopWidget</li>
<li>Remove displayWidth and displayHeight from the compositor</li>
<li>Use screens()->geometry() in Workspace::desktopResized</li>
<li>[screens] Replace DesktopWidgetScreens by XRandRScreens</li>
<li>Do not include utils.h in xcbutils.h</li>
<li>Remove GLTexturePrivate::bind()/unbind()</li>
<li>Mark SceneOpenGL::Texture::discard() as override final</li>
<li>De-virtualize SceneOpenGL::Texture::load()</li>
<li>Inline GLTexture::load(QImage) in the constructor</li>
<li>Remove GLTexture::load(QPixmap) and load(QString)</li>
<li>Remove SceneOpenGL::createTexture(QPixmap)</li>
<li>Make the EffectFrame textures regular GLTextures</li>
<li>Remove GLTexturePrivate::convertToGLFormat()</li>
<li>Remove most calls to checkGLError()</li>
<li>Add support for KHR_debug</li>
<li>Verify Workspace pointer in X11EventFilter::~X11EventFilter</li>
<li>Drop build option KWIN_BUILD_SCREENEDGES</li>
<li>Fix build without KF5Activities</li>
<li>[kcmkwin/tabbox] Find all layouts in lnf package</li>
<li>Use Protocol support in NETWinInfo instead of fetching them ourselves</li>
<li>Use consistent names for the static members in GLTexturePrivate</li>
<li>Make s_supportsUnpack a static member of GLTexturePrivate</li>
<li>Remove the NPOT texture support checks</li>
<li>Remove the saturation support checks</li>
<li>Remove the #ifdefs for GLES in GLTexture</li>
<li>fixuifiles</li>
<li>DecorationShadow elements are QRects</li>
<li>DecorationShadow uses QRect to set element sizes</li>
<li>DecorationShadow uses QMargins for padding</li>
<li>Rename DecoratedClient::borderingScreenEdges to adjacentScreenEdges</li>
<li>[aurorae] s/requestMaximize/requestToggleMaximization/g</li>
<li>Decoration::windowFrameSection renamed to ::sectionUnderMouse</li>
<li>[aurorae] Decoration::titleRect renamed to ::titleBar</li>
<li>Decoration::extendedBorders renamed to ::resizeOnlyBorders</li>
<li>[aurorae] Decoration sets borders using QMargins</li>
<li>s/requestMaximize/requestToggleMaximization/g</li>
<li>Decoration::paint takes the repaint region</li>
<li>[kwin_wayland] Fix printing out the X11 display</li>
<li>[kwin_wayland] Do not require --display argument for starting X server</li>
<li>Fix --x-server argument of kwin_wayland</li>
<li>added missing .data() to make connect/disconnect compile</li>
<li>Add libepoxy's include directory</li>
<li>[kwindecoration] Add a PreviewButtonItem to the declarative plugin</li>
<li>Desktop switchers moved to kdeplasma-addons</li>
<li>[tabbox/qml] Default desktop switcher moved to look and feel package</li>
<li>[tabbox] Get DesktopSwitcher from look and feel package</li>
<li>[tabbox] Window Switcher layouts moved to kdeplasma-addons</li>
<li>[kcmkwin/kwindecoration] Import a new decoration configuration module</li>
<li>[kdecoration] Adjust to change that DecorationBridge is no longer a singleton</li>
<li>[kdecoration2] DecorationSettings takes DecorationBridge in ctor</li>
<li>[kdecoration2] Adjust to DecortionSettings no longer being a singleton</li>
<li>[tabbox] Sidebar window switcher got moved to plasma-workspace</li>
<li>[tabbox] Try locating the WindowSwitcher QML through configured lnf package</li>
<li>[decorations] Add support for runtime switching of decoration plugins</li>
<li>[shadow] Needs to keep a QPointer to the DecorationShadow</li>
<li>[aurorae] Add a ThemeFinder</li>
<li>[aurorae] Set drawBackground property to false on visual parent</li>
<li>[aurorae] Add a dummy Shadow for the configuration mode</li>
<li>[aurorae] Support DecorationShadow</li>
<li>[aurorae] QQuickWindow needs to be a QPointer instead of a ScopedPointer</li>
<li>Add libepoxy's include directory</li>
<li>Initial port of Aurorae to KDecoration2</li>
<li>Support themes in decoration loading</li>
<li>A mouse release event doesn't have the triggered button in Qt::MouseButtons</li>
<li>Call ::init() on created KDecoration2::Decoration</li>
<li>Adjust DecoratedClientImpl to changes in DecoratedClientPrivate</li>
<li>Improve handling of KWIN_HAVE_EGL in CMakeLists.txt</li>
<li>Do not find OpenGL or OpenGLES</li>
<li>Drop usage of DecoratedClient::handle in DecorationBridge::update</li>
<li>Keep a QPointer<DecoratedClientImpl> in Client</li>
<li>KDecoration2::DecorationBridge uses std::unique_ptr</li>
<li>Fix build if there is no libinput</li>
<li>Add sanity checks before updating pointer position and on screen changes</li>
<li>Center initial pointer position on screen in libinput mode</li>
<li>Set a default cursor when creating the WaylandCursor</li>
<li>Connect pointer events from libinput</li>
<li>Do not create a WaylandSeat if we use libinput</li>
<li>Add command line option to enable libinput support</li>
<li>Suspend/Resume libinput when logind session Active changes</li>
<li>Use Logind to open/close devices needed by libinput</li>
<li>Initial support for libinput</li>
<li>[effects] Add type property to StartupFeedback Effect</li>
<li>Drop check whether WAYLAND_DISPLAY env is set</li>
<li>Support setting cursor image as a SubSurface</li>
<li>Move X11CursorTracker from WaylandSeat to WaylandBackend</li>
<li>X11CursorTracker emits signal when a new cursor image needs to be installed</li>
<li>Use Registry::interfacesAnnounced signal for WaylandBackend::createSurface</li>
<li>Add an env var KWIN_DBUS_SERVICE_SUFFIX to set a different dbus service name</li>
<li>Remove excutable attribute</li>
<li>Context menu: Add "Keep Above Others" icon opposite of "Keep Below Others" REVIEW:120465</li>
<li>Make constants const, saves a few symbols in the .data section.</li>
<li>Add test case for ScreenEdge::check</li>
<li>Test case for Client activation on screen edge</li>
<li>Add an auto-test for ScreenEdges</li>
<li>Override ::reparent in X11Renderer</li>
<li>Emit signals from Client when allowed actions change</li>
<li>Drop size from Client::resizeDecoration</li>
<li>Drop CoordinateMode from Client::layoutDecorationRects</li>
<li>Implement borderingScreenEdges for the DecoratedClientImpl</li>
<li>Emit a signal if the QuickTileMode changes</li>
<li>React on border size changes of the Decoration</li>
<li>Bring back extended borders</li>
<li>Provide isCloseOnDoubleClickOnMenu in DecorationSettingsImpl</li>
<li>Add support for WheelEvents on Decoration</li>
<li>Destroy DecorationRenderer in setup/finish compositing</li>
<li>Handle double click on Decoration title bar</li>
<li>Allow Client to disable the rendering of DecorationShadow</li>
<li>Introduce support for DecorationShadow</li>
<li>Add border size to the implementation of DecorationSettings</li>
<li>Pass Decoration::Renderer to Deleted and use it in the Scenes</li>
<li>Bring back CloseButtonCorner in EffectsHandlerImpl</li>
<li>Load settings for KDecoration2 based plugins</li>
<li>Load decoration plugin settings from meta data</li>
<li>Port Client::decorationHasAlpha</li>
<li>Initial import of support for new KDecoration2 based decorations</li>
</ul>
<h3> KSysGuard </h3><ul>
<li>relicence from GPL 2 or 3 to GPL 2 or 3 or later as approved by KDE e.V.  Approved by e-mail with John and Chris</li>
<li>Fix includes</li>
<li>kdelibs4support</li>
<li>KMenu->QMenu</li>
<li>Reverse dns desktop</li>
<li>Fix includes</li>
<li>Fix forward declaration</li>
<li>Remove kde4suppport</li>
<li>Remove not necessary includemoc</li>
<li>Fix macro</li>
<li>Convert to new connect api</li>
<li>Don't use depreacated variable</li>
</ul>
<h3> Baloo </h3><ul>
<li>baloo wants version numbers to match frameworks</li>
<li>Remove unused includes</li>
<li>Rename autotest to autotests</li>
<li>Extractor: Never index text files > 10 mb</li>
<li>Extractor: Avoid duplicating code for debugging io</li>
<li>IndexScheduler: Handle case that case never happen because of gcc</li>
<li>timeline: Explicitly handle NoFolder case</li>
<li>kio_tags: Return after switch case</li>
<li>FileIndexingQueue: Print error</li>
<li>raiselimit: Avoid adding a using namespace in a header</li>
<li>XapianSearchCode: Remove unused return</li>
<li>Term: Assert on Auto case</li>
<li>Solve some of the -WShadow warnings</li>
<li>IndexScheduler: No need for a default case when all the cases are covered</li>
<li>Warning--</li>
<li>Remove -DQT_NO_URL_CAST_FROM_STRING</li>
<li>Move the natural query parser to baloo-widgets</li>
<li>kinotify: stropts.h isn't required</li>
<li>kinotify: Remove internal buffer</li>
<li>Cleaner: Handle SQL errors</li>
<li>Always initialize all member variables</li>
<li>Extractor: Always initialize all member variables</li>
<li>Extractor Result: Convert multilist into list with arrays</li>
<li>Balooshow: Handle lists</li>
<li>BasicIndexingJob: micro optimization</li>
<li>inotify test: Add the same flags as the FileWatcher</li>
<li>kinotify: Add watches in batches of 1000</li>
<li>IndexerConfig: Write to the KDE4 configuration as well</li>
<li>Update Readme.md</li>
<li>Remove main autotests folder</li>
<li>KInotify: Use FilteredDirIterator</li>
<li>FilteredDirIterator Benchmark: Allow it to be a run on a custom dir</li>
<li>FilteredDirIterator: Work without a config file as well</li>
<li>FilteredDirIterator Benckmark: Print IO usage</li>
<li>FilteredDirIterator: Keep an internal list of QStrings not QDirIterators</li>
<li>FilteredDirIterator: Use QDirIterator's QFileInfo instead of contructing a new one</li>
<li>FilteredDirIterator: Optimize by around 2x</li>
<li>Add a FilteredDirIterator Benchmark</li>
<li>FilteredDirIterator: Rename the filters</li>
<li>Introduce a FilteredDirIterator class</li>
<li>BalooDataModel gets renamed to QueryResultsModel and replace the Query::setType, Query::type with the Query::setSearchString in order to make out model more flexible.</li>
<li>Autotests: Remove header files</li>
<li>PendingFileQueue Test: Have larger timers in the test</li>
<li>SearchStore: Remove searchstore lists</li>
<li>Query: Remove property based sorting</li>
<li>Add "type" and "kind" properties</li>
<li>baloosearch: Add help option</li>
<li>XapianDocument: Print out the error</li>
<li>File: Remove the UpdateRecursive flag</li>
<li>BalooSearch: resolve the directory name to its absolute path</li>
<li>Add scribus files as Documents</li>
<li>XapianDocumnet: Catch exceptions in value</li>
<li>BasicIndexingQueue: Use xapian slots to fetch the mtime</li>
<li>Result: Return the filePath instead of a url</li>
<li>File: Always use the canonical path</li>
<li>XapianDB: Output errors</li>
<li>XapianDatabase: Extra assert</li>
<li>Query: Add operator !=</li>
<li>FileIndexerConfig: Do not use QUrls</li>
<li>FileWatchTest: Remove random delays and use QSignalSpy::wait instead</li>
<li>Set dbus interface path first</li>
<li>Remove the KCM. It is now in plasma-desktop</li>
<li>Export the Baloo DBUS interfaces path</li>
<li>Rename baloo dbus name to org.kde.baloo</li>
<li>PendingFileQueue: Implement proper backing off</li>
<li>MetadataMover: Commit the transaction on file renames</li>
<li>Remove not necessary include moc</li>
<li>BasicIndexingJob: Only index the rating if > 0</li>
<li>Remove separate file private header</li>
<li>File: Rename from url to path</li>
<li>searchstore.h is no longer installed</li>
<li>BasicIndexingJob: Use KFileMetaData::UserMetaData</li>
<li>BalooSearch: Remove email search</li>
<li>IndexScheduler: Never fill the queue unless empty</li>
<li>SchedulerTest: Use newer connect syntax</li>
<li>Add option to search a specified directory only in baloosearch</li>
<li>Fix regression introduced by 29b8dda21857e30693282f16c0e5df12ac337098 concenating a QLatin1String and std::string</li>
<li>Fixed a minor issue with the previous commit</li>
<li>Added more tests for BasicIndexingJob</li>
<li>Don't exit the file metadata fetching loop when no file indexing information was found.</li>
<li>Extractor: bdata is no more</li>
<li>Port to new connect api</li>
<li>increase ecm version</li>
<li>Remove PIM Baloo code</li>
<li>Make data tables const, saves a few symbols in the .data section.</li>
<li>ResultIterator: More cleanup</li>
<li>Remove BALOO_LIBRARIES_ONLY build option</li>
<li>SearchStore: Cleanup more unused code</li>
<li>SearchStore: Remove overrideSearchStoress</li>
<li>Remove FileFetchJob</li>
<li>Result: Remove icon</li>
<li>Query: Replace "customOptions" with "includeFolder"</li>
<li>Query Result: Remove text option</li>
<li>Use the AQP in Query::setSearchString</li>
<li>Do not export the SearchStore</li>
<li>Rename BalooCore library to Baloo</li>
<li>KCM: "Desktop Search" --> "File Search"</li>
<li>Set @kde.org email in MAINTAINER file</li>
<li>Remove PIM Baloo code</li>
<li>Rename folder from core to lib</li>
<li>API: Remove tagging, rating and user comments</li>
</ul>
<h3> KWayland </h3><ul>
<li>[tests] Create Output before XWayland is started</li>
<li>[tests] Renderingservertest supports maximizing a window</li>
<li>Implement ShellSurface::setMaximized</li>
<li>Exclude some krazy checks which do not make sense for KWayland</li>
<li>Add missing newline at end of file</li>
<li>Fix typos</li>
<li>Normalize SIGNAL syntax</li>
<li>Mark constref return as TODO for next ABI break</li>
<li>Do not include QtModules</li>
<li>Implement entered surface for Keyboard</li>
<li>[autotests] Verify Client::Pointer::enteredSurface</li>
<li>Fix documentation of Pointer::left</li>
<li>Improve attaching buffer and commiting in SurfaceInterface</li>
<li>renderingservertest: Proper DPI for the Output</li>
<li>renderingservertest: Improve focus handling</li>
<li>Install KF5WaylandServer and headers</li>
<li>Display::start takes a StartMode argument</li>
<li>ConnectionThread can connect to a socket fd instead of name</li>
<li>Add safety checks to DataDeviceInterface</li>
<li>Fix crash in PointerInterface after PointerInterface got destroyed</li>
<li>Handle DataDeviceInterface selection in SeatInterface</li>
<li>pasteclient: read selection in thread</li>
<li>renderingservertest: each new Surface becomes the seat's focused surface</li>
<li>renderingservertest: Create a DataDeviceManagerInterface</li>
<li>Close fd DataSourceInterface::requestData</li>
<li>Split SeatInterface::Private into own header file</li>
<li>Cleanup SeatInterface after refactoring</li>
<li>KeyboardInterface inherits Resource</li>
<li>Refactor KeyboardInterface</li>
<li>Get event timestamp from SeatInterface in KeyboardInterface</li>
<li>Move pointer position into the Pointer struct in SeatInterface::Private</li>
<li>Make PointerInterface::focusedSurface public</li>
<li>Drop global position from PointerInterface</li>
<li>Move pointer button handling from PointerInterface to SeatInterface</li>
<li>Turn PointerInterface into a Resource</li>
<li>Create one PointerInterface for each pointer wl_resource</li>
<li>[server] Add a SeatInterface::focusedPointer</li>
<li>Move focused pointer surface API to SeatInterface</li>
<li>[server] Keep timestamp in SeatInterface instead of PointerInterface</li>
<li>[server] Move pointer position from PointerInterface to SeatInterface</li>
<li>[server] Drop Display* from PointerInterface and KeyboardInterface</li>
<li>[server] Move PointerInterface into own .h and .cpp</li>
<li>[server] Move KeyboardInterface into dedicated .h and .cpp</li>
<li>Use WaylandScanner find module from ECM</li>
<li>Include(ECMMarkAsTest) in tests/</li>
<li>Add a parentResource to Resource</li>
<li>Set physical size to something sensible -- 40x30 cm</li>
<li>Drop incorrect Q_UNUSED</li>
<li>Add createResource to ClientConnection</li>
<li>Add a flush to ClientConnection</li>
<li>Remove client credentials from ShellSurface</li>
<li>Resource holds a ClientConnection instead of a wl_client</li>
<li>Expose all ClientConnections in Display</li>
<li>Add test condition in test_display.cpp</li>
<li>Adding a new ClientConnection class for wrapping wl_client</li>
<li>Create wl_resource in Resource::Private instead of subclasses</li>
<li>Track all created Resources in Resource::Private</li>
<li>Move static unbind method from derived classes to Resource::Private</li>
<li>Move q-ptr to Resource::Private</li>
<li>Add cast from wl_resource to Private to Resource::Private</li>
<li>[server] Introduce a base class Resource</li>
<li>Global::Private can create the wl_global</li>
<li>Add a base class for all server interfaces of a wl_global</li>
<li>[server] Interface method returning wl_resource* is called ::resource</li>
<li>Fix shared memory access in BufferInterface</li>
<li>Adding a test application for paste</li>
<li>Adding a test application for copy</li>
<li>Adding a test application supporting output to a QWidget</li>
<li>Overloads taking Qt::MouseButton added to Server::SeatInterface</li>
<li>Remove destroy listener on BufferInterface in dtor</li>
<li>Fix check whether there is already a ShellSurface for a given Surface</li>
<li>Test server extended to start Xwayland server</li>
<li>Display can start the server before QCoreApplication is created</li>
<li>Add a simple testServer binary</li>
<li>Use a destroy listener in BufferInterface</li>
<li>Add wl_data_offer on Client and Server side</li>
<li>Fix signal DataDeviceManager::dataSourceCreated</li>
<li>Add implementation for wl_data_device in Server and Client</li>
<li>Add static SeatInterface *SeatInterface::get(wl_resource*)</li>
<li>Add static DatatSourceInterface *DataSourceInterface::get(wl_resource*)</li>
<li>Add DataDeviceManager and DataSource in client and server</li>
<li>plasma-effects.xml: Refactor slide a little bit</li>
<li>plasma-effects.xml: Rename protocol to plasma_effects</li>
<li>Remove panel interfaces and move visibility to org_kde_plasma_surface</li>
<li>plasma-shell.xml: Move set_screen_edge to Plasma surface</li>
<li>plasma-shell.xml: Remove cursor surface</li>
<li>Fix build: don't include wayland-client.h in test_wayland_region.cpp</li>
<li>Add support for opaque and input region to Surface</li>
<li>Implement the wl_region interface</li>
<li>Add support for wl_subcompositor and wl_subsurface</li>
<li>Fix export header for WaylandServer</li>
<li>Add Registry::interfacesAnnounced signal</li>
<li>Do not find unused Wayland components</li>
<li>Introduce a smart pointer for managing the native Wayland resources</li>
<li>Improve TestWaylandConnectionThread::testConnectionThread</li>
<li>Improve grammar in api docs</li>
</ul>
<h3> Plasma Workspace </h3><ul>
<li>fix krunner dialog being misplaced on external monitors</li>
<li>set version number to use plasma version number</li>
<li>Only consider a window maximized if it's maximized both horizontally and vertically.</li>
<li>correctly position panel when on right edge</li>
<li>Fix warning</li>
<li>Fixed warning</li>
<li>Remove explicit source request</li>
<li>Ensure the layout doesn't break when there's insanely long text in there</li>
<li>Add missing QDataStream include</li>
<li>Fix the infinite event recursion in the klipper popup.</li>
<li>Fix compile error</li>
<li>[notifications] Add Amarok to the "always replace" list and fix a bug</li>
<li>[notifications] Fix notification popups sometimes being misplaced</li>
<li>hide the arrow if no hidden items</li>
<li>ensure a bit more spacing</li>
<li>notify screenRectChanged on change panel visibility</li>
<li>Improve KRunner behavior on adding/removing screens</li>
<li>[lnf/logout] Center the logout remaining time label</li>
<li>LogoutScreen: Proper i18n handling</li>
<li>use KPluginLoader::findPlugin(libName) to find our kcminit library plugin</li>
<li>Write config when visiblity mode changes</li>
<li>make the call on the right path</li>
<li>Move screenlocker KCM to desktop settings</li>
<li>KSplashQml: Do not use camel case in filenames</li>
<li>[ksld] Use XFlush after XLib calls</li>
<li>[ksld] Non-logind fallback for lock screen on resume</li>
<li>[ksld] Add config option to lock screen on resume</li>
<li>[ksld] ScreenLocker inhibits sleep on logind</li>
<li>[ksld] Don't block till the greeter is started</li>
<li>[ksld] Provide custom QQmlNetworkAccessManagerFactory in greeter</li>
<li>Rework Plasma's notification positioning code</li>
<li>[kglobalaccel] The catalog was renamed to kglobalaccel5</li>
<li>[kglobalaccel] QWS is dead, remove the code for it</li>
<li>Rename also this template as kglobalaccel5</li>
<li>Replace Q_WS_* macros to Q_OS_* macros</li>
<li>Look for kglobalaccel and don't build the internal copy if KF5 >= 5.7</li>
<li>Make DrKonqi actually build w/ Qt5WebKit & KF5WebKit found</li>
<li>Include FeatureSummary before we use it</li>
<li>Replace MEL by MouseArea</li>
<li>Allow to compile plasma-workspace without Qt5WebKit installed.</li>
<li>Uncondtionally monitor for all KAMD service status until we have first loaded</li>
<li>Show album art on mediacontroller tooltip</li>
<li>Simplify OSD silencer</li>
<li>Only show OSD on mousewheel when the plasmoid isn't expanded like the comment already says</li>
<li>Assign expandedFeedback a property that also takes the plasmoid expanded state into account</li>
<li>Check for errors when the GetConfigOperation returns</li>
<li>[screenlocker/autotest] Xvfb has a black background</li>
<li>[ksmserver/ksld] Fix LockWindowTest::testBlankScreen</li>
<li>Use out-of-band communication between ksld and greeter</li>
<li>Use Notification window type for notifications</li>
<li>Use OnScreenDisplay window type for OSD</li>
<li>KRunner: Update the query string when notified by the view</li>
<li>Replace & with "and"</li>
<li>BUG: 337742 Go directly to passwords-only security on Bugzilla.</li>
<li>Load wallpaper immediately on startup</li>
<li>Add "Is Lid Present" property to powermanagement engine</li>
<li>Port to QIcon</li>
<li>kdelibs4support--</li>
<li>We no longer paint the background color if the wallpaper is completely covering it</li>
<li>Remove unused import</li>
<li>Use QtQuick.Dialogs ColorDialog for selecting the wallpaper background color</li>
<li>KSMServer dialog should bypass window manager</li>
<li>Don't show OSD when dragging the slider</li>
<li>Add "silent" parameter to Powermanagement engine brightness jobs</li>
<li>check on compactRepresentation</li>
<li>Reorder the shortcuts</li>
<li>Make the info with available actions and progress bar with available space visible all the time</li>
<li>cleanup, lock_logout subdir already handled by plasma_install_package</li>
<li>drop default config entries for taskbar and desktop fonts</li>
<li>Fix build</li>
<li>Register shortcuts for activities management</li>
<li>Event compress wallpaper sourceSize</li>
<li>Override plasmoid size to zero rather than one</li>
<li>Workaround a crash in GridLayout when using a Repeater</li>
<li>Don't keep updating the values and details when the plasmoid is hidden anyway</li>
<li>Port mouse dataengine</li>
<li>cache QTimeZone for the lifespan of the TimeSource don't recreate every minute</li>
<li>kservice_desktop_to_json -> kcoreaddons_desktop_to_json</li>
<li>Visually more consistent spacing on the left</li>
<li>Allow user to customise wallpaper used in the lock screen</li>
<li>warnings-- add Q_UNUSED to test function</li>
<li>warnings-- Don't include unused QObject in job ctor</li>
<li>warnings-- unused local variable</li>
<li>warnings-- order things correctly</li>
<li>warning--</li>
<li>Improve Capslock Enabled warning</li>
<li>Update brightness availability at runtime</li>
<li>Don't grab keyboard in lockscreen testing mode</li>
<li>[klipper] Drop a kBacktrace in a NOISY_KLIPPER block</li>
<li>don't prepend /usr/share if XDG_DATA_DIR is set</li>
<li>remove obsoleted webbrowser directory</li>
<li>Connect to the proper signal</li>
<li>Get rid of charge animation</li>
<li>Show battery details right in the view when there is only one battery</li>
<li>Fix warning</li>
<li>Make sure the ToolTipArea is always topmost</li>
<li>Fix margins in system tray</li>
<li>Delay seeking over mpris</li>
<li>Implement keyboard handling for wallpaper grid.</li>
<li>Disable password input while trying to login. BUG:341163 REVIEW:121368</li>
<li>Make wheel scrolling more reliable and adjust to behavior changes</li>
<li>Touchpad scroll fix for battery monitor</li>
<li>Manually dragging the slider breaks the binding to the value apparently</li>
<li>Use absolute brightness values in battery monitor</li>
<li>Expose actual and maximum brightness value in dataengine</li>
<li>uppercase style name</li>
<li>Add more DrKonqi mappings</li>
<li>add the next wallpaper action when necessary</li>
<li>Pass along timestamp when launching.</li>
<li>Enable keyboard navigation in wallpaper grid</li>
<li>Escape ampersands in notifications</li>
<li>remove a useless KGlobal::locale() call</li>
<li>remove a void FIXME comment</li>
<li>port to new connect syntax, simplify</li>
<li>try harder to set desktop as default when no parameter</li>
<li>KRunner: forceActiveWindow when triggered and visible but not focussed</li>
<li>Fix password focus in lockscreen</li>
<li>Remove workarounds we had for the nested event loops</li>
<li>Fix plasma shell initialization after port to new libkscreen API</li>
<li>fix the status bar displaying Solid errors</li>
<li>resurrect the Solid notifications, put them back where they logically belong</li>
<li>Port Shell to new KScreen API and fix handling of primary outputs</li>
<li>fix Device Item state</li>
<li>Fix dialog minimum height (always add the margins afterwards)</li>
<li>Improve wallpaper performance</li>
<li>Recent Documents Runner: Run the actual URL when running</li>
<li>ServicesRunner: Give a higher relevance when the exec matches the term</li>
<li>Small code cleanup</li>
<li>Handle resetting OSD values in the shell so you don't need to take care of this in every l'n'f package</li>
<li>Remove useless anchors and ensure the text doesn't wrap partially leaking into the icon</li>
<li>Add context to "Audio Muted" asking the translators to keep the text short</li>
<li>BalooRunner: Baloo now only supports files</li>
<li>KRunner: Remove the timer when sending the text to KRunner</li>
<li>BalooRunner: Send all the results in one go</li>
<li>BalooSearchRunner: Add a delay for queries <= 3 characters</li>
<li>simplify</li>
<li>use plugin from the activity only if explicitly set</li>
<li>used dedicated icon for GHNS</li>
<li>add another experimental touch ui</li>
<li>fixuifiles</li>
<li>don't create SystemTray::Task twice</li>
<li>fix resetting and reverting to default values</li>
<li>fix custom containment type from scripting</li>
<li>fix the temporary popup icon</li>
<li>some const and nullptr fixes</li>
<li>Adapt to the new behavior of QmlObject::setDelayedInitialization</li>
<li>Also take the title label into account when calculating the dialog size</li>
<li>Enforce StyledText in notifications</li>
<li>[dataengines/notifications] Replace \n with <br/></li>
<li>Breeze is the default cursor theme</li>
<li>Add a couple of additional assertions</li>
<li>port to QLibrary, const iterators</li>
<li>add possibility for l&f package to have layouts</li>
<li>cleanup obsolete code</li>
<li>register the kcminit object on dbus</li>
<li>Use new connect api</li>
<li>[freespacenotifier] Make the SNI just Active when free space raised</li>
<li>[freespacenotifier] Fix hiding the SNI when avail space goes up again</li>
<li>Show notification configure button only if application actually has configurable notifications</li>
<li>find messages in QML as well</li>
<li>Fix includes</li>
<li>Clean includes</li>
<li>Remove not necessary includes</li>
<li>Finish cleanup and remove unused config.ui remnant</li>
<li>Only play indeterminate animation when plasmoid is expanded</li>
<li>Refactor JobDelegate</li>
<li>Add tooltip showing the precise date and time the notification arrived</li>
<li>Hide popup when opening configure dialog</li>
<li>Fix notification configure button</li>
<li>Fix binding loops and make popup even more compact</li>
<li>Add context menu entry for the notifications kcm</li>
<li>Rename settings page to "Information"</li>
<li>Give the user a bit more context so he/she knows what to do</li>
<li>Don't leave a gap when label1 is not visible</li>
<li>Don't emit a Job finished notification if the message would be empty</li>
<li>Translate file transfer units ourselves</li>
<li>use standard icons</li>
<li>Install taskmanagerrulesrc again and add a mapping for Google-chrome-stable.</li>
<li>Fix condition</li>
<li>When no percentage is exposed over the dataengine make the progress bar indeterminate</li>
<li>hide the controller if the panel hides</li>
<li>let ourself spam</li>
<li>Make it possible to make plasmawindowed live in a status notifier</li>
<li>Cleanup Jobs code and remove dead code</li>
<li>Add timestamp to notification history</li>
<li>fix the action icon and progress bar for optical discs and MTP players</li>
<li>don't discard Free Space info for optical discs</li>
<li>Alleviate the annoyance of Job Finished notifications</li>
<li>Cleanup Loaders</li>
<li>Bring back Notifications applet settings UI</li>
<li>Delayed primaryOutput processing needs to be delayed</li>
<li>Stop using deprecated API</li>
<li>Make debug output more meaningful and fix a warning while at it</li>
<li>Cleanup Notifications code</li>
<li>Move duplicated code from NotificationDelegate and NotificationPopup into a new NotificationItem</li>
<li>rectify the order of initialization</li>
<li>drop some deprecated stuff, code cleanups</li>
<li>fix opening a removable drive when the mountpoint contains a space</li>
<li>don't consider packages without images</li>
<li>fix CD content types and device major/minor number</li>
<li>port to the correct HAVE_X11 define</li>
<li>no need for kdelibs4support</li>
<li>fix crash</li>
<li>Move ksplash kcm to plasma-desktop with all the other KCMs</li>
<li>Expose new instance launching.</li>
<li>Port to new connect api</li>
<li>Remove not necessary include moc</li>
<li>Port to new connect api. Clean forward declaration</li>
<li>Remove unused argument in ksmserver test</li>
<li>General tidy</li>
<li>Don't include moc manually</li>
<li>Remove unused #define</li>
<li>Clean up includes</li>
<li>Remove unused logout effects in KSMServer</li>
<li>port to new connect api</li>
<li>clean forward declaration</li>
<li>Remove not necessary include moc</li>
<li>convert to new connect api</li>
<li>Move default DesktopSwitcher from KWin to Look and Feel Package</li>
<li>Port to new connect api. Remove not necessary includes moc</li>
<li>Forward wheel events inside panel view</li>
<li>[wallpapers/image] Improve i18n string</li>
<li>Move KWin's window switcher "sidebar" to Look'n'Feel package</li>
<li>Use new connect api</li>
<li>Only show the list of hidden system tray items when a hidden item is expanded</li>
<li>declare and register the KActivities::Consumer::ServiceStatus metatype</li>
<li>React to Baloo API changes</li>
<li>don't mess with positions if the mouse is outside the view</li>
<li>Warning--</li>
<li>Only list image types that we can actually open in wallpaper selection dialog</li>
<li>Port to new connect api</li>
<li>no context menu on label + richtext when it's not necessary</li>
<li>fix build</li>
<li>Baloo Runner: Lower relevance because krunner does not know any better</li>
<li>LocationRunner: Convert case insensitive path to a proper one</li>
<li>Remove unused code</li>
<li>Add missing const</li>
<li>typo--</li>
<li>comments++</li>
<li>proper event translation for all panel locations</li>
<li>prototype for forwarding screen edge fake events</li>
<li>Fix build</li>
<li>Port to new connect api</li>
<li>Fix includes</li>
<li>Remove deprecated function</li>
<li>Remove not necessary include moc</li>
<li>Remove not necessary include moc</li>
<li>Remove kdelibs4support</li>
<li>Fix includes</li>
<li>Port to new connect api. Fix forward declaration. Fix includes</li>
<li>Readd Help button. Fix close help dialog</li>
<li>Fix build with Qt 5.4</li>
<li>Fix includes</li>
<li>Port to new connect api</li>
<li>Fix includes</li>
<li>Remove not necessary include moc</li>
<li>Port to new connect api => fix 2 connect signal/slot error</li>
<li>drkonqi: remove dependency on KStartupInfo, use QX11Info::setNextStartupId instead</li>
<li>--debug</li>
<li>Fix warning</li>
<li>Remove config action for activity bar and panelspacer</li>
<li>Baloo Runner: Port to newer api</li>
<li>show the expanded marker only if the theme has it</li>
<li>check corona pointer</li>
<li>positionPanel() just positions</li>
<li>Overhaul mediacontroller layout</li>
</ul>
<h3> Milou </h3><ul>
<li>SourcesModel: Give results which match all the words in the text higher priority</li>
<li>ResultView: Add an updateQueryString signal</li>
<li>Add a simple ResultsView which has a simpler delegate</li>
<li>Call RunnerManager::matchSessionComplete when clear the model</li>
<li>Fix includes</li>
<li>Revert "SourcesModel: Only consider each types first result for priority"</li>
<li>ResultsView: Handle both Enter and Return</li>
</ul>
<h3> Oxygen Fonts </h3><ul>
<li>set plasma version number</li>
</ul>
<h3> libmm-qt </h3><ul>
<li>MMModemLock is already declared as a metatype and this commented code can be removed</li>
</ul>
<h3> Plasma Addons </h3><ul>
<li>ConverterRunner: The case should not matter for units</li>
<li>add some default sizes</li>
<li>Fix showdesktop applet interaction</li>
<li>Cleanup showdesktop metadata desktop</li>
<li>Fix Messages.sh for "Show Desktop" applet</li>
<li>Port "Show Desktop" applet to Plasma Next</li>
<li>Initial port to frameworks for the comic dataengine.</li>
<li>Port the AudioPlayerControl Runner</li>
<li>make icons match their indended size, Google Code in by ematirov@gmail.com Mikhail Ivchenko</li>
<li>Hyphenate numbers. I knew it!</li>
<li>There are neither .cpp nor .kcfg files in fuzzyclock</li>
<li>Add webbrowser plasmoid</li>
<li>hide preedit and aux if both of them are not visible</li>
<li>kimpanel: try some other way to fix layout flicker</li>
<li>kimpanel: tries to make size fit better</li>
<li>kimpanel: make sure size is correct.</li>
<li>fix typo: window -> inputpanel</li>
<li>don't install kimpanel.xml, ibus might pick panel randomly.</li>
<li>Add imageprovider to fifteenpuzzle.</li>
<li>add plasma version number</li>
<li>Import desktopswitchers from KWin</li>
<li>Import window switchers from KWin repository</li>
<li>Port fifteenPuzzle applet to qml and plasma 5.</li>
<li>Remove unused rtm library. Reviewed-by: David Edmundson <david@davidedmundson.co.uk></li>
<li>Resizing the dictionary widget left large empty spaces. This patch fixes it. Patch by: Kyle Agronick <agronick@gmail.com> REVIEW:119638</li>
<li>kimpanel: Include <locale.h> for LC_CTYPE.</li>
<li>Fix crash due to off-by-one.</li>
<li>kimpanel: don't show other icons when switch im in ibus</li>
<li>kimpanel: add radio and check type support in ibus backend and dataengine</li>
<li>kimpanel: fix text icon size</li>
<li>kimpanel: implement reverse engine navigate with shift</li>
<li>kimpanel: add a timer for property update, to bash some frequent update together</li>
<li>kimpanel: fix some gtk icon, and wrong logo update</li>
<li>fix a property string typo</li>
<li>Fix typo Apperance to Appearance.</li>
<li>Removing extra COPYING GPL-3 license files - They do not match the licenses in the actual files.</li>
<li>fix position.h -> position.height</li>
<li>avoid using var typed property, might trigger a qt crash.</li>
<li>change IconItem to Image, to avoid animation when pixmap changed</li>
<li>adopt new SetLookupTable</li>
<li>port ibus backend 1.5 to xcb and qt5</li>
<li>strength against possible strange ibus engine list (e.g. duplicate)</li>
<li>fix focus out and context</li>
<li>fix focus in event and nagivate</li>
<li>add a check</li>
<li>remove some redundant code</li>
<li>add more check</li>
<li>implement navigation</li>
<li>complete ibus 1.5 port</li>
</ul>
<h3> kio-extras </h3><ul>
<li>Add missing QObject and QDataStream includes</li>
<li>sftp: Use prefixed function for ssh_channel_poll().</li>
<li>sftp: Fix short read receiving files.</li>
<li>Add kio-mtp REVIEW:121090</li>
<li>keep libmolletnetwork private</li>
<li>Fix url without scheme. Port to QUrl.</li>
<li>QUrl from Qt5 can't do setPath('..'), instead use KIO::upUrl</li>
<li>kio_sftp: Use the right type for timeout_sec and timeout_usec.</li>
<li>Install all kioslaves to $libdir/plugins/kf5/kio</li>
<li>Imported the filenamesearch ioslave from Dolphin</li>
<li>Fix includes</li>
<li>Port to qCDebug</li>
<li>Remove not necessary includes</li>
<li>Added support for NFSv3, major refactoring, fixed bugs.</li>
<li>Port to qCDebug</li>
<li>Fix includes</li>
<li>don't screw up the filenames</li>
<li>Port to new connect api</li>
<li>Don't use deprecated variabl</li>
<li>Delete kio_trash as it was moved to kio.git.</li>
<li>set minimum kf5 version to 5.3</li>
<li>Port textcreator thumbnailer</li>
<li>Port comicbook thumbnailer to KF5</li>
<li>Use new KCoreConfigSkeleton methods</li>
<li>Make constants const, avoids unnecessary symbols in the .data section.</li>
</ul>


<?php
  include("footer.inc");
?>
