<?php
  $page_title = "KDE 4.1 Beta 2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Também disponível em:
<?php
  $release = '4.1-beta2';
  include "announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  Projeto KDE disponibiliza o segundo Beta do KDE 4.1
</h3>

<p align="justify">
  <strong>
Comunidade KDE anuncia o segundo Beta do KDE 4.1</strong>
</p>

<p align="justify">
24 de Junho de 2008 (A INTERNET).
A <a href="http://www.kde.org/">Comunidade KDE</a> está orgulhosa de anunciar o
segundo beta do KDE 4.1 com foco em testadores, membros de comunidades
 e entusiastas para a identificação de bugs e regressões, para que então o KDE
4.1 possa substituir totalmente o KDE 3 para usuários finais.  KDE 4.1 beta 2 está disponível como
pacotes binários para uma grande variedade de plataformas, e como código fonte. A versão final do KDE
4.1 está previsto para o final de Julho de 2008.
</p>


<h4>
  <a name="changes">Pontos principais do KDE 4.1 Beta 2</a>
</h4>

<p align="justify">
Após um mês passado desde o ultimo congelamento de ferramentas no KDE 4.1,
os hackers do KDE estiveram trabalhando na melhoria das ferramentas,
integração com o desktop, documentação e tradução dos pacotes. Diversas
sessões para correções de erros foram feitas e bugs exterminados na versão
beta. Ainda temos bugs que precisam ser consertados até a versão final,
mas o KDE 4.1 Beta2 já se mostra uma boa opção. Testar e retornar os erros dessa versão
são apreciados e necessários para fazer o KDE 4.1 real.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/desktop-folderview.png">
        <img src="announce_4.1-beta2/desktop-folderview_thumb.png" align="center"  height="375"  />
    </a><br /><em>KDE 4.1 Beta 2 com o Desktop Plasma</em>
</div>

<ul>
    <li>Bindings para o KDE 4.1 em diversas linguagens, como Python, Ruby
    </li>
    <li>Melhorias na usabilidade e suporte no Dolphin
    </li>
    <li>Melhorias em todos os lugares no Gwenview
    </li>
</ul>

Uma lista mais completa de novas ferramentas no KDE 4.1 está disponível no
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Techbase</a>.
</p>

<h4>
  Bindings de Linguagens
</h4>
<p align="justify">
Enquanto muitas das aplicações KDE 4.1 são escritas em C++, bindings de linguagens permitem a
funcionalidade das bibliotecas do KDE para desenvolvedores de aplicações que preferem
uma linguagem diferente. KDE 4.1 vem com suporte para diversas outras linguagens,
como Python e Ruby. O applet de impressão que foi adicionado no KDE 4.1
é escrito em Python, totalmente transparente para o usuário.
</p>

<h4>
  Dolphin amadurece
</h4>
<p align="justify">
Dolphin o gerenciador de arquivos do KDE4 teve diversas melhorias.


<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/dolphin-tagging.png">
        <img src="announce_4.1-beta2/dolphin-tagging_thumb.png" align="center"  height="293"  />
    </a><br /><em>O Semantic Desktop com o seu sistema de tags</em>
</div>
Os primeiros bits e pedaços do Desktop Semantico Social <a href="http://nepomuk.kde.org/">NEPOMUK</a>
estão se tornando visíveis e mais aplicáveis. Suporte a tags no Dolphin está começando a se mostrar visível,
uma tecnologia desenvolvida num programa de pesquisas Europeu.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/dolphin-selection.png">
        <img src="announce_4.1-beta2/dolphin-selection_thumb.png" align="center"  height="257"  />
    </a><br /><em>Seleção de arquivos no modo clique único</em>
</div>
A seleção de arquivos está mais fácil por um pequeno botão (+) no topo esquerdo
da seleção do arquivo, que seleciona-o ao invés de abri-lo.
Esta mudança faz o uso do gerenciamento de arquivos no modo clique único
mais fácil e previne abertura de arquivos facilmente, além de ser intuitivo e fácil de usar.<br />

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/dolphin-treeview.png">
        <img src="announce_4.1-beta2/dolphin-treeview_thumb.png" align="center"  height="315"  />
    </a><br /><em>Nova visualização em arvore no Dolphin</em>
</div>
Muitos usuários pediram um modo de visualização em arvore no modo de lista detalhada. A
ferramenta foi editada e se combina com o modo de clique único para uma rápida
cópia e movimentação de arquivos.
</p>

<h4>
  Gwenview melhorado
</h4>
<p align="justify">
O visualizador de imagens padrão do KDE4 Gwenview foi melhorado. Opções como rotação
e visualização em tela cheia foi colocado diretamente no contexto da imagem,
fazendo a interface do usuário mais limpa minimizando movimentos do mouse e
fazendo a aplicação mais intuitiva e fácil de usar.
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/gwenview-browse.png">
        <img src="announce_4.1-beta2/gwenview-browse_thumb.png" align="center"  height="276"  />
    </a><br /><em>Visualização de um diretório no Gwenview</em>
</div>

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/gwenview.png">
        <img src="announce_4.1-beta2/gwenview_thumb.png" align="center"  height="276"  />
    </a><br /><em>Gwenview com sua barra de visualização de thumbnails</em>
</div>

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
    <a href="announce_4.1-beta2/systemsettings-emoticons.png">
        <img src="announce_4.1-beta2/systemsettings-emoticons_thumb.png" align="center"  height="276"  />
    </a><br /><em>O novo módulo de configuração de emoticons</em>
</div>

<!--
<h4>
KDE 4 Applications Grow
</h4>
<p align="justify">
Across the KDE community, many applications have now been ported to KDE 4 or
have seen great increases in functionality since KDE 4 was launched.  Dragon
Player, the lightweight media player, makes its debut.  The KDE CD Player
returns.   A new printer applet  provides unparalleled printing power and
flexibility on the Free Software Desktop.  Konqueror gains support for web
browsing sessions, an Undo mode, and improved smooth scrolling.  A new
picture browsing mode including a full-screen interface come to Gwenview.
Dolphin, the file manager, gets tabbed views, and many features appreciated
by KDE 3 users including Copy To, and an improved folder tree.  Many apps,
including the desktop and the KDE Education applications, are now providing
fresh content such as icons, themes, maps, and lesson material via Get New
Stuff, which has an improved interface.  Zeroconf networking has been added
to several games and utilities, taking the pain out of setting up games and
remote access.
</p>


<h4>
Refinement Throughout The Frameworks
</h4>
<p align="justify">
Developers have been busy enriching the core KDE libraries and
infrastructure too.  KHTML gets a speed boost from anticipatory resource
loading, while WebKit, its offspring, is added to Plasma to allow OSX
Dashboard widgets to be used in KDE.  The use of the Widgets on Canvas
feature of Qt 4.4 makes Plasma more stable and lightweight.  KDE's
characteristic single-click based interface gets a new selection mechanism
that promises speed and accessibility.  Phonon, the crossplatform media
framework, gains subtitle support and GStreamer, DirectShow 9 and QuickTime
backends.  The network management layer is extended to support several
versions of NetworkManager.  And recognising that the Free Desktop values
diversity, cross desktop efforts begin, such as supporting the popup
notification specifications and the freedesktop.org desktop bookmark
specification, so that other desktops' applications can fit right into a
KDE 4.1 session.
</p>

-->

<h4>
  Versão final do KDE 4.1
</h4>
<p align="justify">
KDE 4.1 está programado para versão final em 29 de Julho de 2008.  Esta data foi baseada em seis meses após o lançamento do KDE 4.0.
</p>

<h4>
  Pegue-o, rode-o, teste-o
</h4>
<p align="justify">
  Voluntários de comunidades e distribuidores de Linux/UNIX gentilmente disponibilizaram pacotes binários do KDE 4.0.83 (Beta 2) para a maioria das distribuições Linux, Mac OS X e Windows.  Esteja avisado que esses pacotes não são considerados prontos para uso em produção. Procure o sistema de gerenciamento de pacotes do seu sistema.

<h4>
  Compilando o KDE 4.1 Beta 2 (4.0.83)
</h4>
<p align="justify">
  <a name="source_code"></a><em>Código Fonte</em>.
  O código fonte completo para o KDE 4.0.83 pode ser <a
  href="http://www.kde.org/info/4.0.83.php">livremente baixado</a>.
Instruções para compilação e instalação do KDE 4.0.83
  estão disponíveis na <a href="/info/4.0.83.php">Página de informações do KDE 4.0.83</a>, ou no <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Ajudando o KDE
</h4>
<p align="justify">
 KDE é um projeto de <a href="http://www.gnu.org/philosophy/free-sw.html">Software Livre</a>
que existe e cresce pela ajuda de diversos voluntários que
doam seu tempo e esforços. KDE está sempre olhando por novos voluntários e contribuições,
não importando se for com código, correções de bugs, escrita de documentação,
traduções, promoção, dinheiro, etc. Todas as contribuições são
apreciadas e aceitas. Favor ler a página de <a
href="/community/donations/">Suporte do KDE</a> para maiores informações. </p>

<p align="justify">
Nos estamos esperando ouvir de você em breve!
</p>

<h4>Sobre o KDE 4</h4>
<p align="justify">
KDE 4 é um ambiente de trabalho inovador Livre contendo inúmeras aplicações
para uso diário como também para propósitos específicos. Plasma é um novo desktop
desenvolvido para o
KDE 4, provendo uma interface intuitiva para interagir com o desktop e aplicações.
O Browser Web Konqueror integra a web com o Desktop. O gerenciador de arquivos
Dolphin, o leitor de documentos Okular, e o Centro de Controle System Settings
completam o ambiente de desktop básico.
<br />
KDE is built on the KDE Libraries which provide easy access to resources on the
network by means of KIO and advanced visual capabilities through Qt4. Phonon and
Solid, which are also part of the KDE Libraries add a multimedia framework and
better hardware integration to all KDE applications.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contatos de Imprensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
