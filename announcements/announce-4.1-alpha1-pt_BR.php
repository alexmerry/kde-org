<?php
  $page_title = "KDE 4.1 Alpha1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Also available in:
<a href="announce-4.1-alpha1.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.1-alpha1.php">French</a>

<!--
<a href="announce-4.0.3-bn_IN.php">Bengali (India)</a>
<a href="announce-4.0.3-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/announce-4.0.3.php">Chinese</a>
<a href="announce-4.0.3-cz.php">Czech</a>
<a href="announce-4.0.3-nl.php">Dutch</a>
<a href="announce-4.0.3.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-de.php">German</a>
<a href="announce-4.0.3-gu.php">Gujarati</a>
<a href="announce-4.0.3-he.php">Hebrew</a>
<a href="announce-4.0.3-hi.php">Hindi</a>
<a href="announce-4.0.3-it.php">Italian</a>
<a href="announce-4.0.3-lv.php">Latvian</a>
<a href="announce-4.0.3-ml.php">Malayalam</a>
<a href="announce-4.0.3-mr.php">Marathi</a>
<a href="announce-4.0.3-fa.php">Persian</a>
<a href="announce-4.0.3-pl.php">Polish</a>
<a href="announce-4.0.3-pa.php">Punjabi</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-ro.php">Romanian</a>
<a href="announce-4.0.3-ru.php">Russian</a>
<a href="announce-4.0.3-sl.php">Slovenian</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Swedish</a>
<a href="announce-4.0.3-ta.php">Tamil</a>
-->

<!-- // Boilerplate -->

<h3 align="center">
  Projeto KDE distribui o primeiro Alpha do KDE 4.1
</h3>

<p align="justify">
  <strong>
Comunidade KDE distribui a primeira versão Alpha do KDE 4.1, A primeira versão para usuários finais do Ambiente de Trabalho
da próxima geração.</strong>
</p>

<p align="justify">
 29 de Abril de 2008 (A INTERNET). A <a href="http://www.kde.org/">comunidade KDE
 </a> anuncia hoje a disponibilidade imediata do KDE 4.1 Alpha1.
Com o congelamento das funcionalidades em efeito, KDE 4.1 provê um primeiro preview do
que pode ser esperado do KDE 4.1, esperado para Julho desse ano.
</p>


<h4>
  <a name="changes">O que o KDE 4.1 tem?</a>
</h4>

<p align="justify">
<ul>
    <li>
        <strong>Akonadi</strong> é uma das mais novas coisas no KDE 4.1. Akonadi é uma engine para armazenamento
        de PIM independente de desktop. Enquanto ainda não é baseado no Akonadi, KDE 4.1 também possui um port das aplicações
        de PIM como KMail e KOrganizer.
    </li>
    <li>
        O KDE 4.0 está disponível em plataformas X11, KDE 4.1 também está disponível no
        <strong>Windows, Mac OS X e OpenSolaris</strong>. Os ports não estão totalmente completos,
        mas estão bons para um preview.
    </li>
    <li>
        KDE 4.1 é baseado no <strong>Qt 4.4</strong>. Qt 4.4 possui melhorias na
        performance e funcionalidades que ajudam o KDE 4.1. A velocidade da Renderização SVG foi
        vastamente melhorada, e widgets e layouts podem ser usados em canvas como o desktop e painel Plasma.
        Migrar o código-base do Plasma para essas novas funcionalidades ainda é um trabalho em progresso,
        então algumas instabilidades são esperadas.
    </li>
</ul>

Favor notar que o KDE 4.1 Alpha1 não é indicado para uso diário. Ele é previsto como um preview do que está por vir.
</p>

<h4>
  Compilando o KDE 4.1 Alpha1 (4.0.71)
</h4>
<p align="justify">
  <a name="source_code"></a><em>Código-Fonte</em>.
  O código-fonte completo do KDE 4.0.71 pode ser <a
  href="http://www.kde.org/info/4.0.71.php">livremente baixado</a>.
Instruções sobre compilação e instalação do KDE 4.0.71
  estão disponíveis na <a href="/info/4.0.71.php">Página de informações do KDE 4.0.71</a>, ou no <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>.
</p>

<h4>
  Ajudando o KDE
</h4>
<p align="justify">
 KDE é um projeto de <a href="http://www.gnu.org/philosophy/free-sw.html">Software Livre</a>
que existe e cresce pela ajuda de diversos voluntários que
doam seu tempo e esforços. O KDE está sempre procurando novos voluntários e
contribuições, não importa se é com código, consertos ou relato de bugs, escrevendo
documentação, traduzindo, divulgando, dinheiro, etc. Todas as contribuições são
apreciadas e aceitas. Por favor leia a <a href="/community/donations/">Página para Ajuda do KDE</a> para maiores informações.
</p>

<p align="justify">
Nos esperamos ouvir de você em breve!
</p>

<h4>Sobre o KDE 4</h4>
<p align="justify">
KDE 4.0 é um ambiente de Desktop Livre cheio de inovações contendo diversas aplicações para uso diário como também para uso específicos. Plasma é o novo desktop desenvolvido para o KDE 4, provendo uma interface intuitiva para interação com o Desktop e as aplicações. O browser web Konqueror integra a web com o desktop. O gerenciador de arquivos Dolphin, o leitor de documentos Okular e o centro de controle System Settings completam o ambiente de trabalho básico.<br/>
KDE é feito com as bibliotecas KDE que prevêem acesso fácil à recursos na rede pelo KIO e avançadas capacidades visuais através do Qt4. Phonon e Solid, que também são parte das bibliotecas do KDE adicionam um framework multimídia e uma melhor integração com o hardware para todas as aplicações KDE.
</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contatos de imprensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
