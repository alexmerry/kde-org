<?php

  $page_title = "KDE 4.0 Platform Release Candidate 1";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- Other languages translations 
Also available in:
<a href="announce-4.0-beta1-ca.php">Catalan</a>
// No translations available yet.
-->

<h3 align="center">
   KDE Project Ships Release Candidate for the KDE 4 Development Platform "K.I.T.T."
</h3>
<p align="justify">
  <strong>
    With this platform release candidate, the KDE 4.0 release cycle enters the final phase.
  </strong>
</p>
<p align="justify">
October 30, 2007 (The INTERNET). The KDE Community is happy to release the candidate for
the KDE 4.0 Development Platform.
</p>

The KDE Platform contains the base libraries and tools to develop KDE applications. It 
contains high-level widget libraries, a network abstraction layer and various libraries 
for multimedia integration, hardware integration and transparent access to resources on the 
network.
<p />
The ultimate resource for information about the KDE Development Platform can be found 
on KDE's knowledge site TechBase. On TechBase, you can find 
<a href="http://techbase.kde.org/Getting_Started/Build/KDE4">build instructions</a>.
<p />
In the future, the KDE libraries will also support two new platforms, Microsoft Windows 
and Apple's Mac OS. Work on that is currently ongoing, and while there is good progress 
being made, those versions are not yet fully up.
<p />
You can download this release candidate from the 
<a href="http://www.kde.org/info/3.95.php#platform">info page</a>. Please report bugs 
you find via <a href="http://bugs.kde.org">KDE's Bugzilla</a> or on the relevant mailinglist.

<?php
  include("../contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include("../contact/press_contacts.inc");
  include("footer.inc");
?>
