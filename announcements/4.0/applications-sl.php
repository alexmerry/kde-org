<?php
  $page_title = "Slikovni vodič po KDE 4.0: Programi";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

  guide_links();

?>
<p>
Also available in:
<a href="applications-it.php">English</a>
<a href="applications-it.php">Italian</a>
<a href="applications-pa.php">Punjabi</a>
</p>


<h2>Upravljalnik datotek Dolphin</h2>
<p>
Dolphin je novi upravljalnik datotek v KDE 4.0. Z Dolphinom lahko brskate po datotekah,
jih odpirate, kopirate in premikate. Dolphin se osredotoča na preprostost uporabe in
nadomešča upravljalnika datotek Konqueror, ki je bil na voljo v KDE 3 in starejših serijah.
Konqueror lahko še vedno uporabljate, a se je ekipa KDE-ja odločila vpeljati program,
ki je prav posebej prilagojen za upravljanje datotek: Dolphin.
</p>
<?php
    screenshot("dolphin-splitview_thumb.jpg", "dolphin-splitview.jpg",
        "center", "Delo z datotekami v Dolphinu");
?>

<p>
Če imate v neki mapi veliko slik, lahko v Dolphinovi orodjarni kliknete gumb Ogled
in namesto ikon boste videli oglede vsebine datotek v tej mapi. Za hitro pomikanje
po mapah uporabite navigacijsko vrstico, ki se nahaja nad pogledom na datoteke. Če
kliknete na puščico med deli poti, se lahko hitro premaknete v drugo podmapo. Da bi
dobili pogled na dve mapi obenem, kar olajša kopiranje datotek, kliknite na gumb
Razdeli. Dolphin si lahko zapomni nastavitve za vsako mapo posebej. Privzete nastavitve
lahko prilagodite, če iz menija Nastavitve izberete ukaz Nastavi Dolphin.
</p>

<p>
V stolpcu na levi Dolphin ponuja hiter dostop do najpogosteje uporabljenih lokacij,
ki so imenovane Mesta. V ta stolpec lahko povlečete katerokoli mapo, do katere
potrebujete hiter dostop. To ne velja le za Dolphin, ampak tudi za meni KickOff
(zavihek Računalnik) in za pogovorna okna za odpiranje in shranjevanje datotek.
</p>
<?php
    screenshot("dolphin-groups_thumb.jpg", "dolphin-groups.jpg", "center",
                "Razvrščanje datotek po skupinah v Dolphinu");
?>
<p>
Stolpec Informacije prikazuje ogled vsebine datoteke in nekaj dodatnih podatkov
o izbrani datoteki. Uporabite ga lahko tudi za dodajanje komentarjev datotekam
ter za dodajanje oznak za lažje razvrščanje in iskanje. Za razvrščanje datotek
v skupine po velikosti, vrsti ali drugi lastnosti v meniju Videz omogočite
možnost Prikaži v skupinah.
</p>

<h2>Okular in Gwenview: pregledovanje dokumentov in slik</h2>

<?php
    screenshot("gwenview_thumb.jpg", "gwenview.jpg", "center",
                "Pregledujte svoje slike z Gwenview");
?>
<p>
<strong>Gwenview</strong> je KDE-jev pregledovalnik slik. Na voljo je bil že v
seriji KDE 3, a je v KDE 4.0 doživel poenostavitev uporabniškega vmesnika in je
zato priročnejši za hitro brskanje po vaših zbirkah slik. Gwenview lahko uporabljate
tudi za prikaz slik. Na voljo je celozaslonski vmesnik, s katerim je moč
prikazovati slike v načinu predstavitve.
</p>
<?php
	screenshot("okular_thumb.jpg", "okular.jpg", "center", "Okular je hiter in vsestranski pregledovalnik dokumentov");
?><p>
<strong>Okular</strong> je pregledovalnik dokumentov za KDE 4. Podpira kopico različnih
vrst dokumentov, od datotek PDF do OpenDocument Format (ODF). Okular ni omejen samo na
branje. Na voljo je zmožnost dodajanja opomb v dokumente. Pritisnite tipko F6, izberite
želeno orodje in začnite dodajati svoje oznake in opombe.
</p>


<h2>Sistemske nastavitve</h2>
<p>
Sistemske nastavitve v KDE 4.0 nadomeščajo staro nadzorno središče. Tu lahko spremenite
videz in občutek programov, prilagodite osebne nastavitve, spremenite nastavitve
omrežja in upravljate s svojim računalnikom.
</p>
<?php
    screenshot("systemsettings-appearance_thumb.jpg",
     "systemsettings-appearance.jpg", "center", "Spremenite videz namizja v Sistemskih nastavitvah");
?>

<p>
Odprite modul Videz, da spremenite barvno shemo vseh programov, nastavite kontrast in
prilagodite izgled svojemu osebnemu okusu. Tu lahko nastavite tudi obliko in velikost
pisav. KDE 4.0 sicer prihaja z dobrim privzetim videzom, a zagotovo ta ne ustreza
vsakomur. Zato lahko v tem modulu popolnoma spremenite videz namizja.
</p>
<?php
	screenshot("solid_thumb.jpg", "solid.jpg", "center", "Povezava s strojno opremo prek ogrodja Solid");
?>
<p>
Sistemske nastavitve vsebujejo tudi orodja za nadzor operacijskega sistema, ki teče v ozadju.
Solid poskrbi za upravljanje z energijo, priklapljanje in odklapljanje naprav ter za
povezovanje z omrežjem.
</p>

<h2>Konzola</h2>
<p>
Primer programa, ki je v KDE 4.0 doživel večjo prenovo, je terminalski emulator Konzola.
Pogovorno okno za nastavitve je lažje za uporabo, kljub temu pa so še vedno na voljo
vse zmožnosti programa. Med ostalimi izboljšavami najdemo:
</p>
<?php
	screenshot("konsole_thumb.jpg", "konsole.jpg", "center", "Konzola, KDE-jev terminalski emulator");
?><p>
<ul>
	<li>
		Uporabniški vmesnik je reorganiziran in prečiščen. Dodanih je bilo precej
		tipkovničnih bližnjic za bolj učinkovito uporabo Konzole.
	</li>
	<li>
		Dodana je možnost razdeljenega pogleda. Uporabnik sedaj lahko razdeli okno
		Konzole na več delov. To poenostavi ogled besedila, ki je že ušlo z zaslona,
		ter poenostavi nadzorovanje.
	</li>
	<li>
		Imena zavihkov se spreminjajo samodejno in jih je tako lažje identificirati.
	</li>
	<li>
		Rezultati iskanja so sedaj poudarjeni, iskalna vrstica pa izboljšuje
		potek iskanja. Za začetek iskanja pritisnite tipke Ctrl+Shift+F.
	</li>
	<li>
		Pohitreno je drsenje in iskanje skozi dolge terminalske izpise
	</li>
	<li>
		Terminalska okna imajo možnost prave prosojnosti, ki jo lahko omogočite na
		zavihku Videz. (Pomnite: Konzolo morate zagnati z možnostjo --enable-transparency)
	</li>
</ul>
</p>
<p>
O drugih novostih v Konzoli si lahko preberete v
<a href="http://websvn.kde.org/branches/KDE/4.0/kdebase/apps/konsole/CHANGES-4.0?view=markup">
dnevniku sprememb</a>.
</p>

<h2>Drugi programi</h2>
<p>
Izid KDE 4.0 prinaša dobre novice tudi za uporabnike programov iz modula ExtraGear. Ti
programi imajo običajno svoj tempo izhajanja. Od sedaj pa lahko po želji sledijo urniku
izhajanja, ki se ga držijo glavni moduli KDE-ja. Ekipa za izdajanje paketov je zato
razširila svoje delovanje tudi na programe, ki
<a href="http://techbase.kde.org/Projects/extragearReleases">se želijo pridružiti</a>
rednim izdajam KDE-ja. Prvi nabor paketov že vsebuje nekatere znane programe:
<a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects/KAider">Kaider</a>,
<a href="http://ktorrent.org/">KTorrent[3]</a>,
<a href="http://ktown.kde.org/kphotoalbum/">KPhotoAlbum</a> in
<a href="http://rsibreak.org/">RSIBreak</a>.
Prenos na KDE 4 za te programe mogoče še ni zaključen, a kljub temu želijo prejemati
poročila o hroščih.
</p>

<table width="100%">
	<tr>
		<td width="50%">
				<a href="desktop-sl.php">
				<img src="images/desktop-32.png" />
				Predhodna stran: Namizje
				</a>		
		</td>
		<td align="right" width="50%">
				<a href="education-sl.php">Naslednja stran: Izobraževalni programi
				<img src="images/education-32.png" /></a>
		</td>
	</tr>
</table>
<?php
  include("footer.inc");
?>