<?php
    include_once ("functions.inc");
    $translation_file = "kde-org";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Plasma 5.17: Thunderbolt, X11 Night Color and Redesigned Settings",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.17.0'; // for i18n
    $version = "5.17.0";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

    <?php include "./announce-i18n-bar.inc"; ?>

    <figure class="videoBlock d-flex">
        <iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.mastodon.host/videos/embed/5a315252-2790-42b4-8177-94680a1c78fc" frameborder="0" allowfullscreen></iframe>
    </figure>

    <figure class="topImage">
        <a href="plasma-5.17/plasma-5.17.png" data-toggle="lightbox">
            <img src="plasma-5.17/plasma-5.17-wee.png" height="342" width="600" style="width: 100%; max-width: 600px; height: auto;" alt="Plasma 5.17 Beta" />
        </a>
        <figcaption><?php print i18n_var("KDE Plasma %1", "5.17")?></figcaption>
    </figure>

    <p><?php print str_replace(["Thursday", "Dimarts", "Четвер", "Donderdag", "Quinta"], ["Tuesday", "Dijous", "Вівторок", "Dinsdag", "Terça"], i18n_var("Thursday, 15 October 2019.")) ?></p>
    <p><?php i18n("Plasma 5.17 is out!")?></p>

    <p><?php i18n("Plasma 5.17 is the version where the desktop anticipates your needs. Night Color, the color-grading system that relaxes your eyes when the sun sets, has landed for X11. Your Plasma desktop also recognizes when you are giving a presentation, and stops messages popping up in the middle of your slideshow. If you are using Wayland, Plasma now comes with fractional scaling, which means that you can adjust the size of all your desktop elements, windows, fonts and panels perfectly to your HiDPI monitor."); ?></p>

    <p><?php i18n("The best part? All these improvements do not tax your hardware! Plasma 5.17 is as lightweight and thrifty with resources as ever."); ?></p>
    
    <p><?php print i18n_var("Read on for more features, improvements and goodies, or go ahead and <a href='%1'>download Plasma 5.17</a>.", "#download"); ?></p>

    <h2 id="desktop"><?php i18n("Plasma");?></h2>

    <p><?php i18n("First of all, Plasma now starts even faster! Among many other optimizations, start-up scripts have been converted from Bash (a slow, interpreted language) to C++ (a fast, compiled language) and now run asynchronously. This means they can run several tasks simultaneously, instead of having to run them one after another. As a result, the time it takes to get from the login screen to the fully loaded desktop has been reduced significantly."); ?></p>

    <p><?php i18n("Once your Plasma desktop has loaded, you'll notice changes on the notification front, down in the system tray. One of those changes, as mentioned above, is the ability to avoid interrupting presentations with pop-up notifications. To achieve this, the \"Do Not Disturb\" mode is automatically enabled when mirroring screens. The Notifications widget has also gained new abilities, and now shows a ringing bell icon when there are new notifications, instead of a circle with a number. This makes the meaning of the icon clearer and its appearance cleaner, as the previous icon tended to get cluttered when the number of notifications went up."); ?></p>

    <p><?php i18n("Still in the system tray, you'll discover much better support for public WiFi logins. You will also find that you can set the maximum volume for your audio devices to be lower than 100%."); ?></p>
    
    <p><?php i18n("We improved the widget editing UX, making it easier to move them around and resize them particularly for touch. There's something new in the Task Manager, too. We added a new middle-click behavior to the Task Manager. Middle-clicking on a task of a running app will open a new instance of the app. But if you hover with your cursor on the task and the thumbnail pops-up, middle-clicking on the thumbnail will close that instance of the app."); ?></p>

    
    <figure class="text-center">
        <a href="plasma-5.17/notification-widget.png" data-toggle="lightbox">
            <img src="plasma-5.17/notification-widget-wee.png" class="border-0 img-fluid" width="250" height="230" alt="<?php i18n("Improved Notifications widget and widget editing UX");?>" />
        </a>
        <figcaption><?php i18n("Improved widget editing UX");?></figcaption>
    </figure>

    <p><?php i18n("Both KRunner (Plasma's floating search-and-run widget you can activate with <kbd>Alt</kbd> + <kbd>Space</kbd>) and Kickoff (Plasma's start menu) can now convert fractions of units into other units. This allows you to, for example, type \"<i>3/16 inches</i>\" into the text box, and KRunner and Kickoff will show \"<i>4.7625 mm</i>\" as one of the results. Also new is that Kickoff's recent documents section now works with GNOME/GTK apps. The Kickoff tab appearance, which had issues with vertical panels, has now been fixed. As for sticky notes, pasting text into them now strips the formatting by default."); ?></p>

    <figure class="text-center">
        <a href="plasma-5.17/inches-to-cm.png" data-toggle="lightbox">
            <img src="plasma-5.17/inches-to-cm-wee.png" class="border-0 img-fluid" width="350" height="159" alt="<?php i18n("KRunner now converts fractional units");?>" />
        </a>
        <figcaption><?php i18n("KRunner now converts fractional units");?></figcaption>
    </figure>

    <p><?php i18n("Your desktop's background is more configurable in Plasma 5.17, as you can now choose the order of the images in the wallpaper <i>Slideshow</i>, rather than it always being random. Another cool feature is a new picture-of-the-day wallpaper source, courtesy of <a href='https://unsplash.com/'>Unsplash</a>. The Unsplash pic of the day option lets you choose the category you want to show, too, so you can pick anything from <i>Design</i>, <i>Christmas</i>, <i>Beach</i>, <i>Cars</i>, <i>Galaxy</i>, and more."); ?>
    
    <figure class="text-center">
        <a href="plasma-5.17/unsplash-pic-of-day.png" data-toggle="lightbox">
            <img src="plasma-5.17/unsplash-pic-of-day-wee.png" class="border-0 img-fluid" width="350" height="280" alt="<?php print str_replace(["<a href='https://unsplash.com/'>", "</a>"], "", i18n_var("<a href='https://unsplash.com/'>Unsplash</a> Pic of the Day")); ?>" />
        </a>
        <figcaption><?php i18n("<a href='https://unsplash.com/'>Unsplash</a> Pic of the Day");?></figcaption>
    </figure>

    <h2 id="systemsettings"><?php i18n("System Settings: Thunderbolt, X11 Night Color and Overhauled Interfaces");?></h2>

    <p><?php i18n("Modifying the wallpaper is not the only way to change the look of your desktop in Plasma 5.17. The <i>Night Color</i> feature, implemented for Wayland back in Plasma 5.11, is now also available on X11. <i>Night Color</i> subtly changes the hue and brightness of the elements on your screen when it gets dark, diminishing glare and making it more relaxing on your eyes. This feature boasts a modernized and redesigned user interface in Plasma 5.17, and can be manually invoked in the <i>Settings</i> or with a keyboard shortcut."); ?></p>
    
    <figure class="text-center">
        <a href="plasma-5.17/night-color.png" data-toggle="lightbox">
            <img src="plasma-5.17/night-color-wee.png" class="img-fluid border-0" width="350" height="250" alt="<?php i18n("Night Color settings are now available on X11 too");?>" />
        </a>
        <figcaption><?php i18n("Night Color settings are now available on X11 too");?></figcaption>
    </figure>

    <p><?php i18n("Speaking of color settings, SDDM's advanced settings tab lets you apply Plasma's color scheme, user's font, icon theme, and other settings to the login screen to ensure it is consistent with the rest of your desktop."); ?></p>

    <p><?php i18n("The <i>Settings</i> interface itself has been overhauled in general, and the user interfaces for the <i>Displays</i>, <i>Energy</i>, <i>Activities</i>, <i>Boot Splash</i>, <i>Desktop Effects</i>, <i>Screen Locking</i>, <i>Screen Edges</i>, <i>Touch Screen</i>, and <i>Window Behavior</i> configuration dialogs have all been improved and updated."); ?></p>

    <p><?php i18n("We have also added new features to <i>System Settings</i>, like the new panel for managing and configuring Thunderbolt devices. As always, small things also count, and provide a better user experience. An example of that is how we have reorganized and renamed some <i>System Settings</i> dialogs in the <i>Appearance</i> section, and made basic system information available through <i>System Settings</i>."); ?>

    <figure class="text-center">
        <a href="plasma-5.17/thunderbolt.png" data-toggle="lightbox">
            <img src="plasma-5.17/thunderbolt-wee.png" style="border: 0px" width="350" height="344" alt="<?php i18n("Thunderbolt device management");?>" />
        </a>
        <figcaption><?php i18n("Thunderbolt device management");?></figcaption>
    </figure>

    <p><?php i18n("Plasma 5.17 will be even easier to use thanks to the new accessibility feature that lets you move the cursor with the keyboard when using <i>Libinput</i>. To help you save power, we've added a '<i>sleep for a few hours and then hibernate</i>' feature and the option to assign a global keyboard shortcut to turn off the screen."); ?></p>

    <p><?php i18n("Improving the look-and-feel of Plasma 5.17 across the board was one of our priorities, and we've made a number of changes to achieve this. For instance, we have made the Breeze GTK theme respect your selected color scheme. Active and inactive tabs in Google Chrome and Chromium now have a visually distinct look, and window borders are turned off by default. We have also given sidebars in settings windows a consistent, modernized appearance."); ?></p>

    <figure class="text-center"
        <a href="plasma-5.17/window-borders.png" data-toggle="lightbox">
            <img src="plasma-5.17/window-borders-wee.png" class="border-0 img-fluid" width="350" height="250" alt="<?php i18n("Window borders are now turned off by default");?>" />
        </a>
        <figcaption><?php i18n("Window borders are now turned off by default");?></figcaption>
    </figure>

    <h2 id="ksysguard"><?php i18n("System Monitor");?></h2>

    <p><?php i18n("Plasma's System Monitor - KSysGuard - helps you keep an eye on your system, making it easier to catch processes that eat up your CPU cycles or apps that fill up your RAM. In Plasma 5.17, KSysGuard can show CGroup details, allowing you to see container limits. You can also check if a process is hogging your network, as KSysGuard shows network usage statistics for each process."); ?></p>

    <figure class="text-center">
        <a href="plasma-5.17/ksysguard.png" data-toggle="lightbox">
            <img src="plasma-5.17/ksysguard-wee.png" class="border-0" width="350" height="157" alt="<?php i18n("CGroups in System Monitor");?>" />
        </a>
        <figcaption><?php i18n("CGroups in System Monitor");?></figcaption>
    </figure>

    <h2 id="discover"><?php i18n("Discover");?></h2>

    <p><?php i18n("Discover, Plasma's software manager that lets you install, remove and update applications easily, has also been improved. It now includes real progress bars and spinners in various parts of the UI to better indicate progress information. We have also placed icons in the sidebar, and added icons for Snap apps. To help you diagnose potential problems, Discover comes with better 'No connection' error messages in Plasma 5.17."); ?>

    <figure class="text-center">
        <a href="plasma-5.17/discover.png" data-toggle="lightbox">
            <img src="plasma-5.17/discover-wee.png" class="img-fluid border-0" width="350" height="255" alt="<?php i18n("Discover now has icons on the sidebar");?>" />
        </a>
        <figcaption><?php i18n("Discover now has icons on the sidebar");?></figcaption>
    </figure>

    <h2 id="kwin"><?php i18n("KWin: Improved Display Management");?></h2>

    <p><?php i18n("One of the main new features of the Plasma 5.17 display manager is that fractional scaling is now supported on Wayland. This means you can scale your desktop to suit your HiDPI monitor perfectly -- no more tiny (or gigantic) fonts and icons on 4K screens! Other features landing in Wayland include the ability to resize GTK headerbar windows from window edges, and improved scrolling behavior (the mouse wheel will always scroll the correct number of lines)."); ?></p>

    <p><?php i18n("A feature making a come-back in Plasma 5.17 is the ability to close windows in the Present Windows desktop effect with a middle-click. You will also have the option to apply screen settings to the current screen arrangement, or to all screen arrangements."); ?></p>

    <p><?php i18n("If you are using the X11 window system, you can now map the Meta (“Super” or “Windows”) and some other key as a shortcut to switch between open windows, instead of <kbd>Alt</kbd> + <kbd>Tab</kbd> which has been the default for years."); ?></p>

    <h2><?php i18n("In Memory of Guillermo Amaral"); ?></h2>
    
    <p><?php i18n("The Plasma 5.17 series is dedicated to our friend Guillermo Amaral. Guillermo was an enthusiastic KDE developer who rightly self described as 'an incredibly handsome multidisciplinary self-taught engineer'.  He brought cheer to anyone he met.  He lost his battle with cancer last summer but will be remembered as a friend to all he met.")?></p>

    <figure class="text-center">
        <a href="plasma-5.17/guillermo.png" data-toggle="lightbox">
            <img src="plasma-5.17/guillermo-wee.png" style="padding: 10px" width="350" height="334" alt="<?php i18n("Guillermo Amaral");?>" />
        </a>
        <figcaption><?php i18n("Guillermo Amaral");?></figcaption>
    </figure>
    
    <a href="plasma-5.16.5-5.17.0-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.17.0"); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it" id="download">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <?php include($site_root . "/contact/social_link.inc"); ?>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#plasma:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');


