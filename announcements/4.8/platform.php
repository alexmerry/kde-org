<?php

  $release = '4.8';
  $release_full = '4.8.0';
  $page_title = "KDE Platform 4.8 Enhances Interoperability, Introduces Touch-Friendly Components";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p>
KDE proudly announces the release of <strong>KDE Platform 4.8</strong>. KDE Platform is the foundation for Plasma Workspaces and KDE Applications. It has seen significant improvements. Besides the introduction of new technology, a lot of work has been done on improving the performance and stability of the underlying structure.
</p><p>

<?php
centerThumbScreenshot("ksecretservice-sync.png", "KSecretService improves interoperatbility between applications");
?>
Qt Quick is making its way into Plasma Workspaces. The new Plasma Components provide a standardized API implementation of widgets with native Plasma look and feel. For example, the Device Notifier widget has been ported using these components and is now written in pure QML. KWin's Window Switcher is now also QML-based, paving the way for easier design of new window switchers. The introduction of QML into the Plasma Workspaces significantly lowers the barriers for new developers to improve the Workspaces or use Plasma as the basis for other products.
</p><p>

The new <strong>Plasma QtQuick Components</strong> bring a standard set of user interface elements to Plasma. These new components bring a standardized API and implementation of common UI elements. Plasma Components offer an efficient, elegant and powerful set of widgets, which integrate well with the underlying platform. Combined with Plasma's visual and data integration components, application development has never been easier, more fun and powerful. Plasma Components automatically adapt to the device they are used on, making it easy to share code between Desktop and mobile and touch-based applications and allowing deployment and usability on a wider range of target devices.
</p><p>

<strong>KSecretService</strong> is a new framework for sharing saved passwords and other credentials between a wider range of applications. KSecretService uses KDE's well-known interfaces through a Freedesktop-compliant API with other password systems, transparently for the user. KSecretService improves integration of KDE applications into other workspaces and allows 3rd party applications to plug into KDE's secure password saving system.
</p><p>

<?php // done
centerThumbScreenshot("chat.png", "KDE Telepathy's chat window");
?>
<a href="http://community.kde.org/Real-Time_Communication_and_Collaboration">KDE <strong>Telepathy</strong></a> —the new real-time communication and collaboration framework for KDE Workspaces—enters KDE Extragear with its first beta version. Since the last release, the team fixed over 100 bugs and added several new features along with visual polish. You can read more <a href="http://martys.typepad.com/blog/2012/01/kde-telepathy-03-ninja-released-first-beta.html">here</a>.


<?php // done
centerThumbScreenshot("telepathy.png", "KDE Telepathy's contact list");
?>
</p><p>

The KDE team is currently working on the next iteration of the KDE Development Platform under the codename "Frameworks 5". While Frameworks 5 will be mostly source compatible, it will be based on Qt5. It will provide more granularity, making it easier to use just parts of the platform, and reducing dependencies considerably.
</p>
<h4>Installing the KDE Development Platform</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>
<?php

include("trailer-plasma.inc");
include("trailer-applications.inc");

include("footer.inc");
?>
