<?php

  $release = '4.8';
  $release_full = '4.8.0';
  $page_title = "KDE Plasma Workspaces 4.8 Gain Adaptive Power Management";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<!--
<?php
centerThumbScreenshot("kmail.png", "KMail has seen many performance and stability improvements");
?>
<?php
centerThumbScreenshot("kate.png", "Kate 4.8 shows changes you've made to files");
?>

<?php
centerThumbScreenshot("gwenview.png", "Gwenview 4.8 comes with nicer transitions between images");
?>

<?php
centerThumbScreenshot("gwenview2.png", "Gwenview 4.8 comes with nicer transitions between images");
?>

<?php // done
centerThumbScreenshot("ksecretservice-sync.png", "KSecretService improves interoperatbility between applications");
?>

<?php // done
centerThumbScreenshot("chat.png", "KDE Telepathy's chat window");
?>

<?php // done
centerThumbScreenshot("telepathy.png", "KDE Telepathy's contact list");
?>

-->

<p>
KDE is happy to announce the immediate availability of version 4.8 of both the Plasma Desktop and Plasma Netbook Workspaces. The Plasma Workspaces have seen improvements to existing functionality, as well as the introduction of significant new features.
</p><p>

The <strong>Window Switcher</strong> that is commonly assigned to the Alt+Tab shortcut now has six possible layouts. These choices are especially useful for systems that don't use Desktop Effects. Task Switcher choices can be found at the Window Behavior module inside System Settings. Another round of significant <a href="http://philipp.knechtges.com/?p=10">performance improvements</a> in KWin, Plasma's window manager and compositor further improve the user experience.
</p><p>

<?php
centerThumbScreenshot("window-switcher-layout.png", "You can choose the layout in Plasma's Alt-Tab window switcher");
?>

</p><p>
<strong>KDE Power Management System Settings</strong> have been <a href="http://drfav.wordpress.com/2011/10/04/power-management-a-new-screencast/">redesigned</a>. The user interface has been simplified and the layout improved. Power management no longer relies on user created profiles. Instead, it provides three presets: On AC Power, On Battery and On Low Battery. In addition, power management is now multi-screen aware and adapts to the user's current activity. This means, for example, that power settings can be set to never dim or never turn off the screen in a Photos or Video Activity. If needed, power management can be deactivated quickly via the battery desktop applet.
<?php
centerThumbScreenshot("power-management.png", "Power Management adapts to the user's activity");
?>

</p><p>

KSecretService offers shared password storage, making saved passwords available to other applications. This provides a more secure system and better integration of non-KDE apps with the Plasma Workspaces. With this feature, it is no longer necessary to contend with different password storage systems when using non-KDE applications.
</p><p>
A new splash screen implementation uses QtQuick. This release debuts a visually and feature-wise re-implementation of the device notifier widget as the first default widget of Plasma Desktop that moves to QtQuick for easier maintainability, a smoother user experience and more touch-friendliness.  The on-screen keyboard has seen numerous improvements in terms of bug fixes and performance. Taskbars and docks work better thanks to improvements from Craig Drummond's Icon Tasks Plasmoid. This includes nicer context menus, improved support for launchers, and a number of bug fixes. A new picture-of-the-day wallpaper plugin allows new astronomy, flickr, Wikipedia or other picture on your screen every day. Performance improvements, usability fixes round up the overall user experience.
</p>
<?php
centerThumbScreenshot("tasks.png", "Icon Tasks provides an alternative way to the more traditional task bar");
?>


<h4>Installing Plasma</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>

<?php

include("trailer-applications.inc");
include("trailer-platform.inc");

include("footer.inc");

?>
