<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 17.04.0");
  $site_root = "../";
  $version = "17.04.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("April 20, 2017. KDE Applications 17.04 is here. On the whole, we have worked to make both the applications and the underlying libraries more stable and easier to use. By ironing out wrinkles and listening to your feedback, we have made the KDE Applications suite less prone to glitches and much friendlier.");?>
</p>

<p align="justify">
<?php print i18n_var("Enjoy your new apps!");?>
</p>

<h4 style="clear:both;" ><a href="https://edu.kde.org/kalgebra/"><?php print i18n_var("KAlgebra");?></a></h3>

<figure style="float: right; margin: 0px"><a href="kalgebra1704.jpg"><img src="kalgebra1704.jpg" width="140" height="250" /></a></figure>

<p align="justify">
<?php print i18n_var("The developers of KAlgebra are on their own particular road to convergence, having ported the mobile version of the comprehensive educational program to Kirigami 2.0 -- the preferred framework for integrating KDE applications on desktop and mobile platforms.");?>
</p>
<p align="justify">
<?php print i18n_var("Furthermore, the desktop version has also migrated the 3D back-end to GLES, the software that allows the program to render 3D functions both on the desktop and on mobile devices. This makes the code simpler and easier to maintain.", "https://dot.kde.org/2016/03/30/kde-proudly-presents-kirigami-ui", "https://www.khronos.org/opengles/");?>
</p>


<h4 style="clear:both;" ><a href="http://kdenlive.org/"><?php print i18n_var("Kdenlive");?></a></h3>

<figure style="float: right; margin: 0px"><a href="kdenlive1704.png"><img src="kdenlive1704.png" width="400" height="244" /></a></figure>

<p align="justify">
<?php print i18n_var("KDE's video editor is becoming more stable and more full-featured with every new version. This time, the developers have redesigned the profile selection dialog to make it easier to set screen size, framerate, and other parameters of your film."); ?>
</p>
<p align="justify">
<?php print i18n_var("Now you can also play your video directly from the notification when rendering is finished. Some crashes that happened when moving clips around on the timeline have been corrected, and the DVD Wizard has been improved.");?>
</p>


<h4 style="clear:both;" ><a href="https://userbase.kde.org/Dolphin"><?php print i18n_var("Dolphin");?></a></h3>

<figure style="float: right; margin: 0px"><a href="dolphin1704.png"><img src="dolphin1704.png" width="400" height="238" /></a></figure>

<p align="justify">
<?php print i18n_var("Our favorite file explorer and portal to everything (except maybe the underworld) has had several makeovers and usability improvements to make it even more powerful."); ?>
</p>
<p align="justify">
<?php print i18n_var("The context menus in the <i>Places</i> panel (by default on the left of the main viewing area) have been cleaned up, and it is now possible to interact with the metadata widgets in the tooltips. The tooltips, by the way, now also work on Wayland."); ?>
</p>


<h4 style="clear:both;" ><a href="https://www.kde.org/applications/utilities/ark/"><?php print i18n_var("Ark");?></a></h3>

<figure style="float: right; margin: 0px"><a href="ark1704.png"><img src="ark1704.png" width="400" height="246" /></a></figure>

<p align="justify">
<?php print i18n_var("The popular graphical app for creating, decompressing and managing compressed archives for files and folders now comes with a <i>Search</i> function to help you find files in crowded archives."); ?>
</p>
<p align="justify">
<?php print i18n_var("It also allows you to enable and disable plugins directly from the <i>Configure</i> dialog. Talking of plugins, the new Libzip plugin improves Zip archive support."); ?>
</p>


<h4 style="clear:both;" ><a href="https://minuet.kde.org/"><?php print i18n_var("Minuet");?></a></h3>

<figure style="float: right; margin: 0px"><a href="minuet1704.png"><img src="minuet1704.png" width="400" height="238" /></a></figure>

<p align="justify">
<?php print i18n_var("If you are teaching or learning to play music, you have to check out Minuet. The new version offers more scales exercises and ear-training tasks for bebop, harmonic minor/major, pentatonic, and symmetric scales."); ?>
</p>
<p align="justify">
<?php print i18n_var("You can also set or take tests using the new  <i>Test mode</i> for answering exercises. You can monitor your progress by running a sequence of 10 exercises and you'll get hit ratio statistics when finished."); ?>
</p>



<h3 style="clear:both;" ><?php print i18n_var("And more!");?></h3>

<p align="justify">
<?php print i18n_var("<a href='%1'>Okular</a>, KDE's document viewer, has had at least half a dozen changes that add features and crank up its usability on touchscreens. <a href='%2'>Akonadi</a> and several other apps that make up <a href='%3'>Kontact</a> (KDE's email/calendar/groupware suite) have been revised, debugged and optimized to help you become more productive.", "https://okular.kde.org/", "https://userbase.kde.org/Akonadi", "https://www.kde.org/applications/office/kontact/");?>
</p>

<p align="justify">
<?php print i18n_var("<a href='%1'>Kajongg</a>, <a href='%2'>KCachegrind</a> and more (<a href='%3'>Release Notes</a>) have now been ported to KDE Frameworks 5. We look forward to your feedback and insight into the newest features introduced with this release.", "https://www.kde.org/applications/games/kajongg", "https://www.kde.org/applications/development/kcachegrind/", "https://community.kde.org/Applications/17.04_Release_Notes#Tarballs_that_were_based_on_kdelibs4_and_are_now_KF5_based");?>
</p>

<p align="justify">
<?php print i18n_var("<a href='%1'>K3b</a> has joined the KDE Applications release.", "https://userbase.kde.org/K3b");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("Bug Stomping");?></h3>
<p align="justify">
<?php print i18n_var("More than 95 bugs have been resolved in applications including Kopete, KWalletManager, Marble, Spectacle and more!");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("If you would like to read more about the changes in this release, <a href='%1'>head over to the complete changelog</a>. Although a bit intimidating due to its breadth, the changelog can be an excellent way to learn about KDE's internal workings and discover apps and features you never knew you had.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors play an important part in KDE's success. While proprietary software companies have huge advertising budgets for new software releases, KDE often depends on word of mouth."); ?>
</p>

<p align="justify">
<?php print i18n_var("There are many ways to support the KDE Applications 17.04 release: you can report bugs, encourage others to join the KDE Community, or <a href='%1'>support the non-profit organization behind the KDE Community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Help us spread the word on the social web. You can submit stories to news sites like Reddit, Facebook and Twitter; upload screenshots of your new set-up to services like Snapchat, Instagram and Google+; or create screencasts and upload them to YouTube, Blip.tv, and Vimeo, or stream them live over Twitch!"); ?>
</p>

<p align="justify">
<?php i18n("Remember to tag your posts and uploaded materials with the <i>KDE</i> moniker, as this makes them easier to find and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 17.04 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 17.04 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.04 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 17.04");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 17.04 may be <a href='http://download.kde.org/stable/applications/17.04.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-17.04.0.php'>KDE Applications 17.04.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
