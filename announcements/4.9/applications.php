<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "KDE Applications 4.9 Are More Polished and Stable";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
KDE is happy to announce the release of improved versions of several popular applications. This includes many important basic tools and applications like Okular, Kopete, KDE PIM, educational applications and games.
</p>
<p>
KDE's document viewer Okular can now store and print annotations with PDF documents. Search, bookmarking and text selection have been improved. Okular can be set up so that a laptop won't sleep or turn off its screen during a presentation, and it can now play embedded movies in PDF files. Image viewer Gwenview brings a new full-screen browse option and a number of bug fixes and small improvements.
</p>
<p>
Juk, KDE's default music player, brings last.fm support with scrobbling and cover-fetching as well as MPRIS2 support. It will read cover art embedded in MP4 and AAC files. Dragon, KDE's video player, also works with MPRIS2 now.
</p>
<p>
The versatile chat client Kopete can group all off-line users in a single "Offline Users" group and show contact status change in chat windows. It has a new 'rename' contact option and allows custom display name changes inline.
</p>
<p>
The Lokalize translation application has improved fuzzy search and a better file search tab. It can now also handle .TS files. Umbrello has auto-layout support for diagrams and can export graphviz dot drawings. Okteta introduces view profiles, including an editor/manager.
</p>
<h2>Kontact Suite</h2>
<p>
The world’s most complete PIM suite Kontact received many bugfixes and performance improvements. This release introduces an import wizard to get settings, mails, filters, calendar and addressbook entries from Thunderbird and Evolution into KDE PIM. There is a tool that can backup and restore email, configuration and metadata. KTnef, the standalone TNEF attachment viewer, has been brought back to life from the KDE 3 archives. Google resources can be integrated with KDE PIM, giving users access to their Google contacts and calendar data.
</p>
<h2>KDE Education</h2>

<p>
KDE-Edu introduces Pairs, a new memory game. Rocs, the graph theory application for students and teachers, gained a number of improvements. Algorithms can now be executed step-wise, the undo and cancel-construction system works better, and overlay graphs are now supported. Kstars has improved sorting by meridian transit time / observation time and better Digital Sky Survey <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">image retrieval</a>.
<div align="center" class="screenshot">
<a href="screenshots/kde49-pairs.png"><img src="screenshots/kde49-pairs-thumb.png" /></a></div>
</p>
<p>
Marble received speed optimizations and threading support, and its user interface has been enhanced. Marble routing extensions now include OSRM (Open Source Routing Machine), support for bicycle and pedestrian routing, and an offline data model to manage offline routing and offline search data. Marble can now show positions of aircraft in the FlightGear simulator.
</p>
<h2>KDE Games</h2>
<p>
KDE Games have been upgraded. There has been a lot of polish to Kajongg, KDE's Mahjongg game, including tooltip playing hints, improved robot AI and chat if players are on the same server (kajongg.org now has one!). KGoldrunner has a number of new levels (a contribution by Gabriel Miltschitzky) and KPatience retains game history upon saving. KSudoku has seen small improvements such as better hints, as well as seven new two-dimensional puzzle shapes and three new 3-D shapes.
<div align="center" class="screenshot">
<a href="screenshots/kde49-ksudoku-3d-samurai.png"><img src="screenshots/kde49-ksudoku-3d-samurai-thumb.png" /></a></div>
</p>

<h4>Installing KDE Applications</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>
<h2><a href="plasma.php"><img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.9" width="64" height="64" /> Plasma Workspaces 4.9 – Core Improvements</a></h2>
<p>
Highlights for Plasma Workspaces include substantial improvements to the Dolphin File Manager, Konsole X Terminal Emulator, Activities, and the KWin Window Manager. Read the complete <a href="plasma.php">'Plasma Workspaces Announcement'.</a>
</p>
<h2><a href="platform.php"><img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.9"/> KDE Platform 4.9</a></h2>
<p>
Today’s KDE Platform release includes bugfixes, other quality improvements, networking, and preparation for Frameworks 5
</p>
<?php
  include("footer.inc");
?>
