<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Espacios de trabajo Plasma 4.9 – Mejoras de los componentes base";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>También disponible en:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE se alegra de anunciar la inmediata disponibilidad de la versión 4.9 de los espacios de trabajo del Escritorio Plasma y de Plasma para Netbooks. Contienen mejoras en la funcionalidad existente del espacio de trabajo Plasma y también se han introducido un gran número de nuevas funcionalidades.
</p>
<h2>El gestor de archivos Dolphin</h2>
<p>
El potente gestor de archivos de KDE, Dolphin, incluye ahora los botones «Atrás» y «Adelante» y el regreso del cambio de nombre de archivos en línea. Dolphin puede mostrar metadatos, como puntuaciones, etiquetas, tamaños de imágenes y archivos, autor, fecha, entre otros, así como agrupar y ordenar por propiedades de los metadatos. El nuevo complemento para Mercurial permite manejar este sistema de control de versiones del mismo modo conveniente en el que se implementan git, SVN y CVS, de modo que los usuarios pueden ejecutar las órdenes «pull», «push» y «commit» desde el gestor de archivos. La interfaz de usuario de Dolphin contiene mejoras menores, como un panel de «Lugares» más avanzado, búsqueda mejorada y sincronización con la ubicación del terminal.
<div align="center" class="screenshot">
<a href="screenshots/kde49-dolphin_.png"><img src="screenshots/kde49-dolphin_thumb.png" /></a>
</div>
</p>
<h2>El emulador de terminal X Konsole</h2>
<p>
La bestia de carga Konsole incorpora ahora la habilidad de buscar una selección de texto usando accesos rápidos Web de KDE. Ofrece la opción de contexto «Cambiar directorio a» cuando se suelta una carpeta en la ventana de Konsole. Los usuarios tienen más control para organizar las ventanas de la terminal <strong>desprendiendo pestañas</strong> y arrastrándolas para crear nuevas ventanas con ellas. Las pestañas existentes de pueden clonar en otras con el mismo perfil. La visibilidad del menú y de la barra de pestañas se puede controlar al iniciar Konsole. Para los que gustan de scripts, los títulos de las pestañas se pueden cambiar usando una secuencia de escape.
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole1.png"><img src="screenshots/kde49-konsole1-cropped.png" /></a></div>
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole2.png"><img src="screenshots/kde49-konsole2-cropped.png" /></a></div>
</p>
<h2>El gestor de ventanas KWin</h2>
<p>
El gestor de ventanas de KDE, KWin, ha recibido gran cantidad de trabajo. Las mejoras incluyen cambios sutiles como la elevación de ventanas durante la selección de ventanas y ayuda para preferencias específicas de las ventanas, así como cambios más visibles, como un mejorado módulo de control para el cambio de cajas y mejor rendimiento con las ventanas gelatinosas. Hay cambios para hacer que KWin funcione mejor con las Actividades, incluyendo la adición de reglas de ventanas relacionadas con las Actividades. El trabajo general se ha centrado en mejorar la calidad y el rendimiento de KWin.
<div align="center" class="screenshot">
<a href="screenshots/kde49-window-behaviour_settings.png"><img src="screenshots/kde49-window-behaviour_settings_thumb.png" /></a></div>
</p>
<h2>Las Actividades</h2>
<p>
Las Actividades están ahora integradas más concienzudamente con los espacios de trabajo. Los archivos se pueden enlazar con las Actividades en Dolphin, Konqueror y la vista de carpetas. La vista de carpetas también puede mostrar solo aquellos archivos relacionados con una actividad en el escritorio o en un panel. Hay un nuevo esclavo KIO para Actividades y también es posible cifrar las Actividades privadas.
<div align="center" class="screenshot">
<a href="screenshots/kde49-link-files-to-activities.png"><img src="screenshots/kde49-link-files-to-activities-cropped.png"/></a></div>
</p>
<p>
Los espacios de trabajo introducen una implementación de MPRIS2, por lo que KMix tiene la posibilidad de manejar flujos de sonido y Plasma tiene un motor de datos para manejar este protocolo de reproducción de música. Estos cambios son acordes con la implementación de MPRIS2 en Juk y Dragon, los reproductores de música y de vídeo de KDE.
</p>
<p>
Existen más cambios menores en los espacios de trabajo, incluyendo varias adaptaciones a QML. El mejorado reproductor en miniatura de Plasma incluye un diálogo de propiedades de la pista y mejor filtrado. El menú de Kickoff se puede usar ahora con la única ayuda del teclado. El plasmoide de la gestión de redes contiene trabajos de usabilidad y de mejora visual. El elemento gráfico de Transporte público también contiene un buen número de cambios.
</p>

<h4>Instalación de Plasma</h4>
<?php
  include("boilerplate-es.inc");
?>

<h2>También se han anunciado hoy:</h2>

<h2><a href="applications-es.php"><img src="images/applications.png" class="app-icon" alt="Las aplicaciones de KDE 4.9"/>Aplicaciones de KDE 4.9 nuevas y mejoradas</a></h2>
<p>
Hoy se han liberado aplicaciones de KDE nuevas y mejoradas que incluyen Okular, Kopete, KDE PIM y aplicaciones y juegos educativos. Lea el <a href="applications-es.php">«Anuncio de aplicaciones de KDE»</a>.
</p>
<h2><a href="platform-es.php"><img src="images/platform.png" class="app-icon" alt="La plataforma de desarrollo de KDE 4.9"/>Plataforma de KDE 4.9</a></h2>
<p>
El lanzamiento de la plataforma de KDE de hoy incluye corrección de errores, otras mejoras de calidad, redes y preparación para Frameworks 5.
</p>

<?php
  include("footer.inc");
?>
