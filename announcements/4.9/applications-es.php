<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Las aplicaciones de KDE 4.9 están más pulidas y son más estables";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>También disponible en:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
KDE se congratula de anunciar el lanzamiento de versiones mejoradas de varias aplicaciones populares. Esto incluye muchas importantes herramientas básicas y aplicaciones como Okular, Kopete, KDE PIM y aplicaciones y juegos educativos.
</p>
<p>
El visor de documentos de KDE, Okular, ya puede almacenar e imprimir notas con los documentos PDF. La búsqueda, los marcadores y la selección de texto se han mejorado. Okular se puede configurar para que un portátil no se duerma ni apague la pantalla durante una presentación, y también puede reproducir películas empotradas en archivos PDF. El visor de imágenes Gwenview presenta una nueva opción de navegación a pantalla completa, ha corregido un buen número de problemas y contiene mejoras menores.
</p>
<p>
Juk, el reproductor de música por omisión de KDE, proporciona integración con los perfiles de gustos musicales y la obtención de carátulas de last.fm, así como el uso de MPRIS2. Es capaz de leer las carátulas insertadas en los archivos MP4 y AAC. Dragon, el reproductor de vídeo de KDE, también funciona ahora con MPRIS2.
</p>
<p>
El versátil cliente de charla Kopete puede agrupar todos los usuarios sin conexión en un único grupo de «Usuarios sin conexión» y mostrar los cambios de estado de los contactos en las ventanas de charla. Posee una nueva opción para cambiar el nombre de un contacto y permite mostrar de forma personalizada los cambios de nombre en línea.
</p>
<p>
La aplicación de traducción Lokalize ha mejorado la búsqueda de mensajes dudosos y contiene un mejor apartado de búsqueda en archivos. Ahora también puede manejar archivos .TS. Umbrello posee diseños automáticos para los diagramas y puede exportar dibujos «graphviz dot». Okteta introduce un visor de perfiles, incluyendo un editor/gestor.
</p>
<h2>La suite Kontact</h2>
<p>
Kontact, la suite PIM más completa del mundo, ha recibido muchas correcciones de errores y mejoras de rendimiento. Esta versión introduce un asistente de importación para obtener preferencias, correos, filtros, calendarios y entradas de la libreta de contactos de Thunderbird y Evolution en KDE PIM. Contiene una herramienta que puede hacer copias de seguridad de los mensajes, la configuración y los metadatos, y restaurarlas. KTnef, el visor aislado de adjuntos TNEF, ha sido revivido de los archivos de KDE 3. Los recursos de Google se pueden integrar con KDE PIM, dando acceso a los usuarios a sus contactos y datos de calendario de Google.
</p>
<h2>Los programas educativos de KDE</h2>

<p>
KDE-Edu introduce Pairs, un nuevo juego de memoria. Rocs, la aplicación de teoría de grafos para estudiantes y profesores, ha ganado un buen número de mejoras. Los algoritmos se pueden ejecutar ahora paso a paso, el sistema de deshacer y cancelar la construcción funciona mejor y se permiten grafos superpuestos. Kstars ha mejorado la ordenación por hora de tránsito del meridiano y por hora de observación, y también la <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">obtención de imágenes</a> de Digital Sky Survey.
<div align="center" class="screenshot">
<a href="screenshots/kde49-pairs.png"><img src="screenshots/kde49-pairs-thumb.png" /></a></div>
</p>
<p>
Marble contiene optimizaciones de velocidad y ejecución multihilo, y también se ha mejorado su interfaz de usuario. Las extensiones de rutas de Marble incluyen ahora OSRM (Open Source Routing Machine), implementación de rutas en bicicleta y a pie, y un modelo de datos sin conexión para gestionar rutas y búsqueda de datos sin conexión. Marble puede mostrar posiciones de aviones en el simulador FlightGear.
</p>
<h2>Los juegos de KDE</h2>
<p>
Los juegos de KDE han sido actualizados. Kajongg, el juego de mahjong de KDE, se ha pulido bastante, e incluye consejos emergentes de juego, IA robótica mejorada y charla con los jugadores que estén en el mismo servidor (kajongg.org tiene uno ahora). KGoldrunner contiene un buen número de nuevos niveles (del colaborador Gabriel Miltschitzky) y KPatience retiene el historial del juego cuando se guarda. KSudoku contiene mejoras menores, como mejores pistas, así como siete nuevos rompecabezas de formas de dos dimensiones y tres de formas de tres dimensiones.
<div align="center" class="screenshot">
<a href="screenshots/kde49-ksudoku-3d-samurai.png"><img src="screenshots/kde49-ksudoku-3d-samurai-thumb.png" /></a></div>
</p>

<h4>Instalación de aplicaciones de KDE</h4>
<?php
  include("boilerplate-es.inc");
?>

<h2>También se han anunciado hoy:</h2>
<h2><a href="plasma-es.php"><img src="images/plasma.png" class="app-icon" alt="Los espacios de trabajo Plasma de KDE 4.9" width="64" height="64" />Espacios de trabajo Plasma 4.9 – Mejoras globales</a></h2>
<p>
Lo más destacado de los espacios de trabajo Plasma incluye mejoras sustanciales en el gestor de archivos Dolphin, el emulador de terminal de X Konsole, las Actividades y el gestor de ventanas KWin. Lea el <a href="plasma-es.php">«Anuncio de los espacios de trabajo Plasma»</a>.
</p>
<h2><a href="platform-es.php"><img src="images/platform.png" class="app-icon" alt="La plataforma de desarrollo de KDE 4.9"/>Plataforma de KDE 4.9</a></h2>
<p>
El lanzamiento de la plataforma de KDE de hoy incluye corrección de errores, otras mejoras de calidad, redes y preparación para Frameworks 5.
</p>
<?php
  include("footer.inc");
?>
