<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Plasma töötsoonid 4.9 – põhikomponentide täiustused";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE teatab rõõmuga nii Plasma töölaua kui ka Plasma Netbooki töötsooni versiooni 4.9 väljalaskmisest. Plasma töötsoonide senist funktsionaalsust on tublisti parandatud ning lisandunud on ka mitmeid märkimisväärseid võimalusi.
</p>
<h2>Failihaldur Dolphin</h2>
<p>
KDE võimsal failihalduril Dolphin on nüüd tagasi- ja edasiliikumise nupud ning taas saab ka failide nime otse muuta. Dolphin võib näidata metaandmeid, näiteks hinnangut, silte, pildi- ja failisuurust, autorit, kuupäeva ja veel mitmeid asju. Samuti saab faile rühmitada ja sortida vastavalt metaandmetele. Uus Mercuriali plugin võimaldab seda versioonihaldussüsteemi sama mugavalt kasutada nagu seni giti, SVN-i ja CVS-i, nii et kasutajad saavad oma faile alla või üles laadida otse failihaldurist. Dolphini kasutajaliides on saanud kerget lihvi, muu hulgas on täiustatud asukohtade paneeli, otsimist ja sünkroonimist terminaliga.
<div align="center" class="screenshot">
<a href="screenshots/kde49-dolphin_.png"><img src="screenshots/kde49-dolphin_thumb.png" /></a>
</div>
</p>
<h2>X'i terminali emulaator Konsole</h2>
<p>
Igaks ülesandeks mihkel Konsole võimaldab nüüd otsida valitud teksti KDE veebikiirkorralduste abil. Kui lohistada Konsole peale kataloog, pakutakse võimalust terminalis kohe sellesse kataloogi liikuda. Kasutajad saavad paremini hallata terminaliaknaid <strong>kaarte lahti haakides</strong> ja neid mujale lohistades, millega luuakse uus aken ainult lohistatud kaardiga. Olemasolevaid kaarte saab kloonida sama profiili raames. Menüü- ja kaardiriba näitamist või peitmist saab määrata juba Konsolet käivitades. Kes teavad, mida peale hakata skriptidega, võivad kaartide nimetusi paojadaga muuta.
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole1.png"><img src="screenshots/kde49-konsole1-cropped.png" /></a></div>
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole2.png"><img src="screenshots/kde49-konsole2-cropped.png" /></a></div>
</p>
<h2>Aknahaldur KWin</h2>
<p>
KDE aknahalduriga KWin on palju vaeva nähtud. Täiustuste seas võib ära mainida selliseid pisikesi muudatusi, nagu akende esiletõstmine nende vahel lülitamise ajal või spetsiaalsete aknareeglite abitekstid, kuid mõned muudatused torkavad ka paremini silma, näiteks akende lülitamise täiustatud juhtimismoodul või võbelevate akende etem jõudlus. Mõned muudatused on aidanud parandada KWini ja tegevuste suhtlust, sealhulgas on lisatud tegevustega seotud aknareeglid. Ka üleüldiselt on üritatud eelkõige parandada KWini kvaliteeti ja jõudlust.
<div align="center" class="screenshot">
<a href="screenshots/kde49-window-behaviour_settings.png"><img src="screenshots/kde49-window-behaviour_settings_thumb.png" /></a></div>
</p>
<h2>Tegevused</h2>
<p>
Tegevused on nüüd põhjalikumalt töötsoonidesse lõimitud. Faile võib seostada tegevustega nii Dolphinis, Konqueroris kui ka kataloogivaates. Kataloogivaade võib näidata töölaual või paneelil ainult konkreetse tegevusega seotud faile. Uus asi on tegevuste KIO-moodul, samuti saab nüüd krüptida tegevusi, mida kasutaja ei taha teistele nähtavaks teha.
<div align="center" class="screenshot">
<a href="screenshots/kde49-link-files-to-activities.png"><img src="screenshots/kde49-link-files-to-activities-cropped.png"/></a></div>
</p>
<p>
Muude töötsoonide uuenduste hulka kuuluavd MPRIS2 toetus, KMixi oskus hakkama saada voogudega ning Plasma andmemootori toimetulek selle muusikamängija juhtprotokolliga. Need muudatused on seotud MPRIS2 toetuse lisandumisega KDE muusika- ja videomängijatele Juk ja Dragon.
</p>
<p>
Töötsoonides on samuti hulk väiksemaid muudatusi, sealhulgas mitu portimist QML peale. Täiustatud Plasma minipleier pakub radade omaduste dialoogi ja paremat filtreerimist. Kickoffi menüüd saab nüüd kasutada ka ainult klaviatuuri abil. Võrguhalduse plasmoidil on parandatud väljanägemist ja hõlbustatud kasutamist. Samamoodi on märkimisväärseid muudatusi üle elanud ühiskondliku transpordi vidin.
</p>

<h4>Plasma paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>

<h2><a href="applications-et.php"><img src="images/applications.png" class="app-icon" alt="KDE rakendused 4.9"/>Uued ja täiustatud KDE rakendused 4.9</a></h2>
<p>
Täna ilmunud uute ja täiustatud KDE rakenduste seast väärivad märkimist Okular, Kopete, KDE PIM, õpirakendused ja mängud. Neist kõneleb lähemalt <a href="applications-et.php">'KDE rakenduste teadaanne'</a>
</p>
<h2><a href="platform-et.php"><img src="images/platform.png" class="app-icon" alt="KDE arendusplatvorm 4.9"/> KDE platvorm 4.9</a></h2>
<p>
Tänane KDE platvormi väljalase sisaldab veaparandusi, muulaadseid kvaliteediparandusi, võrgutäiustusi ja valmistumist üleminekuks raamistikule 5
</p>

<?php
  include("footer.inc");
?>