<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "KDE Platform 4.9 – Better Interoperability, Easier Development";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Also available in:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE proudly announces the release of KDE Platform 4.9. KDE Platform is the foundation for Plasma Workspaces and KDE Applications. The KDE team is currently working on the next iteration of the KDE Development Platform referred to as "Frameworks 5". While Frameworks 5 will be mostly source compatible, it will be based on Qt5. It will provide more granularity, making it easier to use parts of the platform selectively and reducing dependencies considerably. As a result, the KDE Platform is mostly frozen. Most Platform efforts went into bugfixing and performance improvements.
</p>
<p>
There has been progress in separating QGraphicsView from Plasma to make way for a pure QML-based Plasma and further improvements to Qt Quick support in KDE libraries and Plasma Workspaces.
</p>
<p>
Some low-level work has been done in the KDE libraries in the area of networking. Accessing network shares has been made much faster for all KDE applications. NFS, Samba and SSHFS connections and KIO won't count items in folders anymore, making them faster. The HTTP protocol was also speeded up by preventing unnecessary server round trips. This is especially noticeable in networked applications like Korganizer, which is about 20% faster due to the fixes. There is also improved password storing for network shares.
</p>
<p>
Many critical bugs have been fixed in Nepomuk and Soprano, making desktop search faster and more reliable for Applications and Workspaces build on KDE Platform 4.9. Performance improvement and stability were the primary goals of these projects for this release.
</p>

<h4>Installing the KDE Development Platform</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>
<h2><a href="plasma.php"><img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.9" width="64" height="64" /> Plasma Workspaces 4.9 – Core Improvements</a></h2>
<p>
Highlights for Plasma Workspaces include substantial improvements to the Dolphin File Manager, Konsole X Terminal Emulator, Activities, and the KWin Window Manager. Read the complete <a href="plasma.php">'Plasma Workspaces Announcement'.</a>
</p>
<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.9"/>New and Improved KDE Applications 4.9</a></h2>
<p>
New and improved KDE Applications released today include Okular, Kopete, KDE PIM, educational applications and games. Read the complete <a href="applications.php">'KDE Applications Announcement'</a>
</p>
<?php
  include("footer.inc");
?>
