<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Plasma 5.2 complete changelog");
  $site_root = "../";
  $release = 'plasma-5.1.95';
  include "header.inc";
?>

<h3>Plasma NM </h3><ul>
<li>Show correct connection name</li>
<li>Properly set disconnected icon when wireless network/connection is available</li>
<li>Properly render ampersand and other characters in tooltip</li>
<li>Improve workaround for older NM versions</li>
</ul>
<h3> KMenuEdit </h3><ul>
<li>Fix includes</li>
<li>Port to new connect api</li>
<li>kdelibs4support--</li>
<li>Fix add shortcutt</li>
<li>Fix signal</li>
</ul>
<h3>KWrited</h3><ul>
<li>Disable session management for kwrited</li>
</ul>
<h3> KHotkeys </h3><ul>
<li>add include(ECMOptionalAddSubdirectory)</li>
<li>use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete</li>
</ul>
<h3> Breeze </h3><ul>
<li>Avoid comparing unintialised variable if BREEZE_HAVE_KSTYLE is not set</li>
<li>Fix crash when switching to/from breeze</li>
<li>explicitly delete sizeGrip in destructor (apparently not done automatically) BUG: 343988</li>
<li>properly adjust menu icon size and positioning, to deal with button's offset in maximized mode. BUG: 343441</li>
<li>Initialize widget only at first successfull call to registerWidget CCBUG: 341940</li>
</ul>
<h3> libksysguard </h3><ul>
<li>Clean forward declaration</li>
</ul>
<h3> kde-cli-tools </h3><ul>
<li>add ECMOptionalAddSubdirectory</li>
<li>use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete</li>
<li>kdelibs4support--</li>
<li>install in KDE_INSTALL_LIBEXECDIR_KF5</li>
</ul>
<h3> libkscreen </h3><ul>
<li>Fix crash when multiple EDID requests for the same output are enqueued</li>
</ul>
<h3> kfilemetadata </h3><ul>
<li>Fix version to 5.6.1</li>
</ul>
<h3> Oxygen </h3><ul>
<li>Improved rendering of checkbox menu item's contrast pixel, especially when selected using Strong highlight. BUG: 343754</li>
<li>set the right widget style</li>
</ul>
<h3> Plasma Desktop </h3><ul>
<li>[Application Menu] Always sort.</li>
<li>[Application Menu] Make subdialog focus explicit.</li>
<li>[Folder View] Improve rubber band feel and consistency with Dolphin.</li>
<li>Use smooth transformation for scaling down the user picture</li>
<li>use QProcess::startDetached instead of KProcess</li>
<li>fix typo in ecm_optional_add_subdirectory</li>
<li>add subdirectories optionally for documentation because translations may be incomplete</li>
<li>When setting colour scheme information for KDE4, don't read from KF5 kdeglobals</li>
<li>Sort apps by name if configured</li>
<li>Baloo KCM: Show proper icons (porting bug)</li>
<li>allow free resize on desktop</li>
<li>the applet object has own Layout</li>
<li>more accurate position restore after delete</li>
<li>the trashcan is always in fullrepresentation</li>
<li>Reverse dns desktop</li>
<li>Fix checking the places a radio button not updating pref.</li>
<li>Add sanity checks to actions operating on selections.</li>
<li>Migrate settings</li>
<li>Fix mapping configured URL to places model entry.</li>
<li>[Folder View] Fix intra-view DND of files into folders generating wrong destination URL.</li>
<li>adjust size policies</li>
<li>kdelibs4support--</li>
<li>Port to qCDebug</li>
<li>Clean includes</li>
<li>kdelibs4support--</li>
<li>Clean up</li>
<li>leave some space for the decoration</li>
<li>[Application Menu] Port to QStyleHints and add porting TODOs for Qt 5.5+.</li>
<li>Make evaluating single vs. double click procedural.</li>
<li>Sanity checks to avoid ending up with multiple submenus.</li>
<li>Still save kde5 config into the right place</li>
<li>Sync back mouse settings into KDE4 configs</li>
<li>[Task Managers] Don't crash when a window closes while it's context menu is open.</li>
<li>properly sort the locales</li>
<li>bring back the flags</li>
<li>fix building the test</li>
<li>use KModifierKeyInfo directly to set the numlock status</li>
</ul>
<h3> polkit-kde-agent-1 </h3><ul>
</ul>
<h3> Powerdevil </h3><ul>
<li>Apply OSD silencing on AC plugging also to keyboard brightness controls</li>
<li>Don't show OSD when brightness changes due to pluggin in/out the AC</li>
<li>Fix crash when one of the combo boxes is not present</li>
<li>Fix hardware shutdown button doing nothing on desktops</li>
<li>Fix PowerDevil brightness calls breaking kded, Volume 3521</li>
<li>Ensure text in those two combo boxes is never truncated</li>
<li>Increase grace time for DPMS to 5 seconds</li>
<li>Unconditionally enable power button handling</li>
<li>Plugging in or out the AC adapter is always explicit</li>
<li>Inhibitions posted over the freedesktop interface should always inhibit everything</li>
</ul>
<h3> KScreen </h3><ul>
<li>KCM: fallback to QComboBox when resolutions list is too long</li>
<li>KCM: prevent nested scrollbars when opened in System Settings</li>
<li>KCM: Block save() unti SetConfigOperation finishes</li>
<li>KLM: correctly initialize output rotation on start</li>
<li>Fix visual representation of output rotation</li>
</ul>
<h3> Systemsettings </h3><ul>
<li>Port to new connect api</li>
</ul>
<h3> KWin </h3><ul>
<li>add include(ECMOptionalAddSubdirectory)</li>
<li>use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete</li>
<li>extract UI messages correctly</li>
<li>Check GL version and/or extension for using texture format GL_R8</li>
<li>Fix glPixelStore in GLTexture::update</li>
<li>[effects] Ensure the correct cursor shape is set for mouse interception</li>
<li>fix e3768b43559fa179f497d5130c0108dd72acf266</li>
<li>save geom_restore before calling for border update</li>
<li>correctly handle virtual desktop on packing</li>
<li>Rules: make WindowRole matching case sensitive</li>
<li>do not wrap text in deco button drag source</li>
<li>updateFocusMousePosition() before some actions</li>
<li>use xcb_time_current_time as setinputfocus default</li>
<li>[kcmkwin/compositing] Fix ordering of items in keep window thumbnail combo box</li>
<li>Fix build with clang and gcc 5.0</li>
<li>[kcmkwin/options] Load/Unload WindowGeometry effect</li>
<li>[effects] Support building the effect_builtins without need to link the lib</li>
<li>Disable libinput integration if >= 0.8 is found</li>
<li>Avoid deleting an engine whilst components from our engine are still in use</li>
<li>use xembed for the qml view of window decorations modul</li>
<li>[kcmkwin/deco] Do not runtime depend on QtQuick 2.4 (Qt 5.4)</li>
<li>Trigger rebuilding of quads after creating a new DecorationShadow</li>
</ul>
<h3> KSysGuard </h3><ul>
<li>Port to new connect api</li>
<li>Port to qcommandlineparser</li>
</ul>
<h3> Muon </h3><ul>
<li>use the static singleshot helper to delay-init the app notifier backend</li>
<li>Remove unneeded&wrong size specification</li>
<li>Add a minimum size to the notifier plasmoid</li>
<li>Fetch updates when network state changes as well.</li>
<li>Fix updates count on PackageKit backend's notifier</li>
<li>Add consistency between the List and the Grid</li>
<li>Add a warning on the console whenever an error happened</li>
<li>Fix mimetype filtering</li>
<li>Provide the mimetypes for appstream resources as well.</li>
<li>Remove jacknjoe featured source</li>
<li>Only advertise as canExecute if the executables could be found</li>
<li>Don't fetch updates details if there's no details to find</li>
<li>Improve error detection</li>
<li>Disable sources button if no sources available</li>
<li>Remove unused code</li>
<li>Fix last updates time report in muon-updater</li>
<li>Re-populate the updates model if the state of a resource changes</li>
<li>Make the installed and available versions subject to state changes</li>
<li>Played around with the Dummy backend</li>
<li>Nitpicking</li>
</ul>
<h3> Plasma Workspace </h3><ul>
<li>Set aboutData for plasmashell</li>
<li>[digital-clock] Invert the JS timezone offset values</li>
<li>[digital-clock] Improve the scrolling on the applet</li>
<li>add include(ECMOptionalAddSubdirectory)</li>
<li>use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete</li>
<li>Don't show "Unknown" as author in wallpaper tooltip</li>
<li>Offer editing only for text entries</li>
<li>properly propagate info and error messages from kcheckpass and PAM</li>
<li>manage applet destruction</li>
<li>queue connection to show the alternatives dialog</li>
<li>sometimes a service from a query is invalid</li>
<li>delete containments before mass deleting views</li>
<li>make sure activtyId is migrated</li>
<li>support 22x22 icons too</li>
<li>update availableScreenRegion when deleting a panel</li>
<li>[screenlocker] Use KAuthorized::authorizeKAction for lock_screen</li>
<li>Treat normal selected timezone as local if it matches the local</li>
<li>set the right widget theme</li>
<li>fix total system freeze on some systems like my 10 inch netbook thanks Thomas Lübking for providing the patch BUG: 340294</li>
<li>Sanitize whitespace in notification popups</li>
<li>Prevent notifications from accessing the network</li>
<li>Convert to qDebug</li>
<li>Remove not necessary kdelibs4 include</li>
<li>Port to qCDebug</li>
<li>Don't let notification popup accept focus</li>
<li>Continuously update notification timestamp</li>
<li>Handle appdir directory structures in icon paths</li>
<li>Simplify and fix IconThemePath in StatusNotifierItems</li>
<li>show busy widget when needed</li>
<li>plasmashell crash reports go to plasmashell->general</li>
<li>Fix kscreenlocker not creating sessions</li>
<li>[ksld/greeter] Disable all IM modules</li>
<li>Qalculate Engine: Increase the precision</li>
<li>Slightly taller battery popup dialog</li>
<li>Fix warnings</li>
</ul>
<h3> Milou </h3><ul>
<li>Elide category names</li>
</ul>
<h3> KDE Plasma Addons </h3><ul>
<li>trigger on plasmoid activated</li>
</ul>
<h3> kio-extras </h3><ul>
<li>add include(ECMOptionalAddSubdirectory)</li>
<li>use ecm_optional_add_subdirectory instead of add_subdirectory for docs, the translations may be incomplete</li>
</ul>

<?php
  include("footer.inc");
?>
