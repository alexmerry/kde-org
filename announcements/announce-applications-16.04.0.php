<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships KDE Applications 16.04.0");
  $site_root = "../";
  $version = "16.04.0";
  $release = "applications-".$version;
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("April 20, 2016. KDE introduces today KDE Applications 16.04 with an impressive array of upgrades when it comes to better ease of access, the introduction of highly useful functionalities and getting rid of those minor issues which lead to KDE Applications now getting a step closer to offering you the perfect setup for your device.");?>
</p>

<p align="justify">
<?php print i18n_var("<a href='%1'>KColorChooser</a>, <a href='%2'>KFloppy</a>, <a href='%3'>KMahjongg</a> and <a href='%4'>KRDC</a> have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release. We would also highly encourage your support for <a href='%5'>Minuet</a> as we welcome it to our KDE Applications and your input on what more you'd like to see.", "https://www.kde.org/applications/graphics/kcolorchooser/", "https://www.kde.org/applications/utilities/kfloppy/", "https://www.kde.org/applications/games/kmahjongg/", "https://www.kde.org/applications/internet/krdc/", "https://www.kde.org/applications/education/minuet/");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("KDE's Newest Addition");?></h3>

<figure style="float: right; margin: 0px"><a href="minuet1604.png"><img src="minuet1604.png" width="400" height="225" /></a></figure>

<p align="justify">
<?php print i18n_var("A new application has been added to the KDE Education suite. <a href='%1'>Minuet</a> is a Music Education Software featuring full MIDI support with tempo, pitch and volume control, which makes it suitable for both novice and experienced musicians.", "https://minuet.kde.org");?>
</p>
<p align="justify">
<?php print i18n_var("Minuet includes 44 ear-training exercises about scales, chords, intervals and rhythm, enables the visualization of musical content on the piano keyboard and allows for the seamless integration of your own exercises.");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("More Help to you");?></h3>

<figure style="float: right; margin: 0px"><a href="https://bugs.kde.org/reports.cgi?product=khelpcenter&datasets=UNCONFIRMED&datasets=CONFIRMED&datasets=ASSIGNED&datasets=REOPENED&datasets=NEEDSINFO"><img src="khelpcenter1604.png" width="400" height="300" /></a></figure>

<p align="justify">
<?php print i18n_var("KHelpCenter which was previously distributed under Plasma is now distributed as a part of KDE Applications.");?>
</p>
<p align="justify">
<?php print i18n_var("A massive bug triaging and cleanup campaign taken up by the KHelpCenter team resulted in 49 resolved bugs, many of which were related to improving and restoring the previously non-functional search functionality.");?>
</p>
<p align="justify">
<?php print i18n_var("The internal document search which relied on the deprecated software ht::/dig has been replaced with a new Xapian-based indexing and search system which leads to the restoring of functionalities such as searching within man pages, info pages and KDE Software provided Documentation, with the added provision of bookmarks for the documentation pages.");?>
</p>
<p align="justify">
<?php print i18n_var("With the removal of KDELibs4 Support and a further cleanup of the code and some other minor bugs, the maintenance of the code has also been thoroughly furnished to give you KHelpCenter in a newly shiny form.");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Aggressive Pest Control");?></h3>
<p align="justify">
<?php print i18n_var("The Kontact Suite had a whopping 55 bugs resolved; some of which were related to issues with setting alarms, and in the import of Thunderbird mails, downsizing Skype &amp; Google talk icons in the Contacts Panel View, KMail related workarounds such as folder imports, vCard imports, opening ODF mail attachments, URL inserts from Chromium, the tool menu differences with the app started as a part of Kontact as opposed to a standalone use, missing 'Send' menu item and a few others. The support for Brazilian Portuguese RSS feeds has been added along with the fixing of the addresses for the Hungarian and Spanish feeds.");?>
</p>
<p align="justify">
<?php print i18n_var("The new features include a redesign of the KAddressbook contact editor, a new default KMail Header theme, improvements to the Settings Exporter and a fix of Favicon support in Akregator. The KMail composer interface has been cleaned up along with the introduction of a new Default KMail Header Theme with the Grantlee theme used for the 'About' page in KMail as well as Kontact and Akregator. Akregator now uses QtWebKit - one of the major engines to render webpages and execute javascript code as the renderer web engine and the process of implementing support for QtWebEngine in Akregator and other Kontact Suite applications has already begun. While several features were shifted as plugins in kdepim-addons, the pim libraries were split into numerous packages.");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("Archiving in Depth");?></h3>

<figure style="float: right; margin: 0px"><a href="ark1604.png"><img src="ark1604.png" width="400" height="205" /></a></figure>

<p align="justify">
<?php print i18n_var("<a href='%1'>Ark</a>, the archive manager, has had two major bugs fixed so that now Ark warns the user if an extraction fails due to lack of sufficient space in the destination folder and it does not fill up the RAM during the extraction of huge files via drag and drop.", "https://www.kde.org/applications/utilities/ark/");?>
</p>
<p align="justify">
<?php print i18n_var("Ark now also includes a properties dialog which displays information like the type of archive, compressed and uncompressed size, MD5/SHA-1/SHA-256 cryptographic hashes about the currently opened archive.");?>
</p>
<p align="justify">
<?php print i18n_var("Ark can also now open and extract RAR archives without utilizing the non-free rar utilities and can open, extract and create TAR archives compressed with the lzop/lzip/lrzip formats.");?>
</p>
<p align="justify">
<?php print i18n_var("The User Interface of Ark has been polished by reorganizing the menubar and the toolbar so as to improve the usability and to remove ambiguities, as well as to save vertical space thanks to the status-bar now hidden by default.");?>
</p>


<h3 style="clear:both;" ><?php print i18n_var("More Precision to Video Edits");?></h3>

<figure style="float: right; margin: 0px"><a href="kdenlive1604.png"><img src="kdenlive1604.png" width="400" height="268" /></a></figure>

<p align="justify">
<?php print i18n_var("<a href='%1'>Kdenlive</a>, the non-linear video editor has numerous new features implemented. The titler now has a grid feature, gradients, the addition of text shadow and adjusting letter/line spacing.", "https://www.kde.org/applications/multimedia/kdenlive/");?>
</p>
<p align="justify">
<?php print i18n_var("Integrated display of audio levels allows easy monitoring of the audio in your project alongwith new video monitor overlays displaying playback framerate, safe zones and audio waveforms and a toolbar to seek to markers and a zoom monitor.");?>
</p>
<p align="justify">
<?php print i18n_var("There has also been a new library feature which allows to copy/paste sequences between projects and a split view in timeline to compare and visualise the effects applied to the clip with the one without them.");?>
</p>
<p align="justify">
<?php print i18n_var("The render dialog has been rewritten with an added option to get faster encoding hence, producing large files and the speed effect has been made to work with sound as well.");?>
</p>
<p align="justify">
<?php print i18n_var("Curves in keyframes have been introduced for a few effects and now your top chosen effects can be accessed quickly via the favourite effects widget. While using the razor tool, now the vertical line in the timeline will show the accurate frame where the cut will be performed and the newly added clip generators will enable you to create color bar clips and counters. Besides this, the clip usage count has been re-introduced in the Project Bin and the audio thumbnails have also been rewritten to make them much faster.");?>
</p>
<p align="justify">
<?php print i18n_var("The major bug fixes include the crash while using titles (which requires the latest MLT version), fixing corruptions occurring when the project frames per second rate is anything other than 25, the crashes on track deletion, the problematic overwrite mode in the timeline and the corruptions/lost effects while using a locale with a comma as the separator. Apart from these, the team has been consistently working to make major improvements in stability, workflow and small usability features.");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("And more!");?></h3>

<p align="justify">
<?php print i18n_var("<a href='%1'>Okular</a>, the document viewer brings in new features in the form of customizable inline annotation width border, allowance of directly opening embedded files instead of only saving them and also the display of a table of content markers when the child links are collapsed.", "https://www.kde.org/applications/graphics/okular/");?>
</p>

<p style="clear:both;" align="justify">
<?php print i18n_var("<a href='%1'>Kleopatra</a> introduced improved support for GnuPG 2.1 with a fixing of selftest errors and the cumbersome key refreshes caused by the new GnuPG (GNU Privacy Guard) directory layout. ECC Key generation has been added with the display of Curve details for ECC certificates now. Kleopatra is now released as a separate package and support for .pfx and .crt files has been included. To resolve the difference in the behaviour of imported secret keys and generated keys, Kleopatra now allows you to set the owner trust to ultimate for imported secret keys. ", "https://www.kde.org/applications/utilities/kleopatra");?>
</p>


<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications.php?version=".$version);?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications 16.04 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%1'>support the nonprofit organization behind the KDE community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 16.04 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 16.04 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 16.04 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 16.04");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 16.04 may be <a href='http://download.kde.org/stable/applications/16.04.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-16.04.0.php'>KDE Applications 16.04.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
