<?php
  $page_title = "KDE 3.2.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE March 09, 2004</p>
<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships First Translation and Service Release for Leading Open Source Desktop
</h3>

<p align="justify">
  <strong>
    KDE Project Ships First Translation and Service Release of the 3.2 Generation
    GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling
    Free and Open Desktop Solution
  </strong>
</p>

<p align="justify">
  March 09, 2004 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.2.1,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes.  KDE 3.2.1
  ships with a basic desktop and eighteen other packages
  (PIM, administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in <strong>49 languages</strong> (now including
  Bengali, Icelandic, Japanese, Lithuanian, Low Saxon, Latin Serbian and Tajik).
</p>

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em><strong>for free</strong></em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from
  <a href="http://download.kde.org/stable/3.2.1/">http://download.kde.org</a> and can
  also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
  or with any of the major GNU/Linux - UNIX systems shipping today.
</p>

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
  KDE 3.2.1 is a maintenance release which provides corrections of problems
  reported using the KDE <a href="http://bugs.kde.org/">bug tracking system</a>
  and greatly enhanced support for existing translations and new translations
  (including 6 more languages meeting the high release criteria).
</p>
<p align="justify">
  For a more detailed list of improvements since the KDE 3.2 release in early
  February, please refer to the
  <a href="changelogs/changelog3_2_0_to_3_2_1.php">KDE 3.2.1 Changelog</a>.
</p>
<p>
  Additional information about the enhancements of the KDE 3.2.x release
  series is available in the
  <a href="announce-3.2.php">KDE 3.2 Announcement</a>.
</p>

<h4>
  Installing KDE 3.2.1 Binary Packages
</h4>
<p align="justify">
  <em>Packaging Policies</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.2.1 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.1/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="http://www.kde.org/info/3.2.1.php">KDE 3.2.1 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.2.1
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.2.1 may be
  <a href="http://download.kde.org/stable/3.2.1/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.2.1
  are available from the <a href="/info/3.2.1.php#binary">KDE
  3.2.1 Info Page</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
  <a href="http://www.trolltech.com/">Trolltech</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion,
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="&#x6d;&#97;il&#116;o:i&#110;fo&#x2d;afri&#99;&#x61;&#64;&#107;de.o&#00114;g">&#x69;nfo-af&#114;ica&#x40;kd&#101;&#0046;&#111;&#x72;&#0103;</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="m&#97;&#105;&#108;t&#111;:&#x69;n&#00102;&#x6f;-a&#x73;ia&#64;&#0107;d&#101;&#46;&#111;r&#103;">&#105;&#110;f&#x6f;&#045;&#97;&#x73;ia&#00064;k&#100;&#101;.&#x6f;&#114;&#x67;</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="&#109;&#0097;ilto:&#105;n&#102;o-europe&#x40;k&#x64;e.&#x6f;rg">i&#x6e;&#00102;o-&#00101;&#x75;&#x72;&#111;p&#x65;&#x40;kde&#x2e;o&#114;g</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="ma&#00105;l&#0116;&#0111;&#x3a;i&#0110;fo&#45;n&#00111;&#x72;&#00116;h&#97;m&#101;&#114;i&#0099;a&#64;&#x6b;&#0100;&#0101;.&#0111;r&#x67;">i&#x6e;&#00102;&#x6f;&#x2d;no&#x72;th&#00097;&#00109;e&#00114;ica&#x40;kd&#x65;.&#x6f;rg</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="m&#97;&#105;lt&#x6f;&#58;&#x69;n&#x66;&#x6f;-oc&#x65;&#97;nia&#64;k&#x64;e&#x2e;&#x6f;rg">inf&#111;&#45;o&#0099;&#0101;a&#x6e;i&#x61;&#64;&#x6b;&#0100;&#101;.&#111;&#114;&#103;</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="mai&#108;to&#058;info-so&#00117;&#116;&#104;&#x61;m&#101;&#x72;i&#0099;a&#64;&#x6b;d&#00101;.&#x6f;&#x72;g">&#x69;&#x6e;&#102;o-so&#117;th&#097;me&#0114;ica&#64;kd&#101;.o&#x72;&#103;</a><br />
</td>


</tr></table>

<?php

  include("footer.inc");
?>
