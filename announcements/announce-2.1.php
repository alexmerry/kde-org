<?php
  $page_title = "KDE 2.1.2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<P>DATELINE FEBRUARY 26, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New KDE Desktop Ready for the Enterprise</H3>
<P><STRONG>KDE Ships Leading Desktop with Advanced Web Browser for Linux
and Other UNIXes</STRONG></P>
<P>February 26, 2001 (The INTERNET).
The <A href="/">KDE
Project</A> today announced the release of KDE 2.1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <A HREF="http://konqueror.kde.org/">Konqueror</A>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<A HREF="http://www.kdevelop.org/">KDevelop</A>,
an advanced IDE, as a central component of KDE's powerful
development environment.
This release marks a leap forward in Linux desktop stability, usability
and maturity and is suitable for enterprise deployment.
The KDE Project strongly encourages all users to upgrade to KDE 2.1.
</P>
<P>
KDE and all its components are available for free under
Open Source licenses from the KDE
<A HREF="http://ftp.kde.org/stable/2.1/distribution/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
KDE 2.1 is available in
<A HREF="http://i18n.kde.org/teams/distributed.html">33 languages</A> and
ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (administration, games,
graphics, multimedia, network, PIM and utilities).
</P>
<P>
"This second major release of the KDE 2 series is a real improvement in
terms of stability, performance
and features," said David Faure, release manager for KDE 2.1 and
KDE Representative at
<A HREF="http://www.mandrakesoft.com/">Mandrakesoft</A>.
"KDE 2 has
now matured into a solid, intuitive and complete desktop for daily use.
Konqueror is a full-featured and robust web browser
and important applications like the mail client (KMail) have greatly
improved.
The multimedia architecture has made great strides and
this release inaugurates the new media player noatun,
which has a modular, plugin design for playing the latest audio and
video formats.
For development, KDE 2.1 for the first time is bundled
with KDevelop, an outstanding IDE/RAD which will be comfortably familiar to
developers with Windows development backgrounds.
In short, KDE 2.1
is a state-of-the-art desktop and development environment,
and positions Linux/Unix to make significant inroads in the home and
enterprise."
</P>
<P>
"KDE 2.1 opens the door to widespread adoption of the Linux desktop
and will help provide the success on the desktop that Linux already
enjoys in the server space," added Dirk Hohndel, CTO of
<A HREF="http://www.suse.com/">Suse AG</A>.
"With its intuitive
interface, code maturity and excellent development tools and environment, I am
confident that enterprises and third party developers will realize
the enormous potential KDE offers and will migrate their workstations
and applications to Linux/KDE."
</P>
<P>
"KDE boasts an outstanding graphical design and robust functionality," said
Sheila Harnett, Senior Technical Staff Member for IBM's Linux Technology
Center.
"KDE 2.1 significantly raises the bar for Linux desktop
functionality, usability and quality in virtually every aspect of the
desktop."
</P>
<P>
<STRONG><EM>KDE 2:  The K Desktop Environment</EM></STRONG>.
<A NAME="Konqueror"></A><A HREF="http://konqueror.kde.org/">Konqueror</A>
is KDE 2's next-generation web browser,
file manager and document viewer.
The standards-compliant
Konqueror has a component-based architecture which combines the features and
functionality of Internet Explorer/Netscape
Communicator and Windows Explorer.
Konqueror supports the full gamut of current Internet technologies,
including JavaScript, Java, XML, HTML 4.0, CSS-1 and -2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator plug-ins (for Flash, RealAudio, RealVideo
and similar technologies).
</P>
<P>
In addition, KDE offers seamless network transparency for accessing
or browsing files on Linux, NFS shares, MS Windows
SMB shares, HTTP pages, FTP directories, LDAP directories and audio CDs.
The modular,
plug-in nature of KDE's file architecture makes it simple to add additional
protocols (such as IPX, WebDAV or digital cameras) to KDE, which would
then automatically be available to all KDE applications.
</P>
<P>
KDE's <A NAME="customizability">configurability and customizability</A>
touches every aspect of the desktop.
KDE offers a unique cascading
customization feature where customization settings are inherited through
different layers, from global to per-user, permitting enterprise-wide
and group-based configurations.
KDE's sophisticated theme support
starts with Qt's style engine, which permits developers and artists to
create their own widget designs.
KDE 2.1 ships with over 14 of these styles,
some of which emulate the look of various operating systems.
Additionally
KDE includes a new theme manager and does an excellent job of
importing themes from GTK and GNOME.
Moreover, KDE 2 fully
supports Unicode and KHTML is the only free HTML rendering engine on
Linux/X11 that features nascent support for BiDi scripts
such as Arabic and Hebrew.
</P>
<P>
KDE 2 features an advanced, network-transparent multimedia architecture
based on <A NAME="arts">aRts</A>, the Analog Realtime Synthesizer.
ARts is a full-featured sound system which
includes a number of effects and filters, a modular analog synthesizer
and a mixer.
The aRts sound server provides network transparent sound support for
both input and output using MCOP, a CORBA-like network design, enabling
applications running on remote computers to output sound and receive
input from the local workstation.
This architecture provides a much-needed complement
to the network transparency provided by X and for the first time permits
users to run their applications remotely with sound enabled.
Moreover, aRts enables multiple applications (local or remote) to
output sound and/or video concurrently.
Video support is <a
href="http://mpeglib.sourceforge.net/">available</a> for MPEG versions
1, 2 and 4 (experimental), as well as the AVI and DivX formats.
Using the aRts component technology, it is very easy to develop
multimedia applications.
</P>
<P>
Besides the exceptional compliance with Internet and file-sharing standards
<A HREF="#Konqueror">mentioned above</A>, KDE 2 is a leader in
compliance with the available Linux desktop standards.
KWin, KDE's new
re-engineered window manager, complies to the new
<A HREF="http://www.freedesktop.org/standards/wm-spec.html">Window Manager
Specification</A>.
Konqueror and KDE comply with the <A
HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/kio/DESKTOP_ENTRY_STANDARD">Desktop
Entry Standard</A>.
Konqueror uses the
<A HREF="http://pyxml.sourceforge.net/topics/xbel/docs/html">XBEL</A>
standard for its bookmarks.
KDE 2 largely complies with the
<A HREF="http://www.newplanetsoftware.com/xdnd/">X Drag-and-Drop (XDND)
protocol</A> as well as with the
<A HREF="http://www.rzg.mpg.de/rzg/batch/NEC/sx4a_doc/g1ae04e/chap12.html">X11R6 session management protocol (XSMP)</A>.
</P>
<P>
<STRONG><EM>KDE 2:  The K Development Environment</EM></STRONG>.
KDE 2.1 offers developers a sophisticated IDE as well as a rich set
of major technological improvements over the critically acclaimed
KDE 1 series.
Chief among the technologies are
the <A HREF="#DCOP">Desktop COmmunication Protocol (DCOP)</A>, the
<A HREF="#KIO">I/O libraries (KIO)</A>, <A HREF="#KParts">the component
object model (KParts)</A>, an <A HREF="#XMLGUI">XML-based GUI class</A>, and 
a <A HREF="#KHTML">standards-compliant HTML rendering engine (KHTML)</A>.
</P>
<P>
KDevelop is a leading Linux IDE
with numerous features for rapid application
development, including a GUI dialog builder, integrated debugging, project
management, documentation and translation facilities, built-in concurrent
development support, and much more.
</P>
<P>
<A NAME="KParts">KParts</A>, KDE 2's proven component object model, handles
all aspects of application embedding, such as positioning toolbars and inserting
the proper menus when the embedded component is activated or deactivated.
KParts can also interface with the KIO trader to locate available handlers for
specific mimetypes or services/protocols.
This technology is used extensively by the
<A HREF="http://www.koffice.org/">KOffice</A> suite and Konqueror.
</P>
<P>
<A NAME="KIO">KIO</A> implements application I/O in a separate
process to enable a
non-blocking GUI without the use of threads.
The class is network and protocol transparent
and hence can be used seamlessly to access HTTP, FTP, POP, IMAP,
NFS, SMB, LDAP and local files.
Moreover, its modular
and extensible design permits developers to "drop in" additional protocols,
such as WebDAV, which will then automatically be available to all KDE
applications.
KIO also implements a trader which can locate handlers
for specified mimetypes; these handlers can then be embedded within
the requesting application using the KParts technology.
</P>
<P>
The <A NAME="XMLGUI">XML GUI</A> employs XML to create and position
menus, toolbars and possibly
other aspects of the GUI.
This technology offers developers and users
the advantage of simplified configurability of these user interface elements
across applications and automatic compliance with the
<A HREF="http://developer.kde.org/documentation/standards/">KDE Standards
and Style Guide</A> irrespective of modifications to the standards.
</P>
<P>
<A NAME="DCOP">DCOP</A> is a client-to-client communications
protocol intermediated by a
server over the standard X11 ICE library.
The protocol supports both
message passing and remote procedure calls using an XML-RPC to DCOP "gateway".
Bindings for C, C++ and Python, as well as experimental Java bindings, are
available.
</P>
<P>
<A NAME="KHTML">KHTML</A> is an HTML 4.0 compliant rendering
and drawing engine.
The class
will support the full gamut of current Internet technologies, including
JavaScript, Java, HTML 4.0, CSS-2
(Cascading Style Sheets), SSL (Secure Socket Layer for secure communications)
and Netscape Communicator plugins (for
viewing Flash,
RealAudio, RealVideo and similar technologies).
The KHTML class can easily
be used by an application as either a widget (using normal window
parenting) or as a component (using the KParts technology).
KHTML, in turn, has the capacity to embed components within itself
using the KParts technology.
</P>
<H4>Downloading and Compiling KDE 2.1</H4>
<P>
The source packages for KDE 2.1 are available for free download at
<A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.1/distribution/tar/generic/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
KDE 2.1 requires
qt-2.2.4, which is available in source code from Trolltech as
<A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</A>.
KDE 2.1 should work with Qt-2.2.3 but Qt-2.2.4 is recommended.
</P>
<P>
For further instructions on compiling and installing KDE 2.1, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<H4>Installing Binary Packages</H4>
<P>
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.
Some of these binary packages for KDE 2.1
will be available for free download under
<A HREF="http://ftp.kde.org/stable/2.1/distribution/">http://ftp.kde.org/stable/2.1/distribution/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>.
Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.
</P>
<P>KDE 2.1 requires qt-2.2.4, the free version of which is available
from the above locations usually under the name qt-x11-2.2.4.
KDE 2.1
should work with Qt-2.2.3 but Qt-2.2.4 is recommended.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI>Caldera eDesktop 2.4: <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/Caldera/eDesktop24/RPMS/">i386</A></LI>
<LI>Debian GNU/Linux:
<UL>
<LI>Potato (2.2):  <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-sparc/">Sparc</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/deb/dists/potato/main/binary-powerpc/">PPC</A></LI>
</UL>
<LI>Linux-Mandrake 7.2:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/Mandrake/7.2/i586/">i586</A></LI>
<LI>RedHat Linux:
<UL>
<LI>Wolverine:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/common/">common</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/alpha/">Alpha</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/common/">common</A> directory for common files</LI>
<LI>6.x:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/alpha/">Alpha</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/sparc/">Sparc</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/common/">common</A> directory for common files</LI>
</UL>
<LI>SuSE Linux:
<UL>
<LI>7.1:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.1-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.1-sparc/">Sparc</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.0-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/7.0-ppc/">PPC</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/6.4-i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
<LI>6.3:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/6.3-i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/i18n/">i18n</A> or <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/SuSE/noarch/">noarch</A> directory for common files</LI>
</LI>
</UL>


<LI><A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/Tru64/">Tru64 Systems</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/FreeBSD/">FreeBSD</A></LI>
</UL> 
<P>
Please check the servers periodically for pre-compiled packages for other
distributions.
More binary packages will become available over the
coming days and weeks.
</P>
<H4>What Others Are Saying</H4>
<P>KDE 2.1 has already earned accolades from industry leaders worldwide.
A sampling of comments follows.
</P>
<P>
"We welcome the release of KDE 2.1," stated Dr. Markus Draeger, Senior
Manager for Partner Relations at Fujitsu Siemens Computers. "The release
introduces several important new components, like KDevelop and the media
player noatun, and overall is a major step forward for this leading GUI
on Linux."
</P>
<P>
"We are very excited about the enhancements in KDE 2.1 and we are pleased
to be able to contribute to the project," said Rene Schmidt, Corel's
Executive Vice-President, Linux Products. "KDE continues to improve with
each release, and these enhancements will make our easy-to-use Linux
distribution for the desktop even better."
</P>
<P>
"A greater number and availability of Linux applications is an important
factor that will determine if Linux permeates the enterprise desktop,"
said Drew Spencer, Chief Technology Officer for Caldera Systems, Inc.
"KDE 2.1 addresses this issue with the integration of the Konqueror
browser and KDevelop, a tool that allows developers to create
applications in C++ for all kinds of environments.  Together with the
existing tools available for KDE, KDevelop is a one-stop solution for
developers."
</P>
<P>
"With the 2.1 release, KDE again demonstrates its capacity to offer rich
software and provide a complete and stable environment for everyday use",
added Ga&euml;l Duval, co-founder of Mandrakesoft. "This latest release
has paved the way for KDE on user's desktops in the enterprise as well
as at home.  From the full-featured web browser to the friendly
configuration center, it provides all the common facilities many
computers users need to abandon Windows&reg; entirely."
</P>
<H4>About KDE</H4>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
More information about KDE 2 is available in two
(<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</A>,
<A HREF="http://mandrakesoft.com/~david/OSDEM/">2</A>) slideshow 
presentations and on
<A href="/">KDE's web site</A>, including an evolving
<A HREF="http://www.kde.org/info/2.1.html">FAQ</A> to answer questions about
migrating to KDE 2.1 from KDE 1.x, a number of
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>, <A HREF="http://developer.kde.org/documentation/kde2arch.html">developer information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
MS Windows, Internet Explorer and Windows Explorer are trademarks or registered
trademarks of Microsoft Corporation.
Netscape and Netscape Communicator are trademarks or registered trademarks of Netscape Communications Corporation in the United States and other countries and JavaScript is a trademark of Netscape Communications Corporation.
Java is a trademark of Sun Microsystems, Inc.
Flash is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
RealAudio and RealVideo are trademarks or registered trademarks of RealNetworks, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
g&#114;an&#x72;oth&#0064;k&#0100;&#x65;.&#111;r&#103;<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
p&#00111;&#00117;&#114;&#x40;kd&#101;&#x2e;&#111;&#x72;g<BR>
(1) 718 456 1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
f&#97;ure&#00064;k&#x64;&#0101;.&#00111;rg<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
konol&#100;&#x40;k&#x64;&#x65;.&#0111;r&#00103;<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
<?php
  include "footer.inc"
?>
