<?php
  $page_title = "The Developer's Perspective: Supporting KDE";
  $site_root = "../../";
  include "header.inc";
?>

<p>Being a software project the KDE project naturally has a steady need of developers. If you are a programmer, preferably with knowledge of at least C++ and Qt, there are many ways to support KDE. In addition to <a href="../announce-3.2_users/supporting.html">all the things that any user can do</a> you may be able to contribute in one or more of the following ways:
</p>

  <h3><a name="Patches">Patches</a></h3>

<p>
KDE's Bug Tracking System is a good starting point for any programmer looking for relatively small works. Especially check out the "most hated bugs" and "most wanted features" links there in the "Bug Hunting Tools" section. Small bugs and feature requests give the opportunity to write small patches and posting them in the respective reports. There are also several web-based systems giving easy access to the necessary source code.
</p>
<p> [ <a href="http://bugs.kde.org/">Visit KDE's Bug Tracking System</a> | <a href="http://lxr.kde.org/">Visit KDE Source Cross Reference</a> | <a href="http://webcvs.kde.org/">Visit KDE CVS Repository</a> ]</p>

  <h3><a name="Code">Code</a></h3>

<p>
When you want to do some serious programming in KDE you can rely on a huge library of information. KDE Developer's Corner is the traditional place for KDE development related information.
</p>
<p> [ <a href="http://developer.kde.org/documentation/index.html">Visit KDE Developer's Corner Documentation</a> ]</p>

  <h3><a name="Maintenance">Maintenance</a></h3>

<p>
There is a list of KDE applications open for maintainance at the Open Job page for those who are interested and have the necessary skills.
</p>
<p> [ <a href="http://www.kde.org/jobs/open.php">Visit Open Jobs</a> ]</p>


<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../announce-3.2_users/whykde.php">Why KDE?</a><br/>
<a href="../announce-3.2_users/deploying.php">Deploying KDE</a><br/>
<a href="../announce-3.2_users/supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="whykde.php">Why KDE?</a><br/>
<a href="developing.php">Developing With KDE</a><br/>
<a href="supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="http://www.kde.org">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>

<?php
  include("footer.inc");
?>
