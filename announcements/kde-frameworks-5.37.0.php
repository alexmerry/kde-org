<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.37.0");
  $site_root = "../";
  $release = '5.37.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
August 13, 2017. KDE today announces the release
of KDE Frameworks 5.37.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("New framework: kirigami, a set of QtQuick plugins to build user interfaces based on the KDE UX guidelines");?></h3>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("update .h and .h++ colors (bug 376680)");?></li>
<li><?php i18n("remove ktorrent small monochrome icon (bug 381370)");?></li>
<li><?php i18n("bookmarks is an action icon not a folder icon (bug 381383)");?></li>
<li><?php i18n("update utilities-system-monitor (bug 381420)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Add --gradle to androiddeployqt");?></li>
<li><?php i18n("Fix install apk target");?></li>
<li><?php i18n("Fix usage of query_qmake: differ between calls expecting qmake or not");?></li>
<li><?php i18n("Add API dox for KDEInstallDirs' KDE_INSTALL_USE_QT_SYS_PATHS");?></li>
<li><?php i18n("Add a metainfo.yaml to make ECM a proper framework");?></li>
<li><?php i18n("Android: scan for qml files in the source dir, not in the install dir");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("emit runningActivityListChanged on activity creation");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Escape HTML from search query");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Add Conan files, as a first experiment to Conan support");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Allow to build KConfig without Qt5Gui");?></li>
<li><?php i18n("Standard shortcuts: use Ctrl+PageUp/PageDown for prev/next tab");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Remove unused init() declaration from K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL");?></li>
<li><?php i18n("New spdx API on KAboutLicense to get SPDX license expressions");?></li>
<li><?php i18n("kdirwatch: Avoid potential crash if d-ptr destroyed before KDirWatch (bug 381583)");?></li>
<li><?php i18n("Fix display of formatDuration with rounding (bug 382069)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("fix plasmashell unsetting QSG_RENDER_LOOP");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix 'Deprecated hint for KUrl::path() is wrong on Windows' (bug 382242)");?></li>
<li><?php i18n("Update kdelibs4support to use the target based support provided by kdewin");?></li>
<li><?php i18n("Mark constructors as deprecated too");?></li>
<li><?php i18n("Sync KDE4Defaults.cmake from kdelibs");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("Add support for the new widget kpasswordlineedit");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Support SVG too (bug 355872)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Allow loading i18n catalogs from arbitrary locations");?></li>
<li><?php i18n("Make sure that the tsfiles target is generated");?></li>
</ul>

<h3><?php i18n("KIdleTime");?></h3>

<ul>
<li><?php i18n("Only require Qt5X11Extras when we actually need it");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Use proper feature flag to include kill(2)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Add new method urlSelectionRequested to KUrlNavigator");?></li>
<li><?php i18n("KUrlNavigator: expose the KUrlNavigatorButton that received a drop event");?></li>
<li><?php i18n("Stash without asking the user with a Copy/Cancel popup");?></li>
<li><?php i18n("Ensure KDirLister updates items whose target URL has changed (bug 382341)");?></li>
<li><?php i18n("Make advanced options of \"open with\" dialog collabsible and hidden by default (bug 359233)");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Give a parent to KMoreToolsMenuFactory menus");?></li>
<li><?php i18n("When requesting from the cache, report all entries at bulk");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("kpackagetool now can output appstream data to a file");?></li>
<li><?php i18n("adopt new KAboutLicense::spdx");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Reset url in closeUrl()");?></li>
<li><?php i18n("Add template for a simple kpart-based application");?></li>
<li><?php i18n("Drop usage of KDE_DEFAULT_WINDOWFLAGS");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Handle fine-grained wheel event in zooming");?></li>
<li><?php i18n("Add template for a ktexteditor plugin");?></li>
<li><?php i18n("copy permissions from original file on save copy (bug 377373)");?></li>
<li><?php i18n("perhaps avoid stringbuild crash (bug 339627)");?></li>
<li><?php i18n("fix problem with * adding for lines outside of comments (bug 360456)");?></li>
<li><?php i18n("fix save as copy, it missed to allow overwriting the destination file (bug 368145)");?></li>
<li><?php i18n("Command 'set-highlight': Join args with space");?></li>
<li><?php i18n("fix crash on view destruction because of non-deterministic cleanup of objects");?></li>
<li><?php i18n("Emit signals from icon border when no mark was clicked");?></li>
<li><?php i18n("Fix crash in vi input mode (sequence: \"o\" \"Esc\" \"O\" \"Esc\" \".\") (bug 377852)");?></li>
<li><?php i18n("Use mutually exclusive group in Default Mark Type");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Mark MPa and PSI as common units");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Use CMAKE_INSTALL_BINDIR for dbus service generation");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Destroy all kwayland objects created by registry when it is destroyed");?></li>
<li><?php i18n("Emit connectionDied if the QPA is destroyed");?></li>
<li><?php i18n("[client] Track all created ConnectionThreads and add API to access them");?></li>
<li><?php i18n("[server] Send text input leave if focused surface gets unbound");?></li>
<li><?php i18n("[server] Send pointer leave if focused surface gets unbound");?></li>
<li><?php i18n("[client] Properly track enteredSurface in Keyboard");?></li>
<li><?php i18n("[server] Send keyboard leave when client destroys the focused surface (bug 382280)");?></li>
<li><?php i18n("check Buffer validity (bug 381953)");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Extract lineedit password widget =&gt; new class KPasswordLineEdit");?></li>
<li><?php i18n("Fixed a crash when searching with accessibility support enabled (bug 374933)");?></li>
<li><?php i18n("[KPageListViewDelegate] Pass widget to drawPrimitive in drawFocus");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Remove header reliance on QWidget");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Drop usage of KDE_DEFAULT_WINDOWFLAGS");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Adding support to ipv*.route-metric");?></li>
<li><?php i18n("Fix undefined NM_SETTING_WIRELESS_POWERSAVE_FOO enums (bug 382051)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Containment Interface] always emit contextualActionsAboutToShow for containment");?></li>
<li><?php i18n("Treat Button/ToolButton labels as plaintext");?></li>
<li><?php i18n("Don't perform wayland specific fixes when on X (bug 381130)");?></li>
<li><?php i18n("Add KF5WindowSystem to link interface");?></li>
<li><?php i18n("Declare AppManager.js as pragma library");?></li>
<li><?php i18n("[PlasmaComponents] Remove Config.js");?></li>
<li><?php i18n("default to plain text for labels");?></li>
<li><?php i18n("Load translations from KPackage files if bundled (bug 374825)");?></li>
<li><?php i18n("[PlasmaComponents Menu] Don't crash on null action");?></li>
<li><?php i18n("[Plasma Dialog] Fix flag conditions");?></li>
<li><?php i18n("update akregator system tray icon (bug 379861)");?></li>
<li><?php i18n("[Containment Interface] Keep containment in RequiresAttentionStatus while context menu is open (bug 351823)");?></li>
<li><?php i18n("Fix tab bar layout key handling in RTL (bug 379894)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Allow to build Sonnet without Qt5Widgets");?></li>
<li><?php i18n("cmake: rewrite FindHUNSPELL.cmake to use pkg-config");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Allow to build KSyntaxHighlighter without Qt5Gui");?></li>
<li><?php i18n("Add cross-compilation support for the highlighting indexer");?></li>
<li><?php i18n("Themes: Remove all unused metadata (license, author, read-only)");?></li>
<li><?php i18n("Theme: Remove license and author fields");?></li>
<li><?php i18n("Theme: Derive read-only flag from file on disk");?></li>
<li><?php i18n("Add syntax highlighting for YANG data modeling language");?></li>
<li><?php i18n("PHP: Add PHP 7 keywords (bug 356383)");?></li>
<li><?php i18n("PHP: Clean up PHP 5 information");?></li>
<li><?php i18n("fix gnuplot, make leading/trailing spaces fatal");?></li>
<li><?php i18n("fix 'else if' detection, we need to switch context, add extra rule");?></li>
<li><?php i18n("indexer checks for leading/trailing whitespaces in XML highlighting");?></li>
<li><?php i18n("Doxygen: Add Doxyfile highlighting");?></li>
<li><?php i18n("add missing standard types to C highlighting and update to C11 (bug 367798)");?></li>
<li><?php i18n("Q_PI D =&gt; Q_PID");?></li>
<li><?php i18n("PHP: Improve highlighting of variables in curly braces in double quotes (bug 382527)");?></li>
<li><?php i18n("Add PowerShell highlighting");?></li>
<li><?php i18n("Haskell: Add file extension .hs-boot (bootstrap module) (bug 354629)");?></li>
<li><?php i18n("Fix replaceCaptures() to work with more than 9 captures");?></li>
<li><?php i18n("Ruby: Use WordDetect instead of StringDetect for full word matching");?></li>
<li><?php i18n("Fix incorrect highlighting for BEGIN and END in words such as \"EXTENDED\" (bug 350709)");?></li>
<li><?php i18n("PHP: Remove mime_content_type() from list of deprecated functions (bug 371973)");?></li>
<li><?php i18n("XML: Add XBEL extension/mimetype to xml highlighting (bug 374573)");?></li>
<li><?php i18n("Bash: Fix incorrect highlighting for command options (bug 375245)");?></li>
<li><?php i18n("Perl: Fix heredoc highlighting with leading spaces in the delimiter (bug 379298)");?></li>
<li><?php i18n("Update SQL (Oracle) syntax file (bug 368755)");?></li>
<li><?php i18n("C++: Fix '-' is not a part of UDL String (bug 380408)");?></li>
<li><?php i18n("C++: printf format specifies: add 'n' and 'p', remove 'P' (bug 380409)");?></li>
<li><?php i18n("C++: Fix char value have the color of the strings (bug 380489)");?></li>
<li><?php i18n("VHDL: Fix highlighting error when using brackets and attributes (bug 368897)");?></li>
<li><?php i18n("zsh highlighting: Fix math expression in a substring expression (bug 380229)");?></li>
<li><?php i18n("JavaScript Highlighting: Add support for E4X xml extension (bug 373713)");?></li>
<li><?php i18n("Remove \"*.conf\" extension rule");?></li>
<li><?php i18n("Pug/Jade syntax");?></li>
</ul>

<h3><?php i18n("ThreadWeaver");?></h3>

<ul>
<li><?php i18n("Add missing export to QueueSignals");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.37");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
