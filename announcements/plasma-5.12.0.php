<?php
	include_once ("functions.inc");
	$translation_file = "kde-org";
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "KDE Plasma 5.12.0 LTS, Speed. Stability. Simplicity.",
		'cssFile' => '/content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = 'plasma-5.12.0'; // for i18n
	$version = "5.12.0";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    text-shadow: 2px 2px 5px light-grey;
}

</style>

<main class="releaseAnnouncment container">

	<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

	<?php include "./announce-i18n-bar.inc"; ?>

	<figure class="videoBlock">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/xha6DJ_v1E4?rel=0" allowfullscreen='true'></iframe>
	</figure>
	
	
	<figure class="topImage">
		<a href="plasma-5.12/plasma-5.12.png">
			<img src="https://www.kde.org/announcements/plasma-5.12/plasma-5.12.png" alt="Plasma 5.12" style="width: 100%">
		</a>
		<figcaption><?php print i18n_var("KDE Plasma %1", "5.12")?></figcaption>
	</figure>

	<p>
		<?php i18n("Tuesday, 06 February 2018.")?>
		<?php print i18n_var("Today KDE releases a %1 update to Plasma 5, versioned %2", "Feature", "5.12.0");?>.
	</p>

	<p>
		<?php print i18n_var("Plasma 5.12 LTS is the second long-term support release from the Plasma 5 team. We have been working hard, focusing on speed and stability for this release. Boot time to desktop has been improved by reviewing the code for anything which blocks execution. The team has been triaging and fixing bugs in every aspect of the codebase, tidying up artwork, removing corner cases, and ensuring cross-desktop integration. For the first time, we offer our Wayland integration on long-term support, so you can be sure we will continue to provide bug fixes and improvements to the Wayland experience.");?>
	</p>

	<br clear="all" />

<h2><?php i18n("New in Plasma 5.12 LTS");?></h2>

<br clear="all" />
<h3><?php i18n("Smoother and Speedier");?></h3>

<p><?php i18n("Speed and memory improvements are the focus of this long-term release. The new Plasma LTS uses less CPU and less memory than previous versions. The process of starting the Plasma desktop is now significantly faster.");?></p>

<br clear="all" />
<h3><?php i18n("New Features");?></h3>

<figure style="float: right; width: 360px;">
<a href="plasma-5.12/weather-applet.png">
<img src="plasma-5.12/weather-applet-wee.png" style="border: 0px" width="161" height="67" alt="<?php i18n("Weather Applet with Temperature");?>" />
</a>
<figcaption><?php i18n("Weather Applet with Temperature");?></figcaption>
<br />
<a href="plasma-5.12/system-monitor.png">
<img src="plasma-5.12/system-monitor-wee.png" style="border: 0px" width="350" height="125" alt="<?php i18n("CPU usage in System Activity");?>" />
</a>
<figcaption><?php i18n("CPU usage in System Monitor");?></figcaption>
<br />
<a href="plasma-5.12/window-shadows.png">
<img src="plasma-5.12/window-shadows-wee.png" style="border: 0px" width="360" height="164" alt="<?php i18n("Larger, horizontally-centered window shadows");?>" />
</a>
<figcaption><?php i18n("Larger, horizontally-centered window shadows");?></figcaption>
</figure>
<br />

<p><?php i18n("The list of new features in Plasma 5.12 LTS doesn't stop with improved performance. You can also look forward to the following:");?></p>

<ul>
<?php i18n("
<li>Wayland-only Night Color feature that lets you adjust the screen color temperature to reduce eye strain</li>
<li>Usability improvement for the global menu: adding a global menu panel or window decoration button enables it without needing an extra configuration step</li>
<li>KRunner can now be completely used with on-screen readers such as Orca</li>
<li>Notification text is selectable again and allows you to copy links from notifications</li>
<li>The weather applet can now show the temperature next to the weather status icon on the panel</li>
<li>Clock widget's text is now sized more appropriately</li>
<li>System Activity and System Monitor display per-process graphs for the CPU usage</li>
<li>Windows shadows are horizontally centered and larger by default</li>
<li>The Properties dialog now shows file metadata</li>
<li>The Icon applet now uses favicons for website shortcuts</li>
<li>The Kickoff application menu has an optimized layout</li>
");?>
</ul>

<br clear="all" />
<h3><?php i18n("Discover");?></h3>
<figure style="float: right; width: 300px;">
<a href="plasma-5.12/discover-app.png">
<img src="plasma-5.12/discover-app-wee.png" style="border: 0px" width="360" height="367" alt="<?php i18n("Discover's new app page");?>" />
</a>
<figcaption><?php i18n("Discover's new app page");?></figcaption>
</figure>

<p><?php i18n("We have made many improvements to the Discover user interface, starting with redesigned app pages to showcase all the amazing software you can install. Non-browsing sections of the interface now have leaner headers, and browsing views are more compact, allowing you to see more apps at once. The screenshots are bigger and support keyboard navigation. The list of installed apps is sorted alphabetically, and the interface for configuring sources has been polished.");?></p>

<p><?php i18n("Discover has also seen major improvements in stability, as well as in Snap and Flatpak support. It supports <tt>apt://</tt> URLs, and will notify you when a package requires a reboot after installation or update. Distributions using Discover can enable offline updates, and the distro upgrade feature can be used to get new distribution releases. Discover comes with better usability on phone form factors (uses Kirigami main action, and has a view specific for searching), and it integrates PackageKit global signals into notifications. All in all, the process of maintaining your software with Discover will feel much smoother.");?></p>

<br clear="all" />
<h3><?php i18n("Continuous Updates for Plasma on Wayland");?></h3>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/kscreen-wayland.png">
<img src="plasma-5.12/kscreen-wayland-wee.png" width="350" height="338" />
</a>
<figcaption><?php i18n("Display Setup Now Supports Wayland");?></figcaption>
</figure>
<p><?php i18n("Plasma's support for running on Wayland continues to improve, and it is now suitable for a wider range of testing. It is included as a Long Term Support feature for the first time, which means we will be fixing bugs throughout the entire 5.12 LTS series. New features include:");?></p>

<ul><?php i18n("
<li>Ability to set output resolution and enable/disable outputs through KScreen</li>
<li>Screen rotation</li>
<li>Automatic screen rotation based on orientation sensor</li>
<li>Automatic touch screen calibration</li>
<li>Fullscreen option for Wayland windows</li>
<li>Automatic selection of the Compositor based on the used platform</li>");?>
</ul>

<p><?php i18n("Plasma on Wayland cares about your vision, so an exclusive, Wayland-only feature called Night Color is also available. Night Color integrates with KWin and removes blue light from your screen at night-time, functioning as an equivalent of the great Redshift app that works on X.");?></p>

<p><?php i18n("We have also started implementing window rules on Wayland and made it possible to use real-time scheduling policy to keep the input responsive. XWayland is no longer required to run the Plasma desktop; however, the applications only supporting X still make use of it.");?></p>

<p><?php i18n("For those who know their Wayland internals, the protocols we added are:");?></p>

<ul><?php i18n("
<li>xdg_shell_unstable_v6</li>
<li>xdg_foreign_unstable_v2</li>
<li>idle_inhibit_unstable_v1</li>
<li>server_decoration_palette</li>
<li>appmenu</li>
<li>wl_data_device_manager raised to version 3</li>");?>
</ul>

<p><?php i18n("There is also an important change of policy: 5.12 is the last release which sees feature development in KWin on X11. With 5.13 onwards, only new features relevant to Wayland are going to be added.");?></p>

<p><?php i18n("We have put a lot of work into making Wayland support in Plasma as good as possible, but there are still some missing features and issues with particular hardware configurations. Therefore, we don't yet recommend it for daily use. More information is available on the  <a href='https://community.kde.org/Plasma/Wayland_Showstoppers'>Wayland status wiki page</a>.");?></p>

<br clear="all" />

<p><?php i18n("Browse the full Plasma 5.12 LTS changelog to find even more tweaks and bug fixes featured in this release: ");?><a href="plasma-5.11.5-5.12.0-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.12.0"); ?></a></p>

<br clear="all" />
<h2><?php i18n("What’s New Since Plasma 5.8 LTS");?></h2>

<p><?php i18n("If you have been using our previous LTS release, Plasma 5.8, you might notice some interesting changes when you upgrade to Plasma 5.12. All these great features are waiting for you:");?></p>

<br clear="all" />
<h2><?php i18n("Previews in Notifications");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/spectacle-notification.png">
<img src="plasma-5.12/spectacle-notification-wee.png" width="350" height="192" />
</a>
<figcaption><?php i18n("Preview in Notifications");?></figcaption>
</figure>

<p><?php i18n("The notification system gained support for interactive previews that allow you to quickly take a screenshot and drag it into a chat window, an email composer, or a web browser form. This way you never have to leave the application you’re currently working with.");?></p>  

<br clear="all" />
<h2><?php i18n("Task Manager Improvement");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/mute.png">
<img src="plasma-5.12/mute.png" width="340" height="399" />
</a>
<figcaption><?php i18n("Audio Icon and Mute Button Context Menu Entry");?></figcaption>
</figure>

<p><?php i18n("Due to popular demand, we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. Is some app making noise in the background while you are trying to focus on one task? Applications currently playing audio are now marked with an icon, similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.");?></p>

<br clear="all" />
<h2><?php i18n("Global Menus");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/global-menus-window-bar.png">
<img src="plasma-5.12/global-menus-window-bar-wee.png" width="350" height="258" />
</a>
<figcaption><?php i18n("Global Menu Screenshots, Applet and Window Decoration");?></figcaption>
</figure>

<p><?php i18n("Global Menus have returned. KDE's pioneering feature to separate the menu bar from the application window allows for a new user interface paradigm, with either a Plasma Widget showing the menu, or with the menu neatly tucked away in the window title bar. Setting it up has been greatly simplified in Plasma 5.12: as soon as you add the Global Menu widget or title bar button, the required background service gets started automatically. No need to reload the desktop or click any confirmation buttons!");?></p>

<br clear="all" />
<h2><?php i18n("Spring-Loaded Folders");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/spring_loading.gif">
<img src="plasma-5.12/spring_loading.gif" style="border: 0px" width="350" height="192" alt="<?php i18n("Spring Loading in Folder View");?>" />
</a>
<figcaption><?php i18n("Spring Loading in Folder View");?></figcaption>
</figure>

<p><?php i18n("Folder View is now the default desktop layout. After some years of shunning icons on the desktop, we have accepted the inevitable and changed to Folder View as the default desktop. This brings some icons to the desktop by default, and allows you to put whatever files or folders you want on it for easy access. To make this work, many improvements have been made to Folder View. Spring Loading makes drag-and- dropping files powerful and quick, and there is a tighter icon grid, along with massively improved performance.");?></p>

<br clear="all" />
<h2><?php i18n("Music Controls in Lock Screen");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/lock-music.png">
<img src="plasma-5.12/lock-music-wee.png" width="350" height="329" />
</a>
<figcaption><?php i18n("Media Controls on Lock Screen");?></figcaption>
</figure>

<p><?php i18n("Media controls have been added to the lock screen. For added privacy, they can be disabled in Plasma 5.12. Moreover, music will automatically pause when the system suspends.");?></p>

<br clear="all" />
<h2><?php i18n("New System Settings Interface");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/system-settings.png">
<img src="plasma-5.12/system-settings-wee.png" width="350" height="302" />
</a>
<figcaption><?php i18n("New Look System Settings");?></figcaption>
</figure>

<p><?php i18n("We introduced a new System Settings user interface for easy access to commonly used settings. It is the first step in making this often-used and complex application easier to navigate and more user-friendly. The new design is added as an option, so users who prefer the older icon or tree views can move back to their preferred way of navigation.");?></p>

<br clear="all" />
<h2><?php i18n("Plasma Vaults");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.12/plasma-vault.png">
<img src="plasma-5.12/plasma-vault-wee.png" width="350" height="331" />
</a>
<figcaption><?php i18n("Plasma Vaults");?></figcaption>
</figure>

<p><?php i18n("KDE has a focus on privacy. After all, our vision is a world in which everyone has control over their digital life and enjoys freedom and privacy.");?></p>

<p><?php i18n("For users who often deal with sensitive, confidential and private information, the new Plasma Vault feature offers strong encryption options presented in a user-friendly way. It allows you to lock and encrypt sets of documents, and hide them from prying eyes even when you are logged in. Plasma Vault extends Plasma's Activities feature with secure storage.");?></p>

<br clear="all" />

<h2><?php i18n("Plasma's Comprehensive Features");?></h2>

<p><?php i18n("Take a look at what Plasma offers - a comprehensive selection of features unparalleled in any desktop software.");?></p>

<h3><?php i18n("Desktop Widgets");?></h3>
<figure style="float: right">
<a href="plasma-5.12/widgets.png">
<img src="plasma-5.12/widgets-wee.png" style="border: 0px" width="250" height="441" alt="<?php i18n("Desktop Widgets");?>" />
</a>
<figcaption><?php i18n("Desktop Widgets");?></figcaption>
</figure>

<p><?php i18n("Cover your desktop in useful widgets to keep you up to date with weather, amuse you with comics, or help you with calculations.");?></p>

<br clear="all" />
<h3><?php i18n("Get Hot New Stuff");?></h3>
<figure style="float: right">
<a href="plasma-5.12/ghns.png">
<img src="plasma-5.12/ghns-wee.png" style="border: 0px" width="350" height="278" alt="<?php i18n("Get Hot New Stuff");?>" />
</a>
<figcaption><?php i18n("Get Hot New Stuff");?></figcaption>
</figure>

<p><?php i18n("Download wallpapers, window styles, widgets, desktop effects and dozens of other resources straight to your desktop. We work with the new <a href=\"http://store.kde.org\">KDE Store</a> to bring you a wide selection of addons ready to be installed.");?></p>

<br clear="all" />
<h3><?php i18n("Desktop Search");?></h3>
<figure style="float: right">
<a href="plasma-5.12/krunner.png">
<img src="plasma-5.12/krunner-wee.png" style="border: 0px" width="350" height="172" alt="<?php i18n("Desktop Search");?>" />
</a>
<figcaption><?php i18n("Desktop Search");?></figcaption>
</figure>

<p><?php i18n("Plasma lets you easily search your desktop for applications, folders, music, video, files... If you have it, Plasma can find it.");?></p>

<br clear="all" />
<h3><?php i18n("Unified Look");?></h3>
<figure style="float: right">
<a href="plasma-5.12/gtk-integration.png">
<img src="plasma-5.12/gtk-integration-wee.png" style="border: 0px" width="350" height="261" alt="<?php i18n("Unified Look");?>" />
</a>
<figcaption><?php i18n("Unified Look");?></figcaption>
</figure>

<p><?php i18n("Plasma's default Breeze theme has a unified look across all the common programming toolkits - Qt 4 &amp; 5, GTK 2 &amp; 3, and LibreOffice.");?></p>

<br clear="all" />
<h3><?php i18n("Phone Integration");?></h3>
<figure style="float: right">
<a href="plasma-5.12/kdeconnect.png">
<img src="plasma-5.12/kdeconnect-wee.png" style="border: 0px" width="350" height="259" alt="<?php i18n("Phone Integration");?>" />
</a>
<figcaption><?php i18n("Phone Integration");?></figcaption>
</figure>
<p><?php i18n("With KDE Connect, you can get notifications for new text messages on the desktop, easily transfer files between your computer and mobile device, silence music during calls, and even use your phone for remote control.");?></p>

<br clear="all" />
<h3><?php i18n("Infinitely Customisable");?></h3>
<figure style="float: right">
<a href="plasma-5.12/customisable.png">
<img src="plasma-5.12/customisable-wee.png" style="border: 0px" width="350" height="197" alt="<?php i18n("Infinitely Customisable");?>" />
</a>
<figcaption><?php i18n("Infinitely Customisable");?></figcaption>
</figure>
<p><?php i18n("Plasma is simple by default, but powerful when needed. You can customise it however you like with new widgets, panels, screens, and styles.");?></p>

<br clear="all" />
	<!-- // Boilerplate again -->
	<section class="row get-it">
		<article class="col-md">
			<h2><?php i18n("Live Images");?></h2>
			<p>
				<?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
			<a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Package Downloads");?></h2>
			<p>
				<?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Packages' class="learn-more"><?php i18n("Package download wiki page");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Source Downloads");?></h2>
			<p>
				<?php i18n("You can install Plasma 5 directly from source.");?>
			</p>
			<a href='http://community.kde.org/Frameworks/Building'><?php i18n("Community instructions to compile it");?></a>
			<a href='../info/plasma-5.12.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
		</article>
	</section>

	<section class="give-feedback">
		<h2><?php i18n("Feedback");?></h2>

		<p>
			<?php print i18n_var("You can give us feedback and get updates on <a href='%1'><img src='%2' /></a> <a href='%3'>Facebook</a>
			or <a href='%4'><img src='%5' /></a> <a href='%6'>Twitter</a>
			or <a href='%7'><img src='%8' /></a> <a href='%9'>Google+</a>.", "https://www.facebook.com/kde", "https://www.kde.org/announcements/facebook.gif", "https://www.facebook.com/kde", "https://twitter.com/kdecommunity", "https://www.kde.org/announcements/twitter.png", "https://twitter.com/kdecommunity", "https://plus.google.com/105126786256705328374/posts", "https://www.kde.org/announcements/googleplus.png", "https://plus.google.com/105126786256705328374/posts"); ?>
		</p>
		<p>
			<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
		</p>

		<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

		<p><?php i18n("Your feedback is greatly appreciated.");?></p>
	</section>

	<h2>
		<?php i18n("Supporting KDE");?>
	</h2>

	<p align="justify">
		<?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
	</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
