<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("KDE Ships Beta for Plasma 5's Second Release");
  $site_root = "../";
  $release = 'plasma-5.0.95';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<div style="float: right; padding: 1ex; margin: 1ex; border: solid thin grey; background-image: none;">
<a href="plasma-5.1/qt4_widgets_plus_icontasks.jpg">
<img src="plasma-5.1/qt4_widgets_plus_icontasks-wee.jpg" style="padding: 1ex; margin: 1ex; border: 0; background-image: none;" width="500" height="281" /></a><br />
<?php i18n("KDE Platform 4 apps now themed to fit in with Plasma 5");?>
</div>

<p>
<?php i18n("September 30, 2014.
Today KDE releases the beta for the second release of Plasma 5.  <a
href='http://kde.org/announcements/plasma5.0/index.php'>Plasma 5</a>
was released three months ago with many feature refinements and
streamlining the existing codebase of KDE's popular desktop for
developers to work on for the years to come.
");?>
</p>

<p>
<?php i18n("
This release is for testers to find bugs before our second release of Plasma 5.
");?>
</p>

<br clear="all" />
<h2><?php i18n("Some New Features");?></h2>

<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<img src="plasma-5.1/alt_switcher.jpg" width="435" height="355" style="border: 0px" /><br />
<?php i18n("Applet Switcher");?>
</div>
<p>
<?php i18n("
Plasma panels have new switchers to easily swap between different widgets for the same task. You can select which application menu, clock or task manager you want with ease.");?>
</p>

<br clear="all" />

<p>
 <?php i18n("The icons-only task manager is back for those who want a clean panel.");?>
</p>
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<img src="plasma-5.1/icons_task_manager.jpg" width="678" height="98" style="border: 0px" /> <br />
<?php i18n("Icons-only Task Manager");?>
</div>

<br clear="all" />
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="plasma-5.1/look-and-feel.png"><img src="plasma-5.1/look-and-feel-wee.png" width="500" height="269" /></a>
</div>
<p>
<?php i18n("A new System Settings module lets you switch between desktop themes.");?>
</p>

<br clear="all" />
<div style="float: right; border: thin solid grey; padding: 1ex; margin: 1ex; text-align: center">
<a href="plasma-5.1/qt4_widgets_plus_icontasks.jpg"><img src="plasma-5.1/qt4_widgets_plus_icontasks-wee.jpg" width="500" height="281"/></a><br />
<?php i18n("Breeze Theme for Qt 4");?>
</div>
<p>
<?php i18n("A new Breeze widget theme for Qt 4 lets applications written with KDE Platform 4 fit in with your Plasma 5 desktop.");?>
</p>

<br clear="all" />

<!-- // Boilerplate again -->

<!-- no live images yet
<h2><?php i18n("Live Images");?></h2>

<p><?php i18n("
The easiest way to try it out is the with a live image booted off a
USB disk.  Images are available for development versions of <a
href='http://cdimage.ubuntu.com/kubuntu-plasma5/'>Kubuntu Plasma 5</a>.
");?></p>
-->

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Some distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>");?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source
Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("<p>We produce beautiful software for your computer, please we'd love you to join us improving it or helping fellow users.  If you can't find the time to contribute directly do consider <a href='https://www.kde.org/community/donations/index.php#money'>sending a donation</a>.");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
