<?php

  $page_title = "KDE 4.0.3 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Também disponível em:
<a href="announce-4.0.3.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-it.php">Italian</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Swedish</a>

<!--
<a href="announce-4.0.3-bn_IN.php">Bengali (India)</a>
<a href="announce-4.0.3-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/announce-4.0.3.php">Chinese</a>
<a href="announce-4.0.3-cz.php">Czech</a>
<a href="announce-4.0.3-nl.php">Dutch</a>
<a href="announce-4.0.3.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-de.php">German</a>
<a href="announce-4.0.3-gu.php">Gujarati</a>
<a href="announce-4.0.3-he.php">Hebrew</a>
<a href="announce-4.0.3-hi.php">Hindi</a>
<a href="announce-4.0.3-it.php">Italian</a>
<a href="announce-4.0.3-lv.php">Latvian</a>
<a href="announce-4.0.3-ml.php">Malayalam</a>
<a href="announce-4.0.3-mr.php">Marathi</a>
<a href="announce-4.0.3-fa.php">Persian</a>
<a href="announce-4.0.3-pl.php">Polish</a>
<a href="announce-4.0.3-pa.php">Punjabi</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-ro.php">Romanian</a>
<a href="announce-4.0.3-ru.php">Russian</a>
<a href="announce-4.0.3-sl.php">Slovenian</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Swedish</a>
<a href="announce-4.0.3-ta.php">Tamil</a>
-->

<!-- // Boilerplate -->

<h3 align="center">
  Projeto KDE distribui traduções de terceiros e lança versão para o Desktop Livre
</h3>

<p align="justify">
  <strong>
A comunidade KDE distribui traduções e lança versão para o Desktop Livre,
contendo diversos consertos de erros, melhorias na performance e
atualizações nas traduções
</strong>
</p>

<p align="justify">
 2 de Abril de 2008 (A INTERNET). A <a href="http://www.kde.org/">Comunidade KDE
</a> lança hoje para disponibilidade imediata o KDE 4.0.3, a segunda
versão de correções de bugs e manutenção para a ultima geração do mais avançado e potente
desktop livre. KDE 4.0.3 é o terceiro update do mês para o <a href="4.0/">KDE 4.0</a>. Ele
distribui um ambiente de trabalho básico e outros pacotes; como programas de administração, ferramentas de rede,
aplicações educacionais, utilidades, programas de multimídia, jogos, arte, 
ferramentas de desenvolvimento web e mais. As aplicações ganhadoras de prêmios do KDE estão
disponíveis em 49 linguagens.
</p>
<p align="justify">
 O KDE, incluindo todas as suas bibliotecas e aplicações, é disponibilizado livremente
usando as licenças Open Source. Podendo ser obtido na forma de código-fonte e em várias
formas binárias através de <a
href="http://download.kde.org/stable/4.0.3/">http://download.kde.org</a> e também 
em <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
ou com qualquer distribuição de <a href="http://www.kde.org/download/distributions.php">GNU/Linux e sistemas UNIX</a>
distribuídos hoje.
</p>

<!-- // Meat -->

<h4>
  <a name="changes">Melhorias</a>
</h4>
<p align="justify">
O KDE 4.0.3 vem com uma impressionante quantidade de correções de bugs e melhorias. Muitas delas são
gravadas no <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php">changelog</a>.
O KDE continua à lançar atualizações para o desktop 4.0 de mês em mês. O KDE 4.1, que ira trazer <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan">muitas melhorias</a> para o ambiente KDE
e as aplicações será lançado em Julho desse ano.
<br />
As melhorias do KDE 4.0.3 são muitas correções de bugs e atualizações das traduções.
Correções foram feitas de uma maneira que o resultado tem um mínimo risco de
regressões. Para o KDE, isso também deixa as correções de bugs rápida para os usuários.

Uma olhada pelo changelog revela que todos os módulos no KDE tiveram
diversas melhorias. Novamente, o time do KHTML fez um ótimo trabalho
melhorando a experiência dos usuários com o Web Browser Konqueror.

<ul>
  <li>Otimizações no Scroll do KHTML, A engine de renderização HTML do KDE</li>
  <li>Melhoria no manuseamento das janelas de dialogo no KWin, O gerenciador de janelas do KDE</li>
  <li>Muitas melhorias de renderização no Okular, o visualizador de documentos do KDE</li>
</ul>

<h4>Extragear</h4>
<p align="justify">
Desde o KDE 4.0.0, as aplicações <a href="http://extragear.kde.org">Extragear</a>
também são parte dos lançamentos regulares do KDE.
As aplicações Extragear são aplicações KDE maduras, mas não parte
de um dos pacotes do KDE. O pacote extragear que é distribuido com o KDE 4.0.3
vem com os seguintes programas:
<ul>
    <li><a href="http://en.wikipedia.org/wiki/KColorEdit">KColoredit</a> -  Um editor 
        para arquivos de paleta de cores que suporta os formatos do KDE e do GIMP</li>
    <li>KFax - Um visualizador de FAX </li>
    <li><a href="http://www.kde-apps.org/content/show.php/KGrab?content=74086">KGrab</a> - 
        Uma aplicação mais avançada para screenshots </li>
    <li><a href="http://extragear.kde.org/apps/kgraphviewer/">KGraphviewer</a> - Um visualizador de gráficos GraphViz para o KDE</li>
    <li><a href="http://w1.1358.telia.com/~u135800018/prog.html#KICONEDIT">KIconedit</a> - 
        Um programa de desenhos para ícones</li>
    <li><a href="http://kmldonkey.org/">KMldonkey</a> - Um cliente gráfico para a rede EDonkey</li>
    <li><a href="http://www.kpovmodeler.org/">KPovmodeler</a> - Um modelador 3D</li>
    <li>Libksane - Uma biblioteca de digitalização</li>
    <li><a href="http://www.rsibreak.org">RSIbreak</a> - Um programa que salva você de ter um RSI forçando intervalos</li>
</ul>
Sendo novo nesse lançamento, o KIO slave Gopher adiciona suporte para o
<a href="http://en.wikipedia.org/wiki/Gopher_(protocol)">protocolo Gopher</a> em todas as aplicações
KDE.
</p>

<h4>
Suite de aplicações educacionais do KDE
</h4>
<p align="justify">
O KDE 4.0.3 distribui uma suite de alta-qualidade com <a href="http://edu.kde.org">programas educacionais</a>.
As aplicações vão do <a href="http://edu.kde.org/marble/">Marble</a>, um globo versátil no Desktop, até um divertido
jogo para crianças pequenas.
<p align="justify">
O Kalzium é uma tabela periódica dos elementos gráfica. Ele visualiza conceitos abstratos como
a atração dos átomos. Trazendo também inúmeras maneiras de mostrar informações detalhadas
sobre os elementos. Sendo feito como uma ferramenta para fazer química fácil para as
crianças do segundo grau - mas também com muita diversão para os mais crescidos.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kalzium.png">
    <img src="announce_4.0.3/kalzium_thumb.png" align="center"  height="261"  />
  </a>
  <br /><em>Experimente a química com o Kalzium</em>
</div>
</p>
<p align="justify">

Parley é um programa para ajudar a memorizar o vocabulário. Suportando muitas ferramentas especificas das linguagens
mas também podendo ser usado para outras tarefas de aprendizado. Ele usa o método de repetição espaçada,
também conhecido como cartões "flash".
Criando novas coleções de vocabulário com Parley é fácil, mas é claro é melhor ainda quando
você pode baixar alguns arquivos já feitos da Internet.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/parley.png">
    <img src="announce_4.0.3/parley_thumb.png" align="center"  height="225"  />
  </a>
  <br /><em>Pratique o seu vocabulário com Parley</em>
</div>
</p>
<p align="justify">

Kmplot é um plotter de funções matemáticas, que proporciona uma ferramenta fácil para um melhor entendimento
de matemática. Você pode facilmente inserir funções matemáticas nele e ver os seus gráficos.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kmplot.png">
    <img src="announce_4.0.3/kmplot_thumb.png" align="center"  height="341"  />
  </a>
  <br /><em>Faça a matemática acontecer com Kmplot</em>
</div>

Aqueles que quiserem maiores informações sobre as aplicações educacionais do KDE devem
pegar o <a href="http://edu.kde.org/tour_kde4.0/">Tour</a>.
</p>

<!-- // Boilerplate again -->

<h4>
  Instalando pacotes binários do KDE 4.0.3
</h4>
<p align="justify">
  <em>Empacotadores</em>.
  Alguns distribuidores de Linux/UNIX disponibilizaram gentilmente pacotes binários do KDE 4.0.3
para algumas versões das suas distribuições, em outros casos voluntários os fizeram.
  Alguns desses pacotes binários estão disponíveis em <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.3/">http://download.kde.org</a>.
  Pacotes binários adicionais, como também atualizações para os pacotes atuais,
  podem ser disponibilizados nas próximas semanas.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.0.3.php">KDE 4.0.3 Info
Page</a>.
</p>

<h4>
  Compilando o KDE 4.0.3
</h4>
<p align="justify">
  <a name="source_code"></a><em>Código-Fonte</em>.
  O código-fonte completo para o KDE 4.0.3 deve ser <a
href="http://download.kde.org/stable/4.0.3/src/">livremente baixado</a>.
Instruções sobre compilação e instalação do KDE 4.0.3
  estão disponíveis na <a href="/info/4.0.3.php#binary">Página de Informações do KDE 4.0.3</a>.
</p>

<h4>
  Ajudando o KDE
</h4>
<p align="justify">
 KDE é um projeto de <a href="http://www.gnu.org/philosophy/free-sw.html">Software Livre</a>
que existe e cresce pela ajuda de diversos voluntários que
doam seu tempo e esforços. O KDE está sempre procurando novos voluntários e
contribuições, não importa se é com código, consertos ou relato de bugs, escrevendo
documentação, traduzindo, divulgando, dinheiro, etc. Todas as contribuições são
apreciadas e aceitas. Por favor leia a <a
href="/community/donations/">Página para Ajuda do KDE</a> para maiores informações. </p>

<p align="justify">
Nós estamos esperando ouvir de você em breve!
</p>

<h4>Sobre o KDE 4</h4>
<p align="justify">
KDE 4.0 é um ambiente de Desktop Livre cheio de inovações contendo diversas aplicações
para uso diário como também para uso específicos. Plasma é o novo desktop desenvolvido
para o KDE 4, provendo uma interface intuitiva para interação com o Desktop e
as aplicações. O browser web Konqueror integra a web com o desktop. O
gerenciador de arquivos Dolphin, o leitor de documentos Okular e o centro de controle System Settings
completam o ambiente de trabalho básico.
<br />
KDE é feito com as bibliotecas KDE que prevêem acesso fácil à recursos na
rede pelo KIO e avançadas capacidades visuais através do Qt4. Phonon e
Solid, que também são parte das bibliotecas do KDE adicionam um framework multimídia e
uma melhor integração com o hardware para todas as aplicações KDE.
</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contatos de imprensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
