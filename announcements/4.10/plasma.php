<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $release = '4.10';
  $release_full = '4.10.0';
  $page_title = i18n_noop("Plasma Workspaces 4.10 Improve Mobile Device Support and Receive Visual Refinement");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>
<p>
<?php i18n("Plasma Workspaces have been refined considerably. Work continues on updating widgets with new ones built with <a href='http://doc.qt.digia.com/qt/qtquick.html'>Qt Quick</a>. This effort brings improvements in consistency, layout behavior, stability, ease of use and performance. It is also now easier to build widgets, entirely new Plasma Workspace layouts and other custom enhancements. A new <a href='http://doc.qt.digia.com/qt/qdeclarativeintroduction.html'>QML</a>-based screen locker makes Workspaces more secure. The wallpaper engine was also updated to QML, so it is easier to write animated wallpapers. (QML is part of the <a href='http://doc.qt.digia.com/qt/qtquick.html'>Qt Quick application framework</a>.)");?>
</p>
<?php showscreenshot("plasma-empty.png", i18n_var("KDE Plasma Workspaces 4.10")); ?>
<p>
<?php i18n("In addition to improvements related to Qt Quick and QML, the task widget received some usability updates, with a <a href='http://aseigo.blogspot.com/2012/11/help-test-task-grouping-experiments.html'>smoother look for groups of windows</a>. There also have been improvements to the notifications system, particularly in the area of power management. There is now improved support for high resolution displays and <a href='http://www.notmart.org/index.php/Graphics/Time_to_refresh_some_air'>a new Air theme</a> reducing visual clutter and giving Plasma Workspaces a cleaner appearance.");?>
</p>
<?php showscreenshot("plasma-tasks.png", i18n_var("Task grouping received visual improvements")); ?>

<h2><?php i18n("KWin Window Manager and Compositor");?></h2>
<p>
<?php i18n("With KWin's Get Hot New Stuff (GHNS) integration, additional effects and scripts are available in the KWin configuration dialog and can also be found at <a href='http://kde-look.org/index.php?xcontentmode=90'>kde-look.org</a>, including <a href='http://kde-look.org/index.php?xcontentmode=91'>behavior modifying scripts</a>. Custom window switchers can also be retrieved in the KWin configuration dialog and <a href='http://kde-look.org/index.php?xcontentmode=92'>that section of kde-look.org</a>. There's a nifty new effect that animates the maximize window state change.");?>
</p>
<?php showscreenshot("kwin-ghns.png", i18n_var("Kwin add-ons can now be installed easily from online sources")); ?>
<p>
<?php i18n("KWin now detects some virtual machines and enables OpenGL compositing if possible. In addition, the proprietary AMD driver now has OpenGL 2 support.");?>
</p>
<p>
<?php i18n("Tiling support in KWin <a href='https://bugs.kde.org/show_bug.cgi?id=303090'>has been removed</a> as it had stability issues, lacked multi-screen support and conflicted with other parts in KWin. In short, KWin developers concluded that the necessary functionality would be more suitable for a plugin using the Javascript API. In that way, users would get more control, and development and maintenance would be easier. Third party developers would be able to customize, improve and experiment. A plugin-based version might be available in the next release; help is requested as none of the current KWin developers are working on tiling support.");?>
</p>
<?php showscreenshot("plasma-animated-wallpaper.png", i18n_var("Animated Plasma wallpapers can now be created using QML")); ?>
<p>
<?php i18n("Several applications now support color correction so that they can be adjusted according to the color profiles of different monitors and printers. The KolorServer KDED module supports per-output color correction, per-window is coming in a later release. Color management support in Kwin is designed to relieve the Compositor of this task. This allows the user to disable color management, and makes code maintenance easier. Multi-monitor setups are also supported. These color management features were given a big boost with a <a href='http://skeletdev.wordpress.com/2012/08/20/gsoc-color-correction-in-kwin-final-report/'>Google Summer of Code project</a>.");?>
</p>
<p>
<?php i18n("The new KDE <a href='http://gnumdk.blogspot.com/2012/11/appmenu-support-in-kde-410.html'>appmenu</a> enables a common menu for multiple applications running simultaneously. It has an option to display a top screen menubar—hidden by default—that appears when the mouse is moved near the top edge of the screen. The menubar follows the window focus so it can be used in multiscreen environments. There is also an option for the menu to be displayed as a sub-menu of a button in the window decoration. The menu can be displayed on the screen wherever the user wants it.");?>
</p>
<div align="center" class="screenshot"><img src="screenshots/kwin-appmenu.gif" alt="<?php i18n("The application menu can now be embedded in the window title bar");?>"/><br /><?php i18n("The application menu can now be embedded in the window title bar");?></div>
<p>
<?php i18n("KWin bug fixing has improved thanks to <a href='http://blog.martin-graesslin.com/blog/2012/07/looking-for-kwin-bug-triagers/'>extra help verifying incoming bug reports</a>. Stay current with KWin Window Manager development at <a href='http://blog.martin-graesslin.com/blog/'>Martin Gräßlin's blog</a>.");?>
</p>

<h2><?php i18n("Faster and more reliable metadata engine");?></h2>
<p>
<?php i18n("Thanks to the work sponsored by Blue Systems, the KDE cross-application semantic search and storage backend has seen over 240 bugs fixed and a significant number of other improvements. Chief among these is <a href='http://vhanda.in/blog/2012/11/nepomuk-without-strigi/'>the new indexer</a>, which makes indexing faster and more robust. A nice feature is that it first quickly indexes the basic information of new files (name and mimetype) so the files are available at once, and then delays full data extraction until the system is idle (or connected to AC!), so it doesn't interfere with the user's workflow. In addition, it is now far simpler to write extractors for new file formats. 
A few formats that were previously supported are not yet available for the new indexer, but support for these can be expected soon. An added benefit of the new indexer is the ability to easily filter on the type of file, which is reflected in the user interface: it is now possible to enable or disable indexing of Audio, Images, Documents, Video's and Source Code. The search and storage user interface and Backup have seen improvements as well. The introduction of the Tags <a href='http://en.wikipedia.org/wiki/KIO'>KIO slave</a> allows users to browse their files by tags from any KDE application.");?></p>
<p>
<?php i18n("Nepomuk Cleaner is a simple new tool for managing semantic storage. It is useful for cleaning up legacy, invalid or duplicate data. Running the Cleaner after upgrading can provide a significant speed improvement. More information on this and other changes in the KDE search technologies can be found in <a href='http://vhanda.in/blog/2013/01/what-new-with-nepomuk-4-10/'>Vishesh Handa's blog</a>.");?>
</p>
<?php showscreenshot("dolphin-metadata.png", i18n_var("Metadata handling has been improved")); ?>

<h2><?php i18n("New Print Manager");?></h2>
<p>
<?php i18n("Printer setup, maintenance and job control are improved with a new implementation of the Print Manager. The Plasma applet shows available printers and provides access and control over queued jobs. The System Setting configuration screen enables users to add and remove printers, giving an overview of the current printers, along with access to important control functions such as sharing and default printer selection. The New Printer Wizard automatically selects proper drivers and control settings on recognized devices. The new Print Manager tools are fully compatible with the latest CUPS printing subsystem, resulting in quick responses and reliable reporting.");?>
</p>
<!-- SCREENSHOT: print-manager.png -->
<h2><?php i18n("Dolphin File Manager");?></h2>
<p>
<?php i18n("The KDE file manager Dolphin has seen many bugfixes, improvements and new features. Transferring files to and from a phone or other mobile device has become easier with support for <a href='http://en.wikipedia.org/wiki/Media_Transfer_Protocol'>MTP</a> devices, which show up in the Places panel. The size of Panel icons can now be changed, and other usability and accessibility options have been added. Dolphin now has the ability to report the current directory and files to the Activity manager (controlled in System Settings). There has also been an impressive number of performance enhancements.
Loading folders, both with and without previews, is significantly faster and requires less memory while using all available processor cores to be as fast as possible. Minor improvements were made to search, drag and drop and other areas. Dolphin also benefits from the improvements in the KDE semantic storage and search backend, reducing the resources needed for metadata handling. More details from <a href='http://freininghaus.wordpress.com/2012/11/27/on-the-way-to-dolphin-2-2/'>Frank Reininghaus, the Dolphin maintainer</a>.");?>
</p>
<?php showscreenshot("kio-mtp.png", i18n_var("Transferring files from mobile devices is now easier")); ?>


<h4><?php i18n("Installing Plasma");?></h4>
<?php
  include("boilerplate.inc");
?>
<h2><?php i18n("Also Announced Today:");?></h2>

<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.10");?>"/> <?php i18n("KDE Applications Improve Usability, Performance and Take You to Mars");?></a></h2>
<p>
<?php i18n("KDE Applications gained feature enhancements to Kate, KMail and Konsole. KDE-Edu applications saw a complete overhaul of KTouch and many other changes. KDE Games introduced the new Picmi game and improvements throughout.");?>

</p>

<h2><a href="platform.php"><img src="images/platform.png" class="app-icon" alt="<?php i18n("The KDE Development Platform 4.10");?>"/> <?php i18n("KDE Platform 4.10 Opens Up More APIs to Qt Quick");?></a></h2>
<p>
<?php i18n("This release makes it easier to contribute to KDE with a Plasma SDK (Software Development Kit), the ability to write Plasma widgets and widget collections in the Qt Markup Language (QML), changes in the libKDEGames library, and new scripting capabilities in window manager KWin.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

<?php
  include("footer.inc");
?>
