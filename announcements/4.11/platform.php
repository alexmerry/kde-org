<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $release = '4.11';
  $release_full = '4.11.0';
  $page_title = i18n_noop("KDE Platform 4.11 Delivers Better Performance");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<?php i18n("August 14, 2013");?>
</p>

<p>
<?php i18n("KDE Platform 4 has been in feature freeze since the 4.9 release. This version consequently only includes a number of bugfixes and performance improvements.");?>
</p>

<p>
<?php i18n("The  Nepomuk semantic storage and search engine received massive performance improvements, such as a set of read optimizations that make reading data up to six times faster. Indexing has become smarter, being split in two stages.  The first stage retrieves general information (such as file type and name) immediately; additional information like media tags, author information, etc. is extracted in a second, somewhat slower stage.  Metadata display on newly-created or freshly-downloaded content is now much faster. In addition, the Nepomuk developers improved the backup and restore system. Last but not least, Nepomuk can now also index a variety of  document formats including ODF and docx.");?>
</p>

<?php showscreenshotpng("dolphin.png", i18n_var("Semantic features in action in Dolphin")); ?>

<p>
<?php i18n("Nepomuk’s optimized storage format and rewritten e-mail indexer require reindexing some of the hard drive’s content. Consequently the reindexing run will consume an unusual amount of computing performance for a certain period – depending on the amount of content that needs to be reindexed. An automatic conversion of the Nepomuk database will run on the first login.");?>
</p>

<p>
<?php i18n("There have been more minor fixes which <a href='https://projects.kde.org/projects/kde/kdelibs/repository/revisions?rev=KDE%2F4.11'>can be found in the git logs</a>.");?>
</p>

<h4><?php i18n("Installing the KDE Development Platform");?></h4>
<?php
  include("boilerplate.inc");
?>

<h2><?php i18n("Also Announced Today:");?></h2>
<h2><a href="plasma.php"><img src="images/plasma.png" class="app-icon" alt="<?php i18n("The KDE Plasma Workspaces 4.11");?>" width="64" height="64" /> <?php i18n("Plasma Workspaces 4.11 Continues to Refine User Experience");?></a></h2>
<p>
<?php i18n("Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.");?>
</p>

<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.11");?>"/> <?php i18n("KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over");?></a></h2>
<p>
<?php i18n("This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

<?php
  include("footer.inc");
?>
