<?php
  $page_title = "KDE 2.1.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<P>DATELINE MARCH 27, 2001</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New KDE Release for Linux Desktop Ready for Enterprise</H3>
<P><STRONG>KDE Ships Leading Desktop with Advanced Web Browser, Anti-Aliased Font Capabilities for Linux and Other UNIXes</STRONG></P>
<P>March 27, 2001 (The INTERNET).
The <A HREF="http://www.kde.org/">KDE
Project</A> today announced the release of KDE 2.1.1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <A HREF="http://konqueror.kde.org/">Konqueror</A>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<A HREF="http://www.kdevelop.org/">KDevelop</A>,
an advanced IDE, as a central component of KDE's powerful
development environment.  KDE 2.1 is the first stable Linux desktop
completely to integrate the
<A HREF="http://xfree86.org/4.0.2/fonts5.html#30">new XFree anti-aliased
font extensions</A> and can provide
a fully anti-aliased font-enabled desktop.
</P>
<P>
This release follows four weeks after the release of the 
<a href="http://www.kde.org/announcements/announce-2.1.html">industry-acclaimed</a> KDE 2.1, which
marked a leap forward in Linux desktop stability, usability
and maturity.  KDE 2.1.1 is suitable for enterprise deployment and the
KDE Project strongly encourages all users of the
<A HREF="http://www.kde.com/News/Awards/index.php">award-winning</A> KDE 1.x
series and of the KDE 2.0 series to upgrade to KDE 2.1.1.
</P>
<P>
The primary goals of the 2.1.1 release are to improve documentation
and provide additional language translations for the user interface,
although the release includes a few bugfixes, and improvements to the HTML
rendering engine.  A
<A HREF="http://www.kde.org/announcements/changelog2_1to2_1_1.html">list of
these changes</A> and a <A HREF="http://www.kde.org/info/2.1.1.html">FAQ about
the release</A> are available at the KDE
<A HREF="http://www.kde.org/">website</A>.
Code development is currently focused
on the branch that will lead to KDE 2.2, scheduled for its first beta release
in two weeks.
</P>
<P>
KDE and all its components are available for free under
Open Source licenses from the KDE
<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/">server</A>
and its <A HREF="http://www.kde.org/mirrors.html">mirrors</A> and can
also be obtained on <A HREF="http://www.kde.org/cdrom.html">CD-ROM</A>.
As a result of the dedicated efforts of hundreds of translators,
KDE 2.1.1 is available in
<A HREF="http://i18n.kde.org/teams/distributed.html">34 languages and
dialects</A>, including the addition of Lithuanian in
this release.  KDE 2.1.1 ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (administration, games,
graphics, multimedia, network, PIM and utilities).
</P>
<P>
For more information about the KDE 2.1 series, please see the
<A HREF="http://www.kde.org/announcements/announce-2.1.html">KDE 2.1 
press release</A> and the <A HREF="http://www.kde.org/info/2.1.1.html">KDE
2.1.1 Info Page</A>, which is an evolving FAQ about the release.
Information on using anti-aliased fonts with KDE is available
<A HREF="http://dot.kde.org/984693709/">here</A>.
</P>
<H4>Downloading and Compiling KDE</H4>
<P>
The source packages for KDE 2.1.1 are available for free download at
<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.1.1/distribution/tar/generic/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>
(<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/tar/generic/src/diffs/">diffs</A>
are also available).  KDE 2.1.1 requires
qt-2.2.3, which is available from
<A HREF="http://www.trolltech.com/">Trolltech</A> at
<A HREF="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</A>
under the name <A HREF="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.3.tar.gz">qt-x11-2.2.3.tar.gz</A>,
although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.4.tar.gz">qt-2.2.4</A>
or
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>
is recommended (for anti-aliased fonts,
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</A>
and <A HREF="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</A> or
newer is required).
KDE 2.1.1 will not work with versions of Qt older than 2.2.3.
</P>
<P>
For further instructions on compiling and installing KDE, please consult
the <A HREF="http://www.kde.org/install-source.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://www.kde.org/compilationfaq.html">compilation FAQ</A>.
</P>
<H4>Installing Binary Packages</H4>
<P>
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for KDE 2.1.1
will be available for free download under
<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/">http://ftp.kde.org/stable/2.1.1/distribution/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="http://www.kde.org/mirrors.html">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.
</P>
<P>KDE 2.1.1 requires qt-2.2.3, the free version of which is available
from the above locations usually under the name qt-x11-2.2.3, although
qt-2.2.4 or qt-2.3.0 is recommended (for anti-aliased fonts,
qt-2.3.0 and XFree 4.0.3 or newer is required).
KDE 2.1.1 will not work with versions of Qt
older than 2.2.3.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI><A HREF="http://www.caldera.com/">Caldera</A> eDesktop 2.4: <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/Caldera/eDesktop24/RPMS/">i386</A></LI>
<LI><A HREF="http://www.debian.org/">Debian GNU/Linux</A> stable (2.2):  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-powerpc/">PPC</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/deb/dists/stable/main/binary-all/">main</A> directory for common files</LI>
<LI><A HREF="http://www.linux-mandrake.com/en/">Linux-Mandrake</A> 7.2:  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/Mandrake/7.2/RPMS/">i586</A></LI>
<!--
<LI><A HREF="http://www.redhat.com/">RedHat Linux</A>:
<UL>
<LI>Wolverine:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/i386/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/wolverine/common/">common</A> directory for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/i386/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/alpha/">Alpha</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/7.0/common/">common</A> directory for common files</LI>
<LI>6.x:  <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/i386/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/alpha/">Alpha</A> and <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/sparc/">Sparc</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1/distribution/rpm/RedHat/6.x/common/">common</A> directory for common files</LI>
</UL>
-->
<LI><A HREF="http://www.suse.com/">SuSE Linux</A> (<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/README">README</A>):
<UL>
<LI>7.1:  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/i386/7.1/">i386</A>, <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/sparc/7.1/">Sparc</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/ppc/7.1/">PPC</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/localisation/">i18n</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/noarch/">noarch</A> directories for common files</LI>
<LI>7.0:  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/i386/7.0/">i386</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/ppc/7.0/">PPC</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/localisation/">i18n</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/noarch/">noarch</A> directories for common files</LI>
<LI>6.4:  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/i386/6.4/">i386</A>; please also check the <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/localisation/">i18n</A> and <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/rpm/SuSE/noarch/">noarch</A> directories for common files</LI>
</UL>
<LI><A HREF="http://www.tru64unix.compaq.com/">Tru64</A> Systems:  <A HREF="http://ftp.kde.org/stable/2.1.1/distribution/tar/Tru64/">4.0e,f,g, or 5.x</A> (<A HREF="http://ftp.kde.org/stable/2.1.1/distribution/tar/Tru64/README.Tru64">README</A>)</LI>
<!--
<LI><A HREF="http://ftp.kde.org/stable/2.1/distribution/tar/FreeBSD/">FreeBSD</A></LI>
-->
</UL>
<P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.  In particular
<A HREF="http://www.redhat.com/">RedHat</A> packages are expected soon.
</P>
<H4>About KDE</H4>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="http://www.kde.org/whatiskde/">web site</A>.
More information about KDE 2 is available in two
(<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">1</A>,
<A HREF="http://mandrakesoft.com/~david/OSDEM/">2</A>) slideshow
presentations and on
<A HREF="http://www.kde.org/">KDE's web site</A>, including an evolving
<A HREF="http://www.kde.org/info/2.1.html">FAQ</A> to answer questions about
migrating to KDE 2.1 from KDE 1.x,
<A HREF="http://dot.kde.org/984693709/">anti-aliased font tutorials</A>, a
number of
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>, <A HREF="http://developer.kde.org/documentation/kde2arch.html">developer information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<P>
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
</FONT>
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
&#103;r&#x61;&#110;&#114;&#111;t&#x68;&#00064;&#x6b;d&#x65;&#x2e;&#x6f;r&#103;<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
po&#117;r&#0064;kd&#x65;&#x2e;&#x6f;&#00114;g<BR>
(1) 917 312 3122
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
faure&#x40;k&#x64;e.org<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
&#x6b;ono&#x6c;&#x64;&#64;kde&#x2e;&#111;&#114;g<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
<?php
  include "footer.inc"
?>
