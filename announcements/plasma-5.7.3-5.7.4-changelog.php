<?php
include_once ("functions.inc");
$translation_file = "kde-org";
$page_title = i18n_noop("Plasma 5.7.4 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.7.4';
include "header.inc";
?>
<p><a href="plasma-5.7.4.php">Plasma 5.7.4</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Remove redundant includes. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ffa3be8a5897c9b802124f9ed901f74569614898'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>[Notifier] Use "update-high" for security updates. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c707f9aab68d05767d3ffcec3306aea1ebc02c7a'>Commit.</a> </li>
</ul>


<h3><a name='kactivitymanagerd' href='http://quickgit.kde.org/?p=kactivitymanagerd.git'>kactivitymanagerd</a> </h3>
<ul id='ulkactivitymanagerd' style='display: block'>
<li>All config updates have the same importance. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=b7f8c6f1c0fd433863944f24a75288ded95f15cf'>Commit.</a> </li>
<li>Even in kiosk mode, we need to be able to create at least one activity. <a href='http://quickgit.kde.org/?p=kactivitymanagerd.git&amp;a=commit&amp;h=9c1e5bb02d526be585c73d8fe13e158954bb1483'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[Show Desktop applet] Fix showing desktop through applet keyboard shortcut. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=570d08e59f5c59dca385b2384c2e2e05e638b7d4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365825'>#365825</a></li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>[kcm] decrease snapping area. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=bdaa4258090782982364ac70ac83c4035272891a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365353'>#365353</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Ensure to directly delete old Shadow on update. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=fe8fc6f83d3a567b0774b433df011f899f7230d3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361154'>#361154</a></li>
<li>Select also raw button press/release in XInput2 based polling. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=71c996fe33d0b4032c0e71293a1c283d69b9c2f8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366612'>#366612</a></li>
<li>[wayland] Ensure that pointer enter event carries the correct coordinates. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=921e27257cf13d7e3acf5ac6ee3c7dc18417d955'>Commit.</a> </li>
<li>[autotest/intergration] Wait for pointer enter before simulating button press. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f31989f701a33ae697f06ac8df6ee83f46a61bc5'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Xrandr-crtc: clear outputs in update(). <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b41afa319d91683e99ba0d7d8c5760aaf0fe86fe'>Commit.</a> </li>
<li>Update CRTCs before enabling an output. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5ad4370cce960c5b5192a47169b353ed94414815'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366346'>#366346</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Kicker Dash] Use ComplementaryColorGroup for icon tinting. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=22b909a856312843ef7ede362828d1d8ba088849'>Commit.</a> </li>
<li>[kcm_mouse] Sync KDE4 config after writing the KF5 settings, not before. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4f4da83327e66f125c8dd1749df8efe1b5084945'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348254'>#348254</a>. Fixes bug <a href='https://bugs.kde.org/367074'>#367074</a>. Code review <a href='https://git.reviewboard.kde.org/r/128703'>#128703</a></li>
<li>[Pager] Hide minimized windows. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=092aeb53d36e5d7f9bdf937b5fdb3388e8eaebe1'>Commit.</a> </li>
<li>[Task Manager ToolTipDelegate] Silence warning on startup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c8799879688febeefbf6b53d4ad01e0fd4fa2269'>Commit.</a> </li>
<li>[Kickoff] Fix start row for drag not always being correct. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5eaadb75de3dfdd997bdab7acbbeda2a8390858f'>Commit.</a> </li>
<li>[Kickoff] Fix being unable to reorder entries in favorites menu after scrolling down. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=784b07f26ae8fa68000c844a888cf8d5845ff06c'>Commit.</a> </li>
<li>Use a Timer to switch on hover event handling and do the initial geo export. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=46919b609d0d6fc148d5c211e0ec5c41208f2b10'>Commit.</a> </li>
<li>Revert "Use a Timer to switch on hover event handling and do the initial geo export.". <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e737c905f095c2380d881ea76b05425567e0e152'>Commit.</a> </li>
<li>Use a Timer to switch on hover event handling and do the initial geo export. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=41f293891f06f2f4e14a5dec56005aa69e2546b8'>Commit.</a> </li>
<li>Fix ReferenceError on external drag hover with !separateLaunchers. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b87748c8ed29c1e8585d280cda63d770c25f6fba'>Commit.</a> </li>
</ul>


<h3><a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Add 1s delay before trying to reconnect to pa. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=aa5efa1344ab34ffc580d73669b1eb2bd77d416c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366320'>#366320</a></li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix mistake between Lock and Leave action in lock_logout applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=aeff2eafb0e1200b02d89da4cfd9a51d6e320dcf'>Commit.</a> </li>
<li>[Plasma Calendar Integration] Filter holiday regions case-insensitively. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7516a8fb9dacdaf031badfd3a3a08266607f5abb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365621'>#365621</a></li>
<li>[System Tray] Don't reserve space for expander if it's not visible. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=cd19257085e89015b6ca38e255d555c613b21527'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/365636'>#365636</a></li>
</ul>


<h3><a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Revert "Don't unconditionally emit buttonPressed on profile load". <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=71e9a3369c850df9822396e624a0847eae95cc24'>Commit.</a> </li>
<li>Don't unconditionally emit buttonPressed on profile load. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=48c3dca61d7cb808c904a3a3659b2cd36fba1866'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366125'>#366125</a></li>
</ul>


<?php
  include("footer.inc");
?>
