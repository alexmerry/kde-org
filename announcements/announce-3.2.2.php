<?php
  $page_title = "KDE 3.2.2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE April 19, 2004</p>
<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Second Translation and Service Release for Leading Open Source Desktop
</h3>

<p align="justify">
  <strong>
    KDE Project Ships Second Translation and Service Release of the 3.2 Generation
    GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling
    Free and Open Desktop Solution
  </strong>
</p>

<p align="justify">
  April 19, 2004 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.2.2,
  a maintenance release for the latest generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes.  KDE 3.2.2
  ships with a basic desktop and eighteen other packages
  (PIM, administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in <strong>49 languages</strong> (now including
  Bengali, Icelandic, Japanese, Lithuanian, Low Saxon, Latin Serbian and Tajik).
</p>

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em><strong>for free</strong></em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from
  <a href="http://download.kde.org/stable/3.2.2/">http://download.kde.org</a> and can
  also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
  or with any of the major GNU/Linux - UNIX systems shipping today.
</p>

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
  KDE 3.2.2 is a maintenance release which provides corrections of problems
  reported using the KDE <a href="http://bugs.kde.org/">bug tracking system</a>
  and enhanced support for existing translations.
</p>
<p align="justify">
  For a more detailed list of improvements since the KDE 3.2 release in early
  February, please refer to the
  <a href="changelogs/changelog3_2_1_to_3_2_2.php">KDE 3.2.2 Changelog</a>.
</p>
<p>
  Additional information about the enhancements of the KDE 3.2.x release
  series is available in the
  <a href="announce-3.2.php">KDE 3.2 Announcement</a>.
</p>

<h4>
  Installing KDE 3.2.2 Binary Packages
</h4>
<p align="justify">
  <em>Packaging Policies</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.2.2 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.2.2/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="http://www.kde.org/info/3.2.2.php">KDE 3.2.2 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.2.2
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.2.2 may be
  <a href="http://download.kde.org/stable/3.2.2/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.2.2
  are available from the <a href="/info/3.2.2.php#binary">KDE
  3.2.2 Info Page</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a>,
  <a href="http://www.trolltech.com/">Trolltech</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion,
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<h4>Press Contacts</h4>
<table cellpadding="10"><tr valign="top">
<td>

<b>Africa</b><br />
Uwe Thiem<br />
P.P.Box 30955<br />
Windhoek<br />
Namibia<br />
Phone: +264 - 61 - 24 92 49<br />
<a href="mail&#00116;&#x6f;:&#105;n&#00102;&#0111;&#45;&#97;&#x66;&#114;i&#99;&#0097;&#x40;k&#x64;&#x65;&#x2e;&#111;rg">&#x69;nf&#x6f;-af&#114;&#105;ca&#x40;k&#0100;&#x65;&#x2e;&#111;&#114;&#103;</a><br />
</td>

<td>
<b>Asia</b><br />
Sirtaj S. Kang <br />
C-324 Defence Colony <br />
New Delhi <br />
India 110024 <br />
Phone: +91-981807-8372 <br />
<a href="ma&#105;&#x6c;&#x74;&#x6f;&#058;&#x69;&#x6e;&#102;o&#x2d;a&#0115;&#x69;a&#64;kde&#x2e;&#0111;rg">in&#x66;o&#45;&#x61;s&#x69;&#0097;&#64;&#x6b;&#100;e.&#0111;&#x72;&#x67;</a>
</td>

</tr>
<tr valign="top">

<td>
<b>Europe</b><br />
Matthias Kalle Dalheimer<br />
Rysktorp<br />
S-683 92 Hagfors<br />
Sweden<br />
Phone: +46-563-540023<br />
Fax: +46-563-540028<br />
<a href="ma&#x69;&#108;&#116;&#00111;&#058;&#00105;n&#x66;o&#0045;eur&#111;&#0112;e&#64;kde&#46;&#111;r&#x67;">i&#x6e;&#102;&#x6f;&#045;&#x65;&#x75;&#114;op&#101;&#x40;&#107;&#x64;&#x65;.o&#x72;&#x67;</a>
</td>

<td>
<b>North America</b><br />
George Staikos <br />
889 Bay St. #205 <br />
Toronto, ON, M5S 3K5 <br />
Canada<br />
Phone: (416)-925-4030 <br />
<a href="m&#00097;i&#x6c;&#116;&#0111;:&#105;&#x6e;f&#x6f;&#00045;north&#x61;&#x6d;e&#x72;&#x69;ca&#64;k&#x64;&#101;&#46;o&#0114;g">inf&#111;-nor&#0116;hame&#114;&#x69;ca&#x40;&#107;&#00100;e.&#x6f;&#114;&#103;</a><br />
</td>

</tr>

<tr>
<td>
<b>Oceania</b><br />
Hamish Rodda<br />
11 Eucalyptus Road<br />
Eltham VIC 3095<br />
Australia<br />
Phone: (+61)402 346684<br />
<a href="ma&#105;lto&#58;&#105;n&#x66;o&#x2d;&#111;&#99;&#101;&#x61;n&#0105;&#00097;&#x40;&#x6b;&#100;e&#x2e;&#x6f;r&#00103;">&#105;nfo-oc&#x65;a&#x6e;&#x69;&#x61;&#00064;&#x6b;&#x64;&#x65;.org</a><br />
</td>

<td>
<b>South America</b><br />
Helio Chissini de Castro<br />
R. Jos&eacute; de Alencar 120, apto 1906<br />
Curitiba, PR 80050-240<br />
Brazil<br />
Phone: +55(41)262-0782 / +55(41)360-2670<br />
<a href="&#109;a&#x69;&#108;&#116;o:i&#110;&#x66;o&#00045;s&#x6f;u&#116;h&#00097;m&#101;r&#105;&#0099;&#x61;&#064;&#107;de.o&#114;g">in&#x66;o-&#00115;&#0111;ut&#x68;a&#109;&#101;ri&#099;a&#00064;&#107;de&#x2e;org</a><br />
</td>


</tr></table>

<?php

  include("footer.inc");
?>
