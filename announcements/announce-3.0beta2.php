<?php
  $page_title = "KDE 3.0-beta2 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<p>DATELINE FEBRUARY 13, 2002</p>
<p>FOR IMMEDIATE RELEASE</p>
<h3>Third Generation KDE Desktop in Second Round Beta Stage</h3>
<p><strong>The KDE Project Ships Second Beta of the Leading Desktop for Linux/UNIX</strong></p>
<p>February 13, 2002 (The INTERNET).
The <a href="http://www.kde.org/">KDE
Project</a> today announced the immediate release of KDE 3.0beta2,
the third pre-release of the third generation of KDE's free, powerful,
easy-to-use, Internet-enabled desktop for Linux and other UNIXes.
This KDE 3.0 beta ships with the core KDE libraries, the core
desktop environment, and over 100 applications from the other
base KDE packages (administration, games, multimedia, network, PIM,
utilities, artwork, addons, development, edutainment, bindings, etc.).
KDE 3.0 is
<a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">scheduled</a>
for final release early in the second quarter 2002, with one or more
intervening "RC" releases preceding the final release.
</p>
<p>
"Beta2 has come a long way since beta1", explained 
<a href="http://www.kde.org/people/dirk.html">Dirk Mueller</a>, the KDE 3
release coordinator. "At this juncture a lot of
user testing would be very beneficial, as a large group of KDE developers
are meeting for a week at the end of February in N&uuml;rnberg, Germany
to ready the KDE 3.0 final release.  In addition, this release
provides one of the last great opportunities for developers to port
KDE 2 applications to KDE 3 prior to the final release.<br />
We're especially interested in feedback about the support for right-to-left languages
and about the new KDE 3 features.  For developers, now is a great time
to complete <a href="#porting">porting</a> your applications to KDE 3, or
to pitch in and help implement the still-incomplete
<a href="http://developer.kde.org/development-versions/kde-3.0-features.html">planned
features</a>."
</p>
<p>
The primary goals of the 3.0beta2 release are to:
</p>
<ul>
<li>enlist volunteers to help
find elusive and platform-specific bugs, and to provide constructive
feedback through the KDE <a href="http://bugs.kde.org/">bugs database</a>
prior to the developer meeting later this month.<br />
<b>Please check the list of <a href="/info/3.0.html#bugs">known bugs</a> first
before opening a new problem report!</b>;
</li>
<li>provide a stable API so developers can complete
<a href="#porting">porting</a> their KDE 2 applications to KDE 3; and</li>
<li>provide a consistent snapshot for the
<a href="http://i18n.kde.org/teams/index.shtml">translation teams</a>
(this release already provides some support for
<a href="http://i18n.kde.org/teams/distributed.html"><strong>46</strong>
languages</a>).</li>
</ul>
<p>
Additional information about KDE 3, including
<a href="http://www.kde.org/kde2-and-kde3.html">instructions</a> for
setting up a KDE 3 system side-by-side with a KDE 2 system,
tentative
<a href="http://developer.kde.org/development-versions/kde-3.0-release-plan.html">KDE 3.0</a> release plan,
a KDE 3 <a href="http://www.kde.org/info/3.0.html">info page</a>, a list of
<a href="http://developer.kde.org/development-versions/kde-3.0-features.html">planned
features</a>, as well as a
<a href="http://www.kde.org/jobs/jobs-open.html">list of open
tasks</a> containing interesting projects for both users and developers
who wish to contribute to KDE,
is available at the KDE websites.
Please use the <a href="http://bugs.kde.org/">KDE bugs database</a> to
report bugs or make feature requests.
</p>
<p>
KDE and all its components (including the IDE
<a href="http://www.kdevelop.org/">KDevelop</a>) are available
<em><strong>for free</strong></em> under Open Source licenses from the KDE
<a href="http://download.kde.org/">ftp server</a>
and its <a href="http://www.kde.org/ftpmirrors.html">mirrors</a>.
<!--and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.
-->
</p>
<h4>Improvements</h4>
<p>
"One of the major improvements brought by KDE 3.0 over KDE 2.2 is the
Javascript/DHTML support in Konqueror," stated
<a href="http://people.mandrakesoft.com/~david/">David Faure</a>, a
Konqueror and KOffice developer.  "The DOM 2 model, used to render
an HTML page, is now mostly implemented, and changes
to the DOM tree are handled much better.  The Javascript bindings and
support is almost complete, faster and more stable than in KDE 2.  These
changes result in a much-improved rendering of dynamic websites and is
something users will immediately appreciate."
</p>
<p>
Besides the improvements to the underlying Qt library noted in the
<a href="http://www.kde.org/announcements/announce-3.0alpha1.html">KDE
3.0alpha1 announcement</a> and the improvements to KDE noted in the
<a href="http://www.kde.org/announcements/announce-3.0beta1.html">KDE
3.0beta1 announcement</a>, this release offers the following additional
improvements compared to the KDE 2 series:
</p>
<ul>
<li><a href="http://www.kde.org/">General</a>:
<ul>
  <li>support for multi-key shortcuts (emacs style), which uses
  a sequence of keys to take a specific action (<em>e.g.</em>,
  <code>Meta-I, K</code> launches Konqueror);</li>
  <li>improvements to service activation;</li>
  <li>improved <a href="http://www.cups.org/">CUPS</a> 
  <a href="http://printing.kde.org">printing support</a>; and</li>
  <li>added <a href="http://www.webdav.org/">WebDAV</a> support;</li>
</ul>
</li>
<li><a href="http://konqueror.kde.org/konq-browser.html">KHTML (HTML rendering engine)</a>:
<ul>
  <li>major improvements to the JavaScript implementation;</li>
  <li>added a smarter "window open" JavaScript policy;</li>
  <li>improvements in dynamic HTML/increased compatibility with other
browsers; and</li>
</ul>
</li>
<li><a href="http://konqueror.kde.org/">Konqueror</a> (web browser/file
manager/document viewer):
<ul>
  <li>added a GUI for configuring animated images;</li>
  <li>added a "new directory" feature in the sidebar's directory tree; and</li>
  <li>added the <em>kuick</em> plugin (for fast copying/moving);</li>
</ul>
</li>
<li><a href="http://noatun.kde.org/">Noatun</a> (multimedia player):
<ul>
  <li>added a Winamp skin loader; and</li>
  <li>added support for Icecast/Shoutcast streaming;</li>
</ul>
</li>
<li><a href="http://kate.sourceforge.net/">Kate</a>:  added a plugin
architecture and a new KTextEditor interface;</li>
<li>KWin:  the window manager now switches desktops as necessary when
dragging a window;</li>
<li>Kicker (panel) applets:  added the webserver kpf applet, for easy
sharing of files;</li>
<li><a href="http://www.slac.com/pilone/kpilot_home/">KPilot</a>:  added
support for USB Visors;</li>
<li><a href="http://devel-home.kde.org/~kdvi/">KDvi</a>:
<ul>
  <li>copying and pasting text from a .DVI file;</li>
  <li>full text search;</li>
  <li>export to plain text files;</li>
  <li>forward search with Emacs and XEmacs;</li>
  <li>inverse search with a variety of editors;</li>
  <li>a DCOP interface; and</li>
  <li>improved command-line options; and</li>
</ul>
</li>
<li><a href="http://edu.kde.org/">KDE Edu</a> (the new edutainment package)
comes with numerous new applications:
<ul>
  <li><a href="http://keduca.sourceforge.net/">KEduca</a> (an educational
project to enable the creation and revision of form-based tests and
exams);</li>
  <li><a href="http://kgeo.sourceforge.net/">KGeo</a> (an interactive geometry
learning program similar to Euklid(tm));</li>
  <li><a href="http://edu.kde.org/khangman/">KHangMan</a> (the well-known
word-solving game);</li>
  <li><a href="http://edu.kde.org/klatin/">KLatin</a> (a utility to help
revise or learn Latin);</li>
  <li><a href="http://edu.kde.org/klettres/">KLettres</a> (an alphabet and
sound-recognition game (in French));</li>
  <li><a href="http://edu.kde.org/kmessedwords/">KMessedWords</a> (a simple
mind-training game in which you have to guess a scrambled word);</li>
  <li><a href="http://kvoctrain.sourceforge.net/">kvoctrain</a> (a foreign
language vocabulary trainer).</li>
  <li><a href="http://edu.kde.org/kstars/">KStars</a> (a 
Desktop Planetarium for KDE).</li>
  <li><a href="http://edu.kde.org/ktouch/">KTouch</a> (a 
program for learning touch typing).</li>
</ul></li>
<li><a href="http://www.kdevelop.org/">KDevelop</a>:
 <ul>
  <li>more bugfixes for non-latin languages</li>
  <li>made addcxxflags working</li>
  <li>Accelerator clash with bookmarks and views repaired</li>
  <li>Added possibility to generate autoconf-magic for either Qt/KDE 2 or Qt/KDE 3</li>
  <li>many bugfixes</li>
  </ul>
  <a href="http://www.kdevelop.org/index.html?filename=changes.html">A detailed list of KDevelop-changes</a>.
</li>  
</ul>
<p>
Few additional feature enhancements to the KDE libraries and applications
are planned prior to the first stable KDE 3.0. 
</p>
<h4><a name="porting">Porting to KDE 3</a></h4>
<p>
Since KDE 3 is mostly source compatible with KDE 2, porting applications
from KDE 2 to KDE 3 can usually be done with relative ease and comfort.
The process is <em>substantially</em> easier than the one for porting
KDE 1 applications to KDE 2; even very complicated applications have been
ported in a matter of hours.
</p>
<p>
Instructions for porting KDE 2 applications to KDE 3 are available
separately for the
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE3PORTING.html">KDE
libraries</a> and the
<a href="http://doc.trolltech.com/3.0/porting.html">Qt libraries</a>.
Most of the changes required for the port applications pertain to changes
in the Qt API.  Although the KDE 3 API is not yet frozen, a few
changes are still anticipated for the final release of KDE 3.0.
</p>
<h4>Installing KDE 3.0beta2 Binary Packages</h4>
<p>
<em>Binary Packages</em>.
Some Linux distributors and some Unix distributors have provided
binary packages of KDE 3.0beta2 for recent versions of their distribution.
Some of these binary packages are available for free download under
<a href="http://download.kde.org/unstable/kde-3.0-beta2/">http://download.kde.org/unstable/kde-3.0-beta2/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="http://www.kde.org/ftpmirrors.html">mirrors</a>, and additional
binary packages, as well as updates to the packages now available, may
become available over the coming weeks.
</p>
<p>
Please note that the KDE team makes these packages available from the
KDE web site as a convenience to KDE users.  The KDE project is <u>not
responsible for these packages</u> as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.  If you cannot find a binary package for your distribution, 
please check again later or 
read the <a href="http://www.kde.org/packagepolicy.html">KDE Binary
Package Policy</a>.
</p>
<p>
<em>Library Requirements</em>.
The library requirements for a particular binary package vary with the
system on which the package was compiled.  Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was included with the applicable distribution (e.g., LinuxDistro 8.0
may have shipped with Qt-3.0beta6 but the packages below may require
Qt-3.0.1).  For general library requirements for KDE, please see the text at
<a href="#source_code-library_requirements">Source Code - Library
Requirements</a> below.
</p>
<p>
<a name="package_locations"><em>Package Locations</em></a>.
At the time of this release, pre-compiled packages are available for:
</p>
<ul>
<!-- NOT YET
  <li><a href="http://www.conectiva.com/">Conectiva Linux</a> (<a href="http://download.kde.org/unstable/kde-3.0-beta2/Conectiva/README">README</a>)
  <ul>
    <li>7.0:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/Conectiva/7.0/RPMS.kde/">Intel i386</a></li>
  </ul></li>
-->
  <li><a href="http://www.linux-mandrake.com/en/">Mandrake Linux</a>
  <ul>
    <li>8.1:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/Mandrake/8.1/">Intel i586</a>, <a href="http://download.kde.org/unstable/kde-3.0-beta2/Mandrake/ppc/">IBM PowerPC</a> and <a href="http://download.kde.org/unstable/kde-3.0-beta2/Mandrake/ia64/RPMS/">HP/Intel IA-64</a> (in each case, see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/Mandrake/noarch/">noarch</a> directory)</li>
    <li>8.0:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/Mandrake/8.0/RPMS/">Intel i586</a> (see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/Mandrake/noarch/">noarch</a> directory)</li>
  </ul>
  </li>
<!-- NOT YET
  <li><a href="http://www.redhat.com/">RedHat Linux</a>:
  <ul>
    <li>7.2:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/RedHat/7.2/i386/">Intel i386</a>, <a href="http://download.kde.org/unstable/kde-3.0-beta2/RedHat/7.2/ia64/">HP/Intel IA-64</a> and <a href="http://download.kde.org/unstable/kde-3.0-beta2/RedHat/7.2/alpha/">Compaq Alpha</a> (see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/RedHat/7.2/noarch/">noarch</a> directory)</li>
  </ul></li>
-->
  <li><a href="http://www.slackware-linux.org">Slackware</a>
  <ul>
    <li>8.0:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/Slackware/i386/8.0">Intel i386</a></li>
   </ul></li>
  <li><a href="http://www.suse.com/">SuSE Linux</a> (<a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/README">README</a>):
  <ul>
    <li>7.3:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/i386/7.3/">Intel i386</a>, <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/ppc/7.3/">IBM PowerPC</a> and <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/sparc/7.3/">Sun Sparc</a> (see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.2:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/i386/7.2/">Intel i386</a> (see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.1:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/i386/7.1/">Intel i386</a> (see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/noarch/">noarch</a> directory)</li>
    <li>7.0:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/i386/7.0/">Intel i386</a> and <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/s390/7.0/">IBM S390</a> (see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/noarch/">noarch</a> directory)</li>
    <li>6.4:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/i386/6.4/">Intel i386</a> (see also the <a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/noarch/">noarch</a> directory)</li>
  </ul></li>
  <li><a href="http://www.tru64unix.compaq.com/">Tru64 Systems</a> (<a href="http://download.kde.org/unstable/kde-3.0-beta2/Tru64/README.Tru64">README</a>)
  <ul>
    <li>Tru64 4.0d, e, f and g (and perhaps a, b and c), and 5.x:  <a href="http://download.kde.org/unstable/kde-3.0-beta2/Tru64/">Compaq Alpha</a></li>
  </ul></li>
</ul>
<p>
All of the above packages can also be obtained from the
<code>/pub/kde/unstable/kde-3.0-beta2/</code>
directory at one of the many KDE
<a href="http://www.kde.org/ftpmirrors.html">ftp server mirrors</a>.
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will likely become available over the
coming days and weeks.
</p>
<h4>Downloading and Compiling KDE 3.0beta2</h4>
<p>
<a name="source_code-library_requirements"></a><em>Library
Requirements</em>.
KDE 3.0beta2 requires the following libraries:
</p>
<ul>
<!-- not yet :-(
<li>qt-3.0.2, which is available in source code
from Trolltech as
<a href="ftp://ftp.trolltech.com/qt/source/qt-x11-free-3.0.2.tar.gz">qt-x11-free-3.0.2.tar.gz</a>;</li>
-->
<li>qt-pre-3.0.2, which is available as Qt RSYNC snapshot from
<a href="http://download.kde.org/unstable/kde-3.0-beta2/src/qt-rsync.tar.gz">KDE download</a></li>
<li>for reading help pages and KDE documentation,
<a href="ftp://speakeasy.rpmfind.net/pub/libxml/">libxml2</a> &gt;= 2.3.13
and <a href="http://xmlsoft.org/XSLT/">libxslt</a> &gt;= 1.0.7;</li>
<li>for JavaScript/ECMAScript regular expression support,
<a href="http://www.pcre.org/">PCRE</a> &gt;= 3.5;</li>
<li>for SSL support,
<a href="http://www.openssl.org/">OpenSSL</a> &gt;= 0.9.6
(versions 0.9.5x are no longer supported;<br />
<b>Note:</b> OpenSSL 0.9.6c has a symbol clash with Linux/glibc - don't use it on that platform!);</li>
<li>for Java support, a Java Virtual Machine (JVM) &gt;= 1.3;</li>
<li>for Netscape Communicator plugin support, a recent version of
<a href="http://www.lesstif.org/">Lesstif</a> or Motif;</li>
<li>for searching documentation,
<a href="http://www.htdig.org/">ht://dig</a>; and</li>
<li>for other special features, such as drag'n'drop audio CD ripping,
certain other packages.</li>
</ul>
<p>
<em>Compiler Requirements</em>.
Please note that some components of
KDE 3.0beta2 <b>might not</b> compile with older versions of
<a href="http://gcc.gnu.org/">gcc/egcs</a>, such as egcs-1.1.2 or
gcc-2.7.2. To be on the safe side, gcc-2.95 is required as minimum.  In addition, some
components of KDE 3.0beta2 (such as the multimedia backbone of KDE,
<a href="http://www.arts-project.org/">aRts</a>) will be miscompiled by
<a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0.x
(this problem is being addressed, but no time frame is available for the fix. We
heard rumors that it works with the GCC 3.1 branch but can not confirm this yet. 
We discourage the use of a gcc CVS snapshot for compiling KDE.).
</p>
<p>
<a name="source_code"></a><em>Source Code/SRPMs</em>.
The complete source code for KDE 3.0beta2 is available for
<a href="http://download.kde.org/unstable/kde-3.0-beta2/src/">download</a>
(<a href="http://download.kde.org/unstable/kde-3.0-beta2/kdevelop/">KDevelop</a>):
</p>
  <ul>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdeaddons-3.0beta2.tar.bz2">kdeaddons-3.0beta2.tar.bz2</a> (749k, sha1sum: <tt>332c6079ae74aaf30009e42731e8d9c5b12d2d0a</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdeadmin-3.0beta2.tar.bz2">kdeadmin-3.0beta2.tar.bz2</a> (1.1M, sha1sum: <tt>deee6aba1bc67aee47bd02c19af0692729e75361</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdeartwork-3.0beta2.tar.bz2">kdeartwork-3.0beta2.tar.bz2</a> (8.6M, sha1sum: <tt>ec1012811e19e1f603c8489ab920e2bf3c264601</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdebase-3.0beta2.tar.bz2">kdebase-3.0beta2.tar.bz2</a> (13M, sha1sum: <tt>db4fd925e20e01fdbdd3c38263b5e4ffb946dd85</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdebindings-3.0beta2.tar.bz2">kdebindings-3.0beta2.tar.bz2</a> (4.3M, sha1sum: <tt>164125760ea1b1cf2a4b02346f141794463478c0</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdeedu-3.0beta2.tar.bz2">kdeedu-3.0beta2.tar.bz2</a> (8.4M, sha1sum: <tt>39089eb446951198c60865ca90736fa728c2896f</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdegames-3.0beta2.tar.bz2">kdegames-3.0beta2.tar.bz2</a> (9.2M, sha1sum: <tt>3dfc4c1aa7ff192fc5e776e6a376c22fb919bae3</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdegraphics-3.0beta2.tar.bz2">kdegraphics-3.0beta2.tar.bz2</a> (2.0M, sha1sum: <tt>250ae9a562141ef6694a380e9cb089e16f9c1393</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdelibs-3.0beta2.tar.bz2">kdelibs-3.0beta2.tar.bz2</a> (7.1M, sha1sum: <tt>ca6344db44ec8e08a30f2d289c00e8cc74af8fe8</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdemultimedia-3.0beta2.tar.bz2">kdemultimedia-3.0beta2.tar.bz2</a> (5.3M, sha1sum: <tt>93980143b3f7ad6f51dbab80f42421fb32cd6cf3</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdenetwork-3.0beta2.tar.bz2">kdenetwork-3.0beta2.tar.bz2</a> (3.6M, sha1sum: <tt>3d12dcbc41fccab60c49a62134a24455c7137a18</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdepim-3.0beta2.tar.bz2">kdepim-3.0beta2.tar.bz2</a> (2.8M, sha1sum: <tt>28cd15bf6b9e7f3b0c7c009f62c267b8a883e8a4</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdesdk-3.0beta2.tar.bz2">kdesdk-3.0beta2.tar.bz2</a> (1.6M, sha1sum: <tt>317a60b06ed69aa1c286f45229219a64bcfd1773</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdetoys-3.0beta2.tar.bz2">kdetoys-3.0beta2.tar.bz2</a> (1.3M, sha1sum: <tt>385cde793024362bfdcf689e996945a6662c7254</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/kdeutils-3.0beta2.tar.bz2">kdeutils-3.0beta2.tar.bz2</a> (1.6M, sha1sum: <tt>e7ec10a5e6b47ad10b5d35247b28ba36f1ea81cc</tt>)</li>
</ul>
<p>The internationalisation packages (you only need either the complete kde-i18n package or the subpackage for your language):</p>
<ul>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-3.0beta2.tar.bz2">kde-i18n-3.0beta2.tar.bz2</a> (82M, sha1sum: <tt>132c5a08a8d77f01e135531cd32b2016357fd62a</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-af-3.0beta2.tar.bz2">kde-i18n-af-3.0beta2.tar.bz2</a> (1.7M, sha1sum: <tt>acec52d27d8d3e2696bda7fbb8612ae8327427a8</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-ar-3.0beta2.tar.bz2">kde-i18n-ar-3.0beta2.tar.bz2</a> (1.1M, sha1sum: <tt>d171f780d6812026245e76280eaec5f083fc8fd5</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-az-3.0beta2.tar.bz2">kde-i18n-az-3.0beta2.tar.bz2</a> (1.7M, sha1sum: <tt>7674b0300c359ed9aee972068e56d2f7c0ba543e</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-bg-3.0beta2.tar.bz2">kde-i18n-bg-3.0beta2.tar.bz2</a> (1.5M, sha1sum: <tt>698129756ba6121c94f8d2ec1274b653d5755ceb</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-ca-3.0beta2.tar.bz2">kde-i18n-ca-3.0beta2.tar.bz2</a> (997k, sha1sum: <tt>7540b6037deac9d9f9578375c20812fa038c55e4</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-cs-3.0beta2.tar.bz2">kde-i18n-cs-3.0beta2.tar.bz2</a> (1.9M, sha1sum: <tt>f6b4f48afa1597c3eb7a50fac327439b53ce0cc8</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-da-3.0beta2.tar.bz2">kde-i18n-da-3.0beta2.tar.bz2</a> (6.3M, sha1sum: <tt>19ac155342d282c0b8740c3c8d25245be64d9c70</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-de-3.0beta2.tar.bz2">kde-i18n-de-3.0beta2.tar.bz2</a> (8.1M, sha1sum: <tt>669e007cfb75737bf09084f3e2e74c2277b9aadc</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-el-3.0beta2.tar.bz2">kde-i18n-el-3.0beta2.tar.bz2</a> (909k, sha1sum: <tt>4bc9a6b86e4d4d543f5407402e099af429e418a5</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-en_GB-3.0beta2.tar.bz2">kde-i18n-en_GB-3.0beta2.tar.bz2</a> (1.8M, sha1sum: <tt>249a48fd38cfbff5f45cc615ae640f16b3dd463d</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-eo-3.0beta2.tar.bz2">kde-i18n-eo-3.0beta2.tar.bz2</a> (1.3M, sha1sum: <tt>ab1e41c7e27535bc735df7e2793ca0ba210bf7d2</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-es-3.0beta2.tar.bz2">kde-i18n-es-3.0beta2.tar.bz2</a> (6.1M, sha1sum: <tt>7364bada9f0a1497db77e729ef4af8b5327eed93</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-et-3.0beta2.tar.bz2">kde-i18n-et-3.0beta2.tar.bz2</a> (1.5M, sha1sum: <tt>cb9f34a66113d067cdf22684839cc3503ca16fd2</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-fi-3.0beta2.tar.bz2">kde-i18n-fi-3.0beta2.tar.bz2</a> (1.5M, sha1sum: <tt>5c5f47121ba46af10d1e50cb3c3372a5d5508a3b</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-fr-3.0beta2.tar.bz2">kde-i18n-fr-3.0beta2.tar.bz2</a> (9.0M, sha1sum: <tt>5d229ff948945a38a926d7ea8f87a4b093fb673f</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-he-3.0beta2.tar.bz2">kde-i18n-he-3.0beta2.tar.bz2</a> (1.8M, sha1sum: <tt>5b692e1aa240b5362c51bd3b352534051cf4f14a</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-hr-3.0beta2.tar.bz2">kde-i18n-hr-3.0beta2.tar.bz2</a> (1.1M, sha1sum: <tt>fb62f15bc88668fb7c1cc86cde0b6848614345f6</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-hu-3.0beta2.tar.bz2">kde-i18n-hu-3.0beta2.tar.bz2</a> (3.7M, sha1sum: <tt>21c0373c528987145e29cfe06c3eb3def8a720eb</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-id-3.0beta2.tar.bz2">kde-i18n-id-3.0beta2.tar.bz2</a> (1.3M, sha1sum: <tt>386fad604cd2c1e6de2e17b5c7a253f957fab94b</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-is-3.0beta2.tar.bz2">kde-i18n-is-3.0beta2.tar.bz2</a> (1.3M, sha1sum: <tt>01d7e8c1ce2f9709418c7c6599fff48a15804ce4</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-it-3.0beta2.tar.bz2">kde-i18n-it-3.0beta2.tar.bz2</a> (1.9M, sha1sum: <tt>be904599bc93fe798e0bb84f3eae6603e2af701b</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-ja-3.0beta2.tar.bz2">kde-i18n-ja-3.0beta2.tar.bz2</a> (1.6M, sha1sum: <tt>3339c2b85fbc4d01c6cce4180869d67b6c194eba</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-ko-3.0beta2.tar.bz2">kde-i18n-ko-3.0beta2.tar.bz2</a> (1.1M, sha1sum: <tt>9fe1c268a966b4dfc3aa6dd80031a6ce07ccf2e6</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-lt-3.0beta2.tar.bz2">kde-i18n-lt-3.0beta2.tar.bz2</a> (969k, sha1sum: <tt>93dc5286497ca669daf221c0fb1a61a0994053c4</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-lv-3.0beta2.tar.bz2">kde-i18n-lv-3.0beta2.tar.bz2</a> (1.4M, sha1sum: <tt>fddfb86166c00c99071481e60bf28876ab575d31</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-mt-3.0beta2.tar.bz2">kde-i18n-mt-3.0beta2.tar.bz2</a> (1.1M, sha1sum: <tt>1a67a81d77f5eaed3927563911ff675b0e98af10</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-nb-3.0beta2.tar.bz2">kde-i18n-nb-3.0beta2.tar.bz2</a> (1.7M, sha1sum: <tt>8c5a67e0466c8fe60318d7779eb09a155434c38d</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-nl-3.0beta2.tar.bz2">kde-i18n-nl-3.0beta2.tar.bz2</a> (2.9M, sha1sum: <tt>9f65d2f9090aa4f60c721f9f21ff0ebc57f5554e</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-nn-3.0beta2.tar.bz2">kde-i18n-nn-3.0beta2.tar.bz2</a> (1.6M, sha1sum: <tt>ef223a0addbc29d934daf01a0aec0d16d053c0de</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-pl-3.0beta2.tar.bz2">kde-i18n-pl-3.0beta2.tar.bz2</a> (1.3M, sha1sum: <tt>a4f0ee189bf52b3f56f7d4803e4f1f55a44dec7c</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-pt-3.0beta2.tar.bz2">kde-i18n-pt-3.0beta2.tar.bz2</a> (8.8M, sha1sum: <tt>5cc4893fd7d15871230be2b80d8855cffd635277</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-pt_BR-3.0beta2.tar.bz2">kde-i18n-pt_BR-3.0beta2.tar.bz2</a> (1.5M, sha1sum: <tt>c3a677752008916d01d6816db14728c63b21a2e1</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-ro-3.0beta2.tar.bz2">kde-i18n-ro-3.0beta2.tar.bz2</a> (1.7M, sha1sum: <tt>4971af30e619f1251fb407bc806c37f4a9dcfa90</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-ru-3.0beta2.tar.bz2">kde-i18n-ru-3.0beta2.tar.bz2</a> (2.7M, sha1sum: <tt>e36c1cb372383593e2a302b8f01cdb889454f99d</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-sk-3.0beta2.tar.bz2">kde-i18n-sk-3.0beta2.tar.bz2</a> (4.4M, sha1sum: <tt>c1d7b07e439216ce9cfe71924c3939ad8e6b5b51</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-sl-3.0beta2.tar.bz2">kde-i18n-sl-3.0beta2.tar.bz2</a> (4.8M, sha1sum: <tt>db8e2e10512b20b0ff7dde183332aabf8ec11f54</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-sr-3.0beta2.tar.bz2">kde-i18n-sr-3.0beta2.tar.bz2</a> (1.2M, sha1sum: <tt>cd7011553e10cb0b0dbbd6e8a77edf4b419f09ca</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-sv-3.0beta2.tar.bz2">kde-i18n-sv-3.0beta2.tar.bz2</a> (12M, sha1sum: <tt>cad6f03b8076fd194cb80845286b44a9a4059715</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-ta-3.0beta2.tar.bz2">kde-i18n-ta-3.0beta2.tar.bz2</a> (1.2M, sha1sum: <tt>b819ac5ee9c75cfdfb213c8d17ccb171868afa06</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-th-3.0beta2.tar.bz2">kde-i18n-th-3.0beta2.tar.bz2</a> (1.2M, sha1sum: <tt>bca5d9a81f8a5af138612433c7bd63d7fd4154fc</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-tr-3.0beta2.tar.bz2">kde-i18n-tr-3.0beta2.tar.bz2</a> (1.6M, sha1sum: <tt>cf84457cf82678e5823a4bd472b6a7998d75355d</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-uk-3.0beta2.tar.bz2">kde-i18n-uk-3.0beta2.tar.bz2</a> (1.5M, sha1sum: <tt>f2baa5a0094a6ed1e77cc86efa8dd4028cdd9be1</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/src/i18n/kde-i18n-xh-3.0beta2.tar.bz2">kde-i18n-xh-3.0beta2.tar.bz2</a> (2.6M, sha1sum: <tt>c0db58b83f87f1c512f9a2ff9eb4a0ca60bbfebb</tt>)</li>
</ul>
<p>KDevelop:</p>
<ul>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/kdevelop/kdevelop-2.1beta2_for_KDE_2.2.tar.bz2">kdevelop-2.1beta2_for_KDE_2.2.tar.bz2</a> (3.1M, sha1sum: <tt>5e5b83ea5f745d2901ee75a78ff4f638b8552245</tt>)</li>
<li><a href="http://download.kde.org/unstable/kde-3.0-beta2/kdevelop/kdevelop-2.1beta2_for_KDE_3.0.tar.bz2">kdevelop-2.1beta2_for_KDE_3.0.tar.bz2</a> (3.1M, sha1sum: <tt>7c0857b11ce619d59ccb3422449dd0f7f653235b</tt>)</li>
</ul>
<p>
Additionally, source rpms are available for the following distributions:</p>
<ul>
  <li><a href="http://download.kde.org/unstable/kde-3.0-beta2/Mandrake/SRPMS/">Mandrake Linux</a></li>
<!-- NOT YET
  <li><a href="http://download.kde.org/unstable/kde-3.0-beta2/SuSE/SRPMS/">SuSE Linux</a></li>
-->
</ul>
<p>All of the above packages can also be obtained from the
<code>/pub/kde/unstable/kde-3.0-beta2/</code>
directory at one of the many KDE
<a href="http://www.kde.org/ftpmirrors.html">ftp server mirrors</a>.
</p>
<p>
<em>Further Information</em>.  For further
instructions on compiling and installing KDE 3.0beta2, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you should encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>.
</p>
<h4>About KDE</h4>
<p>
KDE is an independent, collaborative project by hundreds of developers
worldwide working over the Internet to create a sophisticated,
customizable and stable desktop environment employing a component-based,
network-transparent architecture.  KDE provides a stable, mature desktop,
an office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools, and an efficient and intuitive
development environment, including an excellent IDE
(<a href="http://www.kdevelop.org/">KDevelop</a>).
KDE is working proof of the power of
the Open Source "Bazaar-style" software development model to create
first-rate technologies on par with and superior to even the most complex
commercial software.
</p>
<p>
Please visit the KDE family of web sites for the
<a href="http://www.kde.org/faq.html">KDE FAQ</a>, screenshots
(<a href="http://www.kde.org/screenshots/kde2shots.html">KDE 2</a>,
<a href="http://www.kde.org/screenshots/kde3shots.html">KDE 3</a>),
<a href="http://www.koffice.org/">KOffice information</a> and
<a href="http://developer.kde.org/documentation/kde3arch.html">developer
information</a>.
Much more <a href="http://www.kde.org/whatiskde/">information</a>
about KDE is available from KDE's
<a href="http://www.kde.org/family.html">family of web sites</a>.
</p>
<h4>Corporate KDE Sponsors</h4>
<p>
Besides the valuable and excellent efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>.  In addition,
the members of the <a href="http://www.kdeleague.org/">KDE League</a> provide
significant support for promoting KDE.  Thanks!
</p>
<hr noshade="noshade" size="1" width="98%" align="center" />
<font size="2">
<em>Trademarks Notices.</em>
KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V.

Compaq, Alpha, iPAQ and Tru64 are either trademarks and/or service marks or
registered trademarks and/or service marks of Compaq Computer Corporation.

HP is a registered trademark of Hewlett-Packard Company.

IBM and PowerPC are registered trademarks of IBM Corporation.

Intel, i386 and i586 are trademarks or registered trademarks of Intel
Corporation or its subsidiaries in the United States and other countries.

Linux is a registered trademark of Linus Torvalds.

Netscape Communicator is a trademark or registered trademark of
Netscape Communications Corporation in the United States and other countries.

Java and Sun are trademarks or registered trademarks of Sun Microsystems, Inc.
in the United States and other countries.

Trolltech and Qt are trademarks of Trolltech AS.

UNIX and Motif are registered trademarks of The Open Group.

<!--Zaurus is a trademark of Sharp Electronics Corporation in the United
States and/or other countries.
-->

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
<br />
</font>
<hr noshade="noshade" size="1" width="98%" align="center" />
<table border="0" cellpadding="8" cellspacing="0">
<tr><th colspan="2" align="left">
Press Contacts:
</th></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
United&nbsp;States:
</td><td nowrap="nowrap">
Eunice Kim<br />
The Terpin Group<br />
ekim@terpin.com<br />
(1) 650 344 4944 ext. 105<br />&nbsp;<br />
Andreas Pour<br />
KDE League, Inc.<br />
po&#00117;&#114;&#064;kd&#x65;.&#111;r&#x67;<br />
(1) 917 312 3122
</td></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
Europe (French and English):
</td><td nowrap="nowrap">
David Faure<br />
&#0102;a&#117;re&#x40;&#x6b;&#00100;e&#x2e;o&#x72;g<br />
(33) 4 3250 1445
</td></tr>
<tr valign="top"><td align="right" nowrap="nowrap">
Europe (German and English):
</td><td nowrap="nowrap">
Ralf Nolden<br />
&#0110;o&#0108;&#100;e&#110;&#x40;kde&#0046;&#x6f;r&#103;<br />
(49) 2421 502758
</td></tr>
</table>
<!-- $Id: announce-3.0beta2.php 523083 2006-03-27 11:22:27Z scripty $ -->
<?php
  include "footer.inc"
?>
