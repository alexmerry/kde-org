<?php
  $page_title = "The User's Perspective: Deploying KDE";
  $site_root = "../../";
  include "header.inc";
?>

<p>The easiest way to install KDE is to download or purchase a Linux, BSD or other UNIX operating system
that includes KDE (which includes most of them) and install the Operating System from CD or DVD media onto a computer. Many of these systems default to KDE for the user interface, though you may have to explicitly select KDE as an option during installation.</p>

<p>Alternatively, KDE can be downloaded over the Internet by visiting <a href="http://download.kde.org/stable/3.2">download.kde.org</a>. Source code along with vendor supplied and supported binary packages are available. KDE can also be purchased separately on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>.</p>

<p>Once KDE is installed, there are <a href="http://www.kde.org/family/">many online resources</a> to be tapped. There is a <a href="https://www.kde.org/documentation/quickstart/index.html">quickstart guide to KDE</a>, an <a href="https://www.kde.org/documentation/userguide/index.html">online users guide</a>, a <a href="https://www.kde.org/documentation/faq/index.html">Frequently Asked Questions list</a> and a guide
for <a href="https://www.kde.org/areas/sysadmin/">system administrators who suport KDE installations</a>. There are <a href="https://mail.kde.org/mailman/listinfo/kde">email lists</a> for users to ask questions and get timely answers on, <a href="http://kde-forum.org/">web forums</a> and <a href="http://wiki.kdenews.org/">community wiki</a> sites that contain vast stores of information for users.</p>

<p>There is also <a href="http://enterprise.kde.org/bizdir">commercial support</a> available for KDE in
addition to the support provided by the many <a href="http://www.kde.org/download/distributions.php">operating
system vendors</a> that ship KDE.</p>

<table style="border: solid 1px;" align="center" cellpadding="6" cellspacing="0">
<tr>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Users</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">Developers</th>
    <th style="border-left: solid 1px black; background: #3E91EB; color: white;" nowrap="nowrap">About KDE</th>
</tr>
<tr>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="whykde.php">Why KDE?</a><br/>
<a href="deploying.php">Deploying KDE</a><br/>
<a href="supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap">
<a href="../announce-3.2_developers/whykde.php">Why KDE?</a><br/>
<a href="../announce-3.2_developers/developing.php">Developing With KDE</a><br/>
<a href="../announce-3.2_developers/supporting.php">Supporting KDE</a>
</td>
<td valign="top" style="border-left: solid 1px black;" nowrap="nowrap"><a href="http://www.kde.org">The KDE Project</a><br/>
<a href="http://www.kde.org/areas/kde-ev/">KDE e.V.</a><br/>

</td>
</tr>
</table>

<?php
  include("footer.inc");
?>
