<?php

  include_once ("functions.inc");
  $translation_file = "kde-org";
  $release = '4.12';
  $release_full = '4.12.0';
  $page_title = i18n_noop("KDE Applications 4.12 Bring Huge Step Forward in Personal Information Management and Improvements All Over");
  $site_root = "..";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<?php i18n("December 18, 2013");?>
</p>

<p>
<?php i18n("The KDE Community is proud to announce the latest major updates to the KDE Applications delivering new features and fixes. This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate streamlined the integration of Python plugins and added initial Vim-macro support and the games and educational applications bring a variety of new features.");?>
</p>

<?php showscreenshotpng("kate.png", ""); ?>

<p>
<?php i18n("The most advanced Linux graphical text editor Kate has again received work on code completion, this time introducing <a href='http://scummos.blogspot.com/2013/10/advanced-code-completion-filtering-in.html'>advanced matching code, handling abbreviation and partial matching in classes</a>. For example, the new code would match a typed 'QualIdent' with 'QualifiedIdentifier'. Kate also gets <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-18th-august-2013'>initial Vim macro support</a>. Best of all, these improvements also trickle through to KDevelop and other applications using the Kate technology.");?>
</p>

<?php showscreenshotpng("okular.png", ""); ?>

<p>
<?php i18n("Document viewer Okular <a href='http://tsdgeos.blogspot.com/2013/10/changes-in-okular-printing-for-412.html'>now takes printer hardware margins into account</a>, has audio and video support for epub, better search and can now handle more transformations including those from Exif image metadata. In the UML diagram tool Umbrello, associations can now be <a href='http://dot.kde.org/2013/09/20/kde-commit-digest-1st-september-2013'>drawn with different layouts</a> and Umbrello <a href='http://dot.kde.org/2013/09/09/kde-commit-digest-25th-august-2013'>adds visual feedback if a widget is documented</a>.");?>
</p>

<p>
<?php i18n("Privacy guard KGpg shows more information to users and KWalletManager, the tool to save your passwords, can now <a href='http://www.rusu.info/wp/?p=248'>store them in GPG form</a>. Konsole introduces a new feature: Ctrl-click to directly launch URLs in console output. It can now also <a href='http://martinsandsmark.wordpress.com/2013/11/02/mangonel-1-1-and-more/'>list processes when warning about quit</a>.");?>
</p>

<?php showscreenshotpng("dolphin.png", ""); ?>

<p>
<?php i18n("KWebKit adds the ability to <a href='http://dot.kde.org/2013/08/09/kde-commit-digest-7th-july-2013'>automatically scale content to match desktop resolution</a>. File manager Dolphin introduced a number of performance improvements in sorting and showing files, reducing memory usage and speeding things up. KRDC introduced automatic reconnecting in VNC and KDialog now provides access to 'detailedsorry' and 'detailederror' message boxes for more informative console scripts. Kopete updated its OTR plugin and the Jabber protocol has support for XEP-0264: File Transfer Thumbnails. Besides these features the main focus was on cleaning code up and fixing compile warnings.
");?>
</p>

<h3><?php i18n("Games and educational software");?></h3>
<p>
<?php i18n("The KDE Games have seen work in various areas. KReversi is <a href='http://tsdgeos.blogspot.ch/2013/10/kreversi-master-is-now-qt-quick-based.html'>now QML and Qt Quick based</a>, making for a prettier and more fluid game experience. KNetWalk has also <a href='http://tsdgeos.blogspot.ch/2013/08/knetwalk-portedx-to-qtquick.html'>been ported</a> with the same benefit as well as the ability to set a grid with custom width and height. Konquest now has a new challenging AI player named 'Becai'.");?>
</p>

<p>
<?php i18n("In the Educational applications there have been some major changes. KTouch <a href='http://blog.sebasgo.net/blog/2013/11/12/what-is-new-for-ktouch-in-kde-sc-4-dot-12/'>introduces custom lesson support and several new courses</a>; KStars has a new, more accurate <a href='http://knro.blogspot.ch/2013/10/demo-of-ekos-alignment-module.html'>alignment module for telescopes</a>, find a <a href='http://www.youtube.com/watch?v=7Dcn5aFI-vA'>youtube video here</a> of the new features. Cantor, which offers an easy and powerful UI for a variety of mathematical backends, now has backends <a href='http://blog.filipesaraiva.info/?p=1171'>for Python2 and Scilab</a>. Read more about the powerful Scilab backend <a href='http://blog.filipesaraiva.info/?p=1159'>here</a>. Marble adds integration with ownCloud (settings are available in Preferences) and adds overlay rendering support. KAlgebra makes it possible to export 3D plots to PDF, giving a great way of sharing your work. Last but not least, many bugs have been fixed in the various KDE Education applications.
");?>
</p>

<h3><?php i18n("Mail, calendar and personal information");?></h3>

<p>
<?php i18n("KDE PIM, KDE's set of applications for handling mail, calendar and other personal information, has seen a lot of work.");?>
</p>

<p>
<?php i18n("Starting with email client KMail, there is now <a href='http://dot.kde.org/2013/10/11/kde-commit-digest-29th-september-2013'>AdBlock support</a> (when HTML is enabled) and improved scam detection support by extending shortened URLs. A new Akonadi Agent named FolderArchiveAgent allows users to archive read emails in specific folders and the GUI of the Send Later functionality has been cleaned up. KMail also benefits from improved Sieve filter support. Sieve allows for server-side filtering of emails and you can now <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-script-parsing-22/'>create and modify the filters on the servers</a> and <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-sieve-12/'>convert existing KMail filters to server filters</a>. KMail's mbox support <a href='http://www.aegiap.eu/kdeblog/2013/08/new-in-kdepim-4-12-mboximporter/'>has also been improved</a>.
");?>
</p>

<p>
<?php i18n("In other applications, several changes make work easier and more enjoyable. A new tool is introduced, <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>the  ContactThemeEditor</a>, which allows for creating KAddressBook Grantlee themes for displaying contacts. The addressbook can now also show previews before printing data. KNotes has seen some <a href='http://www.aegiap.eu/kdeblog/2013/11/what-news-in-kdepim-4-12-knotes/'>serious work on solving bugs</a>. Blogging tool Blogilo can now deal with translations and there are a wide variety of fixes and improvements all over the KDE PIM applications.
");?>
</p>

<p>
<?php i18n("Benefiting all applications, the underlying KDE PIM data cache has <a href='http://ltinkl.blogspot.ch/2013/11/this-month-october-in-red-hat-kde.html'>seen much work on performance, stability and scalability</a>, fixing <a href='http://www.progdan.cz/2013/10/akonadi-1-10-3-with-postgresql-fix/'>support for PostgreSQL with the latest Qt 4.8.5</a>. And there is a new command line tool, the calendarjanitor which can scan all calendar data for buggy incidences and adds a debug dialog for search. Some very special hugs go to Laurent Montel for the work he is doing on KDE PIM features!
");?>
</p>

<h4><?php i18n("Installing KDE Applications");?></h4>
<?php
  include("boilerplate.inc");
?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

<?php
  include("footer.inc");
?>
