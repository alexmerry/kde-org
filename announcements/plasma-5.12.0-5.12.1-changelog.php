<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.12.1 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.12.1";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>Use Qt.openUrlExternally instead of Kio. <a href='https://commits.kde.org/bluedevil/52d1950213b91b80f5e2d087b6672eab8d6c8249'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10397'>D10397</a></li>
<li>Use plasmoid.icon instead of Logic.icon(). <a href='https://commits.kde.org/bluedevil/2c5de6e121b5f9ac06da83b50a6c917e1ba2f4bf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10395'>D10395</a></li>
</ul>


<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Fix LnF theme data for window decoration. <a href='https://commits.kde.org/breeze/ed754c8ba48de279f63e8eb36160f537bef9543d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10386'>D10386</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Update copyright year. <a href='https://commits.kde.org/discover/fed1f5da1b1f0ffc99d242721e3abc0ad6bf3d27'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390358'>#390358</a></li>
<li>Notify about the backend. <a href='https://commits.kde.org/discover/3dd3fed38b41f679d7a9748a8dd2d0a67758cd7d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390339'>#390339</a></li>
<li>Don't notify when the number of updates becomes smaller. <a href='https://commits.kde.org/discover/670bdaef73042facc66b9aaf6fd39697ede8167b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390322'>#390322</a></li>
<li>Remove commented out code. <a href='https://commits.kde.org/discover/1cbb58aaacc6f6c2ad1cae90f0694fec48576780'>Commit.</a> </li>
<li>Forgot to add file. <a href='https://commits.kde.org/discover/894c4dec7995bc16703f8ddc8b0940ee72436b9f'>Commit.</a> </li>
<li>Fix freeze when showing the ApplicationPage. <a href='https://commits.kde.org/discover/a2d6e79586b8c73f2e2d6bb1262d348eb53f4b52'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390123'>#390123</a></li>
<li>Only report changes when they actually happen. <a href='https://commits.kde.org/discover/bc3597f615e5b6a9f43c338e2107a7d0ca40232f'>Commit.</a> </li>
<li>Fix binding loop when showing the update description. <a href='https://commits.kde.org/discover/dcb1c342ef6990bb9a924e05f2c5d18a9b3bff33'>Commit.</a> </li>
<li>Disable Review button for resources that aren't installed. <a href='https://commits.kde.org/discover/f66ab0fa45a1180f82873d74229c21616d4681c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390053'>#390053</a></li>
<li>Show a beautiful disabled icon for updates. <a href='https://commits.kde.org/discover/d7d7904b5a8e8cca03216907f1b3ee0707aa0f08'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390076'>#390076</a></li>
<li>Make sure we mark as busy before clearling the list. <a href='https://commits.kde.org/discover/12138479ca378d4e36fb0e9b6dcc5ecef88ab200'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390114'>#390114</a></li>
<li>Add some paddings to the Review button. <a href='https://commits.kde.org/discover/30575ac706dee8b2ee4ed9a6343922309d414884'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390030'>#390030</a></li>
<li>Make it possible to specify a fallback appstream id. <a href='https://commits.kde.org/discover/83f96d8d654a6a95619e6de92980bb6eae4c2e2b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390016'>#390016</a></li>
<li>Use the default passive notification amount. <a href='https://commits.kde.org/discover/4aebbb7041e5665ca2d5bc8e30e8b38de2643de7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388087'>#388087</a></li>
<li>Remove redundant action. <a href='https://commits.kde.org/discover/f8c669a83ee607a591b8039b90f7b98e41b118a9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389767'>#389767</a></li>
<li>Fix tests, now we don't show a list from a URL. <a href='https://commits.kde.org/discover/ffb3e258e34553c8b0c75848102a68474a78bd24'>Commit.</a> </li>
<li>Only show the description for PackageKit sources. <a href='https://commits.kde.org/discover/e8df9489c89b491c3eea3a88c10760376257322e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389715'>#389715</a></li>
<li>Print a warning when a remote can't be removed. <a href='https://commits.kde.org/discover/da017bcba05e94ff17181fef6bceb2db219e803c'>Commit.</a> </li>
<li>Fix dummy build. <a href='https://commits.kde.org/discover/5cce834474bb41afea7033b8be1ef8769826837b'>Commit.</a> </li>
<li>Address source by id look-up in Flatpak source. <a href='https://commits.kde.org/discover/35554f560e243ceb8dcd983a50026adda6abf4df'>Commit.</a> </li>
<li>Fix source removal. <a href='https://commits.kde.org/discover/27a58799fa49e45ecbadb0b0576f04c7435dea4b'>Commit.</a> </li>
<li>Make it possible for the source id to be different from the display string. <a href='https://commits.kde.org/discover/3243f99b75ecebbc2bfa59b42de3b024eca4e3a2'>Commit.</a> See bug <a href='https://bugs.kde.org/389715'>#389715</a></li>
<li>Fix warnings. <a href='https://commits.kde.org/discover/a1053e72a9d58d36cd89ffd147e68de722665d47'>Commit.</a> </li>
<li>Reduce duplication in flatpak sources display. <a href='https://commits.kde.org/discover/6588facb6a815e7a2a1a18e663201e289df78071'>Commit.</a> </li>
<li>Flatpak: don't show full url, just the hostname. <a href='https://commits.kde.org/discover/71d06e3c9f2dabeef571d0c27c91b041f4eea0a2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389714'>#389714</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Require KDE Frameworks 5.42 and Qt 5.0. <a href='https://commits.kde.org/drkonqi/c6df0ca6a33b1aee93758b74ba5a8fdcc2732ace'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>[kbroadcastnotification] Fix missing KAboutData cmld args processing. <a href='https://commits.kde.org/kde-cli-tools/ec4a3abb2192e22a9149f6174d3aad6471175c29'>Commit.</a> </li>
<li>[kbroadcastnotification] Fix missing KLS::setApplicationDomain. <a href='https://commits.kde.org/kde-cli-tools/ca0e1c28a35444228524370298ce48a4d6021da3'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/kde-cli-tools/fe98682fba719cca2c427aace77ae0d5548d18a0'>Commit.</a> </li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Make sure there are less unnecessary changed() emissions. <a href='https://commits.kde.org/kde-gtk-config/54d8e029422e30e7197fe40e35ff4c6345f6962c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388126'>#388126</a></li>
<li>Use new connect syntax. <a href='https://commits.kde.org/kde-gtk-config/b81629d37f2eaf0da5dfb6b55c4aaaab03c696a5'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Ensure Registry gets destroyed before the Wayland connection. <a href='https://commits.kde.org/kinfocenter/a7e887975d0181d9039e3d727b0f8172b0bfc2e3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387479'>#387479</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10391'>D10391</a></li>
<li>Show high-resolution and vector logos properly in HighDPI mode. <a href='https://commits.kde.org/kinfocenter/82e64226c2e628aab2afdcefdadf0246a59f1b71'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388633'>#388633</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10357'>D10357</a></li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Require Qt 5.9. <a href='https://commits.kde.org/kscreenlocker/19809533f7fa38aae3113b5366f387f53f2c9f02'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/ksysguard/8f4929f94da92a6a786477d3488f8b91b086c3da'>Commit.</a> </li>
<li>Correction with the & problem in tabs. <a href='https://commits.kde.org/ksysguard/2a95e337cfd4a0c0943bee653b6e446c2ce1d32c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382512'>#382512</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10151'>D10151</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Fix compilation without libinput. <a href='https://commits.kde.org/kwin/b1035b6826b7b91b8a337890fc2fc84fb1fdc6c2'>Commit.</a> </li>
<li>Make it possible to compile 5.12 branch with a kdecoration from master. <a href='https://commits.kde.org/kwin/8245a74b31dc69af06b273bd61ac4a3c5a121be4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10296'>D10296</a></li>
<li>[x11] Fix interactive point selection. <a href='https://commits.kde.org/kwin/ea5e70116456975b3ca3f379777b8a83291bb4e8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387720'>#387720</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10302'>D10302</a></li>
<li>Block geometry updates on move resize finish and don't configure xdg shell surfaces while blocked. <a href='https://commits.kde.org/kwin/aefa11f11a251d1270da08a73b367088baec0d56'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/388072'>#388072</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10156'>D10156</a></li>
<li>Enable blending if a subsurface has an alpha channel. <a href='https://commits.kde.org/kwin/e22d9d957b12fa84d552c8787df4426b22d4d0fe'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10060'>D10060</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Fix missing Qt5::Gui in public link interface, pulled in by QQuaternion. <a href='https://commits.kde.org/libkscreen/db520eae9597a1f551371c4c0360e4e8c97e4ef6'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42 and Qt 5.9. <a href='https://commits.kde.org/libkscreen/e3cedff68a53758a5cc7653030d770d096a4d2ad'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Fix LnF theme data for window decoration. <a href='https://commits.kde.org/oxygen/0f9d5c8e1bdcd56ee2f566eefa9b77c87d178818'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10384'>D10384</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Implement Tab/Backtab navigation. <a href='https://commits.kde.org/plasma-desktop/c85eb1aef49e42b0240e1b456d757c644ba142c0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387316'>#387316</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10456'>D10456</a></li>
<li>[touchpad kcm] No KLocalizedString::setApplicationDomain() in kded plugin. <a href='https://commits.kde.org/plasma-desktop/8991459da2bd2fbe804b30127cc362545a9a6a95'>Commit.</a> </li>
<li>[knetattach] Fix i18n calls being made before QApp creation. <a href='https://commits.kde.org/plasma-desktop/760196c9f4e3207c7a8cb37d7bdc2fbe21877383'>Commit.</a> </li>
<li>Fixed mouse settings module crashing on Wayland. <a href='https://commits.kde.org/plasma-desktop/483565374f7992a087585bbf5af55ab05b60d212'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/389978'>#389978</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10359'>D10359</a></li>
<li>[Task Manager] Use KFilePlacesModel::isHidden. <a href='https://commits.kde.org/plasma-desktop/51a927375c90f5de583dbe29d5c154a1217a4871'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10328'>D10328</a></li>
<li>Improve preview thumbnail quality. <a href='https://commits.kde.org/plasma-desktop/db42961ad4d8c87877e549ce63f81b9f2bf7b0b3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376848'>#376848</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10223'>D10223</a></li>
<li>Minor optimization: use reserve(). <a href='https://commits.kde.org/plasma-desktop/84120565350cce8c2ccc0cf0f2c1462634adc295'>Commit.</a> </li>
<li>[kwin & plasma-desktop runners] add missing TRANSLATION_DOMAIN definition. <a href='https://commits.kde.org/plasma-desktop/98978e0bf6032c6914c0421a20372ee3578d178b'>Commit.</a> </li>
<li>Fix kded5 startup warning:. <a href='https://commits.kde.org/plasma-desktop/c666a0f3c180266f434d050113f79f19196a4c96'>Commit.</a> </li>
<li>Fix warnings in kded5 startup, due to i18n(""). <a href='https://commits.kde.org/plasma-desktop/aac598dbcb795f4773541be643189c263398accb'>Commit.</a> </li>
<li>Require KDE Frameworks 5.42. <a href='https://commits.kde.org/plasma-desktop/9a10dbf5846ef2ce2c2314206a1a6eeaa79b2638'>Commit.</a> </li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>Support "phone" form factor. <a href='https://commits.kde.org/plasma-pa/2b8fd79fbf14a35f9f90227fa0a8fdbb5987bfe2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10396'>D10396</a></li>
<li>Applet: Fix accessing SourceModel from main item. <a href='https://commits.kde.org/plasma-pa/fb789820c69ed6b7ab78dde305b46d7ae558a2fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/390161'>#390161</a>. Phabricator Code review <a href='https://phabricator.kde.org/D10426'>D10426</a></li>
</ul>


<h3><a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a> </h3>
<ul id='ulplasma-vault' style='display: block'>
<li>Set icons for the Vault creation, configuration and mounting dialogues. <a href='https://commits.kde.org/plasma-vault/45ad41edad0df1d118317cba9497c02fb1b3870d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387556'>#387556</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Improve appearance of the logout dialog on wayland. <a href='https://commits.kde.org/plasma-workspace/4d7c1345b784f0b582301886651c8807ecfaac5a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10425'>D10425</a></li>
<li>Fix Breeze LnF theme data for window decoration. <a href='https://commits.kde.org/plasma-workspace/a2b84af70a11085289b6bd89c84c98a01dcbf1ba'>Commit.</a> </li>
<li>Fix dupe handling in requestAddLauncherToActivities; improve unit test. <a href='https://commits.kde.org/plasma-workspace/7abd8d0da4472c33d9def3a4b6786417a3195c1e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10253'>D10253</a></li>
<li>[Notifications] Let clear entry also clear history. <a href='https://commits.kde.org/plasma-workspace/a3664acb605419eca732493fe81ca74fa2b366ab'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10321'>D10321</a></li>
<li>[LauncherTasksModel] Support applications: URLs in requestOpenUrls. <a href='https://commits.kde.org/plasma-workspace/eff4bc7c3cf32927878776f444dc5b46912d6e87'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10327'>D10327</a></li>
<li>[appstream runner] Add missing Messages.sh file and set TRANSLATION_DOMAIN. <a href='https://commits.kde.org/plasma-workspace/2b7dbb611b35ca76efafcc30f4cbb7e650731e59'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10268'>D10268</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Require KDE Frameworks 5.42 and Qt 5.0. <a href='https://commits.kde.org/systemsettings/a7d8b76bc8c224bdd504b98602ed8713f15fdca5'>Commit.</a> </li>
<li>Fix crash when searching. <a href='https://commits.kde.org/systemsettings/d314bce549f63735e1746101aaae8880011b6704'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D10272'>D10272</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
