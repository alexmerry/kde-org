------------------------------------------------------------------------
r1096628 | tokoe | 2010-02-27 19:40:44 +1300 (Sat, 27 Feb 2010) | 2 lines

Use plugins from $INSTALL_DIR/lib/ and not $INSTALL_DIR/share/apps

------------------------------------------------------------------------
r1096719 | mlaurent | 2010-02-28 02:31:46 +1300 (Sun, 28 Feb 2010) | 5 lines

Backport:
Fix crash when we paste element, we didn't look at if mimeData->format() was not empty
and QMimeData was deleting during menu.exec()


------------------------------------------------------------------------
r1096724 | mlaurent | 2010-02-28 02:42:08 +1300 (Sun, 28 Feb 2010) | 3 lines

Backport:
Fix crash when we dnd file

------------------------------------------------------------------------
r1096766 | djarvie | 2010-02-28 04:35:59 +1300 (Sun, 28 Feb 2010) | 2 lines

Allow a time-out before killing the audio thread

------------------------------------------------------------------------
r1096850 | mlaurent | 2010-02-28 09:24:37 +1300 (Sun, 28 Feb 2010) | 6 lines

Backport:
Fix paste in monthview when we don't select an item, just select a cell.
It was never working before it

MERGE: e4/e5/3.5 ?

------------------------------------------------------------------------
r1097387 | mlaurent | 2010-03-01 21:29:36 +1300 (Mon, 01 Mar 2010) | 2 lines

Korganizer config items were renaming but wizards were never adapting.

------------------------------------------------------------------------
r1097397 | mlaurent | 2010-03-01 21:48:39 +1300 (Mon, 01 Mar 2010) | 2 lines

Migrate freebusy config

------------------------------------------------------------------------
r1097566 | mkoller | 2010-03-02 06:00:28 +1300 (Tue, 02 Mar 2010) | 3 lines

BUG: 228080
Allow single key shortcuts again

------------------------------------------------------------------------
r1098037 | mlaurent | 2010-03-03 07:22:04 +1300 (Wed, 03 Mar 2010) | 3 lines

Fix bug #228530
CCBUG: 228530

------------------------------------------------------------------------
r1098093 | tmcguire | 2010-03-03 10:03:27 +1300 (Wed, 03 Mar 2010) | 6 lines

Update message indicator DrawAttentionProperty when a new mail is received.

Patch by "sylv1", thanks!

http://reviewboard.kde.org/r/3082/

------------------------------------------------------------------------
r1098120 | tmcguire | 2010-03-03 11:18:16 +1300 (Wed, 03 Mar 2010) | 2 lines

For consistency: Disable Nepomuk error message here as well.

------------------------------------------------------------------------
r1098399 | lueck | 2010-03-04 05:56:38 +1300 (Thu, 04 Mar 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1098672 | scripty | 2010-03-04 15:17:53 +1300 (Thu, 04 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1098962 | smartins | 2010-03-05 08:39:06 +1300 (Fri, 05 Mar 2010) | 7 lines

Backport r1097494 by smartins from trunk to branch 4.4:

Only calculate the duration for events.

Fixes kolab/issue4168


------------------------------------------------------------------------
r1098965 | smartins | 2010-03-05 08:52:37 +1300 (Fri, 05 Mar 2010) | 7 lines

Backport r1098554 by smartins from trunk to branch 4.4:

First ask the view for a start and end date hint, then, if the dates are invalid, fallback.

Fixes a bug where the event editor appeared with nothing in the start and end fields because the calculated date
was overwritten with an invalid one

------------------------------------------------------------------------
r1098984 | smartins | 2010-03-05 10:21:07 +1300 (Fri, 05 Mar 2010) | 5 lines

Backport r1097744,r1097757 by smartins from trunk to branch4.4:

Remember the agenda mode (work, nextX, week, day, other) in config.
kolab/issue4055

------------------------------------------------------------------------
r1099034 | djarvie | 2010-03-05 11:52:31 +1300 (Fri, 05 Mar 2010) | 2 lines

Fix compile warning

------------------------------------------------------------------------
r1099041 | djarvie | 2010-03-05 11:58:18 +1300 (Fri, 05 Mar 2010) | 1 line

Remove unused #includes
------------------------------------------------------------------------
r1099047 | winterz | 2010-03-05 12:02:00 +1300 (Fri, 05 Mar 2010) | 5 lines

merge SVN commit 1099019 by winterz:

crash guard in setBodyPartMemento if the memento argument is 0.
possible fix for kolab/issue4187

------------------------------------------------------------------------
r1099282 | tmcguire | 2010-03-06 01:06:32 +1300 (Sat, 06 Mar 2010) | 7 lines

Disable photo display, the exec() in the ContactSearchJob is causing too many problems.

In this case, the body parts that were loaded on demand arrived during the exec(), and
modified the MIME tree, which was unexpected by the current parseOTP() run.

BUG: 186502

------------------------------------------------------------------------
r1099548 | reed | 2010-03-06 07:43:15 +1300 (Sat, 06 Mar 2010) | 1 line

missing strigi include
------------------------------------------------------------------------
r1099992 | scripty | 2010-03-07 02:39:37 +1300 (Sun, 07 Mar 2010) | 3 lines

do not delete all the cpp files
this is Albert

------------------------------------------------------------------------
r1100054 | smartins | 2010-03-07 06:24:47 +1300 (Sun, 07 Mar 2010) | 6 lines

forward port r1099063 by smartins from e35 to branch 4.4:

Fix startdate calculation for recurring to-dos.

kolab/issue4109

------------------------------------------------------------------------
r1100055 | smartins | 2010-03-07 06:29:02 +1300 (Sun, 07 Mar 2010) | 5 lines

Forwardport r1097339 by winterz from e35 to trunk:

fix due date computation for recurring to-dos when an active date is specified.
kolab/issue4110

------------------------------------------------------------------------
r1100175 | smartins | 2010-03-07 13:59:21 +1300 (Sun, 07 Mar 2010) | 6 lines

Backport r1100143 by smartins from e35 to branch4.4:

When pasting to-dos in the todo-view don't change the start/due dates because todo-view doesn't support date navigation.

Fixes kolab/issue4195

------------------------------------------------------------------------
r1100469 | smartins | 2010-03-08 05:09:02 +1300 (Mon, 08 Mar 2010) | 6 lines

Forwardport r1100202 by smartins from e35 to branch4.4:

Renamed Baseview's selectedDates() to selectedIncidenceDates() so it's clear it returns the dates of selected incidences and not 
dates of selected cells.


------------------------------------------------------------------------
r1100477 | smartins | 2010-03-08 05:21:17 +1300 (Mon, 08 Mar 2010) | 4 lines

Forwardport r1100203 by smartins from e35 to branch 4.4:

When pasting, we want the date of the selected cell, not the selected incidence.

------------------------------------------------------------------------
r1101302 | tmcguire | 2010-03-10 10:24:18 +1300 (Wed, 10 Mar 2010) | 6 lines

Respect the attachment strategy when printing.
Now, I can set it to "Hide" and print my hotel reservation, without printing annyoing
pictures of the Hotel logo, a photo of the Hotel and so on.

MERGE: trunk

------------------------------------------------------------------------
r1101315 | tmcguire | 2010-03-10 10:51:35 +1300 (Wed, 10 Mar 2010) | 2 lines

Revert parts that shouldn't have been committed.

------------------------------------------------------------------------
r1102269 | osterfeld | 2010-03-12 20:00:14 +1300 (Fri, 12 Mar 2010) | 2 lines

bump version
SVN_SILENT
------------------------------------------------------------------------
r1102439 | winterz | 2010-03-13 03:28:02 +1300 (Sat, 13 Mar 2010) | 6 lines

backport SVN commit 1102436 by winterz

make the timeoffset spinbox allow only 5 digits, just like when
setting a simple reminder.
kolab/issue4207

------------------------------------------------------------------------
r1102559 | ahartmetz | 2010-03-13 10:14:32 +1300 (Sat, 13 Mar 2010) | 1 line

Disable nepomuk_email_feeder until it compiles again - seems to be a Nepomuk issue.
------------------------------------------------------------------------
r1102827 | smartins | 2010-03-14 08:00:14 +1300 (Sun, 14 Mar 2010) | 9 lines

Backport r1102818 by smartins from trunk to 4.4:

Added some comments on CalendarView::activeDate(), it doesn't return the selected incidence date but the selected date in the view,
some views like agenda support both incidence selection and time range selection, and the dates can be different.

I wrote CalendarView::activeIncidenceDate() which returns the date of the selected incidence.

Fixes a bug where you edited the second occurrence of a recurring to-do but had a range selection in the first day.

------------------------------------------------------------------------
r1102849 | smartins | 2010-03-14 08:37:30 +1300 (Sun, 14 Mar 2010) | 4 lines

Backport r1102845 by smartins from trunk to 4.4:

Fix cornercase where, if the first occurrence is in the exception list, the editor's start and end date will be invalid.

------------------------------------------------------------------------
r1103366 | mlaurent | 2010-03-15 11:31:58 +1300 (Mon, 15 Mar 2010) | 4 lines

Fix bug #230493
Too many bullet points displayed in Journal entry description
CCBUG: 230493

------------------------------------------------------------------------
r1103775 | mlaurent | 2010-03-16 10:10:20 +1300 (Tue, 16 Mar 2010) | 7 lines

Backport:
Fix bug #230508
=> don't show option for printing month/day element when we want to select one element
CCBUG: 230508

MERGE: e4/e5/3.5

------------------------------------------------------------------------
r1103780 | mlaurent | 2010-03-16 10:19:42 +1300 (Tue, 16 Mar 2010) | 2 lines

Add missing signal

------------------------------------------------------------------------
r1103788 | mlaurent | 2010-03-16 10:50:43 +1300 (Tue, 16 Mar 2010) | 4 lines

Fix bug #230516
timelabel widget was not updating when we changed zoom
CCBUG: 230516

------------------------------------------------------------------------
r1103917 | mlaurent | 2010-03-16 22:11:38 +1300 (Tue, 16 Mar 2010) | 2 lines

Fix show popupmenu when we select a todo item which doesn't have start date

------------------------------------------------------------------------
r1103976 | smartins | 2010-03-17 01:15:17 +1300 (Wed, 17 Mar 2010) | 14 lines

SVN_MERGE:
Merged revisions 1103767 via svnmerge from
svn+ssh://smartins@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim

........
  r1103767 | smartins | 2010-03-15 16:56:28 -0400 (Mon, 15 Mar 2010) | 6 lines

  Events ending at 00h appear in the wrong day in day-view

  Fixes kolab/issue4215.

  MERGE: trunk, 4.4
........

------------------------------------------------------------------------
r1104086 | smartins | 2010-03-17 06:07:22 +1300 (Wed, 17 Mar 2010) | 5 lines

Backport r1104065 by smartins from trunk to 4.4:

To-dos due at 00:00 shouldn't appear in the allday zone.

Fixes kolab/issue4064.
------------------------------------------------------------------------
r1104509 | djarvie | 2010-03-18 10:57:20 +1300 (Thu, 18 Mar 2010) | 2 lines

Bug 230819: Fix command output display alarms with an audio file specified, being reloaded as audio-only alarms.

------------------------------------------------------------------------
r1104518 | djarvie | 2010-03-18 12:26:00 +1300 (Thu, 18 Mar 2010) | 2 lines

Update version number

------------------------------------------------------------------------
r1104661 | mlaurent | 2010-03-18 21:08:16 +1300 (Thu, 18 Mar 2010) | 4 lines

Backport:
Remove duplicate code
Don't show all print option because we select just an element

------------------------------------------------------------------------
r1104687 | smartins | 2010-03-18 23:09:26 +1300 (Thu, 18 Mar 2010) | 6 lines

Backport r1104683 by smartins from trunk to branch 4.4:

We have no guarantee that mAgendaViews isn't empty here, because a view could be removed right before this slot is called.

Fixes a crash found while trying to reproduce kolab/issue3750.

------------------------------------------------------------------------
r1104713 | mlaurent | 2010-03-19 00:09:43 +1300 (Fri, 19 Mar 2010) | 2 lines

Don't load all plugins when not necessary

------------------------------------------------------------------------
r1104726 | mlaurent | 2010-03-19 00:38:35 +1300 (Fri, 19 Mar 2010) | 2 lines

Minor fix

------------------------------------------------------------------------
r1104798 | dfaure | 2010-03-19 05:43:11 +1300 (Fri, 19 Mar 2010) | 3 lines

Fix sorting of tasks by priority so that "--" (unspecified) is less than specified priorities.
CCMAIL: iamsergio@gmail.com

------------------------------------------------------------------------
r1104884 | dfaure | 2010-03-19 09:15:23 +1300 (Fri, 19 Mar 2010) | 2 lines

revert last commit after sergio told me that 1 is higher priority than 9; the bug was in yokadi.

------------------------------------------------------------------------
r1104887 | dfaure | 2010-03-19 09:21:00 +1300 (Fri, 19 Mar 2010) | 2 lines

oops, actually revert.

------------------------------------------------------------------------
r1105232 | smartins | 2010-03-20 05:07:27 +1300 (Sat, 20 Mar 2010) | 6 lines

Forwardport r1104929 by smartins from e35 to 4.4:

Don't crash kmail when renaming newly created kolab sub-resources in korganizer.

kolab/issue3658

------------------------------------------------------------------------
r1105243 | smartins | 2010-03-20 05:29:32 +1300 (Sat, 20 Mar 2010) | 4 lines

Backport r1105222 by smartins from trunk to branch 4.4:

Disable option in the import dialog that isn't yet supported in kontact.

------------------------------------------------------------------------
r1105352 | djarvie | 2010-03-20 11:53:44 +1300 (Sat, 20 Mar 2010) | 2 lines

Fix missing AUDIO alarm type

------------------------------------------------------------------------
r1105354 | djarvie | 2010-03-20 11:54:59 +1300 (Sat, 20 Mar 2010) | 2 lines

Remove unused #include

------------------------------------------------------------------------
r1105507 | mlaurent | 2010-03-21 02:00:35 +1300 (Sun, 21 Mar 2010) | 6 lines

Fix bug #231372 and #231373
When we right click on item in timeline cut/copy/send as Ical didn't work because
we couldn't remove of selected item.
CCBUG: 231372
CCBUG: 231373

------------------------------------------------------------------------
r1105554 | mlaurent | 2010-03-21 04:08:35 +1300 (Sun, 21 Mar 2010) | 4 lines

Backport:
fix bug #231379
MERGE: e4/e5/3.5?

------------------------------------------------------------------------
r1105839 | mlaurent | 2010-03-22 02:46:26 +1300 (Mon, 22 Mar 2010) | 2 lines

Remove "return" not necessary here

------------------------------------------------------------------------
r1105841 | mlaurent | 2010-03-22 02:54:41 +1300 (Mon, 22 Mar 2010) | 2 lines

Fix create event 

------------------------------------------------------------------------
r1105860 | mlaurent | 2010-03-22 03:29:02 +1300 (Mon, 22 Mar 2010) | 3 lines

Fix crash when we try to rename filter and list of pattern is empty
MERGE: e4/e5/3.5

------------------------------------------------------------------------
r1106503 | smartins | 2010-03-23 14:27:46 +1300 (Tue, 23 Mar 2010) | 6 lines

Backport r1106500 by smartins from trunk to branch 4.4:

Make to-dos show in agenda view. Month view is ok.

CCBUG: 231380

------------------------------------------------------------------------
r1106636 | smartins | 2010-03-24 02:31:36 +1300 (Wed, 24 Mar 2010) | 6 lines

Backport r1106634 by smartins from trunk to branch 4.4:

Fix to-dos sometimes not being displayed in day-view

kolab/issue4064

------------------------------------------------------------------------
r1106951 | cgiboudeaux | 2010-03-24 22:59:17 +1300 (Wed, 24 Mar 2010) | 1 line

Bump version.
------------------------------------------------------------------------
r1107288 | mlaurent | 2010-03-25 21:55:20 +1300 (Thu, 25 Mar 2010) | 2 lines

Backport fix #232057

------------------------------------------------------------------------
r1107296 | mlaurent | 2010-03-25 22:17:59 +1300 (Thu, 25 Mar 2010) | 4 lines

Fix bug #232055
We can't add a reminder on a journal => don't show menu entry for it.
BUG: 232055

------------------------------------------------------------------------
r1107322 | mlaurent | 2010-03-25 23:58:05 +1300 (Thu, 25 Mar 2010) | 5 lines

Fix first part of bug #231891
combobox item was not put to minutes when we put it to minutes
Need to look at  second part of bug
CCBUG: 231891

------------------------------------------------------------------------
r1107617 | mlaurent | 2010-03-26 22:24:23 +1300 (Fri, 26 Mar 2010) | 2 lines

We use qt>=4.6 in kde >= 4.4

------------------------------------------------------------------------
