------------------------------------------------------------------------
r853907 | marten | 2008-08-28 14:00:18 +0200 (Thu, 28 Aug 2008) | 7 lines

Backport of trunk commit 843864 (sorry, missed the 4.1.1 tagging deadline):

Set the destination folder for a new account, so that the inbox will appear
if it was previously hidden.

CCBUG:168544

------------------------------------------------------------------------
r854871 | djarvie | 2008-08-30 15:46:35 +0200 (Sat, 30 Aug 2008) | 2 lines

Improve mime type detection for file display alarms

------------------------------------------------------------------------
r854899 | lueck | 2008-08-30 17:09:41 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r855231 | tmcguire | 2008-08-31 12:44:39 +0200 (Sun, 31 Aug 2008) | 38 lines

Backport r855184 by tmcguire from trunk to the 4.1 branch:

Merged revisions 853947,853957 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r853947 | ervin | 2008-08-28 16:01:59 +0200 (Thu, 28 Aug 2008) | 24 lines
  
  Merged revisions 853423,853436,853494 via svnmerge from 
  svn+ssh://ervin@svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r853423 | vkrause | 2008-08-27 19:47:40 +0200 (Wed, 27 Aug 2008) | 4 lines
    
    Don't trigger a complete sync when selecting just a single account.
    
    Kolab issue 2489
  ........
    r853436 | vkrause | 2008-08-27 20:11:00 +0200 (Wed, 27 Aug 2008) | 4 lines
    
    That website doesn't exist anymore.
    
    Kolab issue 2111
  ........
    r853494 | vkrause | 2008-08-27 21:39:51 +0200 (Wed, 27 Aug 2008) | 5 lines
    
    Don't abort the completion if the user has changed the selection while
    we were waiting for the LDAP search.
    
    Kolab issue 2655
  ........
................
  r853957 | ervin | 2008-08-28 16:12:30 +0200 (Thu, 28 Aug 2008) | 2 lines
  
  Yeah, this one was pretty stupid... *sigh*
................


------------------------------------------------------------------------
r855273 | tmcguire | 2008-08-31 13:50:59 +0200 (Sun, 31 Aug 2008) | 2 lines

Uups, disabled this by accident in r853314.

------------------------------------------------------------------------
r855863 | winterz | 2008-09-01 18:10:55 +0200 (Mon, 01 Sep 2008) | 7 lines

backport SVN commit 855862 by winterz:

remove the HAVE_NIE conditional which is no longer needed.
now we get the feeder binary build and installed properly.

CCMAIL: kde-packagers@kde.org

------------------------------------------------------------------------
r855970 | ereslibre | 2008-09-01 22:10:25 +0200 (Mon, 01 Sep 2008) | 3 lines

Backport fix (Fix crash on Kontact when loading Akregator part. The XML file wasn't being found, because the plugin wasn't calling setComponentData 
on its)

------------------------------------------------------------------------
r855979 | ereslibre | 2008-09-01 22:22:55 +0200 (Mon, 01 Sep 2008) | 2 lines

Backport (Since it is a KParts::Plugin, install the rc and desktop file where they should go...)

------------------------------------------------------------------------
r856012 | ereslibre | 2008-09-02 01:01:02 +0200 (Tue, 02 Sep 2008) | 3 lines

Backport fix (Now... since we set the component data, the rc file is search on its directory. This should completely fix this crash of not found 
file"

------------------------------------------------------------------------
r856015 | djarvie | 2008-09-02 01:19:21 +0200 (Tue, 02 Sep 2008) | 2 lines

Double click accepts template in pick list

------------------------------------------------------------------------
r856061 | scripty | 2008-09-02 07:56:40 +0200 (Tue, 02 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r856396 | ereslibre | 2008-09-02 21:29:35 +0200 (Tue, 02 Sep 2008) | 3 lines

Backport of fix (I changed the lessThan method for the navigator toolbar that is on the inverse layout direction than the rest of the system for the 
actions being on the same order, but hey... I was giving the shortcuts on order too. Invert them, since I am inverting the order they are added.)

------------------------------------------------------------------------
r856468 | winterz | 2008-09-03 00:27:05 +0200 (Wed, 03 Sep 2008) | 6 lines

backport SVN commit 856466 by winterz:

patch from Ingmar that prevents application plugins from being built
if their associated applications are not being built.


------------------------------------------------------------------------
r856885 | winterz | 2008-09-04 04:03:39 +0200 (Thu, 04 Sep 2008) | 5 lines

backport SVN commit 856882 by winterz:

fix display of multiday event when the current day isn't the first day
of that event.

------------------------------------------------------------------------
r857064 | wstephens | 2008-09-04 17:20:10 +0200 (Thu, 04 Sep 2008) | 10 lines

Backport r856969

"
No need to rebuild the extension widget after configuration
changed, as the editor is not part of the widget any longer.

BUG: 169948
"
CC: tokoe@kde.org

------------------------------------------------------------------------
r857104 | osterfeld | 2008-09-04 20:24:41 +0200 (Thu, 04 Sep 2008) | 3 lines

backport totalCount() caching
CCBUG:170156

------------------------------------------------------------------------
r857894 | djarvie | 2008-09-06 22:57:27 +0200 (Sat, 06 Sep 2008) | 1 line

Remove unused #include
------------------------------------------------------------------------
r857916 | djarvie | 2008-09-07 00:56:51 +0200 (Sun, 07 Sep 2008) | 2 lines

Use KColorButton for picking colours

------------------------------------------------------------------------
r858152 | djarvie | 2008-09-07 15:35:14 +0200 (Sun, 07 Sep 2008) | 2 lines

Make text in edit alarm dialogue change colour when foreground colour changed.

------------------------------------------------------------------------
r858206 | djarvie | 2008-09-07 17:22:10 +0200 (Sun, 07 Sep 2008) | 2 lines

Fix comments/apidox

------------------------------------------------------------------------
r858265 | tmcguire | 2008-09-07 18:41:52 +0200 (Sun, 07 Sep 2008) | 9 lines

Backport r855882 by tmcguire from trunk to the 4.1 branch:

Don't abort() here, but silently ignore the error.
This fixes a crash when renaming an account while the root IMAP folder is selected,
but probably only hides another problem...

CCBUG: 170149.


------------------------------------------------------------------------
r858266 | tmcguire | 2008-09-07 18:45:00 +0200 (Sun, 07 Sep 2008) | 13 lines

Backport r856781 by tmcguire from trunk to the 4.1 branch:

Don't crash when replying after changing folder settings.

This was caused by a dangling KMsgBase* in MessageActions.

Now, when a folder is closed (for example by the folder dialog),
emit the selectionChanged() signal to make sure everyone can udpate
their idea about the current selected message.

CCBUG: 170302


------------------------------------------------------------------------
r858267 | tmcguire | 2008-09-07 18:47:18 +0200 (Sun, 07 Sep 2008) | 6 lines

Backport r857007 by tmcguire from trunk to the 4.1 branch:

Make checkHighest() work again.
Now TLS and SSL are checked automatically, no longer "None".


------------------------------------------------------------------------
r858690 | osterfeld | 2008-09-08 14:53:22 +0200 (Mon, 08 Sep 2008) | 1 line

backport: avoid UI lockup when the article list header menu is shown
------------------------------------------------------------------------
r858827 | osterfeld | 2008-09-08 21:45:29 +0200 (Mon, 08 Sep 2008) | 1 line

backport: avoid multiple connects
------------------------------------------------------------------------
r859356 | scripty | 2008-09-10 08:47:54 +0200 (Wed, 10 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r860088 | winterz | 2008-09-12 01:46:35 +0200 (Fri, 12 Sep 2008) | 8 lines

backport SVN commit 860086 by winterz:

work around what krake and I think is a Qt bug when converting hh:mm to a QTime.
we do a silly check to see if the user supplied seconds for the start/end times,
and if not, we append ":00" for them.

i.e. "10:00" becomes "10:00:00" so QTime::fromString( Qt::ISODate) doesn't fail.

------------------------------------------------------------------------
r860260 | tmcguire | 2008-09-12 15:07:17 +0200 (Fri, 12 Sep 2008) | 3 lines

Backport fixes for multiday events not being displayed:
r857547 and minimal version of r860220.

------------------------------------------------------------------------
r860261 | tmcguire | 2008-09-12 15:10:06 +0200 (Fri, 12 Sep 2008) | 10 lines

Backport r860031 by tmcguire from trunk to the 4.1 branch:

Fix KMail notifications not working in Kontact.
Also, convert to notifications to proper error dialogs.

Akregator probably needs a similar fix.

CCBUG: 170314


------------------------------------------------------------------------
r860263 | tmcguire | 2008-09-12 15:12:43 +0200 (Fri, 12 Sep 2008) | 14 lines

Backport r860255 by tmcguire from trunk to the 4.1 branch:

Merged revisions 860040 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

........
  r860040 | vkrause | 2008-09-11 22:13:52 +0200 (Thu, 11 Sep 2008) | 4 lines
  
  Disable job auto-deletion during the error handling, the subeventloop of
  the error dialog might otherwise execute the delayed deletion too
  early, causing a crash.
........


------------------------------------------------------------------------
r860276 | tmcguire | 2008-09-12 15:47:11 +0200 (Fri, 12 Sep 2008) | 4 lines

Backport SVN commit 860274 by tmcguire:

Fix days-to-go for multiday, non-allday events in the future.

------------------------------------------------------------------------
r860950 | tmcguire | 2008-09-14 19:47:53 +0200 (Sun, 14 Sep 2008) | 4 lines

Backport r860837 by tmcguire from trunk to the 4.1 branch:

Fix crash when connecting to an IMAP account.

------------------------------------------------------------------------
r861352 | mlaurent | 2008-09-15 23:08:58 +0200 (Mon, 15 Sep 2008) | 3 lines

Backport:
disable/enabled line edit

------------------------------------------------------------------------
r861766 | scripty | 2008-09-17 08:53:28 +0200 (Wed, 17 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r862567 | tokoe | 2008-09-19 11:48:47 +0200 (Fri, 19 Sep 2008) | 5 lines

Backport of bugfix #171228

Check whether image was loaded successfully to not run
into a false assert in KPixmapRegionSelectionDialog

------------------------------------------------------------------------
r863197 | mlaurent | 2008-09-21 14:04:31 +0200 (Sun, 21 Sep 2008) | 3 lines

Backport:
Fix translator

------------------------------------------------------------------------
r863203 | mlaurent | 2008-09-21 14:20:48 +0200 (Sun, 21 Sep 2008) | 3 lines

Backport:
fix enable/disable date select

------------------------------------------------------------------------
r863369 | lcorbasson | 2008-09-22 00:09:17 +0200 (Mon, 22 Sep 2008) | 4 lines

automatically merged revision 863340:
Fix the URL and the parsing of the picture page to adapt to newer MediaWiki versions.

Thanks Arnout Boelens for reporting this bug.
------------------------------------------------------------------------
r863416 | scripty | 2008-09-22 08:02:29 +0200 (Mon, 22 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r863770 | scripty | 2008-09-23 08:09:04 +0200 (Tue, 23 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r863842 | tokoe | 2008-09-23 12:36:04 +0200 (Tue, 23 Sep 2008) | 2 lines

Backport of rev 857017

------------------------------------------------------------------------
r864084 | mlaurent | 2008-09-23 23:38:16 +0200 (Tue, 23 Sep 2008) | 1 line

Allow to compile with strigi < and >= 0.6.0
------------------------------------------------------------------------
r864185 | scripty | 2008-09-24 09:03:36 +0200 (Wed, 24 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r864308 | winterz | 2008-09-24 14:41:46 +0200 (Wed, 24 Sep 2008) | 6 lines

backport SVN commit 861138 by tokoe:

Don't use the setCheckedState() method to rename 'Show XYZ' to 'Hide XYZ'
as stated in the API docs of KToggleAction!


------------------------------------------------------------------------
r864309 | winterz | 2008-09-24 14:47:19 +0200 (Wed, 24 Sep 2008) | 5 lines

backport SVN commit 860909 by alexmerry:

Make clicking the tray icon show the Akregator window.
CCMAIL: kde@randomguy3.me.uk

------------------------------------------------------------------------
r864322 | tstaerk | 2008-09-24 15:44:25 +0200 (Wed, 24 Sep 2008) | 1 line

Start as gui application when called by clicking onto ktimetracker's default icon. That means, do not fall in konsole mode if called with the parameters -caption whatever.
------------------------------------------------------------------------
r864418 | djarvie | 2008-09-24 21:10:48 +0200 (Wed, 24 Sep 2008) | 1 line

Fix missing work-time parameter value in D-Bus interface
------------------------------------------------------------------------
