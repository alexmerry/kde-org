<?php 
      # AUTOGENERATED FILE! ALL CHANGES MADE HERE WILL BE LOST.
      # Please edit the XML file instead
      $page_title = "KDE 4.6.5 Changelog";
      $site_root = "../../";
      include "header.inc";?>
  

  

  

  

  <h2>Changes in KDE 4.6.5</h2>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_6_5/kdebase.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="konq-plugins">konq-plugins</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Make the image gallery plugin generate correctly-encoded comments. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=209426">209426</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/f5a4f96df01ddcf32fdc0be01d557e85d66bfca3">f5a4f96</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_6_5/kdeutils.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/sweeper" name="sweeper">Sweeper</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix to clear "Closed Items History" Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=183197">183197</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1236147&amp;view=rev">1236147</a>. </li>
        <li class="bugfix normal">Fix dbus call to clear "Run Command History" Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=202811">202811</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1235740&amp;view=rev">1235740</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdewebdev"><a name="kdewebdev">kdewebdev</a><span class="allsvnchanges"> [ <a href="4_6_5/kdewebdev.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kfilereplace">KFileReplace</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Load translation catalog kfilereplace when using kfilereplacepart in Konqueror Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=273470">273470</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1236692&amp;view=rev">1236692</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdemultimedia"><a name="kdemultimedia">kdemultimedia</a><span class="allsvnchanges"> [ <a href="4_6_5/kdemultimedia.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://developer.kde.org/~wheeler/juk.html" name="juk">JuK</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Show only the selected columns in new or dynamic playlists. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=243449">243449</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1234940&amp;view=rev">1234940</a>. </li>
        <li class="bugfix normal">Do not abort when loading, and thus losing, the saved search-playlists that contain empty patterns. See SVN commit <a href="http://websvn.kde.org/?rev=1235483&amp;view=rev">1235483</a>. </li>
        <li class="bugfix normal">Keep the order of tracks when loading a manually sorted plain playlist. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=70402">70402</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1235753&amp;view=rev">1235753</a>. </li>
        <li class="bugfix normal">Make sure the collection list cache is saved no matter how we exit the application. See SVN commit <a href="http://websvn.kde.org/?rev=1236078&amp;view=rev">1236078</a>. </li>
        <li class="bugfix normal">Fix crash when adding items to toolbars via context menu. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=258641">258641</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1238534&amp;view=rev">1238534</a>. </li>
        <li class="bugfix normal">Do not crash-on-exit when playing a track, no matter how the application is terminated. See SVN commit <a href="http://websvn.kde.org/?rev=1238537&amp;view=rev">1238537</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegames"><a name="kdegames">kdegames</a><span class="allsvnchanges"> [ <a href="4_6_5/kdegames.txt">all SVN changes</a> ]</span></h3>
    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kapman">Kapman</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix bug where the initial text was not removed when pausing game. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=267865">267865</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1238793&amp;view=rev">1238793</a>. </li>
      </ul>
      </div>
    </div>
  

<?php include "footer.inc"?>
