commit 5419098301b624ceaa256dfbe2f0fdc782eb42e6
Author: Dirk Mueller <mueller@kde.org>
Date:   Wed Jun 8 13:29:19 2011 +0200

    Revert "use monotonic clock if available"
    
    Seems to break in some setups, and time for debugging is short.
    see you in 4.6.5
    
    This reverts commit a38d29482cbada78f1ab9f894c871c4135cd8c17.

commit 3fa9208cadaec4727898ebe78231ea4f3b79d563
Author: Dirk Mueller <mueller@kde.org>
Date:   Wed Jun 8 13:24:13 2011 +0200

    avoid coverity warning

commit 40c1ea95fd71e3a79db445993cc0883f312b4332
Author: Nicolas Lécureuil <neoclust.kde@free.fr>
Date:   Sun Jun 5 19:19:59 2011 +0200

    Fix variables initialisation

commit c06c9012248560f1a89c1763cb23187b107f590c
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Jun 2 19:25:21 2011 +0200

    KDE 4.6.4

commit 6ea1b783880b6396dc09f8cb7f2260e2f8e70d85
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Apr 28 15:20:16 2011 +0200

    bump version to 4.6.3

commit b370a071f5f1dbc6dde9eae05f649061855eb99c
Author: Alex Fiestas <afiestas@kde.org>
Date:   Mon May 16 21:37:01 2011 +0200

    Added lightDM, it is just like GDM (ConsoleKit) + custom startReserve
    
    Added support for lightDM, which basically is managed like GDM but with
    a custom startReserve method (using lightDM dbus interface).
    LightDM is under heavy development right now, though it is fully working
    it has some API's that may change in the near future (for example now it
    identify itself as "GDMSESSION", something that is expected to be fixed
    soon.
    
    As final note, there is Qt greeter for lightDM, and David Edmundson is
    already working on a KDE one.
    
    CCMAIL: ossi@kde.org
    (cherry picked from commit b508e6e29274839b8c505b6c5a56f3feed6020f5)

commit 41121be56750e2f09b9ada4d2ae31a5799671054
Author: Script Kiddy <scripty@kde.org>
Date:   Tue May 31 18:06:50 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 60f36effb43e16ae9078e50b20521273373d354f
Author: Luboš Luňák <l.lunak@suse.cz>
Date:   Tue May 31 11:56:49 2011 +0200

    do not access NULL ICE objects (kde#195034)
    
    BUG: 195034

commit 80a57330b29e1fc1887932782852562f8a5bc30a
Author: Davide Bettio <bettio@kde.org>
Date:   Mon May 30 14:52:55 2011 +0200

    Replaced 1280x1024 background with the correct one because (background was 1278x1024 instead of 1280x1024).
    BUG: 273731

commit 708eb5b9d0e09d2349c3e8cf6d337e63f24dfae2
Author: Script Kiddy <scripty@kde.org>
Date:   Tue May 24 17:55:54 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit a38d29482cbada78f1ab9f894c871c4135cd8c17
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun May 15 12:19:32 2011 +0200

    use monotonic clock if available
    
    this avoids that clock jumps (e.g., ntp syncs) cause timeouts.
    (cherry picked from commit 858927c9d650697e8bd74697b8815c17cc93402d)
    
    BUG: 269522
    FIXED-IN: 4.6.4

commit a5b17b11bae0f2a1a263b5869541a25d6f0ae430
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Fri May 20 16:14:48 2011 -0300

    Marking Solid's NetworkManager backend to compile only if NM-0.7 or
    0.8 headers are installed. The NM-0.9 backend is in
    git://anongit.kde.org/networkmanagement/solidcontrolfuture (nm09 branch)
    now and is automatically compiled/installed when building Plasma NM.
    When it is ready we are going to move the backend back to kde-workspace.
    The networkmanager-0.7 backend supports and will only support 0.7
    and 0.8 versions of NetworkManager.
    
    BUG: 271289

commit 8e1307ccda75b14be9e949a23be41a7481a6bf32
Author: Script Kiddy <scripty@kde.org>
Date:   Fri May 20 16:08:01 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 2ce0f3a54915742f929d52b81653f8fc153a8fb8
Author: Aaron Seigo <aseigo@kde.org>
Date:   Wed May 18 21:48:31 2011 +0200

    check the value of the incidence pointer before using it
    
    BUG:273566

commit fd7ae565b058e1be75f96b4e23ad2181a05f0d13
Author: Lasse Liehu <lliehu@kolumbus.fi>
Date:   Sun May 15 21:46:02 2011 +0300

    Make sure "search term" is localized in krunner
    
    BUG:264794
    (cherry picked from commit 9ea90f9bf35899b8183c1bac80b3a4c1805542b5)

commit 792c2eefedd8480d0d8f55dcb1429bf58618cb63
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun May 15 11:06:13 2011 +0200

    clear background pixmap before painting
    
    this avoids that incomplete themes leave garbage on the screen.
    this is an ugly workaround, but specifying a background for non-primary
    screens is a somewhat new feature, and people typically wouldn't think
    of it, so many themes are broken in this regard.
    (cherry picked from commit 5ab6a7917852466be48958d3cccf128c9cceb1e5)
    
    CCBUG: 268993

commit 1fc5611611b9dc358cc7654c70f088a416a73b57
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Sun May 15 11:06:00 2011 +0200

    reuse same painter for all background painting
    
    (cherry picked from commit 6184fa9de9be733159f5733128d0fef9d0b692c0)

commit ddb6bc6efce2bb6a2078856177a3a012e99ff86b
Author: Script Kiddy <scripty@kde.org>
Date:   Sat May 14 16:37:04 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 7b2f2335d15c0b2137af66ba56f37f0718229413
Author: Script Kiddy <scripty@kde.org>
Date:   Fri May 13 16:06:07 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit f514539209a31232e050f9bd2e8d079fb04501ba
Author: Script Kiddy <scripty@kde.org>
Date:   Thu May 12 17:25:28 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 474e6032270c25e27449a5ee88700c6558d9cd6d
Author: Ben Cooksley <bcooksley@kde.org>
Date:   Fri Feb 25 22:38:24 2011 +1300

    HACK
    Prevent KAuth backends which use local event loops from crashing System Settings due to a double module load.
    Will show the layouting and other initialisation of control modules to use though unfortunately.
    Correct fix should ultimately be done in the KAuth backend by removing it's local event loop.
    CCBUG: 247830
    (cherry picked from commit f51be49c3867e9845ba07a7c1c1e54f371371146)

commit a44b6f43fd956424ece6a1a32a9d0de8306a5fe2
Author: Alex Fiestas <afiestas@kde.org>
Date:   Wed May 11 14:14:27 2011 +0200

    Similar to 101330 REVIEW but for hal backend.
    
    Actually HAL backend wasn't calling onBrightnessChanged at all, so this
    commit also fix that.
    (cherry picked from commit 4060366cd5dc31324e3b70462c1ec1cd7758ef54)

commit 918f7eed82c00b3b4adf88f97269bc087b599d6c
Author: Alex Fiestas <afiestas@kde.org>
Date:   Wed May 11 12:29:54 2011 +0200

    Show the brightness OSD only when brigthnessKeys are pressed.
    
    REVIEW: 101330
    (cherry picked from commit 57f8036beedfec2d67c7a485558480e3193c77ba)

commit 0197011030d6e8a6fba95325a6ca6da30421b2b6
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Wed May 11 08:35:01 2011 +0200

    apparently, /etc/X11/Xresources can be a file, too
    
    CCBUG: 249139
    FIXED-IN: 4.6.4
    (cherry picked from commit 36516393320760bbcb0dfd867cdd635e935fdb38)

commit 015bcb131a69178a6e585a1c2c97f32997125191
Author: Albert Astals Cid <aacid@kde.org>
Date:   Tue May 10 22:19:11 2011 +0100

    Convert these extracomment to comment
    
    Because:
     * We have a bug in our scripts that makes extracomment not work (will try to fix this asap)
     * Makes sense to be a comment since we want it to be a disambiguation text

commit 71cddd554bcd9127919ab358f6a73484ddeed458
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Fri Apr 29 20:58:37 2011 +0200

    do not eat mouse-move + button press events caught by the window manager at end of grab.
    
    CCBUG: 271926

commit 2c0083e82306f7ad6c36060b350fbc7ebc8e5619
Author: Script Kiddy <scripty@kde.org>
Date:   Tue May 10 19:42:39 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit d060ab6125229772439af5b7c5171a5edfd12ea5
Author: Ryan Rix <ry@n.rix.si>
Date:   Tue May 10 09:57:08 2011 -0700

    BUGFIX: Correctly populate incidenceData's Type
    
    Backport of 27bbec14986188825bca4a5af65a27284a34c1a3 to KDE/4.6
    
    BUG: 258125

commit 0624d4f3d4ba82a04beea682f79982030e63259c
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Mon May 9 21:22:02 2011 -0300

    Adjust StackDialog's custom position after a resize event. The position is
    not saved, so next time it will appear at the same position where it was
    before the resize event.
    
    BUG: 271703
    FIXED-IN: 4.6.4
    (cherry picked from commit 9d4f1f9d3271ab7a1e4238882c10c5ed9e669918)

commit a2d4c3ebdf7137f3543c88245f0bfd3dba308ebf
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Sun May 8 16:27:31 2011 +0200

    Don't store virtual desktop settings while loading them
    
    BUG: 272666
    FIXED-IN: 4.6.4

commit be6f86f8f7035ece5bd31c6ac80dcf87a363aec8
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sat May 7 15:22:20 2011 +0200

    fixed parameters initialization order.

commit 31faa597aca425a793c559fcd3350c17fa3505a3
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sat May 7 02:20:36 2011 -0300

    Fix memory leak.
    (cherry picked from commit 859d2094225ff85332fab1a41cc832d0fcc35d90)

commit 006809a52c8a138a8e1476da1ee9cbcad0d879bd
Author: Aaron Seigo <aseigo@kde.org>
Date:   Fri May 6 22:51:50 2011 +0200

    get rid of duplicate connect
    
    patch contributed by Luc Menut
    BUG:262614
    REVIEW:101284

commit be0ddd281ef815ed7e31463050d8b9419465bdaa
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Thu May 5 16:35:57 2011 +0200

    Fixed typo on arrow number options for subline.

commit 8d703b46d4082b76998d7d97d5d5a13f64ef6184
Author: Script Kiddy <scripty@kde.org>
Date:   Thu May 5 15:44:56 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 142eafca146dffc7dbb6cf643d390c7ade4f071b
Author: Script Kiddy <scripty@kde.org>
Date:   Wed May 4 15:47:51 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit ecd93a756dd8141d39ea19e8696f16b5e742e6ad
Author: Script Kiddy <scripty@kde.org>
Date:   Mon May 2 16:02:07 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 33fa0c965013d7abaa39cc17aa69fa2b18eefda3
Author: Aaron Seigo <aseigo@kde.org>
Date:   Mon May 2 14:54:11 2011 +0200

    prevent crash when hiding the year boxes more than once
    
    BUG:272077
    
    QSpinBox emits the finished signal when enter is pressed and when
    it loses focus. so if you hide it on enter, it then loses focus
    and the signal is emitted again. even if the value hasn't changed
    and no editing was done (not even returned from the previous signal
    emission). moderately lame.

commit 03337bf8d4f1b97c9e5ee5589ef90f47deb256b9
Author: Rafael Fernández López <ereslibre@kde.org>
Date:   Sat Apr 30 12:28:57 2011 +0200

    Fix panel controller width when running on multiple monitors.
    
    The panel controller was taking only a portion of the bigger screen
    width (that was primary). With this patch, we force the widget to
    calculate its size before being shown.
    
    Also, syncToLocation needed a small tweak, since when updating
    struts were needed (this is, dragging the panel from horizontal to
    vertical [or reverse] layout), the panel itself was taking the whole
    screen (because of setting the maximum size to QWIDGETSIZE_MAX).
    
    REVIEW: 101254

commit cc2f3b1a9ce0c181e1200d050ee4092dd5916c98
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Apr 29 16:45:43 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit e0b37836949b3b9a80694b10708e81ace399ff87
Author: David Naylor <naylor.b.david@gmail.com>
Date:   Fri Apr 22 23:45:39 2011 +0200

    Correctly calculate MSI interrupt offsets.
    
    Although MSI offsets were calculated correctly, a condition existed
    for the last normal interrupt to be included as a MSI interrupt,
    causing a buffer underrun and segfault.  The number extracted from
    the command is already the "proper" number.  i.e. MSI start at 256,
    not `256 - msi_offset'.
    
    CCMAIL: naylor.b.david@gmail.com

commit e8871b092e2103012bad256c69edfe21a5f5eb64
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Wed Apr 27 05:03:37 2011 -0300

    Prevents compiling Solid's NetworkManager backend version 0.7 against
    NetworkManager 0.9.0, it won't work anyway.
    (cherry picked from commit b42f48e38de7729874f6c64e857a97d97491f448)

commit 73c9706b179cf881865fe02a2c25d1ac9b1bdda5
Merge: fbb141a b6087c0
Author: Björn Ruberg <bjoern@ruberg-wegener.de>
Date:   Wed Apr 27 07:19:03 2011 +0200

    Merge branch 'fixpowerdevilshortcuts' into KDE/4.6

commit b6087c028016409c73e8b96c3600369edc89817b
Author: Björn Ruberg <bjoern@ruberg-wegener.de>
Date:   Wed Apr 27 07:14:24 2011 +0200

    Fix global key shortcuts (powermanagement: suspend, hibernate, ...) being resetted to their default value at each login.
    BUG: 262908

commit fbb141affb96d5baf764f657c08cb9b1cc0ffb75
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Apr 26 17:33:33 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit b8fbc422521cecc43bbc891f0d5c9e984eeeea78
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Apr 26 14:36:01 2011 +0200

    correct config group

commit 387d817aef83cdbcf6b700707a99195dc8533c80
Author: Aaron Seigo <aseigo@kde.org>
Date:   Mon Apr 25 16:00:44 2011 +0200

    set the index show it shows

commit 75cf79c088825d570ac75909e607d82c6dd173c6
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Apr 25 15:21:34 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 007fdd04c7d0fcfc29a476846380544d95693276
Author: Martin Gräßlin <mgraesslin@kde.org>
Date:   Sun Apr 24 20:20:01 2011 +0200

    Perform glFlush before deleting the EffectFrame's pixmaps
    
    On NVIDIA it is possible that the actual rendering gets delayed to
    after the deletion of the pixmap during the end of fullscreen effects.
    This was causing freezes. By using glFlush before deleting the pixmaps
    we can ensure that the pixmap is not needed anymore after the pixmaps
    are deleted.
    
    BUG: 261323
    FIXED-IN: 4.6.3

commit b8db9bfcbdf8862fa919c64ce94d93e5c6701c48
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Apr 24 15:36:04 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 6d4248b5475e5b602839918354c8d3486f24d54d
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sun Apr 24 09:25:13 2011 -0300

    Revert 6e455733662313e476541b1904f810ce4ec5ebda (Network Bluetooth
    support to Solid's NetworkManager backend.) That should have gone to
    master not 4.6 branch.

commit cb3161645ec26d5541025c42033fc2ebd9789602
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sun Apr 24 09:22:31 2011 -0300

    Removes duplicated if clause in NetworkManager backend.

commit 6e455733662313e476541b1904f810ce4ec5ebda
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sun Apr 24 04:44:09 2011 -0300

    Network Bluetooth support to Solid's NetworkManager backend.

commit 9db642c2ca7d2e6d9ded127b150f0597f4101b0c
Author: Rafael Fernández López <ereslibre@ereslibre.es>
Date:   Sat Apr 23 23:44:07 2011 +0200

    Adapt also this element for right to left environments

commit 3094396c5c58e8335541bde535e8b81e973c4575
Author: Rafael Fernández López <ereslibre@ereslibre.es>
Date:   Sat Apr 23 21:47:15 2011 +0200

    Adapt category drawer for RTL environments. Still an element to adapt (the inner white line)

commit 76e8740a88c05cc2a51365297687e20ac9ac327a
Author: David Palacio <dpalacio@orbitalibre.org>
Date:   Sat Apr 23 11:27:24 2011 -0500

    Plugin names may be in domain name form:
    
    com.example.engine

commit cc2cbb12ef4c1a5aaf0e7543ac43d81f2ba053a3
Merge: d1c5fbd a389a45
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sat Apr 23 10:12:27 2011 -0400

    Merge branch 'KDE/4.6' of git://anongit.kde.org/kde-workspace into KDE/4.6

commit d1c5fbd3d3914349b3676017d52aabafec247810
Author: Andriy Rysin <arysin@gmail.com>
Date:   Sat Apr 23 10:01:21 2011 -0400

    Read extra rules
    Fix filtering layouts/variants by language
    BUG: 270429
    FIXED-IN: 4.6.3

commit a389a45ecaea2577f5162edd1485356c81263cce
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Tue Feb 15 00:55:45 2011 +0100

    prevent swapping desktops w/o explicit button press after animations have finished
    
    CCBUG: 220088

commit 077bdb81810119afcfe149626797745d34c2f0d0
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Apr 22 16:25:36 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit a789bceeb1a62fb4d1b6ec9246f1e98fd96e6921
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Fri Apr 22 02:44:08 2011 -0300

    Add some checks to Solid's Wicd backend to prevent crash.
    
    BUG: 271408
    FIXED-IN: 4.6.3
    (cherry picked from commit fc7011b1d8454c6378d88ea870c341d1a3d96374)

commit 0c57ad428282ef1d42eaaaba1267f45fd9b158f2
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Tue Apr 19 22:58:54 2011 +0200

    added some consts to make kdepepo happy :)

commit 85dee28b064eabe1485cb2b49150e4e3a91a0ec2
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Tue Apr 19 22:24:35 2011 +0200

    use dedicated property to handle progressbar busy position.
    
    CCBUG: 271290
    
    Conflicts:
    
    	kstyles/oxygen/animations/oxygenprogressbarengine.cpp

commit 1a0c544b96d5fc9f71caa56d014aa5ac3f9f1288
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Apr 19 16:55:41 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 505f7cc14a7ee9914a7d75128e28476ead34b9e0
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Tue Apr 19 02:41:59 2011 +0200

    Validate/check logarry in lanczos filter
    
    this will *probably* fix the mysterious __cxa_rethrow bug
    we still need validation from someone encountering the bug
    
    CCBUG: 241163

commit 79187a9feb1ae0a0c5f49b7669a65b3606271ea1
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Mon Apr 18 21:16:40 2011 +0200

    complete backport for setNumberOfDesktops() fix
    
    CCBUG: 200213
    completes backport f3f255fef1be7d36c0a6fb931a46f52178440cb3 to match
    master version 40b1f456da80ff24223920749128b742280506bb since the
    incomplete backport lead to unset number of desktops on the root window
    (applies only after relogin)

commit adf3a5b6b1d41dc157ff265bb5e1271bb736d443
Author: Craig Drummond <craig@kde.org>
Date:   Mon Apr 18 20:18:42 2011 +0100

    Fix crash when attmepting to toggle a non-existant font.

commit 487cca25a89c283472d096828d5eb0883a55fca0
Author: Craig Drummond <craig@kde.org>
Date:   Mon Apr 18 20:17:50 2011 +0100

    Add qAddPostRoutine() to save disabled fonts list when helper terminates - as destructor is not called.

commit e7821f530cc506843523e6b846ba1095cea54513
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sun Apr 17 16:15:38 2011 +0200

    Do not render window background in case a custom texture is attached to a palette.
    
    CCBUG: 242271

commit 7c04367c9b3c61f488a0a3fe0aad0795a5668ba4
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sun Apr 17 14:46:11 2011 +0200

    Fixed margin around toolbox title so that it aligns well with toolbox tab content.

commit 3594e2f62a1da896a71b4a1999fe62a99109fcdd
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sun Apr 17 11:32:44 2011 +0200

    adjusted hover toolbutton's gradient.

commit 2993ea670099d155f1b762de25eefd26627c49a4
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sat Apr 16 16:46:05 2011 +0200

    changed variable name for consistency.

commit 932d2b28ed03ff6b16cd9b8e51b587b08bde5c6b
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sat Apr 16 13:06:59 2011 +0200

    fix gradient positionning for maximized windows

commit 731c25c77a783fe9505ebc4f1da7b09076694c19
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Fri Apr 15 18:45:29 2011 +0200

    fixed rendering bug of vertical scrollbars due to incorrect setting of the "smallShadow"
    flag.

commit aa799f2a3451e3d83b539310bed6afcd66cccafe
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Fri Apr 15 18:42:24 2011 +0200

    added missing TileSet::Center

commit 2e3cf1264518c4465c6cf94ebf8ac8e9f9bd5d4a
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sat Apr 16 17:55:12 2011 +0200

    Fixed focused hole rendering so that unfocused shadow is not draw below focused glow.
    
    Conflicts:
    
    	kstyles/oxygen/oxygenstylehelper.cpp

commit ff610ae1e0f9f67972051865a9a2d01c1b5967a8
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Fri Apr 15 16:11:21 2011 +0200

    fixed toolbutton hover rect.

commit 564e535d29732dd69ea8c51eed00368cbf7f2fa1
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sat Apr 16 13:06:01 2011 +0200

    only penalize multihead users with the overhead of the setLanguage call

commit d443c25ff4fb4add35ca6bad580be31d6bb4476c
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sat Apr 16 12:58:21 2011 +0200

    even better; let's grab the main config

commit 66d4f570ef51c7522e1ba4bdb10bdc582872ec5c
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sat Apr 16 12:56:48 2011 +0200

    don't bother with a Kconfig object here

commit f4deadd339d6b2ee48e8db153837ddbaaf2e3bf0
Author: Alberto Mattea <alberto@mattea.info>
Date:   Sat Apr 16 00:43:38 2011 +0200

    Correctly set locale on secondary screens in multi head configurations
    REVIEW: 101133

commit 691fac265441680c8353fc7ba145c96f70931d8f
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Fri Apr 15 00:31:54 2011 -0300

    Doing the same changes done in 56e1371b47c3ca2000728ac4d6d7d6025fe09974
    to the NetworkManager backend.
    (cherry picked from commit 5d121d2e0040720db1664755f89f7ec7ea4723e6)

commit a0fc87f7e8ef5b01290a3ca5d2db59928e3d6449
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Thu Apr 14 22:19:12 2011 -0300

    Reverting e831217493451118982629fb747dac04b5628076 and applying a better
    solution suggested by André Wöbbeking. Thanks for helping me solve the
    MMModemInterface invalid free problem.
    
    CCBUG: 258852

commit 45b90537458232377f447254362fffab15064d59
Author: Script Kiddy <scripty@kde.org>
Date:   Thu Apr 14 16:09:44 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit f3f255fef1be7d36c0a6fb931a46f52178440cb3
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Wed Apr 13 22:58:41 2011 +0200

    fix & cleanup Workspace::setNumberOfDesktops()
    
    CCBUG: 200213

commit a56c26346c2041cd6f5036101012aa4a7718e2eb
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Wed Apr 13 20:57:00 2011 +0200

    validate deleted moving window in desktop grid effect
    
    BUG:233363
    FIXED-IN:4.6.3

commit ea4db06507244212e04a57e4ef41470919828bfb
Author: Thomas Lübking <thomas.luebking@gmail.com>
Date:   Tue Mar 15 21:25:47 2011 +0100

    sanitize special window pointers in flip/coverswitch
    
    BUG:253870
    FIXED-IN:4.6.3

commit 9842babd6a7cfc6f19e692f9dc33ed4a5a7dd470
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Apr 13 15:22:53 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit d5e14a0cdbd99cb80682e473912fd82f9ba6e40c
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Wed Apr 13 05:23:05 2011 -0300

    Using static method KNotification::event() does not work here, creating a
    KNotification object does.
    
    BUG: 243849
    FIXED-IN: 4.6.3

commit e831217493451118982629fb747dac04b5628076
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Tue Feb 22 23:12:59 2011 -0300

    Try to avoid the crash reported in bug 258852. I have not been able to
    reproduce it, I am not sure if this will work it out.
    
    CCBUG: 258852

commit e19416dd9fa5c2fb9782f8208bca9a56a5fd866d
Author: Aaron Seigo <aseigo@kde.org>
Date:   Tue Apr 12 15:39:13 2011 +0200

    allow the drag to end, don't start it over again
    
    patch by Giorgos Tsiapaliwkas
    REVIEW:101044

commit 06d95d97d2c2257b6c40efbacbb46c61883ba50d
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Tue Apr 12 13:56:38 2011 +0200

    blend separator color based on position in window.

commit c69e94c3856227bd26a598a128ec5329d3acd48a
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Tue Apr 12 14:05:37 2011 +0200

    changed UI so that Busy indicator does not get grayed out anymore when "enable animations" is unchecked.
    
    Conflicts:
    
    	kstyles/oxygen/config/oxygenanimationconfigwidget.cpp

commit 2997e73a3a392b60c46f88656301e527a12cbcda
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Tue Apr 12 14:03:55 2011 +0200

    Do not condition "busy" progressbar animation by the global "enable animations" flag.
    They must be disabled explicitely via oxygen-settings.
    
    CCBUG: 270751
    
    Conflicts:
    
    	kstyles/oxygen/animations/oxygenanimations.cpp

commit b38a5b721ff08130acfa5cf27386d5b2c2e665d0
Author: Artur Duque de Souza <asouza@kde.org>
Date:   Mon Apr 11 09:33:33 2011 -0300

    Make the battery applet to remain inactive when batteries are in "NoCharge" state
    
    Fix BUG #235236, making the battery inactive when it's not charging. The systray
    will make the work to show/hide the applet when necessary and the user can configure
    it's behavior through systray's config options.
    
    Bonus points for not adding a new option to the applet's settings ;)
    
    Tested with kdelibs and kdebase-workspace, both master branches.
    
    Patch by: Thiago Jung Bauermann <thiago.bauermann@gmail.com>
    Signed-off-by: Artur Duque de Souza <asouza@kde.org>
    
    BUG:235236
    FIXED-IN:4.6.3
    CCMAIL:thiago.bauermann@gmail.com

commit cb662c10df9d8d3e48359749040689bb3b939a54
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Apr 10 14:50:29 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 24057014687040ef404486608a53419320b42890
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Apr 9 15:50:58 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 7875187069fbda932852fc7638c2db551e31dbcb
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sat Apr 9 10:45:28 2011 +0200

    also check on active action validity in leaveEvent follow-mouse animation.

commit bf9c02021e415e648ba0a6c1d255a32cbe6f1918
Author: Hugo Pereira Da Costa <hugo@oxygen-icons.org>
Date:   Sat Apr 9 10:29:35 2011 +0200

    added missing target update for MenuAnimations leave Event.

commit 5c47f16d67065e239c380cc369a1da854acef19a
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sat Apr 9 02:44:47 2011 -0300

    Fix SimulateUserActivity not working for a second when resuming from
    ram/disk.
    
    The problem is in krunner/lock/lockprocess.cc, LockProcess::lock(...),
    lines:
    
    mBusy = true;
    ...
    QTimer::singleShot(1000, this, SLOT(slotDeadTimePassed()));
    
    The "dead-time" makes LockProcess ignores any SimulateUserActivity
    requests during that one second-time.
    I have added "mInitialLock = true" in LockProcess::signalPipeSignal() to
    simulate the "--showunlock" parameter after the one second-time.
    
    BUG: 269737
    FIXED-IN: 4.6.3

commit 28b79d8d04cce4d0a0899cadcc8fe6fd69d38e01
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Fri Apr 8 20:58:29 2011 +0200

    actually, it wants -h -P, because -P is just a modifier
    
    FAIL ...
    
    BUG: 270228 (take 2)

commit 4b7ec7c9966df0c705bc0c9b9bff1010475347b6
Author: Oswald Buddenhagen <ossi@kde.org>
Date:   Fri Apr 8 19:02:01 2011 +0200

    linux shutdown wants -P instead of -p
    
    BUG: 270228
    FIXED-IN: 4.6.3

commit 3dbdc68fa9fb84fbcc63dbb9acffdf43a1ebfdda
Author: Sergio Martins <iamsergio@gmail.com>
Date:   Thu Apr 7 23:43:17 2011 +0100

    Don't segfault in release mode.
    
    No idea why it's 0, but we're going to delete this copy of calendar.cpp soon,
    so it's a waste of time to debugging it.
    
    PIM applications don't crash at all regarding this, in plasma the ETM is probably
    set incorrectly and has items with unsupported payloads.
    
    BUG: 261593
    (cherry picked from commit b2a322448e644cdf55b9990c90920ce8c0ffcc7f)

commit 05396585fe1e6136d5dac41a0fc52437d7be366f
Merge: 09f7dbd 44a9f9c
Author: Jeremy Whiting <jpwhiting@kde.org>
Date:   Thu Apr 7 10:32:19 2011 -0600

    Merge branch 'colorpreviewfix' into KDE/4.6

commit 44a9f9c2b10468ddcb97a7c0205bd1e84d21abe4
Author: Jeremy Whiting <jpwhiting@kde.org>
Date:   Wed Apr 6 14:43:10 2011 -0600

    Fix preview widget color scheme.
    Use new KGlobalSettings::createNewApplicationPalette which doesn't cache.

commit 09f7dbda30fea57a83346bf6cfffaffef20e2e45
Author: Lukas Tinkl <lukas@kde.org>
Date:   Thu Apr 7 13:00:51 2011 +0200

    give nv_backlight lower preference
    
    CCBUG: 257948

commit 0e151ea2ab1663f3649688139739712060ae3259
Author: John Tapsell <johnflux@gmail.com>
Date:   Thu Apr 7 10:59:20 2011 +0100

    Fix accidental removal of a #include, when adding freebsd support.  This broke the lmsensors at minimum
    
    BUG: 270246
    BUG: 270246
    CCMAIL: naylor.b.david@gmail.com

commit abcd170758a32efd8b082bacb5918ee4a6ac05ea
Author: Marco Martin <notmart@gmail.com>
Date:   Wed Apr 6 22:32:49 2011 +0200

    create the menu in contextualActions()
    
    fix for right mouse button
    BUG:262614

commit 8d78d3989eda69d4d3dc3317579387a7561322c9
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Apr 6 14:32:24 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit d463ee3d71802ba4e92a2ce998f52867221d50c9
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Apr 5 15:52:17 2011 +0200

    SVN_SILENT made messages (.desktop file)
