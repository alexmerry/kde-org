2007-06-06 19:28 +0000 [r672324]  mlaurent

	* branches/KDE/3.5/kdegames/ktuberling/main.cpp,
	  branches/KDE/3.5/kdegames/knetwalk/src/main.cpp,
	  branches/KDE/3.5/kdegames/kwin4/kwin4/main.cpp: Fix mem leak

2007-06-21 14:03 +0000 [r678489]  binner

	* branches/KDE/3.5/kdegames/ktuberling/ktuberling.desktop,
	  branches/KDE/3.5/kdegames/kolf/kolf.desktop,
	  branches/KDE/3.5/kdegames/kpoker/kpoker.desktop: fix invalid
	  .desktop files

2007-06-25 14:35 +0000 [r680146]  mueller

	* branches/KDE/3.5/kdegames/atlantik/libatlantikui/portfolioview.cpp:
	  the usual "daily unbreak compilation"

2007-09-03 11:55 +0000 [r707935]  mueller

	* branches/KDE/3.5/kdegames/kbattleship/kbattleship/Makefile.am:
	  fix build dependencies

2007-09-18 18:37 +0000 [r714095]  jriddell

	* branches/KDE/3.5/kdegames/atlantik/libatlantikui/token.cpp: build
	  fix for gcc 4.3

2007-10-03 08:57 +0000 [r720582]  coolo

	* branches/KDE/3.5/kdegames/kmines/highscores.cpp,
	  branches/KDE/3.5/kdegames/kmines/status.cpp,
	  branches/KDE/3.5/kdegames/kmines/version.h: don't clash defines
	  with config.h

2007-10-08 11:05 +0000 [r722965]  coolo

	* branches/KDE/3.5/kdegames/kdegames.lsm: updating lsm

