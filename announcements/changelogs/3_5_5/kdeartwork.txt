2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-18 03:12 +0000 [r574084]  tyrerj

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/view_fit_width.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/tab_remove_other.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/tab_remove_other.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/tab_remove_other.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/view_fit_window.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/show_side_panel.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/view_fit_height.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/show_side_panel.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/view_fit_window.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/view_fit_height.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/apps/photobook.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/view_fit_window.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/show_side_panel.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/apps/photobook.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/actions/view_fit_height.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/actions/view_fit_width.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/16x16/apps/photobook.png
	  (added),
	  branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/32x32/actions/view_fit_width.png
	  (added): Adding HiColor icons as KDEClassic

2006-08-19 09:49 +0000 [r574520]  iastrubni

	* branches/KDE/3.5/kdeartwork/styles/dotnet/dotnet.cpp: this is a
	  partial fix for #132024 it fixes the rendering of the menus. the
	  bug was caused by wrong caching of the
	  QApplication::reverseLayout() property. the fix is ugly (since
	  the internal property reverseLayout is only used in the
	  constructor), but it works, and i want to modify as little as
	  possible in KDE 3.5.X. (i have the wierd feeling that the same
	  fix was applied sevedral years ago to the same code... svn
	  log...? svn diff...?) i also messed up Fredrik's name in the
	  credits. i will try to fix this on the next commit... whats our
	  policy about non latin1 text in source code...? CCMAIL:
	  kde-il@yahoogruops.com, yanivmailbox@gmail.com

2006-08-19 12:08 +0000 [r574556]  iastrubni

	* branches/KDE/3.5/kdeartwork/styles/dotnet/dotnet.cpp: CCMAIL:
	  kde-il@yahoogroups.com, yanivmailbox@gmail.com BUG: 132024 This
	  will fix and close bug 132024. The issue with the direction of
	  windows was fixed on commit 574520 and this fixes the drawing of
	  the frame's input line in spin boxes. By this, I can safly close
	  this bug, in worst case scenario I can always re-open it... even
	  tough this means usign this style for several days to catch up
	  any regressions.... blah... Note to developers: please try to use
	  QStyle::visualRect(). No need to check for the diretion of the
	  desktop, as the function does this for you.

2006-08-20 04:32 +0000 [r574762]  tyrerj

	* branches/KDE/3.5/kdeartwork/IconThemes/kdeclassic/22x22/apps/kview.png
	  (added): Adding icon: kview 22x22 KDEClassic

2006-08-21 15:05 +0000 [r575431]  jriddell

	* branches/KDE/3.5/kdeartwork/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-10-02 11:07 +0000 [r591325]  coolo

	* branches/KDE/3.5/kdeartwork/kdeartwork.lsm: updates for 3.5.5

