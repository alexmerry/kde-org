2006-01-30 16:16 +0000 [r503850]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/rbkconfig_compiler/tests/Makefile.am:
	  * Removed redundant .SUFFIXES line

2006-01-31 09:57 +0000 [r504061]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp:
	  * Don't delete KAboutData or KCmdLineArgs instances as they need
	  to last for the lifetime of the program. Fixes problem reported
	  by Han Holl. CCMAIL: kde-bindings@kde.org

2006-02-01 18:36 +0000 [r504667]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToSmoke.pm: *
	  Skip the QUpdateLaterEvent class as it sometimes causes the Smoke
	  lib to fail to link.

2006-02-03 19:21 +0000 [r505384]  jriddell

	* branches/KDE/3.5/kdebindings/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdebindings
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdebindings

2006-02-07 20:20 +0000 [r506876]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/AUTHORS,
	  branches/KDE/3.5/kdebindings/qtruby/INSTALL: * Added some info
	  from Ryutaro Amano about how to create a bundle on Mac OS X which
	  can be double clicked from the Finder

2006-02-07 20:25 +0000 [r506877]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToSmoke.pm: *
	  Some fixes for the Qt4 classes, no longer including the
	  QAbstractItemDelegate or QUpdateLaterEvent as they don't link.

2006-02-08 18:59 +0000 [r507218]  rdale

	* branches/KDE/3.5/kdebindings/korundum/ChangeLog,
	  branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/lib/KDE/korundum.rb:
	  * When KDE::CmdLineArgs.init() was called with just a single
	  KDE::AboutData arg, it crashed. Fixes problem reported by Han
	  Holl. * KDE::CmdLineArgs and KDE::AboutData instances are no
	  longer deleted by qtruby on garbage collection. Fixes another
	  problem reported by Han Holl. CCMAIL: kde-bindings@kde.org

2006-02-09 11:03 +0000 [r507479]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * The
	  Kernel#select method was being redefined as it clashed with
	  Qt::SqlCursor#select and Qt::SqlSelectCursor#select methods. This
	  caused a problem when QtRuby was used with Rails ActiveRecord
	  which also has a select method. So the methods are now defined in
	  the Sql classes, and use method_missing() to explictly invoke the
	  methods in the Smoke library. Fixes problem reported by Imo Sosa.
	  CCMAIL: imo@foton.es

2006-02-14 12:52 +0000 [r509351]  rdale

	* branches/KDE/3.5/kdebindings/qtjava/javalib/qtjava/QStyleOption.h
	  (added), branches/KDE/3.5/kdebindings/qtjava/ChangeLog,
	  branches/KDE/3.5/kdebindings/qtjava/javalib/qtjava/Makefile.am,
	  branches/KDE/3.5/kdebindings/qtjava/javalib/qtjava/QStyleOption.cpp
	  (added): * Added QStyleOption.h and QStyleOption.cpp sources
	  which were missing. Fixes problem reported by Sekou DIAKITE
	  CCMAIL: sekou.diakite@laposte.net

2006-03-09 19:54 +0000 [r517047]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptus,
	  branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToSmoke.pm: *
	  Added patch from Bernhard Rosenkraenzer to get it to work at all
	  (with Qt build with -fvisibility=hidden support, and CJK codecs
	  built as plugins) * Skip the QThread and related classes when
	  generating the Smoke library as they don't really work with
	  non-C++ languages without native threads. * Skip '#ifdef
	  Q_MOC_RUN .. #endif' code * Applied patch from Thomas Moenicke
	  (thanks Thomas!) to parse Q_PROPERTY macros, and add the info to
	  the kalyptus AST. CCMAIL: bero@arklinux.org

2006-03-09 20:38 +0000 [r517067]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptus: * Only generate
	  property info from Qt4 headers

2006-03-09 21:13 +0000 [r517076]  rdale

	* branches/KDE/3.5/kdebindings/smoke/qt/configure.in.in: * Applied
	  fix from Jason Hale to make the enable qscintilla build option
	  work CCBUGS: 123055

2006-03-14 16:31 +0000 [r518601]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Fixed bugs in
	  const lists marshallers, where the items in the ruby Array were
	  wrongly being updated after a method call

2006-03-15 14:34 +0000 [r518861]  pfeiffer

	* branches/KDE/3.5/kdebindings/qtjava/designer/juic/common/util.xsl:
	  backport: don't generate variable names with whitespace

2006-03-17 21:34 +0000 [r519781]  coolo

	* branches/KDE/3.5/kdebindings/kdebindings.lsm: tagging 3.5.2

