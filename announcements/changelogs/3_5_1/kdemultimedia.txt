2005-11-30 17:05 +0000 [r484396]  mueller

	* branches/KDE/3.5/kdemultimedia/mpeglib/lib/mpegplay/mmxidct_asm.S:
	  add no-exec-stack mark

2005-11-30 19:01 +0000 [r484429]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/kcompactdisc.cpp,
	  branches/KDE/3.5/kdemultimedia/kscd/kscd.cpp,
	  branches/KDE/3.5/kdemultimedia/kscd/kscd.kcfg,
	  branches/KDE/3.5/kdemultimedia/kscd/libwm/cdrom.c: Lots of
	  bug-fixes: * Fix random-mode and repeat * Fix auto-play when
	  inserting a new CD * Make sure the Play/Pause button is updated *
	  Don't change the time-slider when dragging it, so it's possible
	  to change track-position * Make the volume-label show the correct
	  volume * Set a default for the audio-device in digital mode And
	  possible some other things I forgot

2005-12-02 21:45 +0000 [r485102]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/playlist.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/playlist.h: Fix bug 117541
	  (JuK crashes on shutdown due to memory corruption). The fix is
	  simple, the cause is complex. The bug report has the details if
	  you want to read it. BUG:117541

2005-12-02 21:49 +0000 [r485104]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/playlist.cpp: Add warning
	  comments to the insertItem() and takeItem() functions to
	  hopefully avoid a repeat of the crasher bug I just fixed.

2005-12-04 14:54 +0000 [r485461]  larkang

	* branches/KDE/3.5/kdemultimedia/libkcddb/cdinfo.h: Can't compile
	  with KDE_NO_COMPAT since the data is stored in those variables

2005-12-04 14:59 +0000 [r485465]  larkang

	* branches/KDE/3.5/kdemultimedia/libkcddb/submit.h,
	  branches/KDE/3.5/kdemultimedia/libkcddb/cddb.cpp,
	  branches/KDE/3.5/kdemultimedia/libkcddb/cddb.h,
	  branches/KDE/3.5/kdemultimedia/libkcddb/submit.cpp: Fix length
	  calculation in submit. The length of the disc is the position of
	  the lead-out in seconds, while the length used for calculation of
	  the disc-id is the position of the lead-out track minus the
	  position of the first track (in seconds) BUG: 110782 Recalculate
	  the disc-id when submitting, since the disc-id wouldn't match the
	  track offsets if the user has chosen an inexact match from the
	  freedb server

2005-12-04 15:31 +0000 [r485471]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/kcompactdisc.cpp: Fix
	  disc-length and disc-position. The disc-length was actually the
	  total length of the first n-1 tracks. And the position should be
	  the current playing position relative to the start of the first
	  track. This also caused the calculation of the remaning time to
	  appear wrong. BUG: 117609

2005-12-04 16:49 +0000 [r485494]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/kscd.kcfg: Fix stupid mistake
	  in one of my last commits, use /dev/audio as default audio-device
	  and not /dev/cdrom

2005-12-04 17:37 +0000 [r485502]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/configWidgetUI.ui,
	  branches/KDE/3.5/kdemultimedia/kscd/kscd.cpp: Change the default
	  property of KComboBox from currentItem to currentText, so
	  changing audio-system works in the config dialog BUG: 103836

2005-12-09 08:41 +0000 [r486895]  cartman

	* branches/KDE/3.5/kdemultimedia/juk: ignores

2005-12-10 22:25 +0000 [r487525]  chrsmrtn

	* branches/KDE/3.5/kdemultimedia/mpeglib/lib/mpegplay/mmxidct_asm.S:
	  Turns out that @ means something else on ARM; using % instead
	  should be universally acceptable.

2005-12-12 22:04 +0000 [r488028]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/configWidgetUI.ui,
	  branches/KDE/3.5/kdemultimedia/kscd/kscd.cpp,
	  branches/KDE/3.5/kdemultimedia/kscd/configWidget.cpp,
	  branches/KDE/3.5/kdemultimedia/kscd/kscd.kcfg,
	  branches/KDE/3.5/kdemultimedia/kscd/configWidget.h: Fix changing
	  audio backends in digital mode again. With my previous fix
	  changing the property from currentItem to currentText, the
	  current backend was set with QComboBox::setCurrentText, which
	  replaces the text in the current item if the text isn't in the
	  list. So because the default backend is an empty string, "arts"
	  was replaced by "" and it wasn't possible to change the backend
	  to "arts". Instead, make the combo box a sub-class of KComboBox
	  where setCurrentText calls KComboBox::setCurrentItem(text, false)
	  so the text isn't changed if it isn't in the list. Also, remove
	  the default audio-device again because arts doesn't use the
	  device and the alsa devices are in a different format

2005-12-14 11:43 +0000 [r488424]  wheeler

	* branches/KDE/3.5/kdemultimedia/juk/akodeplayer.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/akodeplayer.h: Initialize the
	  aKode volume after instantiating the playback engine. BUG:118265

2005-12-14 20:36 +0000 [r488528]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/libwm/plat_linux_cdda.c:
	  Start playing at the start of the track in digital mode BUG:
	  112368

2005-12-17 19:47 +0000 [r489232]  bmeyer

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/kcmaudiocd.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/audiocdencoder.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/audiocdencoder.h:
	  Backport changes to make bug fixing easier (one of the FSF
	  addresses is wrong, don't know which one though...)

2005-12-17 21:32 +0000 [r489254]  bmeyer

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/audiocdencoder.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/vorbis/encodervorbis.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/flac/encoderflac.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  Don't load a plugin if you have already loaded it -Some Qt4
	  changes (to make backporting easier) BUG: 112431

2005-12-22 22:40 +0000 [r490729]  larkang

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp: Only
	  split Artist / Track if it can be done for all tracks. Remove the
	  selection of the entry with highest revision, since revision
	  number doesn't really say anything.

2005-12-23 03:17 +0000 [r490768]  mcbride

	* branches/KDE/3.5/kdemultimedia/doc/kscd/kscd14.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd5.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd6.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd16.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd18.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd9.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd19.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/index.docbook,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd11.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd2.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscdannounc.png (added),
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd12.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd3.png,
	  branches/KDE/3.5/kdemultimedia/doc/kscd/kscd13.png: Updates to
	  documentation including CD player troubleshooting guide Updated
	  screenshots to match current UI.

2005-12-23 19:31 +0000 [r490941]  cartman

	* branches/KDE/3.5/kdemultimedia/juk/juk.desktop: Add MimeTypes,
	  aKode & Gstreamer supports all these, they also support wma
	  though ;-)

2005-12-24 21:10 +0000 [r491159]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/juk.desktop: Pare down
	  recently-added mimetypes to those actually supported by JuK. JuK
	  is unable to add to its collection files that aren't recognized
	  by TagLib so stuff like AAC and RealMedia is not able to be
	  played by JuK even if aKode or gstreamer supports it. One
	  question I have is if TagLib supports any kind of MPEG-4 tag
	  format (i.e. MPEG-7 as opposed to AAC). I'm pretty sure it does
	  not so I've removed MPEG-4 as well.

2005-12-27 13:46 +0000 [r491754]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/mdwslider.h,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixerwidget.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/volume.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixerwidget.h,
	  branches/KDE/3.5/kdemultimedia/kmix/kmix.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/volume.h,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixprefdlg.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mixer_alsa9.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mixdevice.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/kmix.h,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixtoolbox.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixprefdlg.h,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixtoolbox.h,
	  branches/KDE/3.5/kdemultimedia/kmix/mixdevicewidget.h,
	  branches/KDE/3.5/kdemultimedia/kmix/viewdockareapopup.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/mdwslider.cpp: Patchset with
	  two patches: 1) "Show volume as numbers" (relative/absolute). Add
	  feature,add config dialog item (Thanks for the patch, Mark
	  Nauwelaerts). The added strings were acccepted by kde-18n. 2)
	  Patch for supporting Audigy2 devices better (Thanks for the
	  patch, Erwin Mascher) The patch was succesufully tested and
	  accepted by kde-multimedia CCBUGS:114496 CCBUGS:110807

2005-12-28 14:34 +0000 [r492029]  dfaure

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  Don't call "lame --tg Unknown", lame barfs on that (and btw
	  kio_audiocd doesn't notice and keeps ripping, but creates a
	  0-bytes mp3 file...) Also use a less lame way of finding out if
	  lame is installed :)

2005-12-28 18:36 +0000 [r492085]  dfaure

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.h,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/audiocdencoder.h:
	  Abort when lame aborts, no point in keeping extracting the stuff
	  for nothing. Report messages from lame's stderr in the kio error
	  box. Not that the user can do much about "kio_audiocd: Lame
	  stderr: Unknown genre: Vocal Jazz. Specify genre name or number"
	  though :(

2005-12-28 20:32 +0000 [r492111]  dfaure

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/collectingprocess.cpp
	  (added),
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.h,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/collectingprocess.h
	  (added),
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/Makefile.am,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  Call lame with --genre-list in init(), and check if it knows the
	  genre that we want to pass it via --tg, to avoid errors. To make
	  the code simple I copied collectingprocess from libkdepim, this
	  will go away in kde4 hopefully.

2005-12-28 20:49 +0000 [r492120]  dfaure

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  Fix init problem

2005-12-29 18:41 +0000 [r492367]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/kmixapplet.cpp: Bugfix:
	  Restoring "inactive control colors" in the KMix PanelApplet
	  CCBUGS: 113957

2006-01-02 03:19 +0000 [r493293]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/juk.h,
	  branches/KDE/3.5/kdemultimedia/juk/juk.cpp: Fix an issue for
	  cartman. Ever since the Last Big Redesign, JuK wouldn't open
	  files from the command line when specified on startup. That issue
	  should now be fixed for KDE 3.5.

2006-01-02 14:22 +0000 [r493450]  scripty

	* branches/KDE/3.5/kdemultimedia/kaboodle/pics/hi64-app-kaboodle.png,
	  branches/KDE/3.5/kdemultimedia/kmix/pics/hi64-app-kmix.png,
	  branches/KDE/3.5/kdemultimedia/kaboodle/pics/hi128-app-kaboodle.png,
	  branches/KDE/3.5/kdemultimedia/kmix/pics/hi128-app-kmix.png,
	  branches/KDE/3.5/kdemultimedia/xine_artsplugin/tools/thumbnail/sprocket-medium.png,
	  branches/KDE/3.5/kdemultimedia/juk/hi64-app-juk.png,
	  branches/KDE/3.5/kdemultimedia/juk/hi128-app-juk.png,
	  branches/KDE/3.5/kdemultimedia/arts/builder/pics/hi16-app-artscontrol.png,
	  branches/KDE/3.5/kdemultimedia/kscd/hi64-app-kscd.png,
	  branches/KDE/3.5/kdemultimedia/noatun/hi64-app-noatun.png,
	  branches/KDE/3.5/kdemultimedia/xine_artsplugin/tools/thumbnail/sprocket-small.png,
	  branches/KDE/3.5/kdemultimedia/kscd/hi128-app-kscd.png,
	  branches/KDE/3.5/kdemultimedia/noatun/hi128-app-noatun.png,
	  branches/KDE/3.5/kdemultimedia/arts/builder/pics/hi16-app-artsbuilder.png,
	  branches/KDE/3.5/kdemultimedia/xine_artsplugin/tools/thumbnail/sprocket-large.png:
	  Remove svn:executable from some typical non-executable files
	  (goutte)

2006-01-04 10:44 +0000 [r494211]  dfaure

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp:
	  Avoid out-of-bounds access in info.trackInfoList[track] (in the
	  encoders) when there is no CDDB info for a CD. Note how info is
	  just an empty object in that case, so I see no point in calling
	  fillSongInfo with it. Encoders should be fine with no
	  fillSongInfo called. CCMAIL: ben@meyerhome.net

2006-01-04 13:15 +0000 [r494259]  dfaure

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp: Keep
	  the KCompactDisc instance around so that we don't have to read
	  the cd id and look it up on cddb for every stat() call. This
	  makes copy/paste of 10 mp3 files much faster. Approved by
	  Benjamin too.

2006-01-05 22:39 +0000 [r494647]  adridg

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/kcmaudiocd.cpp,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/kcmaudiocd/audiocdconfig.ui:
	  Somewhere along the way, exactly half of the text describing the
	  autosearch functionality was changed to the logically opposite
	  meaning. Nowadays, the configure option is 'Specify device' as
	  opposed to 'Autosearch'. This makes the code for enabling the
	  textbox simpler, sure. Since the logical sense has changed, we
	  need extra NOTs in the load and save methods. Since the meaning
	  of the option has changed, the whatsthis should describe what the
	  option does, and not what it used to do. This would be a real
	  problem if anyone actually read whatsthis text. CCMAIL:
	  kde@freebsd.org CCMAIL: kde-multimedia@kde.org CCMAIL:
	  kde-i18n-doc@kde.org PS. Docs people get an extra *grovel* in
	  order to fix this totally wrong string. If it's out of the
	  question to do now, drop me a note and I'll revert the whatsthis
	  change ASAP.

2006-01-05 22:45 +0000 [r494651]  adridg

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/plugins/lame/encoderlame.cpp:
	  If proc.collectedStdout() is empty, fromLocal8Bit calls strlen on
	  a NULL pointer in Qt 3.3 which may crash. Instead, to the
	  checking here and only convert a non-empty buffer. Patch thanks
	  to Markus Brueffer and Frerich Raabe. CCMAIL: kde@freebsd.org

2006-01-05 23:25 +0000 [r494661]  adridg

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp: For
	  FreeBSD, cdparanoia's autosearch combined with either ATAPICAM
	  (ATAPI-SCSI) or plain ATAPI doesn't fill in the expected fields
	  in the CAM structure; that causes crashes. So go case-by case and
	  use what makes sense. For the ATAPI case, assume the user has
	  specified a device name because otherwise we need to map an open
	  fd back to a pathname (knowing it's in /dev/ helps, and it's just
	  a bunch of fstat and stat calls, but still). CCMAIL:
	  kde@freebsd.org PS. Markus Brueffer has patches to cdparanoia to
	  add the Linux-style field cdda_device_name so that we can drop
	  this whole chunk of ifdefs and just use that field. This moves
	  the smarts into cdparanoia itself. We will add a configure check
	  when that comes around.

2006-01-11 17:39 +0000 [r496997]  larkang

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/tracksimp.cpp: Fix
	  setting the discId for new CD's, so storing information about a
	  new CD works

2006-01-11 21:31 +0000 [r497076]  larkang

	* branches/KDE/3.5/kdemultimedia/kscd/kscd.cpp: Don't change the
	  autoplay setting just because --start is in the command line args
	  CCBUG: 119311

2006-01-14 15:02 +0000 [r498015]  esken

	* branches/KDE/3.5/kdemultimedia/kmix/kmixdockwidget.cpp,
	  branches/KDE/3.5/kdemultimedia/kmix/dialogselectmaster.h,
	  branches/KDE/3.5/kdemultimedia/kmix/kmixdockwidget.h,
	  branches/KDE/3.5/kdemultimedia/kmix/dialogselectmaster.cpp:
	  Bugfix: The wrong master channel was saved (if the channel was
	  "after" an "enum MDW" or a "switch-only MDW". BUGS: 111152

2006-01-15 23:52 +0000 [r498711]  adridg

	* branches/KDE/3.5/kdemultimedia/configure.in.in,
	  branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp: Take
	  all the weird hacks that were done specifically for FreeBSD and
	  get rid of them. The upstream cdparanoia has been fixed now,
	  which gives us a more straightforward interface. CCMAIL:
	  kde@freebsd.org

2006-01-17 08:48 +0000 [r499192]  scripty

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/upgrade-kaudiocreator-metadata.sh:
	  Improve portatbility by declaring using Bash with the help of
	  /usr/bin/env (Bug #95475)

2006-01-17 08:59 +0000 [r499202]  scripty

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/upgrade-metadata.sh:
	  Improve portability by using: #! /usr/bin/env bash (Bug #95475)
	  (goutte)

2006-01-17 12:54 +0000 [r499269]  mueller

	* branches/KDE/3.5/kdemultimedia/mpeglib/configure.in.in: remove
	  explicit -lstdc++

2006-01-17 15:21 +0000 [r499319]  mueller

	* branches/KDE/3.5/kdemultimedia/juk/ktrm.cpp,
	  branches/KDE/3.5/kdemultimedia/juk/configure.in.in: musicbrainz
	  0.4.x support BUG:116575

2006-01-19 02:53 +0000 [r499931]  mpyne

	* branches/KDE/3.5/kdemultimedia/juk/tracksequenceiterator.cpp: Fix
	  bug 120363 (JuK should use Artist information in album random
	  play). Previously, only the Album name mattered, which meant that
	  common names like "Greatest Hits" would play different artists.
	  Thanks to the reporter for providing a reference patch.
	  BUG:120363

