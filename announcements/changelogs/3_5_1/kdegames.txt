2005-11-29 21:39 +0000 [r484216]  aacid

	* branches/KDE/3.5/kdegames/atlantik/client/atlantik.cpp: Fix bug
	  112041, atlantik can not close This will be on KDE 3.5.1 Stephan
	  you should be really, really, really more careful when doing this
	  substituions ;-( BUG: 112041

2005-12-03 14:35 +0000 [r485246]  scripty

	* branches/KDE/3.5/kdegames/kbounce/pics/hi64-app-kbounce.png,
	  branches/KDE/3.5/kdegames/kfouleggs/pics/hi64-app-kfouleggs.png,
	  branches/KDE/3.5/kdegames/kmines/data/hi22-app-kmines.png,
	  branches/KDE/3.5/kdegames/kreversi/icons/hi128-app-kreversi.png,
	  branches/KDE/3.5/kdegames/kasteroids/hi64-app-kasteroids.png,
	  branches/KDE/3.5/kdegames/kmines/data/hi32-app-kmines.png,
	  branches/KDE/3.5/kdegames/kspaceduel/hi128-app-kspaceduel.png,
	  branches/KDE/3.5/kdegames/ksokoban/data/hi22-app-ksokoban.png,
	  branches/KDE/3.5/kdegames/konquest/hi64-app-konquest.png,
	  branches/KDE/3.5/kdegames/kenolaba/hi22-app-kenolaba.png,
	  branches/KDE/3.5/kdegames/kasteroids/hi48-app-kasteroids.png,
	  branches/KDE/3.5/kdegames/kmines/data/hi16-app-kmines.png,
	  branches/KDE/3.5/kdegames/kbackgammon/icons/hi64-app-kbackgammon_engine.png,
	  branches/KDE/3.5/kdegames/kwin4/hi16-app-kwin4.png,
	  branches/KDE/3.5/kdegames/kbackgammon/icons/hi48-app-kbackgammon_engine.png,
	  branches/KDE/3.5/kdegames/ksnake/hi64-app-ksnake.png,
	  branches/KDE/3.5/kdegames/kshisen/hi128-app-kshisen.png,
	  branches/KDE/3.5/kdegames/kbounce/pics/hi128-app-kbounce.png,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/hi128-app-kgoldrunner.png,
	  branches/KDE/3.5/kdegames/ksmiletris/hi128-app-ksmiletris.png,
	  branches/KDE/3.5/kdegames/kbattleship/kbattleship/pictures/hi48-app-kbattleship.png,
	  branches/KDE/3.5/kdegames/lskat/hi48-app-lskat.png,
	  branches/KDE/3.5/kdegames/kspaceduel/hi64-app-kspaceduel.png,
	  branches/KDE/3.5/kdegames/kjumpingcube/hi32-app-kjumpingcube.png,
	  branches/KDE/3.5/kdegames/kspaceduel/hi48-app-kspaceduel.png,
	  branches/KDE/3.5/kdegames/kjumpingcube/hi16-app-kjumpingcube.png,
	  branches/KDE/3.5/kdegames/klines/hi22-app-klines.png,
	  branches/KDE/3.5/kdegames/klines/hi32-app-klines.png,
	  branches/KDE/3.5/kdegames/kbounce/pics/hi32-app-kbounce.png,
	  branches/KDE/3.5/kdegames/kmines/data/hi128-app-kmines.png,
	  branches/KDE/3.5/kdegames/kjumpingcube/hi64-app-kjumpingcube.png,
	  branches/KDE/3.5/kdegames/ksmiletris/hi22-app-ksmiletris.png,
	  branches/KDE/3.5/kdegames/ksmiletris/hi32-app-ksmiletris.png,
	  branches/KDE/3.5/kdegames/kfouleggs/pics/hi16-app-kfouleggs.png,
	  branches/KDE/3.5/kdegames/klines/hi16-app-klines.png,
	  branches/KDE/3.5/kdegames/libkdegames/carddecks/cards-konqi-modern/8.png,
	  branches/KDE/3.5/kdegames/kjumpingcube/hi48-app-kjumpingcube.png,
	  branches/KDE/3.5/kdegames/ksmiletris/hi16-app-ksmiletris.png,
	  branches/KDE/3.5/kdegames/kbattleship/kbattleship/pictures/hi128-app-kbattleship.png,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/hi48-app-kgoldrunner.png,
	  branches/KDE/3.5/kdegames/kolf/pics/hi22-app-kolf.png,
	  branches/KDE/3.5/kdegames/kpoker/hi22-app-kpoker.png,
	  branches/KDE/3.5/kdegames/kbackgammon/icons/hi128-app-kbackgammon.png,
	  branches/KDE/3.5/kdegames/kpoker/hi32-app-kpoker.png,
	  branches/KDE/3.5/kdegames/kpoker/hi16-app-kpoker.png,
	  branches/KDE/3.5/kdegames/doc/kreversi/kreversi1.png,
	  branches/KDE/3.5/kdegames/lskat/hi32-app-lskat.png,
	  branches/KDE/3.5/kdegames/kjumpingcube/hi128-app-kjumpingcube.png,
	  branches/KDE/3.5/kdegames/kwin4/hi48-app-kwin4.png,
	  branches/KDE/3.5/kdegames/kreversi/icons/hi32-app-kreversi.png,
	  branches/KDE/3.5/kdegames/kpat/icons/hi128-app-kpat.png,
	  branches/KDE/3.5/kdegames/kreversi/icons/hi16-app-kreversi.png,
	  branches/KDE/3.5/kdegames/kreversi/icons/hi64-app-kreversi.png,
	  branches/KDE/3.5/kdegames/kreversi/icons/hi48-app-kreversi.png,
	  branches/KDE/3.5/kdegames/kpat/icons/hi32-app-kpat.png,
	  branches/KDE/3.5/kdegames/kolf/pics/hi128-app-kolf.png,
	  branches/KDE/3.5/kdegames/kpat/icons/hi16-app-kpat.png,
	  branches/KDE/3.5/kdegames/kpoker/hi128-app-kpoker.png,
	  branches/KDE/3.5/kdegames/libkdegames/carddecks/decks/deck24.png,
	  branches/KDE/3.5/kdegames/kwin4/hi128-app-kwin4.png,
	  branches/KDE/3.5/kdegames/kpat/icons/hi64-app-kpat.png,
	  branches/KDE/3.5/kdegames/kasteroids/hi22-app-kasteroids.png,
	  branches/KDE/3.5/kdegames/kpat/icons/hi48-app-kpat.png,
	  branches/KDE/3.5/kdegames/kasteroids/hi32-app-kasteroids.png,
	  branches/KDE/3.5/kdegames/kfouleggs/pics/hi48-app-kfouleggs.png,
	  branches/KDE/3.5/kdegames/kbounce/pics/hi48-app-kbounce.png,
	  branches/KDE/3.5/kdegames/ktuberling/hi64-app-ktuberling.png,
	  branches/KDE/3.5/kdegames/kasteroids/hi16-app-kasteroids.png,
	  branches/KDE/3.5/kdegames/ktuberling/hi48-app-ktuberling.png,
	  branches/KDE/3.5/kdegames/kbackgammon/icons/hi32-app-kbackgammon_engine.png,
	  branches/KDE/3.5/kdegames/ksirtet/ksirtet/hi128-app-ksirtet.png,
	  branches/KDE/3.5/kdegames/kbackgammon/icons/hi16-app-kbackgammon_engine.png,
	  branches/KDE/3.5/kdegames/kblackbox/hi128-app-kblackbox.png,
	  branches/KDE/3.5/kdegames/ksame/hi128-app-ksame.png,
	  branches/KDE/3.5/kdegames/ksnake/hi22-app-ksnake.png,
	  branches/KDE/3.5/kdegames/kwin4/hi32-app-kwin4.png,
	  branches/KDE/3.5/kdegames/kmines/data/hi64-app-kmines.png,
	  branches/KDE/3.5/kdegames/doc/kpoker/kpoker1.png,
	  branches/KDE/3.5/kdegames/lskat/hi22-app-lskat.png,
	  branches/KDE/3.5/kdegames/kmines/data/hi48-app-kmines.png,
	  branches/KDE/3.5/kdegames/doc/kpoker/kpoker2.png,
	  branches/KDE/3.5/kdegames/ksokoban/data/hi64-app-ksokoban.png,
	  branches/KDE/3.5/kdegames/kenolaba/hi64-app-kenolaba.png,
	  branches/KDE/3.5/kdegames/kspaceduel/hi22-app-kspaceduel.png,
	  branches/KDE/3.5/kdegames/kmahjongg/hi128-app-kmahjongg.png,
	  branches/KDE/3.5/kdegames/kfouleggs/pics/hi128-app-kfouleggs.png,
	  branches/KDE/3.5/kdegames/kbattleship/kbattleship/pictures/hi64-app-kbattleship.png,
	  branches/KDE/3.5/kdegames/lskat/hi64-app-lskat.png,
	  branches/KDE/3.5/kdegames/kjumpingcube/hi22-app-kjumpingcube.png,
	  branches/KDE/3.5/kdegames/konquest/hi128-app-konquest.png,
	  branches/KDE/3.5/kdegames/kmahjongg/hi22-app-kmahjongg.png,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/hi22-app-kgoldrunner.png,
	  branches/KDE/3.5/kdegames/kfouleggs/pics/hi32-app-kfouleggs.png,
	  branches/KDE/3.5/kdegames/kbounce/pics/hi16-app-kbounce.png,
	  branches/KDE/3.5/kdegames/kmahjongg/hi64-app-kmahjongg.png,
	  branches/KDE/3.5/kdegames/klines/hi64-app-klines.png,
	  branches/KDE/3.5/kdegames/kgoldrunner/src/hi64-app-kgoldrunner.png,
	  branches/KDE/3.5/kdegames/klines/hi48-app-klines.png,
	  branches/KDE/3.5/kdegames/ksmiletris/hi64-app-ksmiletris.png,
	  branches/KDE/3.5/kdegames/lskat/hi128-app-lskat.png,
	  branches/KDE/3.5/kdegames/ksmiletris/hi48-app-ksmiletris.png,
	  branches/KDE/3.5/kdegames/kolf/pics/hi64-app-kolf.png,
	  branches/KDE/3.5/kdegames/kpoker/hi64-app-kpoker.png,
	  branches/KDE/3.5/kdegames/kpoker/hi48-app-kpoker.png,
	  branches/KDE/3.5/kdegames/kwin4/hi64-app-kwin4.png,
	  branches/KDE/3.5/kdegames/kbackgammon/icons/hi22-app-kbackgammon.png,
	  branches/KDE/3.5/kdegames/kreversi/icons/hi22-app-kreversi.png,
	  branches/KDE/3.5/kdegames/ksirtet/ksirtet/hi22-app-ksirtet.png,
	  branches/KDE/3.5/kdegames/lskat/hi16-app-lskat.png,
	  branches/KDE/3.5/kdegames/kblackbox/hi22-app-kblackbox.png,
	  branches/KDE/3.5/kdegames/klines/hi128-app-klines.png,
	  branches/KDE/3.5/kdegames/ktuberling/hi128-app-ktuberling.png,
	  branches/KDE/3.5/kdegames/kbackgammon/icons/hi64-app-kbackgammon.png,
	  branches/KDE/3.5/kdegames/ksirtet/ksirtet/hi64-app-ksirtet.png,
	  branches/KDE/3.5/kdegames/kblackbox/hi64-app-kblackbox.png,
	  branches/KDE/3.5/kdegames/ksame/hi64-app-ksame.png,
	  branches/KDE/3.5/kdegames/kasteroids/hi128-app-kasteroids.png,
	  branches/KDE/3.5/kdegames/kshisen/hi22-app-kshisen.png,
	  branches/KDE/3.5/kdegames/kpat/icons/hi22-app-kpat.png,
	  branches/KDE/3.5/kdegames/kfouleggs/pics/hi22-app-kfouleggs.png,
	  branches/KDE/3.5/kdegames/kbounce/pics/hi22-app-kbounce.png,
	  branches/KDE/3.5/kdegames/ktuberling/hi22-app-ktuberling.png,
	  branches/KDE/3.5/kdegames/kshisen/hi64-app-kshisen.png,
	  branches/KDE/3.5/kdegames/ksokoban/data/hi128-app-ksokoban.png,
	  branches/KDE/3.5/kdegames/kenolaba/hi128-app-kenolaba.png,
	  branches/KDE/3.5/kdegames/ksnake/hi128-app-ksnake.png,
	  branches/KDE/3.5/kdegames/ktuberling/hi16-app-ktuberling.png:
	  Remove the svn:executable property from documentation and PNG
	  files (goutte)

2005-12-05 15:27 +0000 [r485735]  mueller

	* branches/KDE/3.5/kdegames/kpat/freecell-solver/main.c (removed):
	  not needed here

2005-12-19 16:14 +0000 [r489729]  lueck

	* branches/KDE/3.5/kdegames/doc/kbattleship/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kgoldrunner/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kenolaba/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kblackbox/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kblackbox/kblackboxtbar.png,
	  branches/KDE/3.5/kdegames/doc/kbounce/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kfouleggs/index.docbook,
	  branches/KDE/3.5/kdegames/doc/katomic/index.docbook:
	  documentation update (menus, checked guiitems, sanitized)
	  CCMAIL:phil@kde.org CCMAIL:aseigo@olympusproject.org
	  CCMAIL:brian.beck@mchsi.com

2005-12-20 15:21 +0000 [r490035]  lueck

	* branches/KDE/3.5/kdegames/doc/konquest/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kreversi/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kshisen/kshisen-configuration.png
	  (added), branches/KDE/3.5/kdegames/doc/kolf/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kpoker/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ksame/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kreversi/kreversi1.png,
	  branches/KDE/3.5/kdegames/doc/kjumpingcube/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kpoker/kpoker1.png,
	  branches/KDE/3.5/kdegames/doc/kshisen/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kpoker/kpoker2.png,
	  branches/KDE/3.5/kdegames/doc/ksirtet/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kreversi/kreversi-configuration.png
	  (added): documentation update (menus, checked guiitems, bug
	  fixes, sanitized) CCMAIL:jason@katzbrown.com BUG:99506

2005-12-20 19:58 +0000 [r490094]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/boardwidget.cpp: Change some
	  repaints to update() Does not make the behaviour different but
	  seems a bit less CPU-intensive Thanks for the patch BUGS: 118711

2005-12-20 21:04 +0000 [r490119]  lueck

	* branches/KDE/3.5/kdegames/doc/ksnake/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kwin4/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ktron/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ksmiletris/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ksokoban/index.docbook:
	  documentation update (menus, checked guiitems, bug fixes,
	  sanitized) CCMAIL:justlinux@bellsouth.net
	  CCMAIL:martin@heni-online.de

2005-12-20 23:43 +0000 [r490165]  mueller

	* branches/KDE/3.5/kdegames/kmines/solver/adviseFull.cpp: fix
	  undefined operation

2005-12-21 22:23 +0000 [r490449]  mhunter

	* branches/KDE/3.5/kdegames/knetwalk/src/mainwindow.cpp:
	  Typographical corrections and changes CCMAIL:kde-i18n-doc@kde.org

2005-12-22 15:30 +0000 [r490637]  lueck

	* branches/KDE/3.5/kdegames/doc/kolf/index.docbook: fixed email and
	  kolf homepage link CCMAIL:jasonkb@mit.edu

2005-12-27 14:14 +0000 [r491766-491765]  lauri

	* branches/KDE/3.5/kdegames/doc/klickety/screenshot.png (added),
	  branches/KDE/3.5/kdegames/doc/klickety/index.docbook: update

	* branches/KDE/3.5/kdegames/doc/kmahjongg/gamescreen.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/tileset.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/highscore.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/layout.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/boardeditor.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/shortcuts.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/background.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/numbered.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/showremoved.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/theme.png (added),
	  branches/KDE/3.5/kdegames/doc/kmahjongg/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kmahjongg/config.png (added):
	  update

2005-12-29 15:39 +0000 [r492311]  lueck

	* branches/KDE/3.5/kdegames/doc/ktuberling/menu-playground.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/menu-edit.png,
	  branches/KDE/3.5/kdegames/doc/kmines/kmines1.png,
	  branches/KDE/3.5/kdegames/doc/kmines/kmines2.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/menu-help.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/menu-raw.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/button-redo.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/button-new.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/menu-settings.png
	  (added), branches/KDE/3.5/kdegames/doc/ktuberling/toolbar.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/index.docbook,
	  branches/KDE/3.5/kdegames/doc/atlantik/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ktuberling/button-print.png,
	  branches/KDE/3.5/kdegames/doc/konquest/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ktuberling/button-save.png,
	  branches/KDE/3.5/kdegames/doc/kbackgammon/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ktuberling/menu-speech.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/button-open.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/menu-game.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/button-undo.png,
	  branches/KDE/3.5/kdegames/doc/ktuberling/technical-reference.docbook,
	  branches/KDE/3.5/kdegames/doc/kmines/index.docbook: documentation
	  update (menus, checked guiitems, sanitized)

2006-01-10 21:02 +0000 [r496594]  pvicente

	* branches/KDE/3.5/kdegames/doc/kmahjongg/index.docbook: Fixed two
	  appearances of <trademark> which were incorrect. These were
	  breaking the generation of documentation. I have not regenerated
	  the PO files, because I do not know how to do it and if I should
	  do it (is it automatic?). Not fixed in branches/stable because I
	  am not allowed to do it. CCMAIL: kde-i18n-doc@kde.org

2006-01-16 20:10 +0000 [r499007]  aacid

	* branches/KDE/3.5/kdegames/ksnake/rattler.cpp: Apply a patch to
	  fix bug 111916 inspired in one by Henry Malthus. I've applied
	  mine becasue changes the code less heaviliy. Thanks for reporting
	  BUGS: 111916

