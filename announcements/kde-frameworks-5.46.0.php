<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.46.0");
  $site_root = "../";
  $release = '5.46.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
May 12, 2018. KDE today announces the release
of KDE Frameworks 5.46.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Avoid infinite loops when fetching the URL from DocumentUrlDB (bug 393181)");?></li>
<li><?php i18n("Add Baloo DBus signals for moved or removed files");?></li>
<li><?php i18n("Install pri file for qmake support &amp; document it in metainfo.yaml");?></li>
<li><?php i18n("baloodb: Add clean command");?></li>
<li><?php i18n("balooshow: Colorize only when attached to terminal");?></li>
<li><?php i18n("Remove FSUtils::getDirectoryFileSystem");?></li>
<li><?php i18n("Avoid hardcoding of filesystems supporting CoW");?></li>
<li><?php i18n("Allow disabling of CoW to fail when not supported by filesystem");?></li>
<li><?php i18n("databasesanitizer: Use flags for filtering");?></li>
<li><?php i18n("Fix merging of terms in the AdvancedQueryParser");?></li>
<li><?php i18n("Use QStorageInfo instead of a homegrown implementation");?></li>
<li><?php i18n("sanitizer: Improve device listing");?></li>
<li><?php i18n("Immediately apply termInConstruction when term is complete");?></li>
<li><?php i18n("Handle adjacent special characters correctly (bug 392620)");?></li>
<li><?php i18n("Add test case for parsing of double opening '((' (bug 392620)");?></li>
<li><?php i18n("Use statbuf consistently");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add plasma-browser-integration system tray icon");?></li>
<li><?php i18n("Add Virt-manager icon thanks to ndavis");?></li>
<li><?php i18n("Add video-card-inactive");?></li>
<li><?php i18n("overflow-menu as view-more-symbolic, and horizontal");?></li>
<li><?php i18n("Use the more appropriate \"two sliders\" icon for \"configure\"");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Include FeatureSummary before calling set_package_properties");?></li>
<li><?php i18n("Don't install plugins within lib on android");?></li>
<li><?php i18n("Make it possible to build several apk out of a project");?></li>
<li><?php i18n("Check if the application androiddeployqt package has a main() symbol");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("[KLineEdit] Use Qt's built-in clear button functionality");?></li>
<li><?php i18n("Fix KCompletionBox on wayland");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("[KUser] Check whether .face.icon is actually readable before returning it");?></li>
<li><?php i18n("Make KJob signals public so Qt5 connect syntax can work");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Load NV graphics reset based on config");?></li>
<li><?php i18n("[KUserProxy] Adjust to accounts service (bug 384107)");?></li>
<li><?php i18n("Plasma mobile optimizations");?></li>
<li><?php i18n("Make room for footer and header");?></li>
<li><?php i18n("new resize policy (bug 391910)");?></li>
<li><?php i18n("support actions visibility");?></li>
<li><?php i18n("Support nvidia reset notifications in QtQuickViews");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Add platform detection and adjustment to kded (automatic setting of $QT_QPA_PLATFORM)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Add description and purpose to Xattr dep");?></li>
<li><?php i18n("extractors: Hide warnings from system headers");?></li>
<li><?php i18n("fix detection of taglib when compiling for Android");?></li>
<li><?php i18n("Install pri file for qmake support &amp; document it in metainfo.yaml");?></li>
<li><?php i18n("Make concatenated strings wrappable");?></li>
<li><?php i18n("ffmpegextractor: Silence deprecation warnings");?></li>
<li><?php i18n("taglibextractor: Fix empty genre bug");?></li>
<li><?php i18n("handle more tags in taglibextractor");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("holidays/plan2/holiday_sk_sk - Teacher's Day fix (bug 393245)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("[API dox] New UI marker @info:placeholder");?></li>
<li><?php i18n("[API dox] New UI marker @item:valuesuffix");?></li>
<li><?php i18n("Don't need to run previous iterations commands again (bug 393141)");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("[XCF/GIMP loader] Raise maximimum allowed image size to 32767x32767 on 64 bit platforms (bug 391970)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Thumbnail smooth scaling in filepicker (bug 345578)");?></li>
<li><?php i18n("KFileWidget: Perfectly align filename widget with icon view");?></li>
<li><?php i18n("KFileWidget: Save places panel width also after hiding panel");?></li>
<li><?php i18n("KFileWidget: Prevent places panel width from growing 1px iteratively");?></li>
<li><?php i18n("KFileWidget: Disable zoom buttons once reached minimum or maximum");?></li>
<li><?php i18n("KFileWidget: Set minimum size for zoom slider");?></li>
<li><?php i18n("Don't select file extension");?></li>
<li><?php i18n("concatPaths: process empty path1 correctly");?></li>
<li><?php i18n("Improve grid icon layout in filepicker dialog (bug 334099)");?></li>
<li><?php i18n("Hide KUrlNavigatorProtocolCombo if there is just one protocol supported");?></li>
<li><?php i18n("Only show supported schemes in KUrlNavigatorProtocolCombo");?></li>
<li><?php i18n("Filepicker reads thumbs preview from Dolphin settings (bug 318493)");?></li>
<li><?php i18n("Add Desktop and Downloads to the default list of Places");?></li>
<li><?php i18n("KRecentDocument now stores QGuiApplication::desktopFileName instead of applicationName");?></li>
<li><?php i18n("[KUrlNavigatorButton] Also don't stat MTP");?></li>
<li><?php i18n("getxattr takes 6 parameters in macOS (bug 393304)");?></li>
<li><?php i18n("Add a \"Reload\" menu item to KDirOperator's context menu (bug 199994)");?></li>
<li><?php i18n("Save the dialog view settings even when canceling (bug 209559)");?></li>
<li><?php i18n("[KFileWidget] Hardcode example user name");?></li>
<li><?php i18n("Don't show top \"Open With\" app for folders; only for files");?></li>
<li><?php i18n("Detect incorrect parameter in findProtocol");?></li>
<li><?php i18n("Use text \"Other Application...\" in \"Open With\" submenu");?></li>
<li><?php i18n("Correctly encode URL of thumbnails (bug 393015)");?></li>
<li><?php i18n("Tweak column widths in tree view of file open/save dialogs (bug 96638)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Don't warn when using Page {} outside of a pageStack");?></li>
<li><?php i18n("Rework InlineMessages to address a number of issues");?></li>
<li><?php i18n("fix on Qt 5.11");?></li>
<li><?php i18n("base on units for toolbutton size");?></li>
<li><?php i18n("color close icon on hover");?></li>
<li><?php i18n("show a margin under the footer when needed");?></li>
<li><?php i18n("fix isMobile");?></li>
<li><?php i18n("also fade on open/close anim");?></li>
<li><?php i18n("include the dbus stuff only on unix-non android, non apple");?></li>
<li><?php i18n("watch the tabletMode from KWin");?></li>
<li><?php i18n("on desktop mode show actions on hover (bug 364383)");?></li>
<li><?php i18n("handle in the top toolbar");?></li>
<li><?php i18n("use a gray close button");?></li>
<li><?php i18n("less applicationwindow dependency");?></li>
<li><?php i18n("less warnings without applicationwindow");?></li>
<li><?php i18n("work correctly without applicationWindow");?></li>
<li><?php i18n("Don't have a non-integral size on separators");?></li>
<li><?php i18n("Don't show the actions if they are disabled");?></li>
<li><?php i18n("checkable FormLayout items");?></li>
<li><?php i18n("use different icons in the color set example");?></li>
<li><?php i18n("include icons only on android");?></li>
<li><?php i18n("make it work with Qt 5.7");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Fix double margins around DownloadDialog");?></li>
<li><?php i18n("Fix hints in UI files about subclasses of custom widgets");?></li>
<li><?php i18n("Don't offer qml plugin as a link target");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("use KDE_INSTALL_DATADIR instead of FULL_DATADIR");?></li>
<li><?php i18n("Add donate urls to test data");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Fix PersonSortFilterProxyModel filtering");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Also make installation of translated docs optional");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("DBus runner servicename wildcard support");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("optimization of KTextEditor::DocumentPrivate::views()");?></li>
<li><?php i18n("[ktexteditor] much faster positionFromCursor");?></li>
<li><?php i18n("Implement single click on line number to select line of text");?></li>
<li><?php i18n("Fix missing bold/italic/... markup with modern Qt versions (&gt;= 5.9)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Fix not shown event marker in calendar with air &amp; oxygen themes");?></li>
<li><?php i18n("Use \"Configure %1...\" for text of applet configure action");?></li>
<li><?php i18n("[Button Styles] Fill height and vertical align (bug 393388)");?></li>
<li><?php i18n("add video-card-inactive icon for system tray");?></li>
<li><?php i18n("correct look for flat buttons");?></li>
<li><?php i18n("[Containment Interface] Don't enter edit mode when immutable");?></li>
<li><?php i18n("make sure largespacing is perfect multiple of small");?></li>
<li><?php i18n("call addContainment with proper paramenters");?></li>
<li><?php i18n("Don't show the background if Button.flat");?></li>
<li><?php i18n("ensure the containment we created has the activity we asked for");?></li>
<li><?php i18n("add a version containmentForScreen with activity");?></li>
<li><?php i18n("Don't alter memory management to hide an item (bug 391642)");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Make sure we give some vertical space to configuration plugins");?></li>
<li><?php i18n("Port KDEConnect plugin config to QQC2");?></li>
<li><?php i18n("Port AlternativesView to QQC2");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("export layout paddings from qstyle, start from Control");?></li>
<li><?php i18n("[ComboBox] Fix mouse wheel handling");?></li>
<li><?php i18n("make the mousearea not interfere with controls");?></li>
<li><?php i18n("fix acceptableInput");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Update mount point after mount operations (bug 370975)");?></li>
<li><?php i18n("Invalidate property cache when an interface is removed");?></li>
<li><?php i18n("Avoid creating duplicate property entries in the cache");?></li>
<li><?php i18n("[UDisks] Optimize several property checks");?></li>
<li><?php i18n("[UDisks] Correct handling of removable file systems (bug 389479)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Fix remove enable/disable button");?></li>
<li><?php i18n("Fix enable/disable add button");?></li>
<li><?php i18n("Look into subdirectories for dictionaries");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Update project URL");?></li>
<li><?php i18n("'Headline' is a comment, so base it on dsComment");?></li>
<li><?php i18n("Add highlighting for GDB command listings and gdbinit files");?></li>
<li><?php i18n("Add syntax highlighting for Logcat");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.46");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
