<?php
  $page_title = "December Updates Stabilize KDE 4.5 Further";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- // Boilerplate -->

<h3 align="center">
  KDE Releases December Updates
</h3>

<p align="justify">
  <strong>
KDE Community Ships December Updates
</strong>
</p>

<p align="justify">
December 2nd, 2010. Today, KDE has released a series of updates to the Plasma Desktop and Netbook workspaces, the KDE Applications and the KDE Platform. This update is the fourth in a series of monthly stabilization updates to the 4.5 series. 4.5.4 brings bugfixes and translation updates on top of KDE SC 4.5 series and is a recommended update for everyone running 4.5.3 or earlier versions. As the release only contains bugfixes and translation updates, it will be a safe and pleasant update for everyone. KDE SC 4 is already translated into more than 55 languages, with more to come.
</p>

<p>To download source code or packages to install go to the <a href="/info/4.5.4.php">4.5.4 Info Page</a>.
The <a href="http://www.kde.org/announcements/changelogs/changelog4_5_3to4_5_4.php">changelog</a> lists more, but not all improvements since 4.5.3.
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="4.5/screenshots/plasma-netbook-sal.png"><img src="4.5/screenshots/thumbs/plasma-netbook-sal.png" align="center" width="540" height="338" alt="The KDE Plasma Netbook Workspace" title="The KDE Plasma Netbook Workspace" /></a>
<br />
<em>The KDE Plasma Netbook Workspace</em>
</div>

<p align="justify">
Note that the changelog is incomplete. For a complete list of
changes that went into 4.5.4, you can browse the Subversion log.
4.5.4 also ships a more complete set of translations for many of the 55+ supported languages.</p>

<p>To find out more about the KDE 4.5 Workspace and Applications, please refer to the
<a href="http://www.kde.org/announcements/4.5/">KDE SC 4.5.0</a>,
<a href="http://www.kde.org/announcements/4.4/">KDE SC 4.4.0</a>,
<a href="http://www.kde.org/announcements/4.3/">KDE SC 4.3.0</a>,
<a href="http://www.kde.org/announcements/4.2/">KDE SC 4.2.0</a>,
<a href="http://www.kde.org/announcements/4.1/">KDE SC 4.1.0</a> and
<a href="http://www.kde.org/announcements/4.0/">KDE SC 4.0.0</a> release
notes. 4.5.4 is a recommended update for everyone running KDE SC 4.5.2 or earlier versions.
<p />



<p align="justify">
KDE SC, including all its libraries and its applications, is available for free
under Open Source licenses. KDE software can be obtained in source and various binary
formats from <a
href="http://download.kde.org/stable/4.5.4/">http://download.kde.org</a>
or with any of the <a href="http://www.kde.org/download/distributions.php">major
GNU/Linux and UNIX systems</a> shipping today.
</p>


<!-- // Boilerplate again -->

<h4>
  Installing 4.5.4 Binary Packages
</h4>
<p align="justify">
  <em>Packages</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.5.4
for some versions of their distribution, and in other cases community volunteers
have done so.
  Additional binary packages, as well as updates to the packages now available,
may become available over the coming weeks.
</p>

<p align="justify">
  <a name="package_locations"></a><em>Package Locations</em>.
  For a current list of available binary packages of which the KDE Project has
been informed, please visit the <a href="/info/4.5.4.php#binary">4.5.4 Info
Page</a>.
</p>

<h4>
  Compiling 4.5.4
</h4>
<p align="justify">
  <a name="source_code"></a>
  The complete source code for 4.5.4 may be <a
href="http://download.kde.org/stable/4.5.4/src/">freely downloaded</a>.
Instructions on compiling and installing 4.5.4
  are available from the <a href="/info/4.5.4.php">4.5.4 Info
Page</a>.
</p>

<h4>
  Supporting KDE
</h4>

<p align="justify">
 KDE is a <a href="http://www.gnu.org/philosophy/free-sw.html">Free Software</a>
community that exists and grows only because of the help of many volunteers that
donate their time and effort. KDE is always looking for new volunteers and
contributions, whether it is help with coding, bug fixing or reporting, writing
documentation, translations, promotion, money, etc. All contributions are
gratefully appreciated and eagerly accepted. Please read through the <a
href="/community/donations/">Supporting KDE page</a> for further information or 
become a KDE e.V. supporting member through our new 
<a href="http://jointhegame.kde.org/">Join the Game</a> initiative. </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
