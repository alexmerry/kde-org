<?php
  $page_title = "KDE Announces Support Of KDE Technology";
  $site_root = "../";
  include "header.inc";
?>

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="600" >
<TR VALIGN=TOP>
<TD WIDTH="50">&nbsp;</TD>

<TD WIDTH="500">
<H3>
FOR IMMEDIATE RELEASE</H3>
<B>CALDERA ANNOUNCES SUPPORT OF KDE TECHNOLOGY</B>&nbsp;

<P>&nbsp;<I>Caldera hosts official U.S. FTP site for KDE</I>&nbsp;

<P>&nbsp;OREM, UT - July 16, 1998 - Caldera&reg;, Inc. today announced
the adoption and support of KDE technology. Caldera will include the K
Desktop Environment in the OpenLinux 1.2.2 maintenance release due out
the end of September. KDE will be the default desktop in the Caldera OpenLinux
2.0 product, scheduled for release the fourth quarter of this year.&nbsp;

<P>&nbsp;"Caldera is proud to be the first Linux vendor to fully adopt
KDE technology. Based on the open-source model, KDE offers the customers
of our commercial Linux products an internationally developed, leading-edge
desktop environment," said Ransom Love, General Manager of the OpenLinux
Division at Caldera. "With this partnership, Caldera continues its commitment
to our customers success by providing easy-to-use management and productivity
tools."&nbsp;

<P>&nbsp;Caldera is supporting KDE technology by hosting the official KDE
U.S. FTP site at <A HREF="ftp://ftp.us.kde.org/pub/kde/">ftp.us.kde.org</A>.
Provided by Caldera, KDE 1.0 binary and source rpms for OpenLinux 1.2 are
available for download from the site.&nbsp;

<P>&nbsp;"Caldera's adoption of the KDE technology helps to get not only
the KDE desktop into corporate settings, but also means a big leap forward
for the Linux operating system," said Kalle Dalheimer, member of the KDE
core team. "KDE provides users with the most feature-rich, easy-to-use
desktop available for Unix systems."&nbsp;

<P>&nbsp;The completely new Internet enabled desktop, incorporating a large
suite of applications for Unix workstations and the upcoming Compound Document
Framework based K Office Suite, currently supports more than 25 languages.
The strength of this exceptional environment lies in the interoperability
of its components and the functionality of its applications.&nbsp;

<P>&nbsp;<B>Caldera Information</B>&nbsp;
<BR>Caldera, Inc. develops and markets a line of operating system and networking
technologies including OpenLinux and DR-DOS. For more information about
Caldera products and technologies, please <B>visit our web site at </B><A HREF="http://www.caldera.com/">www.caldera.com</A>,
or call 1-888-GO-LINUX or 1-801-765-4888.&nbsp;

<P>&nbsp;<B>KDE Information</B>&nbsp;
<BR>The K Desktop Environment is an international Internet based volunteer
project which develops the freely available graphical desktop environment
for the UNIX&reg; platform. For more information about KDE and its technology,
please visit the web site at <A href="/">www.kde.org</A>
or contact <A HREF="m&#97;il&#116;o:&#x70;re&#115;&#0115;&#064;kde.&#x6f;&#114;g">press@kde.org.</A>&nbsp;

<P>&nbsp;<B>Media Contacts Only:</B>&nbsp;
<BR>Laura Kenner&nbsp;
<BR>Caldera, Inc.&nbsp;
<BR>(801) 765-4999 x238&nbsp;
<BR><A HREF="mailto:laurak@caldera.com">laurak@caldera.com</A>&nbsp;

<P>&nbsp;Kalle Dalheimer and Martin Konold&nbsp;
<BR>K Desktop Environment e.V.&nbsp;
<BR>+49-7071-29-78653&nbsp;

<P>&nbsp;</TD>

<TD WIDTH="50">&nbsp;</TD>
</TR>
</TABLE>

<?php include "footer.inc" ?>
