<?php

  $release = '4.7';
  $release_full = '4.7.0';

  $page_title = "Uuendatud KDE rakendused pakuvad rohkelt vaimustavaid omadusi";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>
<p>
KDE teatab rõõmuga paljude populaarsete rakenduste uute versioonide väljalaskmisest. Kõik need rakendused, alates mängudest kuni õppevajadusi rahuldavate ja meelelahutust pakkuvate rakendusteni, on nüüd veel võimsamad, ent ometi sama hõlpsasti kasutatavad. Allpool on ära toodud mõnede täna ilmunud rakenduste olulisemad uued omadused.
</p>

<?php
centerThumbScreenshot("dolphin-gwenview.png", "KDE rakendused 4.7");
?>

<p>

KDE grupitöölahendus <strong>Kontact</strong> on alates 4.7 väljalaskest taas ühinenud ülejäänud KDE tarkvaraga. Enamik selle komponente on porditud Akonadi peale ning nii seepärast kui ka muu arendustöö tõttu on paranenud stabiilsus, ühenduste loomine uute teenustega ning teabevahetus rakenduste vahel. Suurim muudatus on KMail 2 väljatulek. Selle liides on jäänud varasemaga sarnaseks, kuid kogu e-posti salvestamise ja hankimise süsteem on porditud Akonadi peale.
</p><p>
KDE failihalduril <strong>Dolphin</strong> on vaikimisi klaarim välimus. Menüüriba on peidetud, kuid soovi korral saab seda lihtsalt tagasi tuua. Täiustatud on failide otsimise liidest. Dolphin pakub nüüd palju korralikumat <a href="http://vishesh-yadav.com/blog/2011/07/03/mercurial-plugin-for-dolphin-work-progress-part-1-2/">lõimimist lähtekoodi haldamise süsteemidega</a>, sealhulgas saab luua ja kloonida hoidlaid, muudatusi tõmmata ja ise üles laadida, erinevusi vaadata ja veel palju muud. Nii Dolphin kui ka Konqueror on võitnud uuest pluginast, mis võimaldab otse menüü vahendusel faile ja katalooge hinnata ja annoteerida, tagades sel moel Nepomuki võimaluste ärakasutamise.
</p>
<?php
centerThumbScreenshot("konqueror-dolphin.png", "Konqueror ja Dolphin 4.7-s");
?>
<p>
Virtuaalne gloobus <strong>Marble</strong> on saanud viimasel poolel aastal palju täiustusi. See toetab nüüd hääljuhtimist, pakub kaardi loomise nõustajat ja mitmeid uusi pluginaid. Võistlusega Voice of Marble tagati tänu KDE kogukonna usinale kaasalöömisele võimalus kasutada hääljuhtimist õige mitmes keeles. Kõigest sellest kõneleb lähemalt <a href="http://edu.kde.org/marble/current_1.2.php">Marble visuaalne muudatuste logi</a>.
</p>
<?php
centerThumbScreenshot("marble.png", "Marble särab 4.7-s");
?>
<p>
KDE pildinäitaja <strong>Gwenview</strong> on muutnud piltide haldamise veel lihtsamaks, pakkudes võimalust võrrelda kaht või enamat pilti üksteise kõrval. Vali lihtsalt sirvimisrežiimis kaks või enam pilti ning lülitu vaate- või täisekraanirežiimi. Vaaterežiimis saab soovi korral pisipildiribalt pilte juurde lisada.
<?php
centerThumbScreenshot("gwenview.png", "Piltide võrdlemine Gwenview 4.7 abiga");
?>

</p><p>
Koomiksisõprade rõõmuks toetab universaalne failinäitaja Okular nüüd terve kataloogi käsitamist koomiksina.
</p><p>
Kogu maailma tähistaevahuvilistele oluline KDE rakendus KStars võimaldab nüüd ette määrata niinimetatud star hopping'u teekondi ning toetab OpenGL ja loomupärase renderdamistaustaprogrammi dünaamilist vahetamist. Taevatrajektooridele võib nüüd omistada nimesid. Komeetide trajektoore renderdatakse OpenGL režmmis.
Matemaatikud ja reaalteaduste esindajad saavad nüüd uurida kõrgema järgu funktsioone Kalgebra abil ning teada saada keemiliste elementide oksüdatsiooniastmeid Kalziumis.
</p><p>
Tarkvara arendajatele pakub KDevelop valmistaandestiile ning Pythoni interpretaatorit Krossi vahendusel. Samuti on parandatud Pythoni automaatse sõnalõpetuse võimalusi ning toetatud on lex/yacc faililaiendid.
</p><p>
KDE täiustatud tekstiredaktor Kate pakub viimistletud pluginaid ja suuremat stabiilsust. Täpsemat teavet leiab <a href="http://kate-editor.org/2011/07/09/kate-in-kde-4-7/">Kate veebileheküljelt</a>.
</p>

<h2>
DigiKam 2.0 võimaldab ära tunda nägusid, toetab piltide eri versioone, geosilte ja palju muud
</h2>
<p>
Täna näeb lisaks ilmavalgust <strong>DigiKam 2.0</strong>. Tee versioonini 2 nõudis enam kui aasta rasket arendamist. Nüüd on aga arendusmeeskond uhkusega valmis teatama uue põlvkonna Digikami sünnist. See sisaldab muu hulgas ammu soovitud nägude tuvastamise võimalust ning toetab piltide versioonide kasutamist, samuti XMP metaandmete sidecar-faile. Oluliselt on täiustatud fotode sildistamist ja tähistamist, võimalik on pöördgeosildistamine ja veel palju muud uuenduslikku. Kõige muu kõrval on parandatud 219 viga.
<!--
(screenshot of the new DigiKam here: http://farm7.static.flickr.com/6124/5964495890_7a5c6a5d2d_o_d.png )
-->
<?php
centerThumbScreenshot("digikam.png", "Digikam 2.0");
?>

</p><p>
Digikam käib käsikäes Kipi pluginatega, mille uus väljalase täna samuti ilmus. See sisaldab uusi eksportimistööriistu kolme veebiteenuse tarbeks: Yandex.Fotki, MediaWiki ja Rajce. GPSSynci plugin pakub nüüd pöördgeokodeerimise võimalust. Ja nagu ikka, on rohkelt parandatud vigu.
Ka paljud teised täna ilmunud KDE rakendused on saanud uusi omadusi ning neis on parandatud arvukalt vigu. Lisaks on nad saanud kasu teistest KDE tarkvarakomplekti viimastest uuendustest ja täiustustest.
</p>


<h4>KDE rakenduste paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>

<?php

include("trailer-plasma-et.inc");
include("trailer-platform-et.inc");

include("footer.inc");
?>
