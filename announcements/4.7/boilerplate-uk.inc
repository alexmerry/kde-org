<!-- header goes into the specific page -->

<p align="justify">
KDE, зокрема всі бібліотеки і програми, з яких складається середовище, доступні для безкоштовного звантаження за умов дотримання ліцензій на програмне забезпечення з відкритим кодом. Програмне забезпечення KDE працює на багатьох апаратних платформах, у багатьох операційних системах та з багатьма інструментами керування вікнами або стільничними середовищами. Окрім Linux та інших операційних систем на базі UNIX ви можете скористатися версіями більшості програм KDE у Microsoft Windows за допомогою сайта<a href="http://windows.kde.org">програмного забезпечення KDE у Windows</a> та версіями для Apple Mac OS X за допомогою сайта <a href="http://mac.kde.org/">програмного забезпечення KDE на Mac</a>. Експериментальні випуски програм KDE для різноманітних мобільних платформ, зокрема MeeGo, MS Windows Mobile і Symbian, можна знайти у інтернеті, але зараз у цих випусків немає супровідників.
<br />
Програмне забезпечення KDE можна отримати у форматі початкових кодів та різноманітних бінарних форматах за посиланнями на сайті <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">http://download.kde.org</a>, а також отримати на <a href="http://www.kde.org/download/cdrom.php">компакт-диску</a>
 або разом з будь-якою з сучасних <a href="http://www.kde.org/download/distributions.php">поширених систем GNU/Linux та UNIX</a>.
</p>
<p align="justify">
  <a name="packages"><em>Пакунки</em></a>.
  Деякі з виробників дистрибутивів операційних систем Linux/UNIX люб’язно надали можливість користувачам своїх дистрибутивів звантажити бінарні пакунки <?php echo $release_full;?> 
для декількох версій дистрибутивів. У інших випадках такі бінарні версії було створено силами ентузіастів зі спільноти дистрибутива. <br />
  Деякі з таких бінарних пакунків доступні для вільного звантаження за адресою <a
href="http://download.kde.org/binarydownload.html?url=/stable/<?php echo $release_full;?>/">http://download.kde.org</a>.
  Додаткові бінарні пакунки, а також оновлення доступних пакунків можна буде звантажити найближчими днями.
<a name="package_locations"><em>Адреси для звантаження пакунків</em></a>.
Поточний список наявних бінарних пакунків, про які відомо проектові KDE, можна знайти на <a href="/info/<?php echo $release_full;?>.php"><?php echo $release;?>Інформаційній сторінці KDE</a>.
</p>
<p align="justify">
  <a name="source_code"></a>
  Повні початкові коди <?php echo $release_full;?> можна <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/src/">безкоштовно звантажити</a>.
Настанови щодо збирання і встановлення програмного забезпечення KDE <?php echo $release_full;?>
 можна знайти на <a href="/info/<?php echo $release_full;?>.php#binary"><?php echo $release_full;?> Інформаційній сторінці KDE</a>.
</p>

<h4>
Вимоги до системи
</h4>
<p align="justify">
Для того, щоб скористатися всіма можливостями найсвіжіших випусків, ми наполегливо рекомендуємо вам встановити найсвіжішу версію Qt, зокрема поточну версію — 4.7.4. Встановлення цієї версії забезпечить стабільність роботи, оскільки покращення програмного забезпечення KDE засновано на покращенні коду бібліотек Qt.<br />
З метою повного використання можливостей програмного забезпечення KDE ми також рекомендуємо вам встановити у вашій системі найсвіжіші графічні драйвери, оскільки це може значно покращити її працездатність шляхом розширення функціональних можливостей та покращення стабільності.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Контакти для преси</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

