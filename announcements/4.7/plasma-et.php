<?php

  $release = '4.7';
  $release_full = '4.7.0';
  $page_title = "Plasma töötsoonid: KWin aitab neid kõikjal kasutada";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p>
KDE teatab rõõmuga nii Plasma töölaua kui ka Plasma Netbooki töötsooni versiooni 4.7 väljalaskmisest. Plasma töötsoonide senist funktsionaalsust on tublisti parandatud ning lisandunud on ka mitmeid märkimisväärseid võimalusi. Viimaste seast tasub esile tuua uusi liidese loomise viise, mis sobivad paremini puuteekraaniga ja mobiilsetele seadmetele.
</p>
<?php
centerThumbScreenshot("plasma.png", "Plasma töölaud 4.7");
?>

<p>
Rohke visuaalne viimistlus avaldub <strong>Oxygeni ikoonide</strong> uuendustes ning tunduvalt on ühtlustunud ka paneeli elementide, näiteks kella ja märguanneteala, väljanägemine. KDE tarkvara modulaarse iseloomu ning võime tõttu kombineerida ja ühendada paljudest eri allikatest pärit rakendusi on KDE täiustanud ka Oxygeni GTK teemasid, mis lubab GNOME rakendustel (ja teistel GTK+ kasutavatel rakendustel) sujuvalt sulanduda Plasma töötsooni KDE rakendustega.
<?php
centerThumbScreenshot("oxygen-icons.png", "Oxygeni ikoonid elasid üle viimistluskuuri");
?>

</p><p>
Plasma töötsoonide aknahalduri KWin koodi on ulatuslikult puhastatud ning see suudab nüüd töötada <strong>OpenGL ES</strong> toetusega riistvaral, mis muudab aknahalduri senisest sobivamaks mobiilsetele seadmetele, aga pakub paremat elamust ka lauaarvutite kasutajatele. KWin kasutab uut varjutussüsteemi, mis aitab paremini toetada kasutajaid, kel on vanem riistvara või selline riistvara, mille draiveri OpenGL toetus on piiratud (Xrenderi taustaprogramm). KWini jõudlus on tänu rohkele optimeerimisele märgatavalt paranenud.
</p><p>
Ohtralt täiustusi on saanud ka Plasma tegevused. Tegevustehalduril on nüüd märksa silmatorkavam asukoht Plasma töölaua vaikimisi paneelil. Tegevused aitavad edendada kasutajate töövoogu, pakkudes nutikaid võimalusi rakendusi, vidinaid ja dokumente rühmitada.
<?php
centerThumbScreenshot("plasma-activities.png", "Plasma tegevustel on 4.7-s silmatorkavam koht");
?>

<!--
Plasma widgets based on the new capabilities of QtQuick have also started to appear, for example, the Battery Monitor, Device Notifier, and Lock/Logout widgets.
-->
</p><p>
Samuti pakuvad Plasma töötsoonid nüüd palju etemaid võrguhalduse võimalusi, sealhulgas on eksperimentaalselt toetatud nii NetworkManager 0.9 kui ka interneti kasutamine üle Bluetoothi, 3G, VPN, MAC-i muutmine ja veel mitmed peened võrguhalduse võimalused.
<?php
centerThumbScreenshot("plasma-networkmanagement.png", "Plasma 4.7 võrguhaldus");
?>

</p><p>
Liikumine rakenduste ja viimati kasutatud failide vahel on nüüd lihtsam tänu rakenduste käivitajale Kickoff lisatud asukohanäitajale, mis aitab kasutajal täpselt aru saada, kus ta parajasti viibib, ning kiiresti tagasi jõuda menüü kõrgematesse tasanditesse.
Töötsoonid on saanud veel palju muid kasutamist lihtsustavaid ja funktsionaalsust täiendavaid uuendusi. Nii näiteks ei blokeeri Konsool enam USB-pulkade eemaldamist ning KMix toetab tunduvalt paremini PulseAudiot.
<?php
centerThumbScreenshot("kickoff.png", "Kickoffi uus asukohanäitaja lihtsustab liikumist");
?>

</p>

<h4>Plasma paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>

<?php

include("trailer-applications-et.inc");
include("trailer-platform-et.inc");

include("footer.inc");

?>
