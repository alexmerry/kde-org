<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.52.0");
  $site_root = "../";
  $release = '5.52.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
November 10, 2018. KDE today announces the release
of KDE Frameworks 5.52.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Unbreak build with BUILD_QCH=TRUE");?></li>
<li><?php i18n("Actually use fileNameTerms and xAttrTerms");?></li>
<li><?php i18n("[Balooshow] Avoid out-of-bounds access when accessing corrupt db data");?></li>
<li><?php i18n("[Extractor] Do not check QFile::exists for an empty url");?></li>
<li><?php i18n("[Scheduler] Use flag to track when a runner is going idle");?></li>
<li><?php i18n("[Extractor] Handle documents correctly where mimetype should not be indexed");?></li>
<li><?php i18n("[Scheduler] Fix wrong usage of obsolete QFileInfo::created() timestamp (bug 397549)");?></li>
<li><?php i18n("[Extractor] Make extractor crash resilient (bug 375131)");?></li>
<li><?php i18n("Pass the FileIndexerConfig as const to the individual indexers");?></li>
<li><?php i18n("[Config] Remove KDE4 config support, stop writing arbitrary config files");?></li>
<li><?php i18n("[Extractor] Improve commandline debugging, forward stderr");?></li>
<li><?php i18n("[Scheduler] Reuse fileinfo from FilteredDirIterator");?></li>
<li><?php i18n("[Scheduler] Reuse mimetype from UnindexedFileIterator in indexer");?></li>
<li><?php i18n("[Scheduler] Remove superfluous m_extractorIdle variable");?></li>
<li><?php i18n("Perform checks for unindexed files and stale index entries on startup");?></li>
<li><?php i18n("[balooctl] Print current state &amp; indexing file when monitor starts (bug 364858)");?></li>
<li><?php i18n("[balooctl] Monitor also for state changes");?></li>
<li><?php i18n("[balooctl] Fix \"index\" command with already indexed, but moved file (bug 397242)");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Add Media and MediaEndpoint API header generation");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Change package manager icons to emblems");?></li>
<li><?php i18n("Re-add monochrome link icon as action");?></li>
<li><?php i18n("Improve emblem contrast, legibility and consistency (bug 399968)");?></li>
<li><?php i18n("Support \"new\" mimetype for .deb files (bug 399421)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("ECMAddQch: help doxygen by predefining more Q_DECL_* macros");?></li>
<li><?php i18n("Bindings: Support using sys paths for python install directory");?></li>
<li><?php i18n("Bindings: Remove INSTALL_DIR_SUFFIX from ecm_generate_python_binding");?></li>
<li><?php i18n("Add support for the fuzzer sanitizer");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("support for multi pages kcms");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Add mechanism to notify other clients of config changes over DBus");?></li>
<li><?php i18n("Expose getter method for KConfig::addConfigSources");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Allow KHelpCenter to open the right pages of KDE help when KHelpClient is invoked with an anchor");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("KCrash: fix crash (ironic heh) when used in an app without QCoreApplication");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("make push/pop part of ConfigModule API");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Remove useless \"No X-KDE-DBus-ServiceName found\" message");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("Reference product \"KF5\" in widget metadata, instead of \"KDE\"");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("API dox: add minimal docs to KDocTools namespace, so doxygen covers it");?></li>
<li><?php i18n("Create a QCH file with the API dox, optionally, using ECMAddQCH");?></li>
<li><?php i18n("Wait for docbookl10nhelper to be built before building our own manpages");?></li>
<li><?php i18n("Use specified Perl interpreter instead of relying on PATH");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("[ExtractorCollection] Use only best matching extractor plugin");?></li>
<li><?php i18n("[KFileMetaData] Add extractor for generic XML and SVG");?></li>
<li><?php i18n("[KFileMetaData] Add helper for XML encoded Dublin Core metadata");?></li>
<li><?php i18n("implement support for reading ID3 tags from aiff and wav files");?></li>
<li><?php i18n("implement more tags for asf metadata");?></li>
<li><?php i18n("extract ape tags from ape and wavpack files");?></li>
<li><?php i18n("provide a list of supported mimetypes for embeddedimagedata");?></li>
<li><?php i18n("compare with QLatin1String and harmonize handling of all types");?></li>
<li><?php i18n("Don't crash on invalid exiv2 data (bug 375131)");?></li>
<li><?php i18n("epubextractor: Add property ReleaseYear");?></li>
<li><?php i18n("refactor taglibextractor to functions specific for metadata type");?></li>
<li><?php i18n("add wma files/asf tags as supported mimetype");?></li>
<li><?php i18n("use own extractor for testing the taglibwriter");?></li>
<li><?php i18n("add a string suffix to test data and use for unicode testing of taglibwriter");?></li>
<li><?php i18n("remove compile time check for taglib version");?></li>
<li><?php i18n("extend test coverage to all supported mimetypes for taglibextractor");?></li>
<li><?php i18n("Use variable with already fetched text instead of fetching again");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Fix keyboard layout change notifications (bug 269403)");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Add Bahrain Holiday File");?></li>
<li><?php i18n("Make KHolidays work as static library too");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Add a QIconEnginePlugin to allow QIcon deserialization (bug 399989)");?></li>
<li><?php i18n("[KIconLoader] Replace heap-allocated QImageReader with stack-allocated one");?></li>
<li><?php i18n("[KIconLoader] Adjust emblem border depending on icon size");?></li>
<li><?php i18n("Center icons properly if size doesn't fit (bug 396990)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Do not try to fallback to \"less secure\" SSL protocols");?></li>
<li><?php i18n("[KSambaShare] Trim trailing / from share path");?></li>
<li><?php i18n("[kdirlistertest] Wait a little longer for the lister to finish");?></li>
<li><?php i18n("Display sorry message if file is not local");?></li>
<li><?php i18n("kio_help: Fix crash in QCoreApplication when accessing help:// (bug 399709)");?></li>
<li><?php i18n("Avoid waiting for user actions when kwin Focus stealing prevention is high or extreme");?></li>
<li><?php i18n("[KNewFileMenu] Don't open an empty QFile");?></li>
<li><?php i18n("Added missing Icons to Places Panel code from KIO");?></li>
<li><?php i18n("Get rid of the raw KFileItem pointers in KCoreDirListerCache");?></li>
<li><?php i18n("Add 'Mount' option to context menu of unmounted device in Places");?></li>
<li><?php i18n("Add a 'Properties' entry in the places panel context menu");?></li>
<li><?php i18n("Fix warning \"macro expansion producing 'defined' has undefined behavior\"");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Fix missing items in static builds");?></li>
<li><?php i18n("basic support for hidden pages");?></li>
<li><?php i18n("load icons from proper icon themes");?></li>
<li><?php i18n("(many other fixes)");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("More useful error messages");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fixed a crash caused by bad lifetime management of canberra-based audio notification (bug 398695)");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Fix Cancel being not handled in deprecated BrowserRun::askEmbedOrSave");?></li>
<li><?php i18n("Port to undeprecated variant of KRun::runUrl");?></li>
<li><?php i18n("Port KIO::Job::ui() -&gt; KJob::uiDelegate()");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add KWayland virtual desktop protocol");?></li>
<li><?php i18n("Guard data source being deleted before processing dataoffer receive event (bug 400311)");?></li>
<li><?php i18n("[server] Respect input region of sub-surfaces on pointer surface focus");?></li>
<li><?php i18n("[xdgshell] Add positioner constraint adjustment flag operators");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("API dox: fix \"Since\" note of KPageWidgetItem::isHeaderVisible");?></li>
<li><?php i18n("Add a new property headerVisible");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Do not compare iterators returned from two separate returned copies");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Take 1..n KMainWindows in kRestoreMainWindows");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Add missing ipv4 setting options");?></li>
<li><?php i18n("Add vxlan setting");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("revert icons scaling on mobile");?></li>
<li><?php i18n("Support mnemonic labels");?></li>
<li><?php i18n("Remove PLASMA_NO_KIO option");?></li>
<li><?php i18n("Properly look for fallback themes");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Set Dialog flag for JobDialog");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[solid-hardware5] List icon in device details");?></li>
<li><?php i18n("[UDisks2] Power down drive on remove if supported (bug 270808)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Fix breakage of language guessing");?></li>
</ul>

<h3><?php i18n("Syndication");?></h3>

<ul>
<li><?php i18n("Add missing README.md file (needed by various scripts)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("z/OS CLIST file syntax highlighting");?></li>
<li><?php i18n("Creating new syntax highlighting file for Job Control Language (JCL)");?></li>
<li><?php i18n("Remove open mode from too new Qt version");?></li>
<li><?php i18n("inc version + fixup required kate version to current framework version");?></li>
<li><?php i18n("keyword rule: Support for keywords inclusion from another language/file");?></li>
<li><?php i18n("No spell checking for Metamath except in comments");?></li>
<li><?php i18n("CMake: Add XCode related variables and properties introduced in 3.13");?></li>
<li><?php i18n("CMake: Introduce new features of upcoming 3.13 release");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.52");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.8");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
