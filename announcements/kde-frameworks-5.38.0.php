<?php
  include_once ("functions.inc");
  $translation_file = "kde-org";
  $page_title = i18n_noop("Release of KDE Frameworks 5.38.0");
  $site_root = "../";
  $release = '5.38.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
September 09, 2017. KDE today announces the release
of KDE Frameworks 5.38.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<p>Missing in last month's announcement: KF5 includes a new framework,
Kirigami, a set of QtQuick components to build user interfaces based on the KDE UX guidelines.</p>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fix directory based search");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Set CMAKE_*_OUTPUT_5.38 to run tests without installing");?></li>
<li><?php i18n("Include a module for finding qml imports as runtime dependencies");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Return high-resolution line edit clear icon");?></li>
<li><?php i18n("Fix accepting dialogs with ctrl+return when buttons are renamed");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Refactor of the query which combines linked and used resources");?></li>
<li><?php i18n("Reloading the model when the resource gets unlinked");?></li>
<li><?php i18n("Fixed the query when merging linked and used resources");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix labels of DeleteFile/RenameFile actions (bug 382450)");?></li>
<li><?php i18n("kconfigini: Strip leading whitespace when reading entry values (bug 310674)");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Deprecate KStandardAction::Help and KStandardAction::SaveOptions");?></li>
<li><?php i18n("Fix labels of DeleteFile/RenameFile actions (bug 382450)");?></li>
<li><?php i18n("Use \"document-close\" as icon for KStandardAction::close");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("DesktopFileParser: add fallback lookup in \":/kservicetypes5/*\"");?></li>
<li><?php i18n("Add support for uninstalled plugins in kcoreaddons_add_plugin");?></li>
<li><?php i18n("desktopfileparser: Fix non-compliant key/value parsing (bug 310674)");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("support X-KDE-OnlyShowOnQtPlatforms");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("CMake: Fix target name shortening when build dir has special characters (bug 377573)");?></li>
<li><?php i18n("Add CC BY-SA 4.0 International and set it as default");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("KGlobalAccel: port to KKeyServer's new method symXModXToKeyQt, to fix numpad keys (bug 183458)");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("klauncher: fix appId matching for flatpak apps");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Port the webshortcuts KCM from KServiceTypeTrader to KPluginLoader::findPlugins");?></li>
<li><?php i18n("[KFilePropsPlugin] Locale-format totalSize during calculation");?></li>
<li><?php i18n("KIO: fix long-standing memory leak on exit");?></li>
<li><?php i18n("Add mimetype filtering capabilities to KUrlCompletion");?></li>
<li><?php i18n("KIO: port the URI filter plugins from KServiceTypeTrader to json+KPluginMetaData");?></li>
<li><?php i18n("[KUrlNavigator] Emit tabRequested when place in menu is middle-clicked (bug 304589)");?></li>
<li><?php i18n("[KUrlNavigator] Emit tabRequested when places selector is middle-clicked (bug 304589)");?></li>
<li><?php i18n("[KACLEditWidget] Allow double clicking to edit entry");?></li>
<li><?php i18n("[kiocore] Fix the logic error in previous commit");?></li>
<li><?php i18n("[kiocore] Check that klauncher is running or not");?></li>
<li><?php i18n("Really rate-limit INF_PROCESSED_SIZE messages (bug 383843)");?></li>
<li><?php i18n("Don't clear Qt's SSL CA certificate store");?></li>
<li><?php i18n("[KDesktopPropsPlugin] Create destination directory if it doesn't exist");?></li>
<li><?php i18n("[File KIO slave] Fix applying special file attributes (bug 365795)");?></li>
<li><?php i18n("Remove busy loop check in TransferJobPrivate::slotDataReqFromDevice");?></li>
<li><?php i18n("make kiod5 an \"agent\" on Mac");?></li>
<li><?php i18n("Fix proxy KCM not loading manual proxies correctly");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("hide scrollbars when useless");?></li>
<li><?php i18n("Add basic example for adjusting column width draggable handle");?></li>
<li><?php i18n("ider layers in handles positioning");?></li>
<li><?php i18n("fix handle placing when overlaps the last page");?></li>
<li><?php i18n("don't show fake handle on the last column");?></li>
<li><?php i18n("don't store stuff in the delegates (bug 383741)");?></li>
<li><?php i18n("as we already set keyNavigationEnabled, set wraps as well");?></li>
<li><?php i18n("better left-alignment for the back button (bug 383751)");?></li>
<li><?php i18n("don't take into account the header 2 times when scrolling (bug 383725)");?></li>
<li><?php i18n("never wrap the header labels");?></li>
<li><?php i18n("address FIXME: remove resetTimer (bug 383772)");?></li>
<li><?php i18n("don't scroll away applicationheader in non mobile");?></li>
<li><?php i18n("Add a property to hide the PageRow separator matching AbstractListItem");?></li>
<li><?php i18n("fix scrolling with originY and bottomtotop flow");?></li>
<li><?php i18n("Get rid of warnings about setting both pixel and point sizes");?></li>
<li><?php i18n("don't trigger reachable mode on inverted views");?></li>
<li><?php i18n("take page footer into account");?></li>
<li><?php i18n("add a slightly more complex example of a chat app");?></li>
<li><?php i18n("more failsafe to find the right footer");?></li>
<li><?php i18n("Check item validity before using it");?></li>
<li><?php i18n("Honour layer position for isCurrentPage");?></li>
<li><?php i18n("use an animation instead of an animator (bug 383761)");?></li>
<li><?php i18n("leave needed space for the page footer, if possible");?></li>
<li><?php i18n("better dimmer for applicationitem drawers");?></li>
<li><?php i18n("background dimming for applicationitem");?></li>
<li><?php i18n("fix properly back button margins");?></li>
<li><?php i18n("proper margins for back button");?></li>
<li><?php i18n("less warnings in ApplicationHeader");?></li>
<li><?php i18n("don't use plasma scaling for icon sizes");?></li>
<li><?php i18n("new look for handles");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Initialize the \"Pause\" button state in the widget tracker");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Don't block starting notification service (bug 382444)");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("refactor kpackagetool away from stringy options");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Clear previous actions on update");?></li>
<li><?php i18n("Add remote runners over DBus");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Port Document/View scripting API to QJSValue-based solution");?></li>
<li><?php i18n("Show icons in icon border context menu");?></li>
<li><?php i18n("Replace KStandardAction::PasteText with KStandardAction::Paste");?></li>
<li><?php i18n("Support fractional scaling in generating the sidebar preview");?></li>
<li><?php i18n("Switch from QtScript to QtQml");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Treat input RGB buffers as premultiplied");?></li>
<li><?php i18n("Update SurfaceInterface outputs when an output global gets destroyed");?></li>
<li><?php i18n("KWayland::Client::Surface track output destruction");?></li>
<li><?php i18n("Avoid sending data offers from an invalid source (bug 383054)");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("simplify setContents by letting Qt do more of the work");?></li>
<li><?php i18n("KSqueezedTextLabel: Add isSqueezed() for convenience");?></li>
<li><?php i18n("KSqueezedTextLabel: Small improvements to API docs");?></li>
<li><?php i18n("[KPasswordLineEdit] Set focus proxy to line edit (bug 383653)");?></li>
<li><?php i18n("[KPasswordDialog] Reset geometry property");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("KKeyServer: fix handling of KeypadModifier (bug 183458)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Save up a bunch of stat() calls on application start");?></li>
<li><?php i18n("Fix KHelpMenu position on Wayland (bug 384193)");?></li>
<li><?php i18n("Drop broken mid-button click handling (bug 383162)");?></li>
<li><?php i18n("KUndoActions: use actionCollection to set the shortcut");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[ConfigModel] Guard against adding a null ConfigCategory");?></li>
<li><?php i18n("[ConfigModel] Allow programmatically adding and removing ConfigCategory (bug 372090)");?></li>
<li><?php i18n("[EventPluginsManager] Expose pluginPath in model");?></li>
<li><?php i18n("[Icon Item] Don't needlessly unset imagePath");?></li>
<li><?php i18n("[FrameSvg] Use QPixmap::mask() instead of deprecated convoluted way via alphaChannel()");?></li>
<li><?php i18n("[FrameSvgItem] Create margins/fixedMargins object on demand");?></li>
<li><?php i18n("fix check state for menu items");?></li>
<li><?php i18n("Force Plasma style for QQC2 in applets");?></li>
<li><?php i18n("Install the PlasmaComponents.3/private folder");?></li>
<li><?php i18n("Drop remains of \"locolor\" themes");?></li>
<li><?php i18n("[Theme] Use KConfig SimpleConfig");?></li>
<li><?php i18n("Avoid some unnecessary theme content lookups");?></li>
<li><?php i18n("ignore spurious resize events to empty sizes (bug 382340)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Add syntax definition for Adblock Plus filter lists");?></li>
<li><?php i18n("Rewrite the Sieve syntax definition");?></li>
<li><?php i18n("Add highlighting for QDoc configuration files");?></li>
<li><?php i18n("Add highlight definition for Tiger");?></li>
<li><?php i18n("Escape hyphen in rest.xml regular expressions (bug 383632)");?></li>
<li><?php i18n("fix: plaintext is highlighted as powershell");?></li>
<li><?php i18n("Add syntax highlighting for Metamath");?></li>
<li><?php i18n("Rebased Less syntax highlighting on SCSS one (bug 369277)");?></li>
<li><?php i18n("Add Pony highlighting");?></li>
<li><?php i18n("Rewrite the email syntax definition");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.38");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
