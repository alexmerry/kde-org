<?php

/**
* News Lib
*
* @copyright 2010 Frank Karlitschek karlitschek@kde.org 
* @copyright 2010 Sayak Banerjee <sayakb@kde.org>
* @copyright 2017 Ken Vermette <vermette@gmail.com>
* @copyright 2019 Carl Schwan <carl@carlschwan.eu>
* 
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
* License as published by the Free Software Foundation; either 
* version 3 of the License, or any later version.
* 
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU AFFERO GENERAL PUBLIC LICENSE for more details.
*  
* You should have received a copy of the GNU Lesser General Public 
* License along with this library.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

namespace AetherLibs;

use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * Extract feeds from database
 */
class Feeds
{
    /**
     * @params int $count Number of news that need to be fetched
     * @return array an array of news
     */
    static function news(int $count): array
    {
        $cache = new FilesystemAdapter();
        $output = $cache->get('dotrss', function (ItemInterface $item) use ($count) {
            $item->expiresAfter(3600);

            $feed = simplexml_load_file('https://dot.kde.org/rss.xml');
            $output = [];
            if ($feed) {
                for($i = 0 ; $i <= $count ; $i++) {
                    $item = $feed->channel->item[$i];
                    if ($item) {
                        $output[] = [
                            'title' => htmlspecialchars($item->title),
                            'time' => htmlspecialchars($item->pubDate),
                            'url' => htmlspecialchars($item->link),
                        ];
                    }
                }
            } else {
                return ['title' => 'empty'];
            }

            return $output;
        });
        return $output;
    }

    static function planet(int $count): array
    {
        $cache = new FilesystemAdapter();
        $output = $cache->get('feedrss', function (ItemInterface $item) use ($count) {
            $item->expiresAfter(3600);

            $feed = simplexml_load_file('https://planet.kde.org/rss20.xml');
            $output = [];
            if ($feed) {
                for($i = 0 ; $i <= $count ; $i++) {
                    $item = $feed->channel->item[$i];
                    if ($item) {
                        $output[] = [
                            'title' => htmlspecialchars($item->title),
                            'url' => htmlspecialchars($item->link),
                        ];
                    }
                }
            }

            return $output;
        });
        return $output;
    }
    
    static function mergesort(&$array, $cmp_function = 'strcmp')
    {
        // Arrays of size < 2 require no action.
        if (count($array) < 2)
            return;


        // Split the array in half
        $halfway = count($array) / 2;
        $array1 = array_slice($array, 0, $halfway);
        $array2 = array_slice($array, $halfway);


        // Recurse to sort the two halves
        mergesort($array1, $cmp_function);
        mergesort($array2, $cmp_function);


        // If all of $array1 is <= all of $array2, just append them.
        if (call_user_func($cmp_function, end($array1), $array2[0]) < 1)
        {
            $array = array_merge($array1, $array2);
            return;
        }


        // Merge the two sorted arrays into a single sorted array
        $array = array();
        $ptr1 = $ptr2 = 0;


        while ($ptr1 < count($array1) && $ptr2 < count($array2))
            if (call_user_func($cmp_function, $array1[$ptr1], $array2[$ptr2]) < 1)
                $array[] = $array1[$ptr1++];
            else
                $array[] = $array2[$ptr2++];


        // Merge the remainder
        while ($ptr1 < count($array1)) $array[] = $array1[$ptr1++];
        while ($ptr2 < count($array2)) $array[] = $array2[$ptr2++];
    }

    static function svsort (&$array = null, $key = null,bool $preserve = false)
    {
        // Set the ssorting function we will use
        if (is_array(reset($array)))
            $sort = function ($a, $b) use ($key) {  if ($a[$key] == $b[$key]) return 0; return ( $a[$key] < $b[$key] ? -1 : 1);  };
        if (is_object(reset($array)))
            $sort = function ($a, $b) use ($key) {  if ($a->{$key} == $b->{$key}) return 0; return ($a->{$key} < $b->{$key} ? -1 : 1);  };
        else
            return;

        // Sort the array.
        if ($preserve)
            mergesort($array, $sort);
        else
            usort($array, $sort);

        return $array;
    }
}
