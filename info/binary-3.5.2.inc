<ul>

<!-- ARCH LINUX -->
<li><a href="http://www.archlinux.org/">Arch Linux</a>
  :
  <ul type="disc">
    <li>Packages: <a href="ftp://ftp.archlinux.org/extra/os/i686">ftp://ftp.archlinux.org/extra/os/i686</a></li>
    <li>
      To install: pacman -S kde
    </li>
  </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         5.10 (Breezy) and development version (Dapper): <a href="http://kubuntu.org/announcements/kde-352.php">Intel i386, AMD64 and 
PowerPC</a>
      </li>
    </ul>
  <p />
</li>

<!-- Pardus -->
<li>
 <a href="http://www.pardus.org.tr/">Pardus</a>
    <ul type="disc">
      <li>
         1.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/Pardus/">Intel i386</a>
      </li>
    </ul>
  <p />
</li>

<!-- kde-redhat -->
<li><a href="http://kde-redhat.sourceforge.net/">KDE RedHat (unofficial) Packages</a>:
(<a href="http://apt.kde-redhat.org/apt/kde-redhat/kde-org.txt">README</a>)
<ul type="disc">

 <li>All distributions: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/all/">(noarch,SRPMS)</a></li>

 <li>Red Hat 7.3: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/7.3/i386/">(i386)</a></li>

 <li>Red Hat 9: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/9/i386/">(i386)</a> </li>

 <li>Red Hat Enterprise Linux 3: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/3/i386/">(i386)</a> </li>

 <li>Red Hat Enterprise Linux 4:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/i386/">(i386)</a>
 	<!-- not available (yet) -->
 	<!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/redhat/4/x86_64/">Red Hat Enterprise 4 (x86_64)</a>-->
 </li>

 <li>Fedora Core 3:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
 	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/3/i386/">(i386)</a>
	<!-- not available (yet) -->
	<!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/3/x86_64/">(x64_64)</a> -->
 </li>

 <li>Fedora Core 4:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/i386/">(i386)</a>
	<!-- not available (yet) -->
 	<!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/x86_64/">(x64_64)</a> -->
 </li>
 <li>Fedora Core 5:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/i386/">(i386)</a>
        <!-- not available (yet) -->
        <!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/x86_64/">(x64_64)</a> -->
 </li>

</ul>
 <p />
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/contrib/Slackware/10.2/README">README</a>)
   :
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/contrib/Slackware/noarch/">Language packages</a>
     </li>
     <li>
        10.2: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/contrib/Slackware/10.2/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- SUSE LINUX -->
<li>
  <a href="http://www.novell.com/linux/suse/">SuSE Linux</a>
  (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/README">README</a>)
      :
  <ul type="disc">
    <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/noarch/">Language
        packages</a> (all versions and architectures)
    </li>
    <li>
      10.0:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/ix86/10.0/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/x86_64/10.0/">AMD x86-64</a>
    </li>
    <li>
      9.3:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/ix86/9.3/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/x86_64/9.3/">AMD x86-64</a>
    </li>
    <li>
      9.2:
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/ix86/9.2/">Intel i586</a> and
      <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.2/SuSE/x86_64/9.2/">AMD x86-64</a>
    </li>
  </ul>
  <p /> 
</li>


</ul>