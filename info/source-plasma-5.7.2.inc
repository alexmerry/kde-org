<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/bluedevil-5.7.2.tar.xz">bluedevil-5.7.2</a></td>
   <td align="right">135kB</td>
   <td><tt>66a726e7c5eef0e0c6a9a3f193bd3219ec7ba1ede762ef102eb4b78d86f38753</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/breeze-5.7.2.tar.xz">breeze-5.7.2</a></td>
   <td align="right">11MB</td>
   <td><tt>5e7523b1b0f4aa6e69d6116308792f380c259ff2f6d7a2515f5a89268dbf1494</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/breeze-grub-5.7.2.tar.xz">breeze-grub-5.7.2</a></td>
   <td align="right">150kB</td>
   <td><tt>b860ba3c61d2355cfc95c263b0a2b5ea04bd079f6403c55b8bac22c5c7c20fe2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/breeze-gtk-5.7.2.tar.xz">breeze-gtk-5.7.2</a></td>
   <td align="right">206kB</td>
   <td><tt>fcb1e5704c3d805dba312856cb59aff9ac33324f974afaa524d3ae5f5ddf4e22</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/breeze-plymouth-5.7.2.tar.xz">breeze-plymouth-5.7.2</a></td>
   <td align="right">55kB</td>
   <td><tt>3dcae149e5602ca7f6ee48ea326275f2cc14128eea44eb8073b7772370c4bc99</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/discover-5.7.2.tar.xz">discover-5.7.2</a></td>
   <td align="right">795kB</td>
   <td><tt>e0bd6cd6973630ec6fe951a12266ada561b72bcae500679e42acff20da69e34e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kactivitymanagerd-5.7.2.tar.xz">kactivitymanagerd-5.7.2</a></td>
   <td align="right">78kB</td>
   <td><tt>39a1a388d1d21d3a4c24ecc9f53cdbda5a885ae9ddcd6d81ee58ed40bf1c572b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kde-cli-tools-5.7.2.tar.xz">kde-cli-tools-5.7.2</a></td>
   <td align="right">476kB</td>
   <td><tt>1a70b99e315e8b940d72c159aa5b2b6b3af4a0f7db303fe4eeece1a8a1e65778</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kdecoration-5.7.2.tar.xz">kdecoration-5.7.2</a></td>
   <td align="right">34kB</td>
   <td><tt>9de9ee00df6a65f2501480618acdfc547b4affb0e8c6d3f27be014bfe30cebf2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kde-gtk-config-5.7.2.tar.xz">kde-gtk-config-5.7.2</a></td>
   <td align="right">144kB</td>
   <td><tt>56907395a16fe9dd5e60f73fc845e7de3ad6aee2148965c169b9144078d9f121</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kdeplasma-addons-5.7.2.tar.xz">kdeplasma-addons-5.7.2</a></td>
   <td align="right">1.9MB</td>
   <td><tt>e756722aaf7a6ccb98f910ab7457d385acbda0541207cbac971ebf739f1b99e2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kgamma5-5.7.2.tar.xz">kgamma5-5.7.2</a></td>
   <td align="right">58kB</td>
   <td><tt>45e8a20390a53bd7278065dda92cc049817bad42c9c411e3abba2b4c3e05734a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/khotkeys-5.7.2.tar.xz">khotkeys-5.7.2</a></td>
   <td align="right">590kB</td>
   <td><tt>55cf1219cfdac8549a79df94c04d016f74ef05d1ec0996671a6cb96224c2600b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kinfocenter-5.7.2.tar.xz">kinfocenter-5.7.2</a></td>
   <td align="right">1.1MB</td>
   <td><tt>e4f5ecc296415c7a381df0fd47d167da0b0f2906f3ab876428bbe3e40b88fef6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kmenuedit-5.7.2.tar.xz">kmenuedit-5.7.2</a></td>
   <td align="right">430kB</td>
   <td><tt>b783db11382b3b46db16f09e4c412ebad687f70923200a0e2cd36a3a60d75b2d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kscreen-5.7.2.tar.xz">kscreen-5.7.2</a></td>
   <td align="right">106kB</td>
   <td><tt>dc684a017d7b0feeb8d9e33c66acc1f50d45cc2db8b168d00820b6c0ab8943b4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kscreenlocker-5.7.2.tar.xz">kscreenlocker-5.7.2</a></td>
   <td align="right">100kB</td>
   <td><tt>8d0e1a7beee1e36cb1593867f476ed1471300546f7f651982f58aca20f738ab0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/ksshaskpass-5.7.2.tar.xz">ksshaskpass-5.7.2</a></td>
   <td align="right">18kB</td>
   <td><tt>ea0aafff906d292ea27c2c061bf6423d84fa7e36daed829be9071a9269b7561d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/ksysguard-5.7.2.tar.xz">ksysguard-5.7.2</a></td>
   <td align="right">476kB</td>
   <td><tt>e6ae4db5111f8d7752946d1c4976377d885de198509499dd832860fd9bdec923</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kwallet-pam-5.7.2.tar.xz">kwallet-pam-5.7.2</a></td>
   <td align="right">17kB</td>
   <td><tt>c26981de51825defd386a53d15f53de1da87082ac758b24bd41ba4cbef52019c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kwayland-integration-5.7.2.tar.xz">kwayland-integration-5.7.2</a></td>
   <td align="right">17kB</td>
   <td><tt>fb6ecedb227d4b3e40e7f316aba92fe90bd0adb2a364d94182e1d81dd166a949</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kwin-5.7.2.tar.xz">kwin-5.7.2</a></td>
   <td align="right">3.7MB</td>
   <td><tt>b0041a8bc3136a136a68f7f78702b7419a2e2561d0e18bb3c9443c1c3c64c901</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/kwrited-5.7.2.tar.xz">kwrited-5.7.2</a></td>
   <td align="right">19kB</td>
   <td><tt>9898b4c3a4741e45be7e8faaf9647c6f08539b94aeee5f13bcd1cf5bd240411a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/libkscreen-5.7.2.tar.xz">libkscreen-5.7.2</a></td>
   <td align="right">88kB</td>
   <td><tt>2ad615811937bbbb2ab12becc7f0d47db0ff10e36fd516c1d697fa087453b77a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/libksysguard-5.7.2.tar.xz">libksysguard-5.7.2</a></td>
   <td align="right">557kB</td>
   <td><tt>53a5300fc982ec4c0b71987b28a86e03e219b2cad20382d42275399814236162</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/milou-5.7.2.tar.xz">milou-5.7.2</a></td>
   <td align="right">52kB</td>
   <td><tt>8aa09a1caa2805cfcbaeb303d90b381c9ae503596f280226d55d75168375898f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/oxygen-5.7.2.tar.xz">oxygen-5.7.2</a></td>
   <td align="right">4.2MB</td>
   <td><tt>4890add23e9e8e6abcb649efba9f8f98fd06807729813d8b0cec2a96d6d9a604</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-desktop-5.7.2.tar.xz">plasma-desktop-5.7.2</a></td>
   <td align="right">5.9MB</td>
   <td><tt>2715035fbdd04899925ea12f03d8648ca35185266398d0fe6d580cbb03247c4e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-integration-5.7.2.tar.xz">plasma-integration-5.7.2</a></td>
   <td align="right">47kB</td>
   <td><tt>cfd8a328041a6793a17f20e9502cece94d7f302d395468a49f87b6de00030ac9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-mediacenter-5.7.2.tar.xz">plasma-mediacenter-5.7.2</a></td>
   <td align="right">158kB</td>
   <td><tt>96bbb013fc134d57963f35bdc7e760e137864730cce10a2e1a03528c693debed</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-nm-5.7.2.tar.xz">plasma-nm-5.7.2</a></td>
   <td align="right">618kB</td>
   <td><tt>75858a305821681bfeb3a7669f30d6c90470c9b12fe1ce35e7817d49bf140eab</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-pa-5.7.2.tar.xz">plasma-pa-5.7.2</a></td>
   <td align="right">140kB</td>
   <td><tt>fbdaf918cefe37877b4e2d825c4ef8774f63f1adc456c360884537d9730e4070</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-sdk-5.7.2.tar.xz">plasma-sdk-5.7.2</a></td>
   <td align="right">681kB</td>
   <td><tt>f49ed3ed3d6409df44ce98833f518968c81d0ef0d380a44651eacdb22bcd4b7f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-workspace-5.7.2.tar.xz">plasma-workspace-5.7.2</a></td>
   <td align="right">6.4MB</td>
   <td><tt>1e18eeb6d1aa6560fabff3a899039a8c42a58b365cb9dac2cc3abfc5b47903cc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/plasma-workspace-wallpapers-5.7.2.tar.xz">plasma-workspace-wallpapers-5.7.2</a></td>
   <td align="right">43MB</td>
   <td><tt>fd9f22cbbb300fb81d11f188cb431acc90a3d9720540abbea6df292b7accf0f3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/polkit-kde-agent-1-5.7.2.tar.xz">polkit-kde-agent-1-5.7.2</a></td>
   <td align="right">39kB</td>
   <td><tt>8565ded2a1150279797130182a3c9d4b651d5c7f71262df67de1ea277b10a04f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/powerdevil-5.7.2.tar.xz">powerdevil-5.7.2</a></td>
   <td align="right">349kB</td>
   <td><tt>6b2792cc3c5c4ba5a3196bd2f4a2f76c4b1a34ecb4e728367207918a1b0a538c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/sddm-kcm-5.7.2.tar.xz">sddm-kcm-5.7.2</a></td>
   <td align="right">47kB</td>
   <td><tt>507ba30ca54cb63be35f2855e7d6bff3298d38739cf99ada33265f80fe5d7da5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/systemsettings-5.7.2.tar.xz">systemsettings-5.7.2</a></td>
   <td align="right">154kB</td>
   <td><tt>d146bd8988b77f187409dbab81b1de31f2c83a1d179ad32b7116d80a5d055838</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.7.2/user-manager-5.7.2.tar.xz">user-manager-5.7.2</a></td>
   <td align="right">273kB</td>
   <td><tt>77106617c42a4ae685f006219919a818f670a7aea831911ee5a6b3b92c274e39</tt></td>
</tr>

</table>
