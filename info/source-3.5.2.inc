<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/arts-1.5.2.tar.bz2">arts-1.5.2</a></td>
   <td align="right">944kB</td>
   <td><tt>e1eb7969ea16aab2bdd9d1a9736d6af3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdeaccessibility-3.5.2.tar.bz2">kdeaccessibility-3.5.2</a></td>
   <td align="right">8.0MB</td>
   <td><tt>bcfd51875aa2a168fbc97055a576a33c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdeaddons-3.5.2.tar.bz2">kdeaddons-3.5.2</a></td>
   <td align="right">1.5MB</td>
   <td><tt>affb74174c6e3eda2c0748d6c052c8ff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdeadmin-3.5.2.tar.bz2">kdeadmin-3.5.2</a></td>
   <td align="right">2.0MB</td>
   <td><tt>ad1f645ed9f140a7c9ce8602cc0c88b8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdeartwork-3.5.2.tar.bz2">kdeartwork-3.5.2</a></td>
   <td align="right">15MB</td>
   <td><tt>211801e7ab7a5e113821625b931f338c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdebase-3.5.2.tar.bz2">kdebase-3.5.2</a></td>
   <td align="right">22MB</td>
   <td><tt>c5685e1be34e033286aa1f37002a0552</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdebindings-3.5.2.tar.bz2">kdebindings-3.5.2</a></td>
   <td align="right">5.1MB</td>
   <td><tt>cb26ce2e4efc35ca2c9c7a47807d679a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdeedu-3.5.2.tar.bz2">kdeedu-3.5.2</a></td>
   <td align="right">28MB</td>
   <td><tt>ce62d2e67579280a3e000152fb365bff</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdegames-3.5.2.tar.bz2">kdegames-3.5.2</a></td>
   <td align="right">10MB</td>
   <td><tt>9c3b0011136f24df623560face8c959b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdegraphics-3.5.2.tar.bz2">kdegraphics-3.5.2</a></td>
   <td align="right">6.9MB</td>
   <td><tt>8e1816a2191ea85b889930159a5d1e46</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdelibs-3.5.2.tar.bz2">kdelibs-3.5.2</a></td>
   <td align="right">14MB</td>
   <td><tt>367738696dc468859cf90d5a6e8f18a9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdemultimedia-3.5.2.tar.bz2">kdemultimedia-3.5.2</a></td>
   <td align="right">5.9MB</td>
   <td><tt>f98ef8465bf4de1eb36bc3bdb1f4f7d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdenetwork-3.5.2.tar.bz2">kdenetwork-3.5.2</a></td>
   <td align="right">7.1MB</td>
   <td><tt>dddc6bb65ffb9e46d118781ca0f26da4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdepim-3.5.2.tar.bz2">kdepim-3.5.2</a></td>
   <td align="right">12MB</td>
   <td><tt>ac6b3b503e27a65a7b883c1e0a57262e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdesdk-3.5.2.tar.bz2">kdesdk-3.5.2</a></td>
   <td align="right">4.6MB</td>
   <td><tt>0047e87532930a390d7a1826fd8cdaf5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdetoys-3.5.2.tar.bz2">kdetoys-3.5.2</a></td>
   <td align="right">3.0MB</td>
   <td><tt>c698bc8724db937734280a2553a4b1d8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdeutils-3.5.2.tar.bz2">kdeutils-3.5.2</a></td>
   <td align="right">2.8MB</td>
   <td><tt>08c01d6ceb8fb2173463838e43f986f5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdevelop-3.3.2.tar.bz2">kdevelop-3.3.2</a></td>
   <td align="right">7.7MB</td>
   <td><tt>494c9320a5f8681b67a3a06fa0ce18b7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.2/src/kdewebdev-3.5.2.tar.bz2">kdewebdev-3.5.2</a></td>
   <td align="right">5.7MB</td>
   <td><tt>ddd2ded8178f7c4d094d73e95075e7fb</tt></td>
</tr>

</table>
