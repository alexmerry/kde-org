
KDE Security Advisory: JPEG-EXIF Meta Information DoS vulnerability
Original Release Date: 2006-11-29
URL: http://www.kde.org/info/security/advisory-20061129-1.txt

0. References

        None.


1. Systems affected:

	kdegraphics as shipped with KDE 3.1.0 up to including 3.5.5.

2. Overview:

	The JPEG kfile-info plugin, which is used in several KDE applications
	for showing image metainformation (for example the embedded
	EXIF information) is vulnerable to a endless recursion
	EXIF parsing bug.  This particular issue was reported by Marcus
	Meissner from SUSE Security.

3. Impact:

	On a regular Linux system, this can cause the process that launched
	the plugin to crash. If ulimits have been removed, it can cause the
	machine run out of memory.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 3.1.0 - KDE 3.5.5 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

	1ce5fb77aff8f97ed21da046c1385000  post-3.5.5-kdegraphics.diff



