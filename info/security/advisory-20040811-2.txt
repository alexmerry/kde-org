-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: DCOPServer Temporary Filename Vulnerability
Original Release Date: 2004-08-11
URL: http://www.kde.org/info/security/advisory-20040811-2.txt

0. References

        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2004-0690
        http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=261386

1. Systems affected:

        KDE 3.2.x up to KDE 3.2.3 inclusive. 


2. Overview:

        The Debian project was alerted that KDE's DCOPServer creates
        temporary files in an insecure manner. Since the temporary
        files are used for authentication related purposes this can
        potentially allow a local attacker to compromise the account of
        any user which runs a KDE application.

        The Common Vulnerabilities and Exposures project (cve.mitre.org)
        has assigned the name CAN-2004-0690 to this issue.


3. Impact:

        KDE's DCOPServer creates temporary files in an insecure manner.
        Since the temporary files are used for authentication related
        purposes this can potentially allow a local attacker to compromise
        the account of any user which runs a KDE application.
        

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        Patches for KDE 3.2.3 are available from
        ftp://ftp.kde.org/pub/kde/security_patches : 

  0046c691fa833b2ff8d7eac15312a68b  post-3.2.3-kdelibs-dcopserver.patch


6. Time line and credits:


        25/07/2004 Debian Project alerted by Colin Phipps
	26/07/2004 KDE Security team informed by Chris Cheney
	26/07/2004 Patch created
	27/07/2004 Vendors notified
        11/08/2004 Public advisory

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2 (GNU/Linux)

iD8DBQFBGiosN4pvrENfboIRApSoAJ0S7zbgId9etA3EDrOv5dnFpSUU4wCfd2JK
kHcL+tcXbrH971YcuoEleTQ=
=VHci
-----END PGP SIGNATURE-----
