-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: Konqueror Referer Leaking Website Authentication Credentials
Original Release Date: 2003-07-29
URL: http://www.kde.org/info/security/advisory-20030729-1.txt

0. References
        http://cve.mitre.org/cgi-bin/cvename.cgi?name=CAN-2003-0459

1. Systems affected:

        All versions of Konqueror as distributed with KDE up to and including
KDE 3.1.2 as well as Konqueror/Embedded

2. Overview:

        Konqueror may inadvertently send authentication credentials to
websites other than the intended website in clear text via the HTTP-referer
header when authentication credentials are passed as part of a URL in the form
of http://user:password@host/

        The Common Vulnerabilities and Exposures project (cve.mitre.org) has
assigned the name CAN-2003-0459 to this issue.

3. Impact:

        Users of Konqueror may unknowingly distribute website authentication
credentials to third parties with links on the password protected website.
This may make it possible for those third parties to gain unauthorized access
to the password protected website.

       Users of Konqueror may unknowingly send website authentication
credentials in clear text across their local network and to websites of third 
parties. This may allow an attacker who is able to eavesdrop on such
communication to obtain the credentials and use them to gain unauthorized
access to the password protected website.

4. Solution:

        Users can reduce, but not totally eliminate, the risk by not providing
any password as part of a URL. Instead they should provide the password in
the KDE authentication dialog when prompted.

        Users of KDE 2.2.2 are advised to upgrade to KDE 3.1.3. A patch for
KDE 2.2.2 is available as well for users that are unable to upgrade to
KDE 3.1.

        Users of KDE 3.0.x are advised to upgrade to KDE 3.1.3. A patch for
KDE 3.0.5b is available as well for users that are unable to upgrade to
KDE 3.1.

        Users of KDE 3.1.x are advised to upgrade to KDE 3.1.3.

        Users of Konqueror/Embedded are advised to upgrade to a snapshot of
Konqueror/Embedded of July 5th, 2003 or later, available from
http://devel-home.kde.org/~hausmann/snapshots/ :

        30dc3e109124e8532c7c0ed6ad3ec6fb  konqueror-embedded-snapshot-20030705.tar.gz

5. Patch:
        A patch for KDE 2.2.2 is available from
ftp://ftp.kde.org/pub/kde/security_patches :

        90d0a6064ee1ba99347b55e303081cd5  post-2.2.2-kdelibs-http.patch

        Patches for KDE 3.0.5b are available from
ftp://ftp.kde.org/pub/kde/security_patches :

        a2bd79b4a78aa7d51afe01c47a8ab6d2  post-3.0.5b-kdelibs-http.patch
        a5ed29d49c07aa5a2c63b9bbaec0e7b2  post-3.0.5b-kdelibs-khtml.patch

        Patches for KDE 3.1.2 are available from
ftp://ftp.kde.org/pub/kde/security_patches :

        8ebafe8432e92cb4e878a37153cf12a4  post-3.1.2-kdelibs-http.patch
        6f27515ca22198b4060f4a4fe3c3a6b1  post-3.1.2-kdelibs-khtml.patch

6. Timeline and credits:

        07/03/2003 Notification of security@kde.org by George Staikos
        07/10/2003 Fixed in KDE CVS.
        07/11/2003 OS vendors / binary package providers alerted and
                   provided with patches.
	07/29/2003 Public Security Advisory by the KDE Security team.


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.2-rc1-SuSE (GNU/Linux)

iD8DBQE/JaZqvsXr+iuy1UoRAkPhAJ4536lHPU7MTTZMpA5+iRWxFUnTCACg7Wek
ddwqwmAs0UiCF+DHrVBKR+8=
=W4W+
-----END PGP SIGNATURE-----
