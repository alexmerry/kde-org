KDE Security Advisory: Okular PDB Processing Memory Corruption Vulnerability
Original Release Date: 2010-08-25
URL: http://www.kde.org/info/security/advisory-20100825-1.txt

0. References:
    CVE-2010-2575
    SA40952

1. Systems affected:

    Okular as shipped with KDE SC 4.3.0 up to including KDE SC 4.5.0. Earlier
    versions of KDE SC may also be affected.

2. Overview:

    The vulnerability is caused due to a boundary error during RLE
    decompression in the "TranscribePalmImageToJPEG()" function in
    generators/plucker/inplug/image.cpp when processing images embedded in
    PDB files, which can be exploited to cause a heap-based buffer overflow
    by e.g. tricking a user into opening a specially crafted PDB file.

    The vulnerabilities were reported by and the above text provided by Stefan
    Cornelius of Secunia Research. 

3. Impact:

    Exploitation may cause Okular to crash or execute arbitrary code.

4. Solution:

    Source code patches have been made available which fix these
    vulnerabilities. At the time of this writing most OS vendor / binary
    package providers should have updated binary packages. Contact your OS
    vendor / binary package provider for information about how to obtain
    updated binary packages.

5. Patch:

    Patches for this issue were provided by Albert Astals Cid (aacid@kde.org).

    Patches have been committed to the KDE Subversion repository in the
    following revision numbers:

    4.3 branch: r1167825
    4.4 branch: r1167826
    4.5 branch: r1167827
    Trunk: r1167828

    Patches for KDE SC 4.3, KDE SC 4.4 and KDE SC 4.5 may be obtained
    directly from the Subversion repository (no checkout needed) with
    the following command and reference SHA1 sums. N.B.: These SHA1 sums are
    for the output of the "svn diff" commands below, including the metadata:

    4.3 branch: f1ad2e50ce0ce8592c767365b87a22a80943aa28
    svn diff -r 1167824:1167825 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.3/kdegraphics

    4.4 branch: 13f06704919f239ef29ff63e6c1ddf8fa162af9c
    svn diff -r 1167825:1167826 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.4/kdegraphics

    4.5 branch: d739c58873599f7324c9d6500d3615f803bff39e
    svn diff -r 1167826:1167827 \
    svn://anonsvn.kde.org/home/kde/branches/KDE/4.5/kdegraphics

