<?php
    require('aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Thank You",
    ]);

    require('aether/header.php');
    $site_root = "";
?>

<main class="container">

<h1>Thank You</h1>

<?php
$donations = array(
    array(  "type" => "Continious",
        "organisation" => "DigitalOcean",
        "description" => "Sponsors free credits on their cloud platform",
        "usage" => "We use a DigitalOcean droplet (virtual private server) to host one of our repository mirrors, and part of the KDE neon infrastructure.",
        "startdate" => "2016-07-04"),
    array(  "type" => "Continious",
        "organisation" => "Datacamp Limited (CDN77.com)",
        "description" => "Donation of Content Distribution Network services",
        "usage" => "CDN77 is used to globally distribute the common images, stylesheets, javascript and other elements which are used on numerous KDE websites, enhancing performance and improving access to our sites in the process.",
        "startdate" => "2016-05-05"),
    array(    "type" => "Continious",
        "organisation" => "stepping stone GmbH",
        "description" => "Donation of a virtual server (mason.kde.org).",
        "usage" => "Mason is used to run an Anonymous Git server, providing access to KDE Git repositories over git:// and http://",
        "startdate" => "2013-12-02"),
    array(    "type" => "Continious", 
        "organisation" => "Ange Optimization",
        "description" => "Sponsors a hosted server (ange.kde.org).",
        "usage" => "Ange Optimization has sponsored a server hosted by Hetzner. This server is used as a build node for the KDE CI system.",
        "startdate" => "2013-09-30"),
    array(    "type" => "Continious", 
        "organisation" => "Gärtner Datensysteme GmbH & Co. KG",
        "description" => "Donation and hosting of a server (sage.kde.org).",
        "usage" => "Gärtner has donated a series of virtual machines, which host Scripty and the generation process for docs.kde.org as well as our server monitoring system.",
        "startdate" => "2013-05-06"),
    array(    "type" => "Continious", 
        "organisation" => "Dalhousie University & ACORN",
        "description" => "Donation and hosting of a server (dalca.kde.org).",
        "usage" => "Dalhousie University has donated dalca.kde.org and hosts it, with network connectivity provided by ACORN. This server is used as a build node for the KDE CI system.",
        "startdate" => "2013-05-28"),
    array(     "type" => "Continious", 
        "organisation" => "Cloudflare",
        "description" => "Donation of CDN services",
        "usage" => "Cloudflare protects some of our sites against SQL-injections and XSS-scripting. It also provides a CDN for common images, javascript and stylesheets which are used on multiple sites to optimize performance.",
        "startdate" => "2019-10-01"),
    array(     "type" => "Continious", 
        "organisation" => "ClouDNS",
        "description" => "Donation of DNS services",
        "usage" => "CloudDNS provides anycast nameservers for all our domains. Normal namesevers, DDOS-protected nameservers for our important domains and also GEO-ip-based nameservers for providing the right SCM-mirror in each continent.",
        "startdate" => "2019-10-29"),
    array(     "type" => "Continious", 
        "organisation" => "Canonical",
        "description" => "Donation and hosting of a server (cano.kde.org)",
        "usage" => "Canonical has donated cano.kde.org and takes care of the hosting. This server hosts a number of websites of varying types, including www.kde.org.",
        "startdate" => "2010-12-04"),
    array(     "type" => "Continious", 
        "organisation" => "Interdominios",
        "description" => "Donation and hosting of a server (fiesta.kde.org)",
        "usage" => "Interdominios has donated fiesta.kde.org and takes care of the hosting. This server is used as a build node for the KDE CI system",
        "startdate" => "2010-07-29"),
    array(     "type" => "Continious", 
        "organisation" => "Bytemark",
        "description" => "Donation of a virtual server (byte.kde.org)",
        "usage" => "Bytemark has provided a small virtual machine which can be used for developers that need shell access to a server. The virtual machine also comes with DNS hosting, which we use for most of our domains.",
        "startdate" => "2011-05-11"),
    array(     "type" => "Continious", 
        "organisation" => "GNU/FSF",
        "description" => "Donation of a virtual server (bluemchen.kde.org)",
        "usage" => "This server is used for a variety of tasks, including crucially being one of two servers supporting the Geolocation DNS service behind anongit.kde.org.",
        "startdate" => "2005-08-01"),
    array(     "type" => "Continious", 
        "organisation" => "OSUOSL",
        "description" => "Donation of a virtual server (stumptown.kde.org)",
        "usage" => "This server is used for a series of small websites, including the Season of KDE management application, Commit Digest and our Limesurvey instance (survey.kde.org)",
        "startdate" => "2005-11-28")
);

# order by date descending
function startdatesort( $a, $b) {
    if ($a["startdate"] == $b["startdate"]) {
        return 0;
    } else {
        $a_time = strtotime($a["startdate"]);
        $b_time = strtotime($b["startdate"]);
        return ($a_time > $b_time) ? -1 : 1;
    }
}
uasort( $donations, 'startdatesort');
?>

<h2>Thanks!</h2>

<p> The work of the KDE Community is made possible thanks to contributions from
KDE Community members, donors and corporations that collaborate with us.
We want to thank those organizations that make significant donations
of goods and services to KDE by publishing their names on this page.
</p>

<h2>Donations</h2>
<table class="thanks" width="100%">
<tr><th>Organisation</th><th>Description</th><th>Usage</th><th>Date</th></tr>
<?php
foreach($donations as $donation) {
    echo "<tr>";
    echo "<td>";
    if (isset($donation["logo"])) {
        echo "<img src=\"".$donation["logo"]."\" align=\"right\" width=60>";
    }
    echo $donation["organisation"]."</td>";
    echo "<td>".$donation["description"]."</td>";
    echo "<td>".$donation["usage"]."</td>";
    echo "<td><nobr>".$donation["startdate"]."</nobr></td>";
    echo "</tr>";
}
?>

</table>
<br><br>
<em>This page is made based on the policy as described in the <a href="http://ev.kde.org/rules/thankyou_policy.php">Thank You Policy</a>.</em>

</main>
<?php
  require('aether/footer.php');
